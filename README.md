# Purpose

Sharing of:

* data generator scripts 
* tests 

between repositories:

* global-deployment
* auth service

# Setup

* Must be added as submodule in parent repository with dir name "tests" due to absolute imports being used in this repository (e.g. `from tests.data_generators ..."´)
* Use pyproject.toml of parent repository

```bash
git submodule add https://gitlab.com/empaia/integration/global-tests.git tests
```

# Codestyle

* No codestyle checks within this repository
* Usage from parent repository, e.g. see global-deployment or auth-service:

```bash
poetry run black tests
poetry run isort tests
poetry run pycodestyle tests
poetry run pylint tests
```
