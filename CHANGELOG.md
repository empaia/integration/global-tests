# CHANGELOG

## 2025-02-28

* fix tests / resources for public global-services refactoring

## 2025-02-27

* replaced typing.ByteString with bytearray

## 2025-02-26
 
* updated resource files such that `hospital_ref_1` in `dummy_deployment_configuration.json` also works with a local deployment (redirect_uris and web_origins allowing localhost)
* also the user `admin_1_hospitalref2@empaia.org` now has all rights (`manager`, `pathologist`, `data-manager`)


## 2024-07-24

* adapted changes in keycloak admin client

## 2024-07-03

* codestyle

## 2024-06-11

* updated

## 2024-03-27

* updated

## 2023-11-10

* initial commit
* removed schmathesis import in
  * response_payload_validator.py
