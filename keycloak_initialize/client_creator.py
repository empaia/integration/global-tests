import dataclasses
from abc import abstractmethod
from typing import Dict, List, Optional

from pydantic import BaseModel

from .empaia_keycloak_admin import EmpaiaKeycloakAdmin


class Client(BaseModel):
    client_id: str
    name: str
    old_client_id: Optional[str] = None
    enable_direct_grant: Optional[bool] = False


class FrontendClient(Client):
    base_url: Optional[str] = None
    root_url: Optional[str] = None
    redirect_uris: Optional[List[str]] = None
    web_origins: Optional[List[str]] = None


class ServiceClient(Client):
    audiences: Optional[List[str]] = None
    secret: Optional[str] = None


@dataclasses.dataclass(init=True, frozen=True)
class ClientCreatorResult:
    id_of_client: str
    was_created: bool


class ClientCreator:
    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin

    def create_client(self, client: Client, organization_client_scope_id: str) -> Dict:
        client_data = self.create_client_data(client)
        id_of_client = self._keycloak_admin.create_client(client_data)
        self._keycloak_admin.add_default_client_scope(
            id_of_client, organization_client_scope_id
        )
        return self._keycloak_admin.get_client(id_of_client)

    def ensure_client_exists(
        self, client: Client, organization_client_scope_id: str
    ) -> ClientCreatorResult:
        keycloak_client = self._keycloak_admin.get_client_by_client_id(client.client_id)
        was_created = False
        if keycloak_client is None:
            keycloak_client = self.create_client(client, organization_client_scope_id)
            was_created = True
        return ClientCreatorResult(keycloak_client["id"], was_created)

    @classmethod
    @abstractmethod
    def create_client_data(cls, client: Client) -> Dict:
        ...

    def update_or_create_client(
        self, client: Client, organization_client_scope_id: str
    ) -> Dict:
        keycloak_client = self._update_client(client)
        if keycloak_client is None:
            # keycloak client does not exist and no old clientId has been provided, so create a new client:
            keycloak_client = self.create_client(client, organization_client_scope_id)
        return keycloak_client

    def _update_client(self, client: Client) -> Optional[Dict]:
        keycloak_client = self._keycloak_admin.get_client_by_client_id(client.client_id)
        if keycloak_client is None and client.old_client_id is not None:
            keycloak_client = self._keycloak_admin.get_client_by_client_id(
                client.old_client_id
            )
        if keycloak_client is None:
            if client.old_client_id is not None:
                raise AssertionError(
                    f"No keycloak client with neither clientId '{client.client_id}', "
                    f"nor with old clientId '{client.old_client_id}' found"
                )
        if keycloak_client is not None:
            self._keycloak_admin.update_client(
                keycloak_client["id"], self.create_client_data(client)
            )
        return keycloak_client


class FrontendClientCreator(ClientCreator):
    @classmethod
    def create_client_data(cls, client: FrontendClient) -> Dict:
        client_data = {
            "attributes": {"backchannel.logout.revoke.offline.tokens": "false"},
            "clientId": client.client_id,
            "directAccessGrantsEnabled": client.enable_direct_grant,
            "name": client.name,
            "publicClient": True,
            "redirectUris": client.redirect_uris,
            "serviceAccountsEnabled": False,
            "standardFlowEnabled": True,
            "webOrigins": client.web_origins,
        }
        if client.base_url is not None:
            client_data["baseUrl"] = client.base_url
        if client.root_url is not None:
            client_data["rootUrl"] = client.root_url
        return client_data


class ServiceClientCreator(ClientCreator):
    @classmethod
    def create_client_data(cls, client: ServiceClient) -> Dict:
        data = {
            "clientId": client.client_id,
            "directAccessGrantsEnabled": client.enable_direct_grant,
            "name": client.name,
            "authorizationServicesEnabled": True,
            "publicClient": False,
            "serviceAccountsEnabled": True,
            "standardFlowEnabled": False,
        }
        if client.secret is not None:
            data["secret"] = client.secret
        return data

    def create_client(
        self, client: ServiceClient, organization_client_scope_id: str
    ) -> Dict:
        keycloak_client = super().create_client(client, organization_client_scope_id)
        if client.audiences is not None:
            self.set_audiences(keycloak_client["id"], client)
        return keycloak_client

    def _update_client(self, client: ServiceClient) -> Optional[Dict]:
        keycloak_client = super()._update_client(client)
        if keycloak_client is not None:
            if client.audiences is not None:
                self.set_audiences(keycloak_client["id"], client)
        return keycloak_client

    def set_audiences(self, id_of_client: str, client: ServiceClient):
        for target_client_name in client.audiences:
            mapper_exists = False
            for mapper_data in self._keycloak_admin.get_mappers_from_client(
                id_of_client
            ):
                if mapper_data["name"] == f"{target_client_name} audience":
                    mapper_exists = True
                    break
            if not mapper_exists:
                self.add_audience_mapper_for_target_client(
                    id_of_client, target_client_name
                )

    def add_audience_mapper_for_target_client(
        self, id_of_client: str, target_client_name: str
    ):
        config = {
            "id.token.claim": "false",
            "access.token.claim": "true",
            "included.client.audience": target_client_name,
        }
        data = {
            "config": config,
            "name": f"{target_client_name} audience",
            "protocol": "openid-connect",
            "protocolMapper": "oidc-audience-mapper",
        }
        self._keycloak_admin.add_mapper_to_client(id_of_client, data)
