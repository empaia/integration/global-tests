from typing import Optional

from pydantic_settings import BaseSettings, SettingsConfigDict

from .global_clients_manager import ClientLists


class InitializeSettings(BaseSettings):
    auth_service_client_id: str
    portal_client_id: str = "portal_client"

    # this is optional to allow migration of old env files
    clients: Optional[ClientLists] = ClientLists(frontends=[], services=[])

    keycloak_empaia_realm_display_name: str = "Empaia PROD"
    keycloak_frontend_url: str = ""
    keycloak_user: str = ""
    keycloak_password: str = ""

    keycloak_enable_smtp_server: bool = False
    keycloak_smtp_server_auth: bool = False
    keycloak_smtp_server_from: str = ""
    keycloak_smtp_server_host: str = ""
    keycloak_smtp_server_port: int = -1
    keycloak_smtp_server_ssl: bool = False
    keycloak_smtp_server_starttls: bool = False
    keycloak_smtp_server_user: str = ""
    keycloak_smtp_server_password: str = ""

    admin_name: str = ""
    admin_password: str = ""
    admin_email_address: Optional[str] = None

    model_config = SettingsConfigDict(env_file=".initialize.env", extra="allow")
