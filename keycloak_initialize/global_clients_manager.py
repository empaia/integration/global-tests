from typing import List, Optional

from pydantic import BaseModel

from .client_creator import (
    FrontendClient,
    FrontendClientCreator,
    ServiceClient,
    ServiceClientCreator,
)
from .client_role_manager import ClientRoleManager
from .empaia_keycloak_admin import EmpaiaKeycloakAdmin


class ClientLists(BaseModel):
    frontends: List[FrontendClient]
    services: List[ServiceClient]


class GlobalClients(BaseModel):
    clients: ClientLists


class GlobalClientsManager:
    def __init__(self, clients: ClientLists, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin
        self._client_role_manager = ClientRoleManager(self._keycloak_admin)
        self._frontend_creator = FrontendClientCreator(keycloak_admin)
        self._service_creator = ServiceClientCreator(keycloak_admin)
        self._global_clients = clients
        self._id_of_auth_service_client: Optional[str] = None
        self._id_of_portal_client: Optional[str] = None

    @property
    def id_of_auth_service_client(self) -> str:
        return self._id_of_auth_service_client

    @property
    def id_of_portal_client(self) -> str:
        return self._id_of_portal_client

    def create_clients(
        self,
        auth_service_client_id: str,
        portal_client_id: str,
        organization_client_scope_id: str,
    ):
        self.create_frontend_clients(portal_client_id, organization_client_scope_id)
        self.create_service_clients(
            auth_service_client_id, organization_client_scope_id
        )

    def create_frontend_clients(
        self, portal_client_id: str, organization_client_scope_id: str
    ):
        id_of_portal_client: Optional[str] = None
        for frontend_client in self._global_clients.frontends:
            result = self._frontend_creator.ensure_client_exists(
                frontend_client, organization_client_scope_id
            )
            if frontend_client.client_id == portal_client_id:
                assert id_of_portal_client is None
                id_of_portal_client = result.id_of_client
        if id_of_portal_client is None:
            raise AssertionError(
                f"Id of portal client with client-id '{id_of_portal_client}' not found"
            )

        self.add_client_audience_mapper(
            id_of_portal_client, "Auth Service Client Audience", "auth_service_client"
        )
        self.add_client_audience_mapper(
            id_of_portal_client, "Event Service Client Audience", "event_service_client"
        )
        self._id_of_portal_client = id_of_portal_client

    def create_service_clients(
        self, auth_service_client_id: str, organization_client_scope_id: str
    ):
        id_of_auth_service_client = self._get_id_of_auth_service_client(
            auth_service_client_id, organization_client_scope_id
        )
        if id_of_auth_service_client is None:
            raise AssertionError(
                f"Id of auth service client with client-id '{auth_service_client_id}' not found"
            )
        self.finish_setup_for_auth_service_client(id_of_auth_service_client)
        self._id_of_auth_service_client = id_of_auth_service_client

    def update_auth_service_client_roles(
        self, auth_service_client_id: str, organization_client_scope_id: str
    ):
        id_of_auth_service_client = self._get_id_of_auth_service_client(
            auth_service_client_id, organization_client_scope_id
        )
        if id_of_auth_service_client is None:
            raise AssertionError(
                f"Id of auth service client with client-id '{auth_service_client_id}' not found"
            )
        return self._client_role_manager.ensure_auth_service_client_roles_exist(
            id_of_auth_service_client
        )

    def _get_id_of_auth_service_client(
        self, auth_service_client_id: str, organization_client_scope_id: str
    ):
        id_of_auth_service_client: Optional[str] = None
        for service_client in self._global_clients.services:
            result = self._service_creator.ensure_client_exists(
                service_client, organization_client_scope_id
            )
            if service_client.client_id == auth_service_client_id:
                assert id_of_auth_service_client is None
                id_of_auth_service_client = result.id_of_client
        return id_of_auth_service_client

    def finish_setup_for_auth_service_client(self, id_of_auth_service_client: str):
        self._client_role_manager.ensure_auth_service_client_roles_exist(
            id_of_auth_service_client
        )
        auth_service_user = self._keycloak_admin.get_client_service_account_user(
            id_of_auth_service_client
        )
        self.assign_realm_management_roles(
            auth_service_user["id"],
            ["impersonation", "manage-realm", "query-realms", "realm-admin"],
        )

    def add_client_audience_mapper(
        self, id_of_portal_client: str, mapper_name: str, audience: str
    ):
        config = {
            "id.token.claim": "false",
            "access.token.claim": "true",
            "included.client.audience": audience,
        }
        data = {
            "config": config,
            "name": mapper_name,
            "protocol": "openid-connect",
            "protocolMapper": "oidc-audience-mapper",
        }
        self._keycloak_admin.add_mapper_to_client(id_of_portal_client, data)

    def assign_realm_management_roles(
        self, user_id: str, realm_management_client_role_names: List[str]
    ):
        realm_management_client = self._keycloak_admin.get_client_by_client_id(
            "realm-management"
        )
        realm_management_client_id = realm_management_client["id"]
        realm_management_client_roles = [
            client_role
            for client_role in self._keycloak_admin.get_client_roles(
                realm_management_client_id
            )
            if client_role["name"] in realm_management_client_role_names
        ]
        self._keycloak_admin.assign_client_role(
            user_id, realm_management_client_id, realm_management_client_roles
        )

    def update_clients(self, organization_client_scope_id: str):
        for client in self._global_clients.frontends:
            self._frontend_creator.update_or_create_client(
                client, organization_client_scope_id
            )
        for client in self._global_clients.services:
            self._service_creator.update_or_create_client(
                client, organization_client_scope_id
            )
