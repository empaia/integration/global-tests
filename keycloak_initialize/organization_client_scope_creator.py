from typing import Dict, List

from .client_scope_protocol_mapper_creator import (
    ClientScopeProtocolMapperCreator,
)
from .empaia_keycloak_admin import EmpaiaKeycloakAdmin


class OrganizationClientScopeCreator:
    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin
        self._protocol_mapper_creator = ClientScopeProtocolMapperCreator(keycloak_admin)

    def ensure_organization_client_scope_exists(self):
        client_scope_name = "organization"
        client_scopes = self._keycloak_admin.get_client_scopes()
        client_scope_names = [s["name"] for s in client_scopes]
        if client_scope_name not in client_scope_names:
            client_scope_id = self._keycloak_admin.create_client_scope(
                {
                    "name": client_scope_name,
                    "description": "Organisation mapping scope",
                    "protocol": "openid-connect",
                }
            )
            organization_client_scope = self._keycloak_admin.get_client_scope(
                client_scope_id
            )
        else:
            organization_client_scope = None
            for client_scope in client_scopes:
                if client_scope["name"] == client_scope_name:
                    organization_client_scope = client_scope
                    break
        self.ensure_protocol_mappers_of_organization_client_scope_exist(
            organization_client_scope
        )
        return organization_client_scope["id"]

    def ensure_protocol_mappers_of_organization_client_scope_exist(
        self, organization_client_scope: Dict
    ):
        protocol_mappers = organization_client_scope.get("protocolMappers", [])
        protocol_mapper_names = [m["name"] for m in protocol_mappers]
        self.ensure_group_audience_protocol_mapper_exists(
            protocol_mapper_names, organization_client_scope["id"]
        )
        self.ensure_organizations_protocol_mapper_exists(
            protocol_mapper_names, organization_client_scope["id"]
        )

    def ensure_group_audience_protocol_mapper_exists(
        self, protocol_mapper_names: List, client_scope_id: str
    ):
        self._protocol_mapper_creator.ensure_protocol_mapper_exists(
            "Group Audience Mapper",
            protocol_mapper_names,
            client_scope_id,
            "script-group-audience-mapper.js",
            config={},
        )

    def ensure_organizations_protocol_mapper_exists(
        self, protocol_mapper_names: List, client_scope_id: str
    ):
        config = {"claim.name": "organizations", "jsonType.label": "JSON"}
        self._protocol_mapper_creator.ensure_protocol_mapper_exists(
            "Organizations Mapper",
            protocol_mapper_names,
            client_scope_id,
            "script-organizations-mapper.js",
            config,
        )
