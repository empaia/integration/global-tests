from .empaia_keycloak_admin import EmpaiaKeycloakAdmin


class ClientRoleManager:
    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin

    def ensure_auth_service_client_roles_exist(self, id_of_client: str):
        client_roles = self._keycloak_admin.get_client_roles(id_of_client)
        client_role_names = [r["name"] for r in client_roles]
        new_roles = []
        for client_role_name in (
            "admin",
            "moderator",
            "empaia-international-associate",
            "super_admin",
            "supporter",
        ):
            if client_role_name not in client_role_names:
                self.create_client_role(client_role_name, id_of_client)
                new_roles.append(client_role_name)
        return new_roles

    def create_client_role(self, client_role_name: str, id_of_client: str):
        self._keycloak_admin.create_client_role(
            id_of_client, {"name": client_role_name}
        )
