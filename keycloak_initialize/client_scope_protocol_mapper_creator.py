import os
from typing import Dict, List

from .empaia_keycloak_admin import EmpaiaKeycloakAdmin

g_path_to_keycloak_resources = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../resources/keycloak")
)


class ClientScopeProtocolMapperCreator:
    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin

    def ensure_protocol_mapper_exists(
        self,
        protocol_mapper_name: str,
        protocol_mapper_names: List,
        client_scope_id: str,
        protocol_mapper_type: str,
        config: Dict,
    ):
        if protocol_mapper_name not in protocol_mapper_names:
            config.update(
                {
                    "access.token.claim": True,
                    "id.token.claim": True,
                    "userinfo.token.claim": True,
                }
            )
            self._keycloak_admin.add_mapper_to_client_scope(
                client_scope_id,
                {
                    "config": config,
                    "protocol": "openid-connect",
                    "protocolMapper": protocol_mapper_type,
                    "name": protocol_mapper_name,
                },
            )

    @staticmethod
    def get_absolute_protocol_mapper_script_file_name(script_file_name: str):
        return os.path.join(g_path_to_keycloak_resources, script_file_name)
