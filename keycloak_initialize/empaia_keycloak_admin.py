import json
from typing import Dict, Optional

import keycloak.exceptions
from keycloak import KeycloakAdmin, KeycloakPutError, urls_patterns


class EmpaiaKeycloakAdmin(KeycloakAdmin):
    URL_ADMIN_DEFAULT_CLIENT_SCOPES = (
        urls_patterns.URL_ADMIN_CLIENT + "/default-client-scopes"
    )
    URL_ADMIN_DEFAULT_CLIENT_SCOPE = URL_ADMIN_DEFAULT_CLIENT_SCOPES + "/{scope-id}"

    def add_default_client_scope(self, id_of_client: str, client_scope_id: str):
        params_path = {
            "realm-name": self.connection.realm_name,
            "id": id_of_client,
            "scope-id": client_scope_id,
        }
        payload = {
            "realm": self.connection.realm_name,
            "client": id_of_client,
            "clientScopeId": client_scope_id,
        }
        data_raw = self.connection.raw_put(
            self.URL_ADMIN_DEFAULT_CLIENT_SCOPE.format(**params_path),
            data=json.dumps(payload),
        )
        return keycloak.exceptions.raise_error_from_response(
            data_raw, KeycloakPutError, expected_codes=[204]
        )

    def add_custom_audience(self, client_id: str, custom_audience: str):
        id_of_client = self.get_client_id(client_id)
        url = (
            urls_patterns.URL_ADMIN_CLIENTS
            + "/"
            + id_of_client
            + "/protocol-mappers/models"
        )
        params_path = {"realm-name": self.connection.realm_name}
        payload = {
            "protocol": "openid-connect",
            "config": {
                "id.token.claim": "false",
                "access.token.claim": "true",
                "included.client.audience": None,
                "included.custom.audience": custom_audience,
            },
            "name": "AudienceMapper_" + custom_audience,
            "protocolMapper": "oidc-audience-mapper",
        }
        data_raw = self.connection.raw_post(
            url.format(**params_path),
            data=json.dumps(payload),
        )
        return data_raw

    def find_id_of_client_by_client_id(self, client_id: str) -> str:
        id_of_client = None
        for client in self.get_clients():
            if client["clientId"] == client_id:
                id_of_client = client["id"]
                break
        return id_of_client

    def get_client_by_client_id(self, client_id: str) -> Optional[Dict]:
        client = None
        id_of_client = self.find_id_of_client_by_client_id(client_id)
        if id_of_client is not None:
            client = self.get_client(id_of_client)
        return client
