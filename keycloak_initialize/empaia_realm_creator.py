import logging
from http import HTTPStatus

from keycloak import KeycloakGetError

from .empaia_keycloak_admin import EmpaiaKeycloakAdmin
from .settings import InitializeSettings

EMPAIA_REALM_NAME = "empaia"


class EMPAIARealmCreator:
    def __init__(
        self,
        keycloak_admin: EmpaiaKeycloakAdmin,
        settings: InitializeSettings,
        logger: logging.Logger,
    ):
        self._keycloak_admin = keycloak_admin
        self._settings = settings
        self._logger = logger

    def ensure_empaia_realm_exists(self, recreate_empaia_realm: bool):
        realm_name = EMPAIA_REALM_NAME
        realm_representation = None
        realm_was_created = False
        try:
            realm_representation = self._keycloak_admin.get_realm(realm_name)
        except KeycloakGetError as e:
            if e.response_code != HTTPStatus.NOT_FOUND:
                raise
        if recreate_empaia_realm and realm_representation is not None:
            self._logger.info("Deleting realm '%s' including all data...", realm_name)
            self._keycloak_admin.delete_realm(realm_name)
            realm_representation = None
        if realm_representation is None:
            payload = {
                "attributes": {"frontendUrl": self._settings.keycloak_frontend_url},
                "displayName": self._settings.keycloak_empaia_realm_display_name,
                "defaultLocale": "en",
                "editUsernameAllowed": True,
                "enabled": True,
                "internationalizationEnabled": True,
                "loginTheme": "empaia-material",
                "emailTheme": "empaia-material",
                "realm": realm_name,
                "registrationAllowed": True,
                "registrationEmailAsUsername": True,
                "resetPasswordAllowed": True,
                "supportedLocales": ["de", "en"],
                "verifyEmail": True,
            }
            if self._settings.keycloak_enable_smtp_server:
                smtp_server_settings = {
                    "auth": "true" if self._settings.keycloak_smtp_server_auth else "",
                    "from": self._settings.keycloak_smtp_server_from,
                    "host": self._settings.keycloak_smtp_server_host,
                    "port": str(self._settings.keycloak_smtp_server_port),
                    "ssl": "true" if self._settings.keycloak_smtp_server_ssl else "",
                    "starttls": (
                        "true" if self._settings.keycloak_smtp_server_starttls else ""
                    ),
                }
                if (
                    self._settings.keycloak_smtp_server_user
                    and self._settings.keycloak_smtp_server_password
                ):
                    smtp_server_settings.update(
                        {
                            "user": self._settings.keycloak_smtp_server_user,
                            "password": self._settings.keycloak_smtp_server_password,
                        }
                    )
                payload.update({"smtpServer": smtp_server_settings})
            self._keycloak_admin.create_realm(payload)
            realm_was_created = True
        return realm_was_created

    def set_password_policy(self):
        realm_name = EMPAIA_REALM_NAME
        password_policy = " and ".join(
            [
                "length(12)",
                "notUsername(undefined)",
                "notEmail(undefined)",
                "upperCase(1)",
                "lowerCase(1)",
                "digits(1)",
                "specialChars(1)",
                "maxLength(1024)",
            ]
        )
        realm_representation = self._keycloak_admin.get_realm(realm_name)
        realm_representation["passwordPolicy"] = password_policy
        self._keycloak_admin.update_realm(realm_name, realm_representation)
