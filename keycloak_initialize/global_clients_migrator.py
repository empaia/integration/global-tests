import json
import logging
import os.path
import shutil
from pathlib import Path
from typing import Dict, List, Optional, Tuple

from tests.keycloak_initialize.client_creator import (
    FrontendClient,
    ServiceClient,
)
from tests.keycloak_initialize.empaia_keycloak_admin import (
    EmpaiaKeycloakAdmin,
)
from tests.keycloak_initialize.global_clients_manager import ClientLists


class GlobalClientsMigrator:
    old_auth_service_client_config_variables = [
        "AUTH_SERVICE_CLIENT_ID",
        "AUTH_SERVICE_CLIENT_NAME",
        "AUTH_SERVICE_CLIENT_SECRET",
    ]
    old_portal_client_config_variables = [
        "PORTAL_CLIENT_ID",
        "PORTAL_CLIENT_NAME",
        "PORTAL_CLIENT_REDIRECT_URIS",
        "PORTAL_CLIENT_WEB_ORIGINS",
        "PORTAL_CLIENT_CUSTOM_AUDIENCES",
    ]
    old_swagger_client_config_variables = [
        "SWAGGER_CLIENT_ID",
        "SWAGGER_CLIENT_NAME",
        "SWAGGER_CLIENT_REDIRECT_URIS",
        "SWAGGER_CLIENT_WEB_ORIGINS",
    ]
    old_config_variables = (
        old_auth_service_client_config_variables
        + old_portal_client_config_variables
        + old_swagger_client_config_variables
    )

    dropped_config_variables = old_config_variables.copy()
    dropped_config_variables.remove("AUTH_SERVICE_CLIENT_ID")
    dropped_config_variables.remove("PORTAL_CLIENT_ID")

    client_ids_to_rename = {"org.empaia.prod.aaa": "auth_service_client"}
    client_ids_by_env_variable_prefix = {
        "AAA_CLIENT_": "auth_service_client",
        "LOGIN_AAA_": "auth_service_client",
    }
    other_env_variable_value_updates = {
        "MPS_AUDIENCE": "marketplace_service_client",
        "SOS_AUDIENCE": "shout_out_service_client",
        "EVES_AUDIENCE": "event_service_client",
    }
    client_attributes_by_keycloak_client_attributes = {
        "baseUrl": "base_url",
        "name": "name",
        "redirectUris": "redirect_uris",
        "rootUrl": "root_url",
        "webOrigins": "web_origins",
    }

    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin
        self._logger = logging.getLogger("GlobalClientsMigrator")
        self._logger.setLevel(logging.DEBUG)
        self._already_migrated_env_variables = set()

    def migrate(
        self,
        dummy_organization_name: str,
        clients_file: Path,
        deployment_directory: Path,
    ):
        self._logger.info(f"Migrating deployment in '{deployment_directory}'.")
        self._migrate_global_clients_from_dummy_organization(
            dummy_organization_name, clients_file
        )
        client_lists, sample_client_lists = self._update_initialize_env(
            deployment_directory / ".initialize.env",
            deployment_directory / "sample.initialize.env",
        )
        self._update_env(
            deployment_directory / ".env",
            client_lists,
            deployment_directory / "sample.env",
            sample_client_lists,
        )
        self._logger.info(
            f"All global clients from the dummy organization '{dummy_organization_name}' have been migrated.\n\n"
            "NOTE: Please *verify* that the following files have been correctly migrated. Correct them if necessary."
            f"\n- '{deployment_directory}/.env',\n- '{deployment_directory}/sample.env',"
            f"\n- '{deployment_directory}/.initialize.env',\n- '{deployment_directory}/sample.initialize.env', and"
            f"\n- '{clients_file}'\nThen *run* "
            "'python -m tests.data_generators.keycloak_initialize --update-global-clients' to finish the migration.\n"
            "Next move any JES to corresponding organizations (e.g. Charite or TU).\nFinally clean up the global "
            "dummy organization including the clients (sss, vs):\n"
            "'python3 ../../tests/data_generators/auth_service/v2/wipe_specific_organization.py <dummy_organization_name>'"
        )

    def _update_initialize_env(
        self, initialize_env_file: Path, sample_initialize_env_file: Path
    ) -> Tuple[Optional[ClientLists], ClientLists]:
        client_data_by_client_variable_prefix = self._get_client_data_from_keycloak()

        auth_service_client_data = client_data_by_client_variable_prefix[
            "AUTH_SERVICE_CLIENT_"
        ]
        portal_client_data = client_data_by_client_variable_prefix["PORTAL_CLIENT_"]
        swagger_client_data = client_data_by_client_variable_prefix["SWAGGER_CLIENT_"]

        client_lists = None
        if initialize_env_file.exists():
            client_lists = self._migrate_initialize_env_file(
                auth_service_client_data,
                client_data_by_client_variable_prefix,
                initialize_env_file,
                portal_client_data,
                swagger_client_data,
            )
        if not sample_initialize_env_file.exists():
            raise AssertionError(f"File does not exist: '{sample_initialize_env_file}'")
        sample_client_lists = self._migrate_initialize_env_file(
            auth_service_client_data,
            client_data_by_client_variable_prefix,
            sample_initialize_env_file,
            portal_client_data,
            swagger_client_data,
        )
        return client_lists, sample_client_lists

    def _get_client_data_from_keycloak(self) -> Dict[str, Dict]:
        auth_service_client_data = dict(
            client_id="auth_service_client",
            audiences=["shout_out_service_client"],
            root_url="${authBaseUrl}",
        )
        portal_client_data = dict(client_id="portal_client")
        swagger_client_data = dict(client_id="swagger_client")

        self._add_client_data_from_keycloak(auth_service_client_data)
        self._add_client_data_from_keycloak(portal_client_data)
        self._add_client_data_from_keycloak(swagger_client_data)

        return {
            "AUTH_SERVICE_CLIENT_": auth_service_client_data,
            "PORTAL_CLIENT_": portal_client_data,
            "SWAGGER_CLIENT_": swagger_client_data,
        }

    def _add_client_data_from_keycloak(self, client_data: Dict):
        old_client_id = client_data.get("old_client_id")
        if old_client_id is None:
            client_id = client_data["client_id"]
        else:
            client_id = old_client_id
        keycloak_client = self._keycloak_admin.get_client_by_client_id(client_id)
        if keycloak_client is None:
            self._logger.warning(
                f"Keycloak client with client id '{client_id}' (old client id: '{old_client_id or 'n/a'}') not found. "
            )
        else:
            for (
                keycloak_key,
                key,
            ) in self.client_attributes_by_keycloak_client_attributes.items():
                value = keycloak_client.get(keycloak_key)
                if value is not None:
                    v = client_data.get(key)
                    if v is not None:
                        if v != value:
                            self._logger.warning(
                                f"Keycloak client attribute '{key}' differs from initialize.env data for "
                                f"client '{client_id}': '{v}' != '{value}'"
                            )
                    client_data[key] = value

    def _migrate_initialize_env_file(
        self,
        auth_service_client_data,
        client_data_by_client_variable_prefix,
        env_file,
        portal_client_data,
        swagger_client_data,
    ) -> ClientLists:
        # backup env file:
        if not os.path.exists(f"{env_file}.old"):
            self._logger.info(
                f"Updating '{env_file.name}' (backup: '{env_file.name}.old')"
            )
            shutil.copyfile(env_file, f"{env_file}.old")
        else:
            self._logger.info(f"Creating '{env_file.name}'")
        return self._take_over_client_settings_and_delete_obsolete_variables(
            client_data_by_client_variable_prefix,
            env_file,
            auth_service_client_data,
            portal_client_data,
            swagger_client_data,
        )

    def _take_over_client_settings_and_delete_obsolete_variables(
        self,
        client_data_by_client_variable_prefix: Dict,
        env_file: Path,
        auth_service_client_data: Dict,
        portal_client_data: Dict,
        swagger_client_data: Dict,
    ) -> ClientLists:
        with open(env_file, "r", encoding="utf8") as f:
            lines = []
            for line in f.readlines():
                self._process_initialize_env_line(
                    client_data_by_client_variable_prefix, line, lines
                )

        client_lists = self._create_clients_lists(
            auth_service_client_data, portal_client_data, swagger_client_data
        )
        self._append_client_settings_lines(client_lists, lines)

        with open(env_file, "w", encoding="utf8") as f:
            f.write("".join(lines))
        return client_lists

    def _remove_null_values(self, data: Dict) -> Dict:
        copy = {}
        for k, v in data.items():
            if v is not None:
                if isinstance(v, dict):
                    v = self._remove_null_values(v)
                elif isinstance(v, list):
                    t = []
                    for vi in v:
                        if isinstance(vi, dict):
                            vi = self._remove_null_values(vi)
                        t.append(vi)
                    v = t
                copy[k] = v
        return copy

    def _append_client_settings_lines(
        self, client_lists: ClientLists, lines: List[str]
    ):
        data = self._remove_null_values(client_lists.model_dump())
        client_lines = [""] + json.dumps(data, indent=4).splitlines(keepends=True)
        client_lines[0] = f"CLIENTS='{client_lines[0]}"
        client_lines[-1] = f"{client_lines[-1]}'\n"
        lines.extend(client_lines)

    @staticmethod
    def _create_clients_lists(
        auth_service_client_data: Dict,
        portal_client_data: Dict,
        swagger_client_data: Dict,
    ):
        marketplace_client = ServiceClient(
            client_id="marketplace_service_client",
            audiences=["shout_out_service_client"],
            name="Marketplace Service Client",
        )
        # rename 'AAA Service' to 'Auth Service Client'
        auth_service_client_data["name"] = "Auth Service Client"
        client_lists = ClientLists(
            frontends=[
                FrontendClient.model_validate(portal_client_data),
                FrontendClient.model_validate(swagger_client_data),
            ],
            services=[
                ServiceClient.model_validate(auth_service_client_data),
                marketplace_client,
            ],
        )
        return client_lists

    def _process_initialize_env_line(
        self, client_data_by_client_variable_prefix: Dict, line: str, lines: List[str]
    ):
        should_drop_initialize_env_line = False
        if "=" in line:
            for variable in self.old_config_variables:
                if line.startswith(f"{variable}="):
                    if variable in self.dropped_config_variables:
                        should_drop_initialize_env_line = True
                    self._set_client_value_from_variable(
                        client_data_by_client_variable_prefix, line, variable
                    )
                    break
        if not should_drop_initialize_env_line:
            lines.append(line)

    def _set_client_value_from_variable(
        self, client_data_by_client_variable_prefix: Dict, line: str, variable: str
    ):
        for (
            variable_prefix,
            client_data,
        ) in client_data_by_client_variable_prefix.items():
            if variable.startswith(variable_prefix):
                key, value = line.replace(variable_prefix, "").strip().split("=")
                key = key.strip().lower()
                value = value.strip()
                if key in ("redirect_uris", "web_origins"):
                    value = json.loads(value)
                elif key == "id":
                    new_client_id = self.client_ids_to_rename.get(value)
                    if new_client_id is not None:
                        client_data[key] = new_client_id
                        key = "old_client_id"
                client_data[key] = value
                break

    def _migrate_global_clients_from_dummy_organization(
        self, dummy_organization_name: str, clients_file: Path
    ):
        old_clients_file = clients_file.parent / f"{clients_file.name}.old"
        global_clients = ("sss", "vs")
        with open(clients_file, encoding="utf8") as f:
            clients_data = json.load(f)
        self._logger.info(
            f"Migrating '{clients_file.name}' (backup: '{old_clients_file.name}.old')"
        )
        shutil.move(clients_file, old_clients_file)
        dummy_clients_data = clients_data[dummy_organization_name]
        client_specs = dummy_clients_data.get("clients")
        if client_specs is not None:
            for client in global_clients:
                del client_specs[client]
            if "jes" in client_specs:
                client_specs["jes"].update(
                    {"client_service_roles": ["SERVICE_COMPUTE_PROVIDER"]}
                )
        if "internal_mappings" in dummy_clients_data:
            internal_mappings = dummy_clients_data["internal_mappings"]
            for mapping in list(internal_mappings):
                if mapping["from"] == "jes" and mapping["to"] in global_clients:
                    internal_mappings.remove(mapping)

        is_empty = len(list(dummy_clients_data.keys())) == 0
        if is_empty:
            self._logger.info(f"Removed '{clients_file}', because no clients remain")
        else:
            with open(clients_file, "w", encoding="utf8") as f:
                json.dump(clients_data, f, indent=4)

    def _update_env(
        self,
        env_file: Path,
        clients_list: Optional[ClientLists],
        sample_env_file: Path,
        sample_clients_list: ClientLists,
    ):
        if env_file.exists():
            self._migrate_env_file(
                env_file, sample_clients_list if clients_list is None else clients_list
            )
        assert sample_env_file.exists()
        self._migrate_env_file(sample_env_file, sample_clients_list)

    def _migrate_env_file(self, env_file: Path, clients_list: ClientLists):
        # backup env file:
        if not os.path.exists(f"{env_file}.old"):
            self._logger.info(
                f"Updating '{env_file.name}' (backup: '{env_file.name}.old')"
            )
            shutil.copyfile(env_file, f"{env_file}.old")
        else:
            self._logger.info(f"Creating '{env_file.name}'")
        with env_file.open("r") as f:
            lines = [
                self._process_env_line(line, clients_list) for line in f.readlines()
            ]
        with open(env_file, "w", encoding="utf8") as f:
            f.write("".join(lines))

    def _process_env_line(self, line: str, client_lists: ClientLists) -> str:
        if "=" in line:
            key, value = line.strip().split("=")
            key = key.strip()
            value = value.strip()
            value_changed = False
            for (
                variable_prefix,
                client_id,
            ) in self.client_ids_by_env_variable_prefix.items():
                if key.startswith(variable_prefix):
                    attribute_name = key.replace(variable_prefix, "").lower()
                    tmp = self._get_value_from_client_attribute(
                        client_id, attribute_name, client_lists
                    )
                    if tmp is not None and tmp != value:
                        if key not in self._already_migrated_env_variables:
                            self._already_migrated_env_variables.add(key)
                            self._logger.info(
                                f"- updating env variable '{key}': '{value}' -> '{tmp}'"
                            )
                        value = tmp
                        value_changed = True
                    break
            if value_changed:
                line = f"{key}={value}\n"
        return line

    @staticmethod
    def _get_value_from_client_attribute(
        client_id: str, attribute_name: str, client_lists: ClientLists
    ) -> Optional[str]:
        client = None
        for c in client_lists.frontends:
            if c.client_id == client_id:
                client = c
                break
        if client is None:
            for c in client_lists.services:
                if c.client_id == client_id:
                    client = c
                    break
        assert client is not None
        return getattr(client, attribute_name, None)
