from ..utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ..utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from ..utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ..utils.singletons import login_manager


def get_all_user_ids():
    moderator_controller = ModeratorController()
    return moderator_controller.get_user_ids(0, 9999)


def wipe_auth_service_data():
    _, decoded_token = login_manager.super_admin_tokens
    super_admin_user_id = decoded_token["sub"]
    admin_controller = AdminController()
    for user_id in get_all_user_ids():
        # don't delete the super admin, because it cannot be created through the api
        if user_id != super_admin_user_id:
            admin_controller.delete_user(user_id)
    for organization_id in admin_controller.get_all_organization_ids():
        admin_controller.delete_organization(organization_id)
    assert_all_data_was_wiped()


def assert_all_data_was_wiped():
    _, decoded_token = login_manager.super_admin_tokens
    super_admin_user_id = decoded_token["sub"]
    admin_controller = AdminController()
    has_super_admin = False
    for user_id in get_all_user_ids():
        # don't delete the super admin, because it cannot be created through the api
        if user_id == super_admin_user_id:
            has_super_admin = True
            break
    organization_count = len(admin_controller.get_all_organization_ids())
    if organization_count > 0:
        print("Remaining organizations:")
        for organization_id in admin_controller.get_all_organization_ids():
            organization = PublicController().get_organization(organization_id)
            print(f"- {organization_id}: {organization.organization_name}")
        raise AssertionError(
            "Not all organizations have been cleaned up. "
            "Run 'python -m tests.ctl drop_all_data' or "
            "set 'PYTEST_DROP_ALL_DATA_BEFORE_RUNNING_TESTS=True' in the '.env' file before running the tests."
        )
    user_count = len(get_all_user_ids())
    # the super admin that is bootstrapped with keycloak is allowed to exist:
    expected_user_count = 1 if has_super_admin else 0
    if user_count != expected_user_count:
        raise AssertionError(
            "Not all users have been cleaned up. Run 'python -m tests.ctl drop_all_data' before running the tests."
        )

    clients = AdminController().get_all_clients()
    if len(clients) != 0:
        raise AssertionError(
            "Not all clients have been cleaned up. Run 'python -m tests.ctl drop_all_data' before running the tests."
        )
