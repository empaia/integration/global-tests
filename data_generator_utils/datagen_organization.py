import json
import os
from pprint import pprint
from typing import Dict, List, Tuple

from ..utils import utils_resources
from ..utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ..utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    OrganizationAccountState,
    OrganizationUserRole,
    PostOrganizationEntity,
    PostUserEntity,
    UserProfileEntity,
)
from ..utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from ..utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from ..utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from ..utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from ..utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ..utils.auth_service.v2_api_model_mapper import (
    map_post_organization_data_to_v2_api,
    set_post_organization_default_values,
)
from ..utils.singletons import general_settings, login_manager
from . import datagen_user


def create_organizations_and_users_from_resource_file(
    organizations_resource_file: str,
    allow_existing_entities: bool = False,
    recreate_entities: bool = False,
):
    organizations_resource_file = utils_resources.resolve_file_name_in_resource_path(
        organizations_resource_file
    )
    organizations_data = utils_resources.get_json_data(str(organizations_resource_file))

    # the resource files for the V2 API specify organizations in a dictionary
    uses_v1_api_model = isinstance(organizations_data, list)
    if uses_v1_api_model:
        organizations = organizations_data
    else:
        organizations = organizations_data["organizations"]

    created_orgas_and_users = []

    for organization in organizations:
        post_media = organization.get("media")
        if post_media:
            icon = post_media.get("icon")
            if not os.path.isabs(icon):
                abs_icon = f"{os.path.dirname(organizations_resource_file)}/{icon}"
                organization["media"]["icon"] = abs_icon
        post_users = organization[
            "organization_users" if uses_v1_api_model else "users"
        ]
        for post_user in post_users:
            datagen_user.resolve_user_profile_picture_path(
                post_user, str(organizations_resource_file)
            )

        created = create_organization_and_users_from_data(
            organization, allow_existing_entities, recreate_entities, uses_v1_api_model
        )
        created["is_orga_out_file"] = True
        created_orgas_and_users.append(created)

    return created_orgas_and_users


def create_organization_and_users_from_data(
    organization_data: dict,
    allow_existing_entities: bool = False,
    recreate_entities: bool = False,
    uses_v1_api_model: bool = False,
):
    organization = create_organization_from_data(
        organization_data,
        allow_existing_entities,
        recreate_entities,
        uses_v1_api_model,
    )

    manager_controller = ManagerController()
    my_controller = MyController()

    organization_users = []
    post_users = organization_data[
        "organization_users" if uses_v1_api_model else "users"
    ]
    for post_user in post_users:
        created_user, post_user_entity = datagen_user.create_organization_user(
            post_user, allow_existing_entities, recreate_entities
        )
        post_picture = post_user.get("profile_picture")
        if post_picture is None:
            post_picture = post_user.get("picture")
        if post_picture is not None:
            datagen_user.add_user_picture(post_user_entity, post_picture)
        user_auth_headers = datagen_user.get_user_auth_headers(post_user_entity)
        my_controller.request_membership(
            organization.organization_id, user_auth_headers
        )
        membership_request = my_controller.get_membership_request(
            organization.organization_id, user_auth_headers
        )

        manager_controller.accept_membership_request(
            organization.organization_id, membership_request.membership_request_id
        )

        organization_user_roles_data = post_user.get("organization_user_roles", [])
        organization_user_roles = [
            OrganizationUserRole.from_token_value(r)
            for r in organization_user_roles_data
        ]
        if uses_v1_api_model:
            make_admin = post_user["admin"]
            if make_admin:
                organization_user_roles.append(OrganizationUserRole.MANAGER)
            # V1 API automatically assgins organizations roles pathologist and data-manager, V2 API does not. Since
            # we are handling a V1 API model resource file here (which may also be used in auth service tests), we
            # also assign those roles now:
            for organization_user_role in (
                OrganizationUserRole.DATA_MANAGER,
                OrganizationUserRole.PATHOLOGIST,
            ):
                if organization_user_role not in organization_user_roles:
                    organization_user_roles.append(organization_user_role)
        # Preserve organization user roles that were initially set by the auth service:
        _, decoded_token = datagen_user.get_user_tokens(post_user_entity)
        for organization_user_role in get_existing_organization_user_roles(
            organization.organization_id, decoded_token
        ):
            if organization_user_role not in organization_user_roles:
                organization_user_roles.append(organization_user_role)
        manager_controller.set_organization_user_roles(
            organization.organization_id, created_user.user_id, organization_user_roles
        )

        created_user = datagen_user.get_user_by_auth_headers(post_user_entity)
        organization_users.append(
            {
                "user": json.loads(created_user.model_dump_json()),
                "organization_user_roles": [r.value for r in organization_user_roles],
                "password": post_user_entity.password,
            }
        )

    # fetch updated organization and return data
    organization = PublicController().get_organization(organization.organization_id)
    return {
        "organization": json.loads(organization.model_dump_json()),
        "organization_users": organization_users,
    }


def create_organization_from_data(
    organization_data: dict,
    allow_existing_organization: bool = False,
    recreate_entities: bool = False,
    uses_v1_api_model: bool = False,
) -> ConfidentialOrganizationProfileEntity:
    organization_properties = organization_data["organization"]

    already_exists, organization = post_organization_data(
        organization_properties, uses_v1_api_model
    )

    if already_exists and recreate_entities:
        AdminController().delete_organization(organization.organization_id)
        already_exists, organization = post_organization_data(
            organization_properties, uses_v1_api_model
        )

    if already_exists:
        if not allow_existing_organization:
            raise Exception("Organization already exists")
    else:
        logo_file_name = organization_data.get("media", {}).get("icon")
        if logo_file_name is not None:
            MyUnapprovedOrganizationsController().set_organization_logo(
                organization.organization_id, logo_file_name
            )

        if not organization_properties.get("deactivate", False):
            MyUnapprovedOrganizationsController().request_organization_approval(
                organization.organization_id
            )
            ModeratorController().accept_organization(organization.organization_id)
            # clear cached super admin auth token, because it must be refreshed to include the updated organizations
            # claim:
            login_manager.clear_cache_for_super_admin()
            organization = ManagerController().get_organization(
                organization.organization_id, auth_headers=login_manager.super_admin()
            )
            assert organization.account_state == OrganizationAccountState.ACTIVE
        else:
            organization = (
                MyUnapprovedOrganizationsController().get_unapproved_organization(
                    organization.organization_id
                )
            )
            assert organization.account_state == OrganizationAccountState.INACTIVE
    return organization


def post_organization_data(
    organization_data: dict, uses_v1_api_model: bool
) -> Tuple[bool, ConfidentialOrganizationProfileEntity]:
    if uses_v1_api_model:
        organization_data = map_post_organization_data_to_v2_api(organization_data)
    set_post_organization_default_values(organization_data)
    post_organization_entity = PostOrganizationEntity.model_validate(organization_data)
    organization = None
    try:
        organization = (
            MyUnapprovedOrganizationsController().create_new_organization_from_data(
                post_organization_entity
            )
        )
        already_exists = False
    except AssertionError as e:
        if "409" in str(e) and "Organization already exists" in str(e):
            already_exists = True
        else:
            raise
    if general_settings.verbose_requests:
        pprint(organization)

    return already_exists, organization


def get_existing_organization_user_roles(
    organization_id: str, decoded_token: Dict
) -> List[OrganizationUserRole]:
    return [
        OrganizationUserRole.from_token_value(role_name)
        for role_name in decoded_token["organizations"][organization_id]["roles"]
    ]


def add_user_to_organization(
    user: UserProfileEntity,
    post_user_data: PostUserEntity,
    organization_name: str,
    organization_user_roles: List[OrganizationUserRole],
):
    organization = PublicController().get_organization_by_name(organization_name)
    user_auth_headers = login_manager.user(
        post_user_data.contact_data.email_address, post_user_data.password
    )
    my_controller = MyController()
    my_controller.request_membership(
        organization.organization_id, auth_headers=user_auth_headers
    )
    membership_request = my_controller.get_membership_request(
        organization.organization_id, auth_headers=user_auth_headers
    )
    manager_controller = ManagerController()
    manager_controller.accept_membership_request(
        organization.organization_id,
        membership_request_id=membership_request.membership_request_id,
    )
    manager_controller.set_organization_user_roles(
        organization_id=organization.organization_id,
        target_user_id=user.user_id,
        organization_user_roles=organization_user_roles,
    )
    login_manager.clear_cache_for_user(post_user_data.contact_data.email_address)
