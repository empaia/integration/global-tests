from typing import Dict, List

from ..utils import utils_resources
from ..utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ..utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    OrganizationAccountState,
    PostOrganizationDeploymentConfiguration,
)


def configure_deployments_from_resource_file(deployment_configuration_input_file: str):
    print(f"Configure deployment from file. {deployment_configuration_input_file}")
    deployment_configuration_input_file = (
        utils_resources.resolve_file_name_in_resource_path(
            deployment_configuration_input_file
        )
    )
    clients_data = utils_resources.get_json_data(
        str(deployment_configuration_input_file)
    )
    return configure_deployments_from_data(clients_data)


def normalize_name(name):
    name = name.replace(" ", "_")
    name = name.replace(",", "_")
    name = name.replace(".", "_")
    name = name.replace("ö", "o")
    name = name.replace("ä", "a")
    name = name.replace("ü", "u")
    name = name.replace("ß", "s")
    name = name.replace("é", "e")
    name = name.replace("è", "e")
    name = name.replace("ê", "e")
    name = name.replace("ó", "o")
    name = name.replace("ò", "o")
    name = name.replace("ô", "o")
    name = name.replace("â", "a")
    name = name.replace("á", "a")
    name = name.replace("à", "a")
    name = name.lower()
    return name


def get_organizations_by_normalized_name(
    organizations: List[ConfidentialOrganizationProfileEntity],
) -> Dict[str, ConfidentialOrganizationProfileEntity]:
    organizations_by_normalized_name = {}
    for organization in organizations:
        normalized_name = normalize_name(organization.organization_name)
        organizations_by_normalized_name[normalized_name] = organization
    return organizations_by_normalized_name


def update_deployment_configuration(clients_data, organizations_by_normalized_name):
    for organization_name, organization_data in clients_data.items():
        organization_id = organizations_by_normalized_name[
            organization_name
        ].organization_id

        if "configuration" in organization_data:
            post_config = PostOrganizationDeploymentConfiguration.model_validate(
                organization_data["configuration"]
            )
            AdminController().put_organization_deployment_configuration(
                organization_id, post_config
            )

        if "external_jes" in organization_data:
            compute_provider_org = organization_data["external_jes"]
            compute_provider_organization_id = organizations_by_normalized_name[
                compute_provider_org["compute_provider_name"]
            ].organization_id
            if organization_id:
                AdminController().set_organization_compute_provider_jes(
                    organization_id, compute_provider_organization_id
                )


def configure_deployments_from_data(clients_data: dict):
    organizations = (
        AdminController()
        .get_organizations(0, 9999, account_states=[OrganizationAccountState.ACTIVE])
        .organizations
    )
    organizations_by_normalized_name = get_organizations_by_normalized_name(
        organizations
    )
    update_deployment_configuration(clients_data, organizations_by_normalized_name)
