import os
from pprint import pformat
from typing import Dict, Tuple
from uuid import uuid4

from ..utils import utils_resources
from ..utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ..utils.auth_service.api_controllers.v2.entity_models import (
    PostUserEntity,
    PostUserRolesEntity,
    UserProfileEntity,
    UserRole,
)
from ..utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from ..utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from ..utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ..utils.auth_service.v2_api_model_mapper import (
    map_post_user_data_to_v2_api,
    set_post_user_default_values,
)
from ..utils.login_manager import Tokens
from ..utils.singletons import login_manager


def resolve_user_profile_picture_path(
    post_user: Dict,
    user_resource_file: str,
):
    uses_v2_api_model = post_user.get("user") is None
    profile_picture_attribute = "profile_picture" if uses_v2_api_model else "picture"
    post_picture = post_user.get(profile_picture_attribute)
    if post_picture is not None:
        if not os.path.isabs(user_resource_file):
            user_resource_file = utils_resources.resolve_file_name_in_resource_path(
                user_resource_file
            )
        if not os.path.isabs(post_picture):
            post_user[
                profile_picture_attribute
            ] = f"{os.path.dirname(user_resource_file)}/{post_picture}"


def create_user_from_resource_file(
    user_resource_file: str,
    allow_existing_user: bool = False,
    recreate_user: bool = False,
    should_activate: bool = True,
) -> Tuple[UserProfileEntity, PostUserEntity]:
    post_user = utils_resources.get_json_data(user_resource_file)
    resolve_user_profile_picture_path(post_user, user_resource_file)
    return create_user(post_user, allow_existing_user, recreate_user, should_activate)


def make_post_user_entity(post_user: Dict) -> Tuple[PostUserEntity, bool]:
    uses_v2_api_model = post_user.get("user") is None
    if uses_v2_api_model:
        user_data = post_user
    else:
        user_data = map_post_user_data_to_v2_api(post_user["user"])
    if "password" not in user_data or user_data["password"] is None:
        user_data["password"] = f"{str(uuid4())[:31]}A"
    set_post_user_default_values(user_data)
    return PostUserEntity.model_validate(user_data), uses_v2_api_model


def create_organization_user(
    post_user: dict,
    allow_existing_user: bool = False,
    recreate_user: bool = False,
    should_activate: bool = True,
) -> Tuple[UserProfileEntity, PostUserEntity]:
    return create_user(
        post_user,
        allow_existing_user,
        recreate_user,
        should_activate,
        is_organization_member=True,
    )


def create_user(
    post_user: dict,
    allow_existing_user: bool = False,
    recreate_user: bool = False,
    should_activate: bool = True,
    is_organization_member: bool = False,
) -> Tuple[UserProfileEntity, PostUserEntity]:
    admin_controller = AdminController()
    moderator_controller = ModeratorController()
    public_controller = PublicController()

    post_user_entity, uses_v2_api_model = make_post_user_entity(post_user)
    user = moderator_controller.get_user_by_username(
        post_user_entity.contact_data.email_address
    )
    already_exists = user is not None
    if already_exists and recreate_user:
        admin_controller.delete_user(user.user_id)
        login_manager.clear_cache_for_user(
            post_user_entity.contact_data.email_address, post_user_entity.password
        )
        user = None
        already_exists = False

    if already_exists:
        if allow_existing_user:
            pass
        else:
            raise Exception(f"User already exists: \n{str(user)}")
    else:
        public_controller.register_new_user(post_user_entity)
        if should_activate:
            activate_user(post_user_entity.contact_data.email_address)
            login_manager.clear_cache_for_user(
                post_user_entity.contact_data.email_address, post_user_entity.password
            )
            user = get_user_by_auth_headers(post_user_entity)
        else:
            # cannot retrieve the user by auth headers if the user is not activated
            user = moderator_controller.get_user_by_username(
                post_user_entity.contact_data.email_address
            )

    if not uses_v2_api_model:
        # the "admin" attribute has different meanings in *_user.json and *_organizations.json:
        # - *._user.json: create an admin user/moderator user
        # - *_organizations.json: set the ORGANIZATION_ADMIN/MANAGER organization user role
        if post_user["admin"] and not is_organization_member:
            add_moderator_user_role(user)

    post_picture = post_user.get("profile_picture" if uses_v2_api_model else "picture")
    if post_picture is not None:
        add_user_picture(post_user_entity, post_picture)

    user = get_user_by_auth_headers(post_user_entity)
    if user is None:
        raise AssertionError(
            "Failed to create user:\n"
            f"  already_exists: {already_exists}\n"
            f"  allow_existing_user: {allow_existing_user}\n"
            f"  recreate_user: {recreate_user}\n"
            f"  should_activate: {should_activate}\n"
            f"{pformat(post_user)}"
        )
    return user, post_user_entity


def activate_user(email: str):
    PublicController().verify_registration(email)


def get_user_auth_headers(post_user_entity: PostUserEntity) -> Dict:
    return login_manager.user(
        post_user_entity.contact_data.email_address, post_user_entity.password
    )


def get_user_tokens(
    post_user_entity: PostUserEntity, clear_cache: bool = True
) -> Tokens:
    if clear_cache:
        login_manager.clear_cache_for_user(
            post_user_entity.contact_data.email_address, post_user_entity.password
        )
    return login_manager.get_user_tokens(
        post_user_entity.contact_data.email_address, post_user_entity.password
    )


def add_user_picture(post_user_entity: PostUserEntity, file_name: str):
    auth_headers = get_user_auth_headers(post_user_entity)
    MyController().set_profile_picture(file_name, auth_headers)


def get_user_by_auth_headers(post_user_entity: PostUserEntity) -> UserProfileEntity:
    auth_headers = get_user_auth_headers(post_user_entity)
    return MyController().get_profile(auth_headers)


def add_moderator_user_role(user: UserProfileEntity):
    ModeratorController().set_user_roles(
        user.user_id, PostUserRolesEntity(roles=[UserRole.MODERATOR.value])
    )
