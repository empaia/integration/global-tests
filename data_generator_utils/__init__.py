import time

import requests

from tests.utils.singletons import general_settings


def wait_for_auth_service_to_be_available():
    start = time.time()
    maximum_seconds_to_wait = 120
    while True:
        try:
            requests.get(
                f"{general_settings.auth_service_host}{general_settings.auth_service_openapi_url_suffix.lstrip('/')}"
            )
        except requests.ConnectionError as e:
            elapsed = time.time() - start
            if elapsed > maximum_seconds_to_wait:
                raise e
            else:
                time.sleep(1)
        else:
            break
