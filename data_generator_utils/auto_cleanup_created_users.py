from typing import List

from ..utils.auth_service import utils_auth_service


class AutoCleanupCreatedUsers:
    def __init__(self):
        self._created_organization_ids: List[int] = []
        self._created_user_emails: List[str] = []

    def add_email_of_created_user(self, email: str):
        self._created_user_emails.append(email)

    def add_id_of_created_organization(self, organization_id: int):
        self._created_organization_ids.append(organization_id)

    def cleanup(self):
        self._delete_created_organizations()
        self._delete_created_users()

    def _delete_created_organizations(self):
        for organization_id in self._created_organization_ids:
            add_headers = {"organization": str(organization_id)}
            utils_auth_service.generic_delete(
                "/api/organization", add_headers=add_headers
            )
        self._created_organization_ids = []

    def _delete_created_users(self):
        for email in self._created_user_emails:
            utils_auth_service.generic_delete(f"/api/users/by_email/{email}")
        self._created_user_emails = []
