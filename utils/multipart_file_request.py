import mimetypes
import os
from typing import Dict, Union

from requests.models import PreparedRequest

from .utils_resources import resolve_file_name_in_resource_path


def make_multipart_file_request(
    file_name: str,
    multipart_filename: str,
    request_parameters: Dict[str, Union[str, bytes, bytearray]] = None,
):
    file_name = resolve_file_name_in_resource_path(file_name)
    with open(file_name, "rb") as f:
        files = {
            multipart_filename: (
                os.path.basename(file_name),
                f,
                mimetypes.guess_type(file_name)[0],
                {"Expires": "0"},
            )
        }
        prepared_request = PreparedRequest()
        prepared_request.prepare_headers({})
        prepared_request.prepare_body(request_parameters, files)
        return prepared_request
