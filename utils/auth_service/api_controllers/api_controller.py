from pprint import pformat
from typing import Any, Dict, List, Optional, Tuple

import requests
from requests import Response

from ...singletons import general_settings, login_manager
from .v2.entity_models import AccessToken


class APIController:
    @staticmethod
    def _prepare_headers(arguments: Dict, auth_headers: Dict = None):
        if auth_headers is None:
            auth_headers = login_manager.super_admin()
        headers = arguments.get("headers", {})
        headers.update(auth_headers)
        arguments["headers"] = headers

    @staticmethod
    def get_auth_headers(username: str, password: str) -> Dict:
        return login_manager.user(username, password)

    @staticmethod
    def get_access_token(
        username: str, password: str, client_id: Optional[str] = None
    ) -> Tuple[str, AccessToken]:
        if client_id is None:
            client_id = login_manager.settings.portal_client_id
        token, decoded_token = login_manager.get_access_flow_tokens(
            client_id, username, password
        )
        return token, AccessToken.model_validate(decoded_token)

    def delete(self, url, auth_headers=None, allowed_errors=None, **kwargs) -> Response:
        self._prepare_headers(kwargs, auth_headers)
        r = requests.delete(
            f"{general_settings.auth_service_host}{url.lstrip('/')}", **kwargs
        )
        self._verify_response(r, allowed_errors, method="DELETE", url=url)
        return r

    def get(self, url, auth_headers=None, allowed_errors=None, **kwargs) -> Response:
        self._prepare_headers(kwargs, auth_headers)
        r = requests.get(
            f"{general_settings.auth_service_host}{url.lstrip('/')}", **kwargs
        )
        self._verify_response(r, allowed_errors, method="GET", url=url, **kwargs)
        return r

    def post(
        self, url, data: Any = None, json: Dict = None, auth_headers=None, **kwargs
    ) -> Response:
        self._prepare_headers(kwargs, auth_headers)
        r = requests.post(
            f"{general_settings.auth_service_host}{url.lstrip('/')}",
            data,
            json,
            **kwargs,
        )
        self._verify_response(r, method="POST", data=data, url=url, json=json, **kwargs)
        return r

    def put(
        self,
        url,
        data: Any = None,
        auth_headers: Optional[Dict[str, str]] = None,
        allowed_errors: Optional[List[int]] = None,
        **kwargs,
    ) -> Response:
        self._prepare_headers(kwargs, auth_headers)
        r = requests.put(
            f"{general_settings.auth_service_host}{url.lstrip('/')}", data, **kwargs
        )
        self._verify_response(
            r, method="PUT", data=data, url=url, allowed_errors=allowed_errors, **kwargs
        )
        return r

    @staticmethod
    def _verify_response(response: Response, allowed_errors=None, **kwargs):
        try:
            if allowed_errors is None or response.status_code not in allowed_errors:
                response.raise_for_status()
        except Exception as e:
            raise AssertionError(
                f"APIController request \"[{kwargs['method']}] {kwargs['url']}\" "
                f"should not fail: {response.status_code} {response.content}\n{pformat(kwargs)}"
            ) from e
