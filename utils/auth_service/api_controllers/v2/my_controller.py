import json
import mimetypes
import os
from typing import Dict, List, Optional

import requests

from ....utils_resources import resolve_file_name_in_resource_path
from ..api_controller import APIController
from .entity_models import (
    DefaultActiveOrganization,
    MembershipRequestEntity,
    MembershipRequestsEntity,
    MembershipsEntity,
    PostUserContactDataEntity,
    PublicOrganizationProfilesEntity,
    UserDetailsEntity,
    UserProfileEntity,
)


class MyController(APIController):
    def set_contact_data(
        self, contact_data: PostUserContactDataEntity, auth_headers: Dict
    ):
        self.put(
            "/api/v2/my/profile/contact",
            json=json.loads(contact_data.model_dump_json()),
            auth_headers=auth_headers,
        )

    def set_details(self, details: UserDetailsEntity, auth_headers: Dict):
        self.put(
            "/api/v2/my/profile/details",
            json=details.model_dump(),
            auth_headers=auth_headers,
        )

    def get_details(self, auth_headers: Dict) -> UserDetailsEntity:
        r = self.get("/api/v2/my/profile/details", auth_headers=auth_headers)
        return UserDetailsEntity.model_validate(r.json())

    def get_profile(self, auth_headers: Dict) -> UserProfileEntity:
        r = self.get("/api/v2/my/profile", auth_headers=auth_headers)
        return UserProfileEntity.model_validate(r.json())

    def get_language(self, auth_headers: Dict) -> str:
        r = self.get("/api/v2/my/profile/language", auth_headers=auth_headers)
        return r.json()

    def set_profile_picture(self, file_name: str, auth_headers: Dict):
        file_name = resolve_file_name_in_resource_path(file_name)
        with open(file_name, "rb") as f:
            files = {
                "picture": (
                    os.path.basename(file_name),
                    f,
                    mimetypes.guess_type(file_name)[0],
                    {"Expires": "0"},
                )
            }
            self.put(
                "/api/v2/my/profile/picture", auth_headers=auth_headers, files=files
            )

    def get_membership_requests(
        self, page: int = 0, page_size: int = 1000, auth_headers: Optional[Dict] = None
    ) -> MembershipRequestsEntity:
        params = {"page": page, "page_size": page_size}
        r = self.get(
            "/api/v2/my/membership-requests", auth_headers=auth_headers, params=params
        )
        return MembershipRequestsEntity.model_validate(r.json())

    def get_membership_request(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ) -> MembershipRequestEntity:
        for membership_request in self.get_membership_requests(
            auth_headers=auth_headers
        ).membership_requests:
            if membership_request.organization_id == organization_id:
                return membership_request

    def request_membership(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ):
        self.post(
            "/api/v2/my/membership-requests",
            auth_headers=auth_headers,
            json={"organization_id": organization_id},
        )

    def get_memberships(self, auth_headers: Optional[Dict] = None) -> MembershipsEntity:
        r = self.get("/api/v2/my/memberships", auth_headers=auth_headers)
        return MembershipsEntity.model_validate(r.json())

    def revoke_membership_request(
        self, membership_requests_id: int, auth_headers: Optional[Dict] = None
    ):
        self.put(
            f"/api/v2/my/membership-requests/{membership_requests_id}/revoke",
            auth_headers=auth_headers,
        )

    def get_default_active_organization(
        self, auth_headers: Optional[Dict] = None
    ) -> DefaultActiveOrganization:
        r = self.get_default_active_organization_request(auth_headers)
        return DefaultActiveOrganization.model_validate(r.json())

    def get_default_active_organization_request(
        self, auth_headers: Optional[Dict] = None
    ) -> requests.Response:
        return self.get(
            "/api/v2/my/default-active-organization", auth_headers=auth_headers
        )

    def put_default_active_organization(
        self,
        default_active_organization_id: Optional[str],
        auth_headers: Optional[Dict],
        allowed_errors: Optional[List[int]] = None,
    ) -> DefaultActiveOrganization:
        r = self.put_default_active_organization_request(
            default_active_organization_id, auth_headers, allowed_errors
        )
        return DefaultActiveOrganization.model_validate(r.json())

    def put_default_active_organization_request(
        self,
        default_active_organization_id: Optional[str],
        auth_headers: Optional[Dict],
        allowed_errors: Optional[List[int]] = None,
    ) -> requests.Response:
        default_active_orga = {"organization_id": default_active_organization_id}
        return self.put(
            "/api/v2/my/default-active-organization",
            auth_headers=auth_headers,
            json=default_active_orga,
            allowed_errors=allowed_errors,
        )

    def delete_default_active_organization(self, auth_headers: Optional[Dict] = None):
        return self.delete(
            "/api/v2/my/default-active-organization", auth_headers=auth_headers
        )

    def get_organizations(
        self, auth_headers: Optional[Dict] = None
    ) -> PublicOrganizationProfilesEntity:
        r = self.get("/api/v2/my/organizations", auth_headers=auth_headers)
        return PublicOrganizationProfilesEntity.model_validate(r.json())

    def delete_account(self, auth_headers: Optional[Dict] = None):
        self.delete("/api/v2/my/account", auth_headers=auth_headers)
