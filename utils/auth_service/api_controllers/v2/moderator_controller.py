import json
from typing import Dict, List, Optional

from ..api_controller import APIController
from .entity_models import (
    ConfidentialOrganizationProfileEntity,
    OrganizationCategoriesEntity,
    OrganizationCategory,
    OrganizationCategoryRequestEntity,
    OrganizationCategoryRequestsEntity,
    PostReviewerComment,
    PostUserRolesEntity,
    PublicOrganizationsEntity,
    RegisteredUserEntity,
    RegisteredUsersEntity,
    UserProfileEntity,
)


class ModeratorController(APIController):
    def get_unapproved_user_ids(self, page: int, page_size: int) -> List[str]:
        r = self.get(
            "/api/v2/moderator/unapproved-users",
            params={"page": page, "page_size": page_size},
        )
        return [user_data["user_id"] for user_data in r.json()["users"]]

    def get_unapproved_users(self, page: int, page_size: int) -> RegisteredUsersEntity:
        r = self.get(
            "/api/v2/moderator/unapproved-users",
            params={"page": page, "page_size": page_size},
        )
        return RegisteredUsersEntity.model_validate(r.json())

    def get_unapproved_user_by_username(
        self, username: str
    ) -> Optional[RegisteredUserEntity]:
        result: Optional[RegisteredUserEntity] = None
        for user in self.get_unapproved_users(0, 9999).users:
            if user.username == username:
                result = user
                break
        return result

    def get_user_by_username(self, username: str) -> Optional[RegisteredUserEntity]:
        result: Optional[RegisteredUserEntity] = None
        for user in self.get_users(0, 9999).users:
            if user.username == username:
                result = user
                break
        return result

    def get_user_ids(self, page: int, page_size: int) -> List[str]:
        r = self.get(
            "/api/v2/moderator/users", params={"page": page, "page_size": page_size}
        )
        return [user_data["user_id"] for user_data in r.json()["users"]]

    def get_users(
        self, page: int, page_size: int, auth_headers: Optional[Dict] = None
    ) -> RegisteredUsersEntity:
        params = {"page": page, "page_size": page_size}
        r = self.get(
            "/api/v2/moderator/users", params=params, auth_headers=auth_headers
        )
        return RegisteredUsersEntity.model_validate(r.json())

    def set_user_roles(
        self,
        target_user_id: str,
        user_roles: PostUserRolesEntity,
        auth_headers: Optional[Dict] = None,
    ):
        self.put(
            f"/api/v2/moderator/users/{target_user_id}/roles",
            json=user_roles.model_dump(),
            auth_headers=auth_headers,
        )

    def get_unapproved_organization(
        self, unapproved_organization_id: str
    ) -> ConfidentialOrganizationProfileEntity:
        r = self.get(
            f"/api/v2/moderator/unapproved-organizations/{unapproved_organization_id}/profile"
        )
        return ConfidentialOrganizationProfileEntity.model_validate(r)

    def accept_organization(self, organization_id: str):
        self.put(f"/api/v2/moderator/unapproved-organizations/{organization_id}/accept")

    def get_user_profile(self, target_user_id: str) -> UserProfileEntity:
        r = self.get(f"/api/v2/moderator/users/{target_user_id}/profile")
        return UserProfileEntity.model_validate(r.json())

    def get_unapproved_user_profile(self, unapproved_user_id: str) -> UserProfileEntity:
        r = self.get(f"/api/v2/moderator/unapproved-users/{unapproved_user_id}/profile")
        return UserProfileEntity.model_validate(r.json())

    def deactivate_user(self, target_user_id: str):
        self.put(f"/api/v2/moderator/users/{target_user_id}/deactivate")

    def delete_unapproved_user(self, unapproved_user_id: str):
        self.delete(f"/api/v2/moderator/unapproved-users/{unapproved_user_id}/account")

    def deactivate_organization(self, organization_id: str):
        self.put(f"/api/v2/moderator/organizations/{organization_id}/deactivate")

    def get_organization(
        self, organization_id: str
    ) -> ConfidentialOrganizationProfileEntity:
        r = self.get(f"/api/v2/moderator/organizations/{organization_id}")
        return ConfidentialOrganizationProfileEntity.model_validate(r.json())

    def get_organizations(self) -> PublicOrganizationsEntity:
        r = self.get(
            "/api/v2/moderator/organizations", params={"page": 0, "page_size": 9999}
        )
        return PublicOrganizationsEntity.model_validate(r.json())

    def get_organization_category_requests(
        self,
    ) -> OrganizationCategoryRequestsEntity:
        r = self.get(
            "/api/v2/moderator/category-requests", params={"page": 0, "page_size": 9999}
        )
        return OrganizationCategoryRequestsEntity.model_validate(r.json())

    def accept_category_request(self, category_request_id: str):
        r = self.put(
            f"/api/v2/moderator/category-requests/{category_request_id}/accept"
        )
        return OrganizationCategoryRequestEntity.model_validate(r.json())

    def deny_category_request(
        self, category_request_id: str, reviewer_comment: str = None
    ):
        body = {}
        if reviewer_comment:
            body = json.loads(
                PostReviewerComment(reviewer_comment=reviewer_comment).model_dump_json()
            )
        r = self.put(
            f"/api/v2/moderator/category-requests/{category_request_id}/deny", json=body
        )
        return OrganizationCategoryRequestEntity.model_validate(r.json())

    def set_category_request(
        self, organization_id: str, categories: List[OrganizationCategory]
    ):
        self.put(
            f"/api/v2/moderator/organizations/{organization_id}/categories",
            json=json.loads(
                OrganizationCategoriesEntity(categories=categories).model_dump_json()
            ),
        )

    def delete_user_account(self, target_user_id: str):
        self.delete(f"/api/v2/moderator/users/{target_user_id}/account")
