import json
from typing import Dict, List, Optional

from ..api_controller import APIController
from .entity_models import (
    MemberProfileEntity,
    MemberProfilesEntity,
    OrganizationUserRole,
    OrganizationUserRoleRequestEntity,
    OrganizationUserRoleRequestsEntity,
    PostOrganizationUserRoleRequestEntity,
    RequestState,
)


class OrganizationControllerV2(APIController):
    def leave(
        self,
        organization_id: str,
        auth_headers: Optional[Dict],
        allowed_errors: Optional[List[int]] = None,
    ):
        return self.put(
            "/api/v2/organization/leave",
            headers={"organization-id": organization_id},
            auth_headers=auth_headers,
            allowed_errors=allowed_errors,
        )

    def can_leave(
        self,
        organization_id: str,
        auth_headers: Optional[Dict],
        allowed_errors: Optional[List[int]] = None,
    ):
        return self.get(
            "/api/v2/organization/can-leave",
            headers={"organization-id": organization_id},
            auth_headers=auth_headers,
            allowed_errors=allowed_errors,
        )

    def get_users(
        self, organization_id: str, auth_headers: Optional[Dict]
    ) -> MemberProfilesEntity:
        r = self.get(
            "/api/v2/organization/users",
            headers={"organization-id": organization_id},
            auth_headers=auth_headers,
        )
        return MemberProfilesEntity.model_validate(r.json())

    def get_user_profile(
        self, target_user_id: str, organization_id: str, auth_headers: Optional[Dict]
    ) -> MemberProfileEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            f"/api/v2/organization/users/{target_user_id}/profile",
            headers=headers,
            auth_headers=auth_headers,
        )
        return MemberProfileEntity.model_validate(r.json())

    def request_organization_user_roles(
        self,
        organization_id: str,
        organization_user_roles: List[OrganizationUserRole],
        auth_headers: Optional[Dict],
        allowed_errors: Optional[List[int]] = None,
    ):
        headers = {"organization-id": organization_id}
        request_entity = PostOrganizationUserRoleRequestEntity(
            requested_roles=[role.value for role in organization_user_roles]
        )
        return self.put(
            "/api/v2/organization/role-requests",
            json=json.loads(request_entity.model_dump_json()),
            headers=headers,
            auth_headers=auth_headers,
            allowed_errors=allowed_errors,
        )

    def get_organization_user_role_requests(
        self, organization_id: str, auth_headers: Optional[Dict]
    ) -> OrganizationUserRoleRequestsEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            "/api/v2/organization/role-requests",
            headers=headers,
            auth_headers=auth_headers,
            params={"page": 0, "page_size": 9999},
        )
        return OrganizationUserRoleRequestsEntity.model_validate(r.json())

    def revoke_organization_role_request(
        self, organization_id: str, role_request_id: int, auth_headers: Optional[Dict]
    ):
        headers = {"organization-id": organization_id}
        self.put(
            f"/api/v2/organization/role-requests/{role_request_id}/revoke",
            headers=headers,
            auth_headers=auth_headers,
        )

    def find_organization_user_role_request(
        self,
        organization_id: str,
        auth_headers: Optional[Dict],
    ) -> OrganizationUserRoleRequestEntity:
        role_request_page = self.get_organization_user_role_requests(
            organization_id=organization_id, auth_headers=auth_headers
        )
        for role_request in role_request_page.organization_user_role_requests:
            if role_request.request_state == RequestState.REQUESTED:
                return role_request
        raise AssertionError("Role request not found")
