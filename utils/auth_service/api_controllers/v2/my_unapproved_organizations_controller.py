import json
import mimetypes
import os
from typing import Collection, Dict, List, Optional

from ....utils_resources import resolve_file_name_in_resource_path
from ..api_controller import APIController
from .entity_models import (
    ConfidentialOrganizationProfileEntity,
    ConfidentialOrganizationProfilesEntity,
    OrganizationAccountState,
    OrganizationAccountStateQueryEntity,
    OrganizationCategory,
    PostOrganizationContactDataEntity,
    PostOrganizationDetailsEntity,
    PostOrganizationEntity,
    UnapprovedConfidentialOrganizationProfileEntity,
)


class MyUnapprovedOrganizationsController(APIController):
    @staticmethod
    def create_new_organization_data(
        email_address: str,
        organization_categories: Optional[Collection[OrganizationCategory]] = None,
        **data,
    ) -> PostOrganizationEntity:
        name = None
        if data is not None:
            name = data.get("name")
        if name is None:
            name = email_address.split("@")[0]
        tmp_data = {
            "organization_name": name,
            "contact_data": PostOrganizationContactDataEntity.create_entity_with_default_values(
                **{
                    "country_code": "DE",
                    "email_address": email_address,
                    "fax_number": "0171 123 456-2",
                    "is_email_address_public": False,
                    "is_fax_number_public": False,
                    "is_phone_number_public": True,
                    "phone_number": "0171 123 456",
                    "place_name": "dummy place name",
                    "street_name": "dummy street name",
                    "street_number": "42",
                    "zip_code": "12345",
                }
            ).model_dump(),
            "details": {
                "categories": [
                    category.value for category in (organization_categories or [])
                ],
                "description_de": "",
                "description_en": "",
                "is_user_count_public": True,
            },
        }
        tmp_data.update(data)
        return PostOrganizationEntity.model_validate(tmp_data)

    def create_new_organization(
        self,
        email_address: str,
        auth_headers: Optional[Dict],
        organization_categories: Optional[Collection[OrganizationCategory]] = None,
        **data,
    ) -> UnapprovedConfidentialOrganizationProfileEntity:
        organization_data = self.create_new_organization_data(
            email_address, organization_categories=organization_categories, **data
        )
        return self.create_new_organization_from_data(organization_data, auth_headers)

    def create_new_organization_from_data(
        self,
        organization_data: PostOrganizationEntity,
        auth_headers: Optional[Dict] = None,
    ) -> UnapprovedConfidentialOrganizationProfileEntity:
        r = self.post(
            "/api/v2/my/unapproved-organizations",
            json=json.loads(organization_data.model_dump_json()),
            auth_headers=auth_headers,
        )
        return UnapprovedConfidentialOrganizationProfileEntity.model_validate(r.json())

    def create_new_organization_without_data(self, auth_headers: Optional[Dict] = None):
        organization_data = PostOrganizationEntity.model_validate({})
        r = self.post(
            "/api/v2/my/unapproved-organizations",
            json=json.loads(organization_data.model_dump_json()),
            auth_headers=auth_headers,
        )
        return UnapprovedConfidentialOrganizationProfileEntity.model_validate(r.json())

    def set_organization_logo(
        self, organization_id: str, file_name: str, auth_headers: Optional[Dict] = None
    ):
        file_name = resolve_file_name_in_resource_path(file_name)
        with open(file_name, "rb") as f:
            files = {
                "logo": (
                    os.path.basename(file_name),
                    f,
                    mimetypes.guess_type(file_name)[0],
                    {"Expires": "0"},
                )
            }
            self.put(
                f"/api/v2/my/unapproved-organizations/{organization_id}/logo",
                files=files,
                auth_headers=auth_headers,
            )

    def revoke_organization_approval(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ):
        self.put(
            f"/api/v2/my/unapproved-organizations/{organization_id}/revoke-approval-request",
            auth_headers=auth_headers,
        )

    def can_request_organization_approval(
        self,
        organization_id: str,
        auth_headers: Optional[Dict] = None,
        allowed_errors: Optional[List[int]] = None,
    ) -> bool:
        r = self.get(
            f"/api/v2/my/unapproved-organizations/{organization_id}/can-request-approval",
            auth_headers=auth_headers,
            allowed_errors=allowed_errors,
        )
        return r.json()["can_request_approval"]

    def request_organization_approval(
        self,
        organization_id: str,
        auth_headers: Optional[Dict] = None,
        allowed_errors: Optional[List[int]] = None,
    ):
        self.put(
            f"/api/v2/my/unapproved-organizations/{organization_id}/request-approval",
            auth_headers=auth_headers,
            allowed_errors=allowed_errors,
        )

    def delete_unapproved_organization(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ):
        return self.delete(
            f"/api/v2/my/unapproved-organizations/{organization_id}",
            auth_headers=auth_headers,
        )

    def get_unapproved_organizations(
        self,
        page: Optional[int] = None,
        page_size: Optional[int] = None,
        auth_headers: Optional[Dict] = None,
    ) -> ConfidentialOrganizationProfilesEntity:
        params = {}
        if page is not None:
            params["page"] = page
        if page_size is not None:
            params["page_size"] = page_size
        data = json.loads(
            OrganizationAccountStateQueryEntity(
                organizationAccountStates=[
                    OrganizationAccountState.AWAITING_ACTIVATION,
                    OrganizationAccountState.REQUIRES_ACCOUNT_ACTIVATION,
                ]
            ).model_dump_json()
        )
        r = self.put(
            "/api/v2/my/unapproved-organizations/query",
            json=data,
            auth_headers=auth_headers,
            params=params,
        )
        return ConfidentialOrganizationProfilesEntity.model_validate(r.json())

    def get_unapproved_organization(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ) -> ConfidentialOrganizationProfileEntity:
        organizations = self.get_unapproved_organizations(0, 1000, auth_headers)
        for organization in organizations.organizations:
            if organization.organization_id == organization_id:
                return organization

    def set_organization_name(
        self, organization_id: str, name: str, auth_headers: Optional[Dict] = None
    ):
        data = {"organization_name": name}
        self.put(
            f"/api/v2/my/unapproved-organizations/{organization_id}/name",
            json=data,
            auth_headers=auth_headers,
        )

    def set_organization_contact_data(
        self,
        organization_id: str,
        contact_data: PostOrganizationContactDataEntity,
        auth_headers: Optional[Dict] = None,
    ):
        self.put(
            f"/api/v2/my/unapproved-organizations/{organization_id}/contact",
            json=json.loads(contact_data.model_dump_json()),
            auth_headers=auth_headers,
        )

    def set_organization_details(
        self,
        organization_id: str,
        details: PostOrganizationDetailsEntity,
        auth_headers: Optional[Dict] = None,
    ) -> UnapprovedConfidentialOrganizationProfileEntity:
        r = self.put(
            f"/api/v2/my/unapproved-organizations/{organization_id}/details",
            json=json.loads(details.model_dump_json()),
            auth_headers=auth_headers,
        )
        return UnapprovedConfidentialOrganizationProfileEntity.model_validate(r.json())
