import json
from typing import List, Optional

from ..api_controller import APIController
from .entity_models import (
    ClientEntity,
    ConfidentialOrganizationProfilesEntity,
    OrganizationAccountState,
    OrganizationDeploymentConfiguration,
    OrganizationDeploymentCredentials,
    PostComputeProvider,
    PostOrganizationDeploymentConfiguration,
)


class AdminController(APIController):
    def delete_user(self, target_user_id: str):
        self.delete(f"/api/v2/admin/users/{target_user_id}/account")

    def grant_admin_privileges(self, target_user_id: str):
        self.put(f"/api/v2/admin/users/{target_user_id}/grant_admin_privileges")

    def get_email_verification_token(self, email_address: str) -> str:
        params = {
            "email_address": email_address,
            "token_lifetime_ms": 24 * 60 * 60 * 1000,
        }
        r = self.get("/api/v2/admin/generate-email-verification-token", params=params)
        return r.content.decode("utf-8")

    def get_organization_ids_by_account_state(
        self, page: int, page_size: int, account_states: List[OrganizationAccountState]
    ) -> List[str]:
        params = {
            "account_states": account_states,
            "page": page,
            "page_size": page_size,
        }
        r = self.get("/api/v2/admin/organizations", params=params)
        # don't use the model classes here to be tolerant to incomplete data (e.g. v1 api organizations)
        return [data["organization_id"] for data in r.json()["organizations"]]

    def get_all_organization_ids(self) -> List[str]:
        all_account_states = [
            accountState.value for accountState in OrganizationAccountState
        ]
        return self.get_organization_ids_by_account_state(0, 9999, all_account_states)

    def get_organizations(
        self,
        page: int,
        page_size: int,
        account_states: Optional[List[OrganizationAccountState]] = None,
        return_json: bool = False,
    ) -> ConfidentialOrganizationProfilesEntity:
        if account_states is None:
            account_states = [
                str(account_state.value) for account_state in OrganizationAccountState
            ]
        else:
            account_states = [account_state.value for account_state in account_states]
        params = {
            "account_states": account_states,
            "page": page,
            "page_size": page_size,
        }
        r = self.get("/api/v2/admin/organizations", params=params)
        if return_json:
            return r.json()
        return ConfidentialOrganizationProfilesEntity.model_validate(r.json())

    def exists_organization(self, organization_id: str) -> bool:
        found = False
        for o_id in self.get_all_organization_ids():
            if o_id == organization_id:
                found = True
                break
        return found

    def delete_organization(self, organization_id: str):
        self.delete(f"/api/v2/admin/organizations/{organization_id}")

    def delete_members_only_in_given_organization(self, organization_id: str):
        self.delete(
            f"/api/v2/admin/organizations/{organization_id}/members_only_in_this_organization"
        )

    def delete_organization_and_members_only_in_given_organization(
        self, organization_id: str
    ):
        self.delete(
            f"/api/v2/admin/organizations/{organization_id}/members_only_in_this_organization"
        )
        self.delete(f"/api/v2/admin/organizations/{organization_id}")

    def get_organization_deployment_configuration(
        self, organization_id: str
    ) -> OrganizationDeploymentConfiguration:
        r = self.get(f"/api/v2/admin/organizations/{organization_id}/deployment/config")
        return OrganizationDeploymentConfiguration.model_validate(r.json())

    def put_organization_deployment_configuration(
        self,
        organization_id: str,
        organization_deployment_configuration: PostOrganizationDeploymentConfiguration,
    ) -> OrganizationDeploymentConfiguration:
        r = self.put(
            f"/api/v2/admin/organizations/{organization_id}/deployment/config",
            json=json.loads(organization_deployment_configuration.model_dump_json()),
        )
        return OrganizationDeploymentConfiguration.model_validate(r.json())

    def set_organization_compute_provider_jes(
        self, organization_id: str, compute_provider_organization_id: str
    ):
        return self.put(
            f"/api/v2/admin/organizations/{organization_id}/deployment/compute-provider-jes",
            json=json.loads(
                PostComputeProvider(
                    compute_provider_organization_id=compute_provider_organization_id
                ).model_dump_json()
            ),
        )

    def remove_compute_provider_jes_from_organization(self, organization_id: str):
        return self.delete(
            f"/api/v2/admin/organizations/{organization_id}/deployment/compute-provider-jes"
        )

    def get_organization_clients(self, organization_id: str) -> List[ClientEntity]:
        params = {"page": 0, "page_size": 9999}
        r = self.get(
            f"/api/v2/admin/organizations/{organization_id}/clients", params=params
        )
        result = r.json()["clients"]
        return [ClientEntity.model_validate(client_data) for client_data in result]

    def get_all_clients(self) -> List[ClientEntity]:
        params = {"page": 0, "page_size": 9999}
        r = self.get("/api/v2/admin/clients", params=params)
        result = r.json()["clients"]
        return [ClientEntity.model_validate(client_data) for client_data in result]

    def get_client_credentials(
        self, organization_id: str
    ) -> OrganizationDeploymentCredentials:
        r = self.get(
            f"/api/v2/admin/organizations/{organization_id}/deployment/client-credentials"
        )
        return OrganizationDeploymentCredentials.model_validate(r.json())
