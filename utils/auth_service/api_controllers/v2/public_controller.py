import json
from typing import Optional, Union

from ..api_controller import APIController
from .admin_controller import AdminController
from .entity_models import (
    LanguageCode,
    PostUserEntity,
    PublicOrganizationProfileEntity,
    PublicOrganizationsEntity,
    UserContactDataEntity,
    UserDetailsEntity,
)


class PublicController(APIController):
    def __init__(self):
        super().__init__()
        self._admin_controller = AdminController()

    def get_organization_logo(self, organization_id: str) -> bytes:
        r = self.get(f"/api/v2/public/organizations/{organization_id}/logo")
        return r.content

    def get_organizations(
        self, page: Optional[int] = None, page_size: Optional[int] = None
    ) -> PublicOrganizationsEntity:
        params = {}
        if page is not None:
            params["page"] = page
        if page_size is not None:
            params["page_size"] = page_size
        r = self.get("/api/v2/public/organizations", params=params)
        return PublicOrganizationsEntity.model_validate(r.json())

    def get_organization(self, organization_id: str) -> PublicOrganizationProfileEntity:
        r = self.get(f"/api/v2/public/organizations/{organization_id}")
        return PublicOrganizationProfileEntity.model_validate(r.json())

    @staticmethod
    def create_post_user_entity(
        email_address: str,
        password: Optional[str] = None,
        first_name: Optional[str] = None,
        language_code: Optional[LanguageCode] = None,
        last_name: Optional[str] = None,
        title: Optional[str] = None,
    ) -> PostUserEntity:
        return PostUserEntity(
            contact_data=UserContactDataEntity(
                country_code="DE",
                email_address=email_address,
                phone_number="",
                place_name="Berlin",
                street_name="Hauptstrasse",
                street_number="1",
                zip_code="0815",
            ),
            details=UserDetailsEntity(
                first_name=first_name or "Jane",
                last_name=last_name or "Doe",
                title=title or "",
            ),
            language_code=language_code or LanguageCode.EN,
            password=password or "Password345!",
        )

    def register_new_user(
        self, user_or_email: Union[str, PostUserEntity]
    ) -> PostUserEntity:
        if isinstance(user_or_email, PostUserEntity):
            user = user_or_email
        else:
            user = self.create_post_user_entity(user_or_email)
        self.post(
            url="/api/v2/public/register-new-user",
            json=json.loads(user.model_dump_json()),
        )
        return user

    def verify_registration(self, email_address: str):
        email_verification_token = self._admin_controller.get_email_verification_token(
            email_address
        )
        params = {"email_address": email_address, "token": email_verification_token}
        self.post(url="/api/v2/public/verify-registration", params=params)

    def get_organization_by_name(
        self, organization_name: str
    ) -> Optional[PublicOrganizationProfileEntity]:
        organizations_found = []
        for o in PublicController().get_organizations(0, 9999).organizations:
            if o.organization_name == organization_name:
                organizations_found.append(o)
        assert (
            len(organizations_found) < 2
        ), f"More than one orga found with name [{organization_name}]"
        assert (
            len(organizations_found) > 0
        ), f"No orga found with name [{organization_name}]"
        return organizations_found[0]
