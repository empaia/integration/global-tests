import json
from typing import Dict, List, Optional

from ..api_controller import APIController
from .entity_models import (
    ConfidentialMemberProfileEntity,
    ConfidentialMemberProfilesEntity,
    ConfidentialOrganizationProfileEntity,
    MembershipRequestEntity,
    MembershipRequestsEntity,
    OrganizationCategory,
    OrganizationCategoryRequestEntity,
    OrganizationCategoryRequestsEntity,
    OrganizationUserRole,
    OrganizationUserRoleRequestEntity,
    PostOrganizationCategoryRequestEntity,
    PostOrganizationUserRolesEntity,
    PostReviewerComment,
    RequestState,
)


class ManagerController(APIController):
    def get_membership_requests(
        self,
        organization_id: str,
        auth_headers: Optional[Dict] = None,
        page: int = 0,
        page_size: int = 1000,
    ) -> MembershipRequestsEntity:
        headers = {"organization-id": organization_id}
        params = {"page": page, "page_size": page_size}
        r = self.get(
            "/api/v2/manager/organization/membership-requests",
            auth_headers=auth_headers,
            headers=headers,
            params=params,
        )
        return MembershipRequestsEntity.model_validate(r.json())

    def get_membership_request(
        self, organization_id: str, user_id: str, auth_headers: Optional[Dict] = None
    ) -> Optional[MembershipRequestEntity]:
        membership_requests = self.get_membership_requests(
            organization_id, auth_headers
        ).membership_requests
        for membership_request in membership_requests:
            if membership_request.user_details.user_id == user_id:
                return membership_request

    def accept_membership_request(
        self,
        organization_id: str,
        membership_request_id: int,
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        self.put(
            f"/api/v2/manager/organization/membership-requests/{membership_request_id}/accept",
            headers=headers,
            auth_headers=auth_headers,
        )

    def deny_membership_request(
        self,
        organization_id: str,
        membership_request_id: int,
        reviewer_comment: str = None,
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        body = {}
        if reviewer_comment:
            body = json.loads(
                PostReviewerComment(reviewer_comment=reviewer_comment).model_dump_json()
            )
        self.put(
            f"/api/v2/manager/organization/membership-requests/{membership_request_id}/deny",
            headers=headers,
            json=body,
            auth_headers=auth_headers,
        )

    def set_organization_user_roles(
        self,
        organization_id: str,
        target_user_id: str,
        organization_user_roles: List[OrganizationUserRole],
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        self.put(
            f"/api/v2/manager/organization/users/{target_user_id}/roles",
            headers=headers,
            json=json.loads(
                PostOrganizationUserRolesEntity(
                    roles=organization_user_roles
                ).model_dump_json()
            ),
            auth_headers=auth_headers,
        )

    def request_organization_category(
        self,
        organization_id: str,
        organization_categories: List[OrganizationCategory],
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        self.put(
            "/api/v2/manager/organization/category-requests",
            headers=headers,
            json=json.loads(
                PostOrganizationCategoryRequestEntity(
                    requested_organization_categories=[
                        c.value for c in organization_categories
                    ]
                ).model_dump_json()
            ),
            auth_headers=auth_headers,
        )

    def get_organization_category_requests(
        self, organization_id: str, auth_headers: Optional[Dict] = None
    ) -> OrganizationCategoryRequestsEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            "/api/v2/manager/organization/category-requests",
            headers=headers,
            auth_headers=auth_headers,
            params={"page": 0, "page_size": 9999},
        )
        return OrganizationCategoryRequestsEntity.model_validate(r.json())

    def find_open_organization_category_request(
        self,
        organization_id: str,
        auth_headers: Optional[Dict] = None,
    ) -> OrganizationCategoryRequestEntity:
        category_requests = self.get_organization_category_requests(
            organization_id, auth_headers
        )
        for category_request in category_requests.organization_category_requests:
            if category_request.request_state == RequestState.REQUESTED:
                return category_request
        raise AssertionError("Category request not found")

    def revoke_category_request(
        self,
        organization_id: str,
        category_request_id: str,
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        self.put(
            f"/api/v2/manager/organization/category-requests/{category_request_id}/revoke",
            headers=headers,
            auth_headers=auth_headers,
        )

    def get_organization(
        self, organization_id: str, auth_headers: Dict
    ) -> ConfidentialOrganizationProfileEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            "/api/v2/manager/organization/profile",
            headers=headers,
            auth_headers=auth_headers,
        )
        return ConfidentialOrganizationProfileEntity.model_validate(r.json())

    def get_organization_role_requests(
        self, organization_id: str, auth_headers: Dict
    ) -> OrganizationCategoryRequestsEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            "/api/v2/manager/organization/role-requests",
            headers=headers,
            auth_headers=auth_headers,
        )
        return OrganizationCategoryRequestsEntity.model_validate(r.json())

    def accept_organization_user_role_request(
        self, organization_id: str, role_request_id: int, auth_headers: Dict
    ):
        headers = {"organization-id": organization_id}
        r = self.put(
            f"/api/v2/manager/organization/role-requests/{role_request_id}/accept",
            headers=headers,
            auth_headers=auth_headers,
        )
        return OrganizationUserRoleRequestEntity.model_validate(r.json())

    def deny_organization_role_request(
        self,
        organization_id: str,
        role_request_id: int,
        reviewer_comment: str = None,
        auth_headers: Optional[Dict] = None,
    ):
        headers = {"organization-id": organization_id}
        body = {}
        if reviewer_comment:
            body = json.loads(
                PostReviewerComment(reviewer_comment=reviewer_comment).model_dump_json()
            )
        self.put(
            f"/api/v2/manager/organization/role-requests/{role_request_id}/deny",
            json=body,
            headers=headers,
            auth_headers=auth_headers,
        )

    def get_users(
        self,
        organization_id: str,
        auth_headers: Optional[Dict],
        return_raw: bool = False,
    ) -> ConfidentialMemberProfilesEntity:
        r = self.get(
            "/api/v2/manager/organization/users",
            headers={"organization-id": organization_id},
            auth_headers=auth_headers,
        )
        if return_raw:
            return r.json()
        return ConfidentialMemberProfilesEntity.model_validate(r.json())

    def get_user(
        self, target_user_id: str, organization_id: str, auth_headers: Optional[Dict]
    ) -> ConfidentialMemberProfileEntity:
        headers = {"organization-id": organization_id}
        r = self.get(
            f"/api/v2/manager/organization/users/{target_user_id}/profile",
            headers=headers,
            auth_headers=auth_headers,
        )
        return ConfidentialMemberProfileEntity.model_validate(r.json())

    def remove_user_from_organization(
        self, target_user_id: str, organization_id: str, auth_headers: Optional[Dict]
    ):
        headers = {"organization-id": organization_id}
        _ = self.put(
            f"/api/v2/manager/organization/users/{target_user_id}/remove",
            headers=headers,
            auth_headers=auth_headers,
        )
