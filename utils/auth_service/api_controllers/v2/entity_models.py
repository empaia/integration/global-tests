from enum import Enum, auto
from typing import Any, Dict, List, Optional, Union

from pydantic import BaseModel, Field


class AllowedRoles(BaseModel):
    roles: List[str]


class AccessToken(BaseModel):
    acr: str
    aud: Union[str, List[str]]
    azp: str
    email_verified: bool
    exp: int
    iat: int
    iss: str
    jti: str
    allowed_origins: Optional[List[str]] = Field(default=None, alias="allowed-origins")
    organization: Any = None  # TODO
    organizations: Any = None  # TODO
    preferred_username: str
    realm_access: AllowedRoles
    resource_access: Dict[str, AllowedRoles]
    scope: str
    sub: str
    typ: str

    def has_audience(self, audience: str) -> bool:
        return (
            audience in self.aud if isinstance(self.aud, list) else audience == self.aud
        )


class ClientToken(AccessToken):
    clientAddress: str
    clientHost: str
    clientId: str


class ClientCredentials(BaseModel):
    id: str
    client_id: str
    secret: Optional[str]


class OrganizationDeploymentCredentials(BaseModel):
    workbench_service: Optional[ClientCredentials] = None
    workbench_client: Optional[ClientCredentials] = None
    medical_data_service: Optional[ClientCredentials] = None
    data_management_client: Optional[ClientCredentials] = None
    id_mapper_service: Optional[ClientCredentials] = None
    upload_service: Optional[ClientCredentials] = None
    app_service: Optional[ClientCredentials] = None
    job_execution_service: Optional[ClientCredentials] = None
    compute_provider_job_execution_service: Optional[ClientCredentials] = None


class PostOrganizationDeploymentConfiguration(BaseModel):
    allow_localhost_web_clients: bool = False
    allow_http: bool = False
    workbench_deployment_domain: str = ""
    data_manager_deployment_domain: str = ""
    workbench_redirect_uris: List[str] = []
    data_manager_redirect_uris: List[str] = []
    workbench_web_origins: List[str] = []
    data_manager_web_origins: List[str] = []


class PostComputeProvider(BaseModel):
    compute_provider_organization_id: str


class OrganizationDeploymentConfiguration(PostOrganizationDeploymentConfiguration):
    organization_id: str


class OrganizationAccountState(Enum):
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"
    REQUIRES_ACCOUNT_ACTIVATION = "REQUIRES_ACTIVATION"
    AWAITING_DEACTIVATION = "AWAITING_DEACTIVATION"
    AWAITING_ACTIVATION = "AWAITING_ACTIVATION"


class OrganizationCategory(Enum):
    APP_CUSTOMER = "APP_CUSTOMER"
    APP_VENDOR = "APP_VENDOR"
    COMPUTE_PROVIDER = "COMPUTE_PROVIDER"
    PRODUCT_PROVIDER = "PRODUCT_PROVIDER"


class LanguageCode(Enum):
    DE = "DE"
    EN = "EN"

    @classmethod
    def _missing_(cls, value):
        for language_code in cls:
            if language_code.value == value.upper():
                return language_code


class LanguageEntity(BaseModel):
    language_code: LanguageCode


class OrganizationUserRole(Enum):
    APP_MAINTAINER = "APP_MAINTAINER"
    DATA_MANAGER = "DATA_MANAGER"
    MANAGER = "MANAGER"
    PATHOLOGIST = "PATHOLOGIST"
    CLEARANCE_MAINTAINER = "CLEARANCE_MAINTAINER"

    @property
    def token_value(self):
        items = self.value.split("_")
        items = [i.lower() for i in items]
        return "-".join(items)

    @classmethod
    def from_token_value(cls, token_value: str):
        for organization_user_role in OrganizationUserRole:
            if token_value == organization_user_role.token_value:
                return organization_user_role
        raise AssertionError(f"Unhandled token value {token_value}")


class PostOrganizationUserRolesEntity(BaseModel):
    roles: List[OrganizationUserRole]


class OrganizationUserRolesEntity(BaseModel):
    owned_roles: List[OrganizationUserRole]
    selectable_roles: List[OrganizationUserRole]


class AddressEntity(BaseModel):
    country_code: str
    place_name: str
    street_name: str
    street_number: str
    zip_code: str


class UnapprovedAddressEntity(BaseModel):
    country_code: Optional[str] = None
    place_name: Optional[str] = None
    street_name: Optional[str] = None
    street_number: Optional[str] = None
    zip_code: Optional[str] = None


class TextTranslationEntity(BaseModel):
    language_code: str
    text: str


class CountryEntity(BaseModel):
    country_code: str
    translated_names: List[TextTranslationEntity]


class UserDetailsEntity(BaseModel):
    last_name: str
    first_name: str
    title: str


class ExtendedUserDetailsEntity(UserDetailsEntity):
    user_id: str
    email_address: str


class RequestState(Enum):
    REQUESTED = "REQUESTED"
    REVOKED = "REVOKED"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"


class PostMembershipRequestEntity(BaseModel):
    organization_id: str


class MembershipRequestEntity(BaseModel):
    membership_request_id: int
    organization_id: str
    organization_name: str
    user_details: ExtendedUserDetailsEntity
    created_at: int
    reviewer_id: Optional[str] = None
    reviewer_comment: Optional[str] = None
    updated_at: Optional[int] = None
    request_state: RequestState


class MembershipRequestsEntity(BaseModel):
    membership_requests: List[MembershipRequestEntity]
    total_membership_requests_count: int


class OrganizationDetailsEntity(BaseModel):
    categories: List[OrganizationCategory]
    description_de: Optional[str] = None
    description_en: Optional[str] = None
    is_user_count_public: bool = False


class UnapprovedOrganizationDetailsEntity(BaseModel):
    categories: Optional[List[OrganizationCategory]] = None
    description_de: Optional[str] = None
    description_en: Optional[str] = None
    is_user_count_public: bool = False


class PostOrganizationNameEntity(BaseModel):
    organization_name: str


class PostOrganizationDetailsEntity(OrganizationDetailsEntity):
    @staticmethod
    def create_entity_with_default_values(
        categories: Optional[List[OrganizationCategory]] = None,
        description_de: Optional[str] = None,
        description_en: Optional[str] = None,
        is_user_count_public: Optional[bool] = None,
    ):
        return PostOrganizationDetailsEntity(
            categories=categories or [],
            description_de=description_de or "",
            description_en=description_en or "",
            is_user_count_public=is_user_count_public or False,
        )


class OrganizationContactDataEntity(AddressEntity):
    department: Optional[str] = None
    email_address: str
    fax_number: Optional[str] = None
    is_email_address_public: bool
    is_fax_number_public: bool
    is_phone_number_public: bool
    phone_number: str
    website: Optional[str] = None


class UnapprovedOrganizationContactDataEntity(AddressEntity):
    department: Optional[str] = None
    email_address: Optional[str] = None
    fax_number: Optional[str] = None
    is_email_address_public: bool
    is_fax_number_public: bool
    is_phone_number_public: bool
    phone_number: Optional[str] = None
    website: Optional[str] = None


class PostOrganizationContactDataEntity(OrganizationContactDataEntity):
    @staticmethod
    def create_entity_with_default_values(
        email_address: str,
        department: Optional[str] = None,
        country_code: Optional[str] = None,
        fax_number: Optional[str] = None,
        is_email_address_public: Optional[bool] = None,
        is_fax_number_public: Optional[bool] = None,
        is_phone_number_public: Optional[bool] = None,
        phone_number: Optional[str] = None,
        place_name: Optional[str] = None,
        street_name: Optional[str] = None,
        street_number: Optional[str] = None,
        website: Optional[str] = None,
        zip_code: Optional[str] = None,
    ) -> "":
        return PostOrganizationContactDataEntity(
            department=department or "",
            country_code=country_code or "DE",
            email_address=email_address,
            fax_number=fax_number or "",
            is_email_address_public=is_email_address_public or False,
            is_fax_number_public=is_fax_number_public or False,
            is_phone_number_public=is_phone_number_public or False,
            phone_number=phone_number or "",
            place_name=place_name or "not defined",
            street_name=street_name or "not defined",
            street_number=street_number or "0",
            website=website or "",
            zip_code=zip_code or "0",
        )


class PostOrganizationEntity(BaseModel):
    contact_data: Optional[PostOrganizationContactDataEntity] = None
    details: Optional[PostOrganizationDetailsEntity] = None
    organization_name: Optional[str] = None


class PostOrganizationCategoryRequestEntity(BaseModel):
    requested_organization_categories: List[str]


class OrganizationCategoryRequestEntity(BaseModel):
    organization_category_request_id: int
    existing_categories: List[OrganizationCategory]
    requested_categories: List[OrganizationCategory]
    retracted_categories: List[OrganizationCategory]
    organization_id: str
    organization_name: str
    user_details: ExtendedUserDetailsEntity
    created_at: int
    reviewer_id: Optional[str] = None
    reviewer_comment: Optional[str] = None
    updated_at: Optional[int] = None
    request_state: RequestState


class OrganizationCategoryRequestsEntity(BaseModel):
    organization_category_requests: List[OrganizationCategoryRequestEntity]
    total_organization_category_requests_count: int


class PostOrganizationUserRoleRequestEntity(BaseModel):
    requested_roles: List[str]


class OrganizationUserRoleRequestEntity(PostOrganizationUserRoleRequestEntity):
    organization_user_role_request_id: int
    organization_id: str
    organization_name: str
    user_details: ExtendedUserDetailsEntity
    existing_roles: List[OrganizationUserRole]
    requested_roles: List[OrganizationUserRole]
    retracted_roles: List[OrganizationUserRole]
    created_at: int
    reviewer_id: Optional[str] = None
    reviewer_comment: Optional[str] = None
    updated_at: Optional[int] = None
    request_state: RequestState


class OrganizationUserRoleRequestsEntity(BaseModel):
    organization_user_role_requests: List[OrganizationUserRoleRequestEntity]
    total_organization_user_role_requests_count: int


class ResizedPictureUrls(BaseModel):
    w60: Optional[str] = None
    w400: Optional[str] = None
    w800: Optional[str] = None
    w1200: Optional[str] = None


class PublicOrganizationContactDataEntity(AddressEntity):
    department: str
    email_address: Optional[str] = None
    fax_number: Optional[str] = None
    phone_number: Optional[str] = None
    website: str


class PublicOrganizationDetailsEntity(BaseModel):
    description: List[TextTranslationEntity]
    number_of_members: Optional[int] = None


class CommonOrganizationDataEntity(BaseModel):
    organization_id: str
    organization_name: str
    logo_url: Optional[str] = None
    resized_logo_urls: Optional[ResizedPictureUrls] = None


class ConfidentialOrganizationProfileEntity(CommonOrganizationDataEntity):
    account_state: OrganizationAccountState
    contact_data: OrganizationContactDataEntity
    details: OrganizationDetailsEntity
    created_timestamp: int
    updated_timestamp: int
    normalized_name: str


class ConfidentialOrganizationProfilesEntity(BaseModel):
    organizations: List[ConfidentialOrganizationProfileEntity]
    total_organizations_count: int


class UnapprovedCommonOrganizationDataEntity(BaseModel):
    organization_id: str
    organization_name: Optional[str] = None
    logo_url: Optional[str] = None
    resized_logo_urls: Optional[ResizedPictureUrls] = None


class UnapprovedConfidentialOrganizationProfileEntity(
    UnapprovedCommonOrganizationDataEntity
):
    account_state: OrganizationAccountState
    contact_data: Optional[UnapprovedOrganizationContactDataEntity] = None
    details: Optional[UnapprovedOrganizationDetailsEntity] = None
    created_timestamp: int
    updated_timestamp: int
    normalized_name: str


class UnapprovedConfidentialOrganizationProfilesEntity(BaseModel):
    organizations: List[UnapprovedConfidentialOrganizationProfileEntity]
    total_organizations_count: int


class PublicOrganizationEntity(CommonOrganizationDataEntity):
    pass


class PublicOrganizationsEntity(BaseModel):
    organizations: List[PublicOrganizationEntity]
    total_organizations_count: int


class PublicOrganizationProfileEntity(PublicOrganizationEntity):
    contact_data: PublicOrganizationContactDataEntity
    details: PublicOrganizationDetailsEntity


class PublicOrganizationProfilesEntity(BaseModel):
    organizations: List[PublicOrganizationProfileEntity]
    total_organizations_count: int


class UserRole(Enum):
    SUPER_ADMIN = "SUPER_ADMIN"
    MODERATOR = "MODERATOR"
    EMPAIA_INTERNATIONAL_ASSOCIATE = "EMPAIA_INTERNATIONAL_ASSOCIATE"


class PostUserRolesEntity(BaseModel):
    roles: List[str]


class UserContactDataEntity(AddressEntity):
    email_address: str
    phone_number: str


class PostUserContactDataEntity(AddressEntity):
    phone_number: str


class BaseUserEntity(BaseModel):
    contact_data: UserContactDataEntity
    details: UserDetailsEntity
    language_code: LanguageCode


class PostUserEntity(BaseUserEntity):
    password: str


class PostUserDetailsEntity(UserDetailsEntity):
    pass


class UserAccountState(Enum):
    DISABLED = "DISABLED"
    ENABLED_AND_NO_ACTIONS_REQUIRED = "ENABLED_AND_NO_ACTIONS_REQUIRED"
    REQUIRES_EMAIL_VERIFICATION = "REQUIRES_EMAIL_VERIFICATION"
    REQUIRES_PROFILE_UPDATE = "REQUIRES_PROFILE_UPDATE"


class UserProfileEntity(BaseUserEntity):
    account_state: UserAccountState
    created_timestamp: int
    profile_picture_url: Optional[str] = None
    resized_profile_picture_urls: Optional[ResizedPictureUrls] = None
    title: Optional[str] = None
    user_id: str
    user_roles: List[UserRole]
    username: str


class RegisteredUserEntity(BaseModel):
    member_of: List[str]
    user_id: str
    user_roles: List[UserRole]
    username: str
    is_active: bool


class RegisteredUsersEntity(BaseModel):
    users: List[RegisteredUserEntity]
    total_users_count: int


class ClientServiceRole(Enum):
    def _generate_next_value_(name, start, count, last_values):  # pylint: disable=E0213
        return name

    SERVICE_APP_READER = auto()
    SERVICE_APP_CONFIG_READER = auto()
    SERVICE_COMPUTE_PROVIDER = auto()


class ClientEntity(BaseModel):
    client_id: str
    keycloak_id: str
    description: str
    name: str
    type: str
    client_service_roles: Optional[List[ClientServiceRole]] = None
    redirect_uris: Optional[List[str]] = None
    web_origins: Optional[List[str]] = None


class AllowedAudiencesByOrganizationUserRoleEntity(BaseModel):
    organization_id: str
    allowed_audiences_by_organization_user_role: Dict[OrganizationUserRole, List[str]]


class MemberProfileEntity(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    title: Optional[str] = None
    organization_user_roles: List[OrganizationUserRole]
    user_id: str


class ConfidentialMemberProfileEntity(MemberProfileEntity):
    email_address: Optional[str]


class MemberProfilesEntity(BaseModel):
    users: List[MemberProfileEntity]


class ConfidentialMemberProfilesEntity(BaseModel):
    users: List[ConfidentialMemberProfileEntity]


class OrganizationCategoriesEntity(BaseModel):
    categories: List[OrganizationCategory]


class MembershipEntity(CommonOrganizationDataEntity):
    membership_request_id: Optional[int]


class MembershipsEntity(BaseModel):
    memberships: List[MembershipEntity]


class PostReviewerComment(BaseModel):
    reviewer_comment: str


class OrganizationAccountStateQueryEntity(BaseModel):
    organization_account_states: Optional[List[OrganizationAccountState]] = None


class DefaultActiveOrganization(BaseModel):
    organization_id: Optional[str] = None
