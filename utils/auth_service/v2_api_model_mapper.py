import copy
from typing import Callable, Dict, List, Optional, Union


class V2APIModelMapper:
    def __init__(self, data: Dict):
        self._data = copy.deepcopy(data)

    def map(self) -> Dict:
        raise NotImplementedError

    @staticmethod
    def map_user_type_to_user_role(user_type: str):
        user_role = user_type
        if user_type == "SUPPORTER":
            user_role = "MODERATOR"
        return user_role

    def dropped_attribute(self, key: str):
        if key in self._data:
            del self._data[key]

    def renamed_attribute(
        self,
        key: Union[List[str], str],
        new_key: Union[List[str], str],
        map_value_func: Optional[Callable] = None,
    ):
        if isinstance(key, list):
            key_list = key
            key = key_list[0]
        else:
            key_list = [key]
        if isinstance(new_key, list):
            new_key_list = new_key
        else:
            new_key_list = [new_key]
        if key in self._data:
            # lookup value:
            value = self._data
            while len(key_list) > 0:
                value = value[key_list.pop(0)]
            if map_value_func is not None:
                value = map_value_func(value)
            # delete previous key:
            del self._data[key]
            # set new value:
            target = self._data
            while len(new_key_list) > 1:
                nk = new_key_list.pop(0)
                tmp = target.get(nk)
                if tmp is None:
                    tmp = {}
                    target[nk] = tmp
                target = tmp
            nk = new_key_list.pop(0)
            if isinstance(nk, str):
                target[nk] = value
            else:
                assert isinstance(nk, tuple)
                for s in nk:
                    target[s] = value


class ContactDataMapper(V2APIModelMapper):
    def get_contact_data_target_key(self, new_key: str) -> Union[List[str], str]:
        return new_key

    def map(self) -> Dict:
        for old_key, new_key in (
            ("country_code", "country_code"),
            ("email", "email_address"),
            ("phone_number", "phone_number"),
            ("place_name", "place_name"),
            ("street_name", "street_name"),
            ("street_number", "street_number"),
            ("zip_code", "zip_code"),
        ):
            new_key = self.get_contact_data_target_key(new_key)
            self.renamed_attribute(old_key, new_key)
        return self._data


class UserEntityDataMapper(ContactDataMapper):
    def get_contact_data_target_key(self, new_key: str) -> Union[List[str], str]:
        return ["contact_data", new_key]

    def map(self) -> Dict:
        self.dropped_attribute("additional_data")
        self.dropped_attribute("username")
        for old_key, new_key in (
            ("first_name", "first_name"),
            ("last_name", "last_name"),
            ("title", "title"),
        ):
            self.renamed_attribute(old_key, ["details", new_key])
        self.renamed_attribute(["settings", "email_language"], "language_code")
        self.renamed_attribute("picture", "profile_picture_url")
        self.renamed_attribute("resized_picture_urls", "profile_picture_resized_urls")
        self.renamed_attribute(
            "user_type", "user_role", self.map_user_type_to_user_role
        )
        return self._data


def set_defaults(data: Dict, defaults: Dict):
    for key, value in defaults.items():
        if key not in data:
            data[key] = value


def set_common_contact_data_default_values(data: Dict):
    if "contact_data" not in data:
        data["contact_data"] = {}
    set_defaults(
        data["contact_data"],
        {
            "country_code": "DE",
            "phone_number": "",
            "place_name": "not defined",
            "street_name": "not defined",
            "street_number": "0",
            "zip_code": "0",
        },
    )


def set_post_user_default_values(data: Dict):
    set_common_contact_data_default_values(data)


class PostUserDataMapper(ContactDataMapper):
    def get_contact_data_target_key(self, new_key: str) -> Union[List[str], str]:
        return ["contact_data", new_key]

    def map(self) -> Dict:
        super().map()
        self.dropped_attribute("username")
        for old_key, new_key in (
            ("first_name", "first_name"),
            ("last_name", "last_name"),
            ("title", "title"),
        ):
            self.renamed_attribute(old_key, ["details", new_key])
        self.renamed_attribute(["settings", "email_language"], "language_code")
        self.renamed_attribute("country_code", ["contact_data", "language_code"])
        self.renamed_attribute("phone_number", ["contact_data", "phone_number"])
        self.renamed_attribute("place_name", ["contact_data", "place_name"])
        self.renamed_attribute("street_name", ["contact_data", "street_name"])
        self.renamed_attribute("street_number", ["contact_data", "street_number"])
        self.renamed_attribute("zip_code", ["contact_data", "zip_code"])
        # V1 API resources files do not specify the following required properties, so we set defaults here:
        set_post_user_default_values(self._data)
        return self._data


def map_post_user_data_to_v2_api(data: Dict) -> Dict:
    return PostUserDataMapper(data).map()


def set_post_organization_default_values(data: Dict):
    set_defaults(
        data["details"],
        {"categories": ["APP_CUSTOMER"], "description_de": "", "description_en": ""},
    )
    set_common_contact_data_default_values(data)
    set_defaults(
        data["contact_data"],
        {
            "department": "",
            "fax_number": "",
            "is_email_address_public": False,
            "is_fax_number_public": False,
            "is_phone_number_public": False,
            "website": "",
        },
    )


class OrganizationEntityMapper(ContactDataMapper):
    def get_contact_data_target_key(self, new_key: str) -> Union[List[str], str]:
        return ["contact_data", new_key]

    def map(self) -> Dict:
        super().map()
        self.renamed_attribute("name", "organization_name")
        self.renamed_attribute(
            "description", ["details", ("description_de", "description_en")]
        )
        self.renamed_attribute(
            "is_user_count_public", ["details", "is_user_count_public"]
        )
        self.renamed_attribute("organization_roles", ["details", "organization_roles"])
        self.renamed_attribute("fax_number", ["contact_data", "fax_number"])
        self.renamed_attribute("phone_number", ["contact_data", "phone_number"])
        self.renamed_attribute("is_email_public", ["contact_data", "is_email_public"])
        self.renamed_attribute(
            "is_fax_number_public", ["contact_data", "is_fax_number_public"]
        )
        self.renamed_attribute(
            "is_phone_number_public", ["contact_data", "is_phone_number_public"]
        )
        return self._data


def map_post_organization_data_to_v2_api(data: Dict):
    return OrganizationEntityMapper(data).map()
