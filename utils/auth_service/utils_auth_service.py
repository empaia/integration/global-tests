import mimetypes
import os
from pathlib import Path
from pprint import pformat, pprint
from uuid import uuid4

import requests

from ..singletons import general_settings, login_manager
from ..utils_resources import get_json_data

USERS = {
    "PATHOLOGIST": "LOGIN_PATHO",
    "DATA_MANAGER": "LOGIN_MTA",
    "MANAGER": "LOGIN_MANAGER",
    "CLEARANCE_MAINTAINER": "LOGIN_CLEARANCE_MAINTAINER",
    "APP_MAINTAINER": "LOGIN_APP_MAINTAINER",
}


CLIENTS = get_json_data(
    Path(__file__).absolute().parent.joinpath("../../resources/clients_config.json")
)


def generic_post(
    url: str,
    data: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    files=None,
    raw_data: bytes = None,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{general_settings.auth_service_host}{url.lstrip('/')}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.post(
        url=url, headers=headers, json=data, params=params, data=raw_data, files=files
    )
    if general_settings.verbose_requests:
        print("POST ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    return r


def generic_put(
    url: str,
    data: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{general_settings.auth_service_host}{url.lstrip('/')}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.put(url=url, headers=headers, json=data, params=params)
    if general_settings.verbose_requests:
        print("PUT ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    return r


def generic_get(
    url: str, add_headers: dict = None, params: dict = None, headers: dict = None
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{general_settings.auth_service_host}{url.lstrip('/')}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    if general_settings.verbose_requests:
        print(headers)
    r = requests.get(url=url, headers=headers, params=params)
    if general_settings.verbose_requests:
        print("GET ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    return r


def generic_delete(
    url: str,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    data: dict = None,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{general_settings.auth_service_host}{url.lstrip('/')}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    if general_settings.verbose_requests:
        print(headers)
    r = requests.delete(url=url, headers=headers, params=params, json=data)
    if general_settings.verbose_requests:
        print("DELETE ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    return r


def get_organization_by_id(organization_id: int, headers: dict = None):
    if headers is None:
        headers = login_manager.super_admin()
    # GET all
    r = generic_get(url=f"/api/organization/by_id/{organization_id}", headers=headers)
    return r.json()


def get_organization_by_name(name: str, headers: dict = None):
    if headers is None:
        headers = login_manager.super_admin()
    # GET all
    r = generic_get(url="/api/organization", headers=headers, params={"size": 9999})

    # Find
    found_orgas = []
    orga_names = []
    for orga in r.json()["content"]:
        if orga["name"] == name:
            found_orgas.append(orga)
        elif orga["normalized_name"] == name:
            found_orgas.append(orga)
        orga_names.append(orga["name"])

    if len(found_orgas) == 1:
        return found_orgas[0]
    if len(found_orgas) > 1:
        raise Exception(f"Found multiple organizations with same name '{name}'")
    if len(found_orgas) == 0:
        raise Exception(f"Organisation not found by name '{name}'")


def create_user(
    post_user: dict,
    allow_existing_user: bool = False,
    recreate_user: bool = False,
    should_activate: bool = True,
):
    user_name = post_user["user"]["username"]
    user_pw = post_user["password"]
    if user_pw is None:
        user_pw = f"{str(uuid4())[:31]}A"
        post_user["password"] = user_pw

    # GET user by email
    r = generic_get(url=f"/api/users/by_email/{post_user['user']['email']}")
    assert r.status_code == 200  # is always 200
    already_exists = True if r.content != b"null" else False
    if already_exists and recreate_user:
        r = generic_delete(f"/api/users/by_email/{post_user['user']['email']}")
        assert r.status_code == 204
        login_manager.clear_cache_for_user(user_name, user_pw)
        already_exists = False

    if already_exists:
        if allow_existing_user:
            user = r.json()
        else:
            raise Exception("User already exists")
    else:
        # POST user
        r = generic_post(url="/api/users", data=post_user, headers={})
        assert r.status_code == 200 or r.status_code == 201

        if should_activate:
            activate_user(user_name)

            # GET me (DUMMY_USER)
            # clear the cache for this user, because during tests the user might have been recreated, which
            # invalidates the cached token
            login_manager.clear_cache_for_user(user_name, user_pw)
            user_headers = login_manager.user(user_name, user_pw)
            r = generic_get(url="/api/users/me", headers=user_headers)
            assert r.status_code == 200
            if general_settings.verbose_requests:
                pprint(r.json())
        else:
            r = generic_get(url=f"/api/users/by_email/{post_user['user']['email']}")
            assert r.status_code == 200  # is always 200
        user = r.json()
    if user is None:
        raise AssertionError(
            "Failed to create user:\n"
            f"  already_exists: {already_exists}\n"
            f"  allow_existing_user: {allow_existing_user}\n"
            f"  recreate_user: {recreate_user}\n"
            f"  should_activate: {should_activate}\n"
            f"{pformat(post_user)}"
        )
    return user, user_pw


def activate_user(email: str):
    # POST get user tokens
    params = {"emails": [email]}
    r = generic_post(url="/api/super_admin/user_tokens", params=params)
    if general_settings.verbose_requests:
        pprint(r.json())
    user_tokens = r.json()[0]
    assert r.status_code == 200

    # ACTIVATE USER
    params = {"email": email, "token": user_tokens["validate_email_token"]}
    r = generic_post(url="/api/users/verify_email", params=params, headers={})
    assert r.status_code == 204


def add_user_picture(username: str, password: str, file_name: str):
    with open(file_name, "rb") as f:
        files = {
            "picture": (
                os.path.basename(file_name),
                f,
                mimetypes.guess_type(file_name)[0],
                {"Expires": "0"},
            )
        }
        headers = login_manager.user(username, password)
        r = generic_post(url="/api/users/picture", headers=headers, files=files)
    if r.status_code != 201:
        raise AssertionError(f"Unexpected status {r.status_code}")


def _post_and_find_orga(post_orga: dict):
    # POST new orga
    r = generic_post(url="/api/organization", data=post_orga)
    if r.status_code != 201 and r.status_code != 409:
        raise AssertionError(
            f"Failed to create organization: {post_orga} {r.status_code} {r.content}"
        )
    already_exists = True if r.status_code == 409 else False

    # Find
    orga = get_organization_by_name(post_orga["name"])
    if general_settings.verbose_requests:
        pprint(orga)
    organization_id = orga["organization_id"]
    add_headers = {"organization": str(organization_id)}
    return already_exists, orga, add_headers


def create_organization(
    post_orga: dict,
    allow_existing_organization: bool = False,
    recreate_entities: bool = False,
):
    already_exists, orga, add_headers = _post_and_find_orga(post_orga)

    if already_exists and recreate_entities:
        r = generic_delete("/api/organization", add_headers=add_headers)
        assert r.status_code == 204
        already_exists, orga, add_headers = _post_and_find_orga(post_orga)

    if already_exists:
        if not allow_existing_organization:
            raise Exception("Organization already exists")
    else:
        assert orga["keycloak_id"] == "null"

        # THIS IS THE APPROVE METHOD
        r = generic_put(
            url="/api/organization/process_creation_request/true",
            add_headers=add_headers,
        )
        assert r.status_code == 204 or r.status_code == 404

        # add roles
        print({"organization_roles": post_orga["organization_roles"]})
        r = generic_post(
            "/api/organization/change_roles",
            data={"organization_roles": post_orga["organization_roles"], "remarks": ""},
            add_headers=add_headers,
        )

        # assert has now keycloak id
        orga = get_organization_by_name(post_orga["name"])
        if general_settings.verbose_requests:
            pprint(orga)
        assert orga["keycloak_id"] != "null"
        assert orga["account_state"] == "ACTIVE"

    return orga


def deactivate_organization(orga_id: int):
    add_headers = {"organization": str(orga_id)}
    r = generic_post(url="/api/organization/deactivate", add_headers=add_headers)
    assert r.status_code == 200

    r = generic_get(url=f"/api/organization/by_id/{orga_id}")
    assert r.status_code == 200
    orga = r.json()
    assert orga["account_state"] == "INACTIVE"
    return orga


def add_organization_logo(orga_id: int, file_name: str):
    add_headers = {"organization": str(orga_id)}
    with open(file_name, "rb") as f:
        files = {
            "logo": (
                os.path.basename(file_name),
                f,
                mimetypes.guess_type(file_name)[0],
                {"Expires": "0"},
            )
        }
        r = generic_post(
            url="/api/organization/logo", add_headers=add_headers, files=files
        )
    assert r.status_code == 204


def add_organization_user(orga_id: int, user_id: int, admin: bool):
    add_headers = {"organization": str(orga_id)}

    if admin:
        # add user as admin
        r = generic_post(
            url=f"/api/organization/add_admin/{user_id}", add_headers=add_headers
        )
        assert r.status_code == 200
    else:
        # add user as member
        r = generic_post(
            url=f"/api/organization/add_member/{user_id}", add_headers=add_headers
        )
        assert r.status_code == 200

    # GET updated user
    r = generic_get(url=f"/api/users/{user_id}")
    assert r.status_code == 200
    user = r.json()

    return user
