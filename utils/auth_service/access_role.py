from enum import Enum
from typing import Set


class AccessRole(Enum):
    ANONYMOUS = "ANONYMOUS"
    REGISTERED_USER = "REGISTERED_USER"
    ADMIN_USER = "ADMIN_USER"
    ORGANIZATION_MEMBER = "ORGANIZATION_MEMBER"
    ORGANIZATION_ADMIN = "ORGANIZATION_ADMIN"
    SUPER_ADMIN = "SUPER_ADMIN"

    ALL = "ALL"
    ALL_V2 = "ALL_V2"
    ALL_REGISTERED_USERS = "ALL_REGISTERED_USERS"
    ALL_REGISTERED_USERS_V2 = "ALL_REGISTERED_USERS_V2"

    # V2 API Only Access Roles:
    ADMIN = "ADMIN"
    MODERATOR = "MODERATOR"
    EMPAIA_INTERNATIONAL_ASSOCIATE = "EMPAIA_INTERNATIONAL_ASSOCIATE"
    APP_MAINTAINER = "APP_MAINTAINER"
    DATA_MANAGER = "DATA_MANAGER"
    ORGANIZATION_MANAGER = "ORGANIZATION_MANAGER"
    PATHOLOGIST = "PATHOLOGIST"

    @classmethod
    def get_v1_api_access_roles(cls) -> Set["AccessRole"]:
        return {
            cls.ADMIN_USER,
            cls.ANONYMOUS,
            cls.ORGANIZATION_ADMIN,
            cls.ORGANIZATION_MEMBER,
            cls.REGISTERED_USER,
            cls.SUPER_ADMIN,
        }

    @classmethod
    def get_v2_api_access_roles(cls) -> Set["AccessRole"]:
        return {
            cls.ADMIN,
            cls.ANONYMOUS,
            cls.APP_MAINTAINER,
            cls.DATA_MANAGER,
            cls.MODERATOR,
            cls.EMPAIA_INTERNATIONAL_ASSOCIATE,
            cls.ORGANIZATION_MANAGER,
            cls.ORGANIZATION_MEMBER,
            cls.PATHOLOGIST,
            cls.REGISTERED_USER,
        }

    @classmethod
    def get_applying_access_roles(cls, access_role: "AccessRole", use_v2_api: bool):
        v2_access_roles = cls.get_v2_api_access_roles()
        access_roles = [access_role]
        if use_v2_api and access_role in v2_access_roles:
            if access_role not in (
                AccessRole.ALL_V2,
                AccessRole.ALL_REGISTERED_USERS_V2,
            ):
                if access_role != AccessRole.ANONYMOUS:
                    access_roles.append(AccessRole.ALL_REGISTERED_USERS_V2)
                access_roles.append(AccessRole.ALL_V2)
        else:
            if access_role not in (AccessRole.ALL, AccessRole.ALL_REGISTERED_USERS):
                if access_role != AccessRole.ANONYMOUS:
                    access_roles.append(AccessRole.ALL_REGISTERED_USERS)
                access_roles.append(AccessRole.ALL)
        return access_roles

    def get_extended_name(self, is_user_of_own_organization: bool) -> str:
        if self in (AccessRole.ORGANIZATION_ADMIN, AccessRole.ORGANIZATION_MEMBER):
            name = f"{self.value} OF {'OWN' if is_user_of_own_organization else 'FOREIGN'} ORGANIZATION"
        else:
            name = self.value
        return name
