from .api_test.api_test_state import APITestState
from .login_manager import LoginManager
from .settings import ComposeSettings, GeneralSettings

compose_settings = ComposeSettings()
general_settings = GeneralSettings()
login_manager = LoginManager(
    general_settings.verbose_requests,
    compose_settings.keycloak_user,
    compose_settings.keycloak_password,
)
api_test_state = APITestState(compose_settings)
