import json
import os
from enum import Enum
from typing import List, Optional, Union

import schemathesis.models

from ..auth_service.access_role import AccessRole
from .endpoint_test.endpoint import Endpoint


class APITestMode(Enum):
    TEST_EVERYTHING = "TEST_EVERYTHING"
    TEST_V1_API_PERMISSIONS_ONLY = "TEST_V1_API_PERMISSIONS_ONLY"
    TEST_V2_API_PERMISSIONS_ONLY = "TEST_V2_API_PERMISSIONS_ONLY"
    TEST_PERMISSIONS_ONLY = "TEST_PERMISSIONS_ONLY"
    TEST_FUNCTIONAL_ONLY = "TEST_FUNCTIONAL_ONLY"
    TEST_DATA_MIGRATION_ONLY = "TEST_DATA_MIGRATION_ONLY"
    TEST_KEYCLOAK_INITIALIZE = "TEST_KEYCLOAK_INITIALIZE"


class APITestFilter:
    _last_failed_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../..",
        "..",
        "..",
        ".last-failed-request.json",
    )

    def __init__(
        self,
        access_roles: Optional[List[AccessRole]],
        endpoint: Optional[str],
        method: Optional[str],
        repeat_last_failed_request: bool,
        skip_schemathesis: bool,
        api_version: str,
        test_functional_only: bool,
        test_data_migration_only: bool,
        test_keycloak_initialize_only: bool,
    ):
        assert api_version in ("all", "v1", "v2")
        self.access_roles = access_roles
        self.endpoint = endpoint
        self.method = method
        if isinstance(self.method, str):
            self.method = self.method.lower()
        elif isinstance(self.method, list):
            self.method = [m.lower() for m in self.method]
        self.skip_schemathesis = skip_schemathesis
        self.repeat_last_failed_request = repeat_last_failed_request
        if repeat_last_failed_request and os.path.exists(self._last_failed_file):
            self._set_from_last_failed_request_file()
        self._api_version = api_version
        self._test_mode = self._detect_test_mode(
            test_functional_only,
            test_data_migration_only,
            test_keycloak_initialize_only,
        )

    @property
    def api_version(self) -> str:
        return self._api_version

    def remove_last_failed_request_file(self):
        if os.path.exists(self._last_failed_file):
            os.remove(self._last_failed_file)

    def update_last_failed_request(
        self,
        access_role: AccessRole,
        endpoint: Union[Endpoint, schemathesis.models.Case],
    ):
        self.remove_last_failed_request_file()
        data = {
            "access_role": access_role.value,
            "method": endpoint.method.lower(),
            "endpoint": endpoint.path,
        }
        with open(self._last_failed_file, "w", encoding="utf-8") as f:
            json.dump(data, f)

    def _set_from_last_failed_request_file(self):
        with open(self._last_failed_file, encoding="utf-8") as f:
            data = json.load(f)
        if self.access_roles is None:
            self.access_roles = [AccessRole[data["access_role"].upper()]]
        if self.method is None:
            self.method = data["method"]
        if self.endpoint is None:
            self.endpoint = data["endpoint"]

    def _detect_test_mode(
        self,
        test_functional_only,
        test_data_migration_only,
        test_keycloak_initialize_only,
    ) -> APITestMode:
        if test_functional_only:
            assert not test_data_migration_only
            assert not test_keycloak_initialize_only
            test_mode = APITestMode.TEST_FUNCTIONAL_ONLY
        elif test_data_migration_only:
            assert not test_functional_only
            assert not test_keycloak_initialize_only
            test_mode = APITestMode.TEST_DATA_MIGRATION_ONLY
        elif test_keycloak_initialize_only:
            test_mode = APITestMode.TEST_KEYCLOAK_INITIALIZE
        else:
            test_mode = APITestMode.TEST_EVERYTHING
            if self.api_version == "v1":
                test_mode = APITestMode.TEST_V1_API_PERMISSIONS_ONLY
            elif self.api_version == "v2":
                test_mode = APITestMode.TEST_V2_API_PERMISSIONS_ONLY
            elif (
                self.access_roles is not None
                or self.endpoint is not None
                or self.method is not None
            ):
                test_mode = APITestMode.TEST_PERMISSIONS_ONLY
        return test_mode

    @property
    def run_functional_tests(self) -> bool:
        return self._test_mode in (
            APITestMode.TEST_EVERYTHING,
            APITestMode.TEST_FUNCTIONAL_ONLY,
        )

    @property
    def run_keycloak_initialize_test(self) -> bool:
        return self._test_mode is APITestMode.TEST_KEYCLOAK_INITIALIZE

    @property
    def run_data_migration_test(self) -> bool:
        return self._test_mode is APITestMode.TEST_DATA_MIGRATION_ONLY

    @property
    def run_v1_api_permission_test(self) -> bool:
        return self._test_mode in (
            APITestMode.TEST_EVERYTHING,
            APITestMode.TEST_V1_API_PERMISSIONS_ONLY,
            APITestMode.TEST_PERMISSIONS_ONLY,
        )

    @property
    def run_v2_api_permission_test(self) -> bool:
        return self._test_mode in (
            APITestMode.TEST_EVERYTHING,
            APITestMode.TEST_V2_API_PERMISSIONS_ONLY,
            APITestMode.TEST_PERMISSIONS_ONLY,
        )
