import requests


class Endpoint:
    def __init__(self, base_url: str, method: str, path: str):
        self._base_url = base_url
        self._method = method
        self._path = path
        self._body = None
        self._headers = None
        self._path_parameters = None
        self._query = None
        self._is_v2_api = None

    def __eq__(self, other: "Endpoint"):
        return self.method == other.method and self.path == other.path

    def set_is_v2_api(self, is_v2_api: bool):
        self._is_v2_api = is_v2_api

    @property
    def is_v2_api(self) -> bool:
        return self._is_v2_api

    @property
    def body(self):
        return self._body

    @body.setter
    def body(self, body):
        self._body = body

    @property
    def headers(self):
        return self._headers

    @headers.setter
    def headers(self, headers):
        self._headers = headers

    @property
    def path_parameters(self):
        return self._path_parameters

    @path_parameters.setter
    def path_parameters(self, path_parameters):
        self._path_parameters = path_parameters

    @property
    def query(self):
        return self._query

    @query.setter
    def query(self, query):
        self._query = query

    @property
    def method(self):
        return self._method

    @property
    def path(self):
        return self._path

    def send_request(self) -> requests.Response:
        path = str(self.path)
        if self.path_parameters is not None:
            path = path.format(**self.path_parameters)
        url = f"{self._base_url}{str(path).lstrip('/')}"
        data = {}
        if self.body is not None:
            attr_name = "json" if isinstance(self.body, dict) else "data"
            data[attr_name] = self.body
        r = requests.request(
            self.method, url, headers=self.headers, params=self.query, **data
        )
        return r
