from typing import Optional

from .request_fixture import RequestFixture
from .response_payload import RequestPayloads


class ExpectedRequestResponse:
    def __init__(
        self,
        status_code: int,
        payloads: RequestPayloads,
        fixture: Optional[RequestFixture],
    ):
        self._fixture = fixture
        self._payloads = payloads
        self._status_code = status_code

    @property
    def status_code(self) -> int:
        return self._status_code

    @property
    def payloads(self) -> Optional[RequestPayloads]:
        return self._payloads

    @property
    def fixture(self) -> Optional[RequestFixture]:
        return self._fixture
