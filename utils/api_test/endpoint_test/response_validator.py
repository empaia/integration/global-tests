from typing import Any, Dict, List, Optional, Type

from ...auth_service.access_role import AccessRole
from ...singletons import api_test_state
from .expected_response import ExpectedResponse
from .expected_responses import ExpectedResponses


class ResponseValidator:
    def __init__(self):
        self.__expected_responses: Dict[AccessRole, ExpectedResponse] = {}

    def set_response_for_access_role(self, access_role: AccessRole, status_code: int):
        assert access_role not in self.__expected_responses
        self.__expected_responses[access_role] = ExpectedResponse(
            status_code, access_role
        )

    def set_ignore_skip(self, access_role: AccessRole):
        self.__expected_responses[access_role].set_ignore_skip()

    def get_expected_responses_for_applying_access_roles(
        self, access_role: AccessRole
    ) -> Optional[ExpectedResponses]:
        result = []
        for ar in AccessRole.get_applying_access_roles(
            access_role, use_v2_api=api_test_state.use_v2_api()
        ):
            er = self.__expected_responses.get(ar)
            if er is not None:
                result.append(er)
        return None if len(result) == 0 else ExpectedResponses(access_role, result)

    def get_allowed_access_roles(
        self, is_user_of_own_organization: bool
    ) -> List[AccessRole]:
        return [
            ar
            for ar, er in self.__expected_responses.items()
            if er.is_access_allowed(is_user_of_own_organization)
        ]

    def set_expected_payload_type(
        self, access_role: AccessRole, expected_payload_type: Type
    ):
        self.__expected_responses[access_role].set_expected_payload_type(
            expected_payload_type
        )

    def set_expected_payload(self, access_role: AccessRole, expected_payload: Any):
        self._get_expected_response(access_role).set_expected_payload(expected_payload)

    def _get_expected_response(self, access_role: AccessRole) -> ExpectedResponse:
        er = self.__expected_responses.get(access_role)
        if er is None:
            if access_role not in (AccessRole.ALL, AccessRole.ALL_REGISTERED_USERS):
                er = self._implicitly_add_expected_response_for_meta_access_role(
                    access_role
                )
        return er

    def _implicitly_add_expected_response_for_meta_access_role(
        self, access_role: AccessRole
    ) -> ExpectedResponse:
        meta_er = None
        if access_role is not AccessRole.ANONYMOUS:
            meta_er = self.__expected_responses.get(AccessRole.ALL_REGISTERED_USERS)
        if meta_er is None:
            meta_er = self.__expected_responses.get(AccessRole.ALL)
        er = None
        if meta_er is not None:
            er = meta_er.clone()
            self.__expected_responses[access_role] = er
        return er
