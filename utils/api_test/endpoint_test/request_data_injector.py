from typing import Any, Optional

from ..lazy_values import LazyValue
from .request_data import RequestData
from .request_fixture import AbstractRequestDataInjector, RequestFixture


class RequestDataInjector(AbstractRequestDataInjector):
    def inject(self, request_data: RequestData, fixture: RequestFixture):
        self._inject("authorization_headers", request_data, fixture)
        self._inject("body", request_data, fixture)
        self._inject("headers", request_data, fixture)
        self._inject("path_parameters", request_data, fixture)
        self._inject("query", request_data, fixture)

    def _inject(
        self, attr_name: str, request_data: RequestData, fixture: RequestFixture
    ):
        attr_data = getattr(self, f"get_{attr_name}")(fixture)
        if attr_data is not None:
            prev_data = getattr(request_data, attr_name)
            if isinstance(prev_data, LazyValue):
                prev_data = prev_data.value
            if prev_data is None:
                setattr(request_data, attr_name, attr_data)
            elif isinstance(prev_data, dict):
                prev_data.update(attr_data)
            elif prev_data == attr_data:
                # ok, the data was already injected
                pass
            else:
                raise AssertionError(
                    f"Cannot merge previous data and injected data: {attr_name}\n"
                    f"  Previous data:\n{prev_data}\n\n"
                    f"  Injected data:\n{attr_data}\n\n"
                )

    def get_authorization_headers(self, fixture: RequestFixture) -> Optional[Any]:
        pass

    def get_body(self, fixture: RequestFixture) -> Optional[Any]:
        pass

    def get_headers(self, fixture: RequestFixture) -> Optional[Any]:
        pass

    def get_path_parameters(self, fixture: RequestFixture) -> Optional[Any]:
        pass

    def get_query(self, fixture: RequestFixture):
        pass


class InjectIntegerAsBody(RequestDataInjector):
    def get_headers(self, fixture: RequestFixture) -> Optional[Any]:
        headers = None
        body = self.get_body(fixture)
        if body is not None:
            headers = {
                "Content-Length": f"{len(body)}",
                "Content-Type": "application/json",
            }
        return headers

    def get_body(self, fixture: RequestFixture) -> Optional[str]:
        value = self.get_int_value(fixture)
        return None if value is None else str(value)

    def get_int_value(self, fixture: RequestFixture) -> Optional[int]:
        raise NotImplementedError
