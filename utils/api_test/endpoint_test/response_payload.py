from typing import Any, Dict, Optional, Tuple, Union

from pydantic import BaseModel

from ...auth_service.access_role import AccessRole
from .request_data import RequestData, RequestDataType
from .request_fixture import RequestFixture


class RequestPayloads(BaseModel):
    valid: Optional[RequestData] = None


RequestPayloadsType = Dict[str, RequestDataType]


class ResponsePayloads(BaseModel):
    valid: Dict[AccessRole, Any]


ResponsePayloadsType = Dict[str, Dict[AccessRole, Any]]


AccessRoleResponse = Union[
    int, Tuple[int, RequestPayloads], Tuple[int, RequestPayloads, RequestFixture]
]
