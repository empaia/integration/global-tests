from copy import deepcopy
from typing import Any, Dict, Optional

import requests
from pydantic import BaseModel

from ...api_test.lazy_values import LazyValue
from ...auth_service.access_role import AccessRole
from ...singletons import api_test_state
from .endpoint import Endpoint
from .request_data import RequestData
from .request_fixture import RequestFixture


class RequestCurator:
    def __init__(self, request_data: Optional[RequestData], fixture: RequestFixture):
        self._static_request_data = request_data or RequestData()
        self._request_data = self._static_request_data.model_copy()
        self._fixture = fixture
        self._replace_body = False
        self._replace_headers = False
        self._replace_path_parameters = False
        self._replace_query = False

    def set_replace_body(self, flag: bool):
        self._replace_body = flag

    def set_replace_headers(self, flag: bool):
        self._replace_headers = flag

    def set_replace_path_parameters(self, flag: bool):
        self._replace_path_parameters = flag

    def set_replace_query(self, flag: bool):
        self._replace_query = flag

    def _add_authorization_header(self, endpoint: Endpoint):
        self._request_data.authorization_headers = (
            api_test_state.get_authorization_headers()
        )
        if self._request_data.authorization_headers is not None:
            if endpoint.headers is None:
                endpoint.headers = {}
            endpoint.headers.update(self._request_data.authorization_headers)

    def prepare_request(
        self,
        endpoint: Endpoint,
        access_role: AccessRole,
        should_test_with_own_organization_of_test_user: bool,
    ):
        # Ensure request data does not contain inject data or auth headers from previous test runs:
        if self._fixture is not None:
            self._request_data = self._fixture.prepare_request(
                self._static_request_data,
                access_role,
                should_test_with_own_organization_of_test_user,
            )
        else:
            self._request_data = self._static_request_data.model_copy()
        self._apply_request_attributes(endpoint)
        self._add_authorization_header(endpoint)

    def _apply_request_attributes(self, endpoint: Endpoint):
        for attr_name in ("body", "headers", "path_parameters", "query"):
            attr_data = getattr(self._request_data, f"{attr_name}", None)
            if attr_data is not None:
                attr_data = deepcopy(attr_data)
                attr_data = self._resolve_lazy_data(attr_data)
                case_attr = getattr(endpoint, attr_name)
                if (
                    not getattr(self, f"_replace_{attr_name}")
                    and isinstance(case_attr, dict)
                    and isinstance(attr_data, dict)
                ):
                    case_attr.update(attr_data)
                else:
                    setattr(endpoint, attr_name, attr_data)

    def _resolve_lazy_data(self, data: Any):
        if isinstance(data, LazyValue):
            data = data.value
        elif isinstance(data, BaseModel):
            data = data.model_dump()
        elif isinstance(data, dict):
            self._resolve_lazy_data_helper(data)
        return data

    def _resolve_lazy_data_helper(self, d: Dict):
        items = list(d.items())
        for key, value in items:
            if isinstance(value, LazyValue):
                d[key] = value.value
            elif isinstance(value, dict):
                self._resolve_lazy_data_helper(value)

    def cleanup_after_request(
        self, access_role: AccessRole, response: requests.Response
    ):
        if self._fixture is not None:
            self._fixture.cleanup_after_request(
                self._request_data, access_role, response
            )

    def validate_response(self, access_role: AccessRole, response: requests.Response):
        if self._fixture is not None:
            self._fixture.validate_response(self._request_data, access_role, response)
