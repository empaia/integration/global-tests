from typing import Any, Dict, Optional, Union

from pydantic import BaseModel
from pydantic_settings import SettingsConfigDict

from ..lazy_values import LazyGetter


class RequestDataConfig:
    arbitrary_types_allowed = True


class RequestData(BaseModel):
    authorization_headers: Optional[Dict] = None
    body: Optional[Union[Any, Dict, LazyGetter]] = None
    headers: Optional[Union[Dict, LazyGetter]] = None
    path_parameters: Optional[Union[Dict, LazyGetter]] = None
    query: Optional[Union[Dict, LazyGetter]] = None

    model_config = SettingsConfigDict(arbitrary_types_allowed=True)


RequestDataType = Dict[str, Dict]
