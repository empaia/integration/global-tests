from typing import Any, Type, Union

from ...auth_service.access_role import AccessRole


class OrganizationMemberStatusCode:
    def __init__(self, foreign: int, own: int):
        self._foreign = foreign
        self._own = own

    def get_value(self, use_own: bool) -> int:
        return self._own if use_own else self._foreign


class ExpectedResponse:
    def __init__(
        self,
        status_code: Union[int, OrganizationMemberStatusCode],
        access_role: AccessRole,
    ):
        self._access_role = access_role
        self._status_code = status_code
        self._ignore_skip = False
        self._expected_payload = None
        self._expected_payload_type = None

    def clone(self) -> "ExpectedResponse":
        clone = ExpectedResponse(self._status_code, self._access_role)
        clone.set_expected_payload_type(self._expected_payload)
        clone.set_expected_payload_type(self._expected_payload_type)
        if self._ignore_skip:
            clone.set_ignore_skip()
        return clone

    @property
    def access_role(self) -> AccessRole:
        return self._access_role

    def get_status_code(self, is_user_of_own_organization: bool) -> int:
        status_code = self._status_code
        if isinstance(self._status_code, OrganizationMemberStatusCode):
            status_code = status_code.get_value(is_user_of_own_organization)
        return status_code

    def has_expected_status_code_for_own_organization(self):
        return isinstance(self._status_code, OrganizationMemberStatusCode)

    def is_access_allowed(self, is_user_of_own_organization: bool) -> bool:
        return self.get_status_code(is_user_of_own_organization) < 400

    @property
    def ignore_skip(self) -> bool:
        return self._ignore_skip

    @property
    def expected_payload(self) -> Type:
        return self._expected_payload

    @property
    def expected_payload_type(self) -> Type:
        return self._expected_payload_type

    def set_ignore_skip(self) -> "ExpectedResponse":
        self._ignore_skip = True
        return self

    def set_expected_payload_type(
        self, expected_payload_type: Type
    ) -> "ExpectedResponse":
        self._expected_payload_type = expected_payload_type
        return self

    def set_expected_payload(self, expected_payload: Any) -> "ExpectedResponse":
        self._expected_payload = expected_payload
        return self.set_expected_payload_type(type(expected_payload))
