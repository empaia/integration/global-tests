import json
from typing import Any, Dict, List, Type

import requests

from .expected_responses import ExpectedResponses


class ResponsePayloadValidator:
    def __init__(
        self,
        response: requests.Response,
        expected_responses: ExpectedResponses,
    ):
        self._response = response
        self._expected_responses = expected_responses
        self._expected_payload_type = (
            self._expected_responses.get_expected_payload_type()
        )
        if self._is_expected_payload_json():
            self._payload = self._load_request_payload()
        else:
            self._payload = response.content

    def _is_expected_payload_json(self):
        return self._expected_payload_type in (dict, Dict, list, List)

    def validate_response_payload(self):
        if self._expected_payload_type is not None:
            self._validate_expected_payload_type(self._expected_payload_type)
        expected_payload = self._expected_responses.get_expected_payload()
        if expected_payload is not None:
            self._validate_expected_payload(expected_payload)

    def _validate_expected_payload_type(self, expected_payload_type: Type):
        if not isinstance(self._payload, expected_payload_type):
            raise AssertionError(
                f"Expected response payload type '{expected_payload_type}', but got '{type(self._payload)}' instead"
            )

    def _validate_expected_payload(self, expected_payload: Any):
        if self._is_expected_payload_json():
            payload = self._payload.copy()
            if "timestamp" in payload:
                # we can't compare the exact timestamp
                del payload["timestamp"]
            payload_str = json.dumps(payload, sort_keys=True, indent=4)
            expected_payload_str = json.dumps(
                expected_payload, sort_keys=True, indent=4
            )
            if payload_str != expected_payload_str:
                raise AssertionError(
                    f"Payload and expected payload differ:\nGot:\n{payload_str}\n\nExpected:\n{expected_payload_str}\n"
                )
        else:
            if self._response.content != expected_payload:
                raise AssertionError(
                    f"Expected response payload '{expected_payload}', but got '{self._response.content}' instead"
                )

    def _load_request_payload(self) -> dict:
        payload = self._response.content
        if self._is_expected_payload_json():
            try:
                payload = json.loads(payload)
            except json.JSONDecodeError:
                pass
        return payload
