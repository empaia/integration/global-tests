from typing import Any, Callable, List, Optional, Type

from ...auth_service.access_role import AccessRole
from .expected_response import ExpectedResponse


class ExpectedResponses:
    """
    This class manages the expected responses for a single ConfiguredRequest and AccessRole. There can be
    multiple ExpectedResponses, because besides the given access_role, the AccessRole.ALL and
    AccessRole.ALL_REGISTERED_USERS may apply.
    """

    def __init__(
        self, access_role: AccessRole, expected_responses: List[ExpectedResponse]
    ):
        self._access_role = access_role
        self._expected_responses = expected_responses

    @property
    def access_role(self) -> AccessRole:
        return self._access_role

    def _get_expected_response_property(
        self,
        get_fn: Callable[[ExpectedResponse], "Any"],
        default_value: Optional[Any] = None,
        skip_value: Optional[Any] = None,
    ) -> Any:
        result = default_value
        for er in self._expected_responses:
            value = get_fn(er)
            if value is not skip_value:
                result = value
                break
        return result

    def is_access_allowed(self, is_user_of_own_organization: bool) -> bool:
        return self._get_expected_response_property(
            lambda er: er.is_access_allowed(is_user_of_own_organization),
            default_value=False,
        )

    def should_ignore_skip(self) -> bool:
        return self._get_expected_response_property(
            lambda er: er.ignore_skip, default_value=False, skip_value=False
        )

    def has_expected_status_code_for_own_organization(self) -> bool:
        return self._get_expected_response_property(
            lambda er: er.has_expected_status_code_for_own_organization(),
            default_value=False,
        )

    def get_expected_status_code(
        self, is_user_of_own_organization: bool, default_status_code: int
    ) -> int:
        return self._get_expected_response_property(
            lambda er: er.get_status_code(is_user_of_own_organization),
            default_value=default_status_code,
        )

    def get_expected_payload_type(self) -> Optional[Type]:
        return self._get_expected_response_property(lambda er: er.expected_payload_type)

    def get_expected_payload(self) -> Any:
        # special case: if payload type is set, but payload is None, then we still want to return None
        # to skip the payload validation, even if another ExpectedResponse of a different access role has defined an
        # expected_payload
        def get_fn(er: ExpectedResponse):
            if er.expected_payload_type is not None:
                return er.expected_payload
            else:
                return None

        return self._get_expected_response_property(
            get_fn, default_value=None, skip_value=None
        )  # lambda er: er.expected_payload)

    def get_expected_response(self, access_role: AccessRole) -> ExpectedResponse:
        result = None
        for er in self._expected_responses:
            if er.access_role == access_role:
                result = er
                break
        if result is None:
            raise AssertionError(
                f"No response expected for access role '{access_role}'"
            )
        return result
