import sys
import traceback
from typing import Any, List, Optional

import requests

from ...auth_service.access_role import AccessRole
from ...auth_service.utils_auth_service import (
    generic_delete,
    generic_get,
    generic_post,
    generic_put,
)
from .request_data import RequestData


class AbstractRequestDataInjector:
    def inject(self, request_data: RequestData, fixture: "RequestFixture"):
        raise NotImplementedError


class RequestFixture:
    def __init__(
        self, request_data_injectors: Optional[List[AbstractRequestDataInjector]] = None
    ):
        self._request_data_injectors = request_data_injectors
        self._data_created = False
        self._should_test_with_own_organization_of_test_user = False

    @property
    def should_test_with_own_organization_of_test_user(self) -> bool:
        return self._should_test_with_own_organization_of_test_user

    @staticmethod
    def delete(
        url: str,
        add_headers: dict = None,
        params: dict = None,
        headers: dict = None,
        data: dict = None,
    ):
        return generic_delete(url, add_headers, params, headers, data)

    @staticmethod
    def get(
        url: str, add_headers: dict = None, params: dict = None, headers: dict = None
    ):
        return generic_get(url, add_headers, params, headers)

    @staticmethod
    def post(
        url: str,
        data: dict = None,
        add_headers: dict = None,
        params: dict = None,
        headers: dict = None,
        files=None,
        raw_data: bytes = None,
    ):
        return generic_post(url, data, add_headers, params, headers, files, raw_data)

    @staticmethod
    def put(
        url: str, add_headers: dict = None, params: dict = None, headers: dict = None
    ):
        return generic_put(url, add_headers, params, headers)

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> Any:
        pass

    def prepare_request(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        should_test_with_own_organization_of_test_user: bool,
    ):
        self._should_test_with_own_organization_of_test_user = (
            should_test_with_own_organization_of_test_user
        )
        if not self._data_created:
            try:
                self.create_request_data(request_data, access_role)
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                raise e
            finally:
                self._data_created = True

        request_data_with_injections = request_data.model_copy()
        if self._request_data_injectors is not None:
            for request_data_injector in self._request_data_injectors:
                request_data_injector.inject(request_data_with_injections, self)
        return request_data_with_injections

    def cleanup_after_request(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        try:
            self.cleanup_request_data(request_data, access_role, response)
        finally:
            self._data_created = False

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        pass

    def validate_response(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        pass
