import json
import sys
from copy import deepcopy
from pprint import pprint
from typing import Any, Dict, Optional, Tuple, Type, Union

import requests
from requests.status_codes import codes as http_codes

from ...api_test.api_results_manager import APIResultState
from ...api_test.api_test_user.api_test_user import APITestUser
from ...auth_service.access_role import AccessRole
from ...auth_service.api_controllers.v2.entity_models import (
    OrganizationUserRole,
    UserProfileEntity,
)
from ...auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from ...singletons import (
    api_test_state,
    general_settings,
    login_manager,
)
from .endpoint import Endpoint
from .expected_responses import ExpectedResponses
from .request_curator import RequestCurator
from .request_fixture import RequestFixture
from .response_payload import RequestPayloads, ResponsePayloads
from .response_payload_validator import ResponsePayloadValidator
from .response_validator import ResponseValidator


class EndpointTest:
    def __init__(self, method: str, path: str, controller_name: str):
        super().__init__()
        self._method = method
        self._path = path
        self._expected_payloads: Optional[ResponsePayloads] = None
        self._request_curators: Optional[Dict[str, RequestCurator]] = None
        self._response_validator = ResponseValidator()
        self._controller_name = controller_name
        self._debug_response = False
        self._is_v2_api = False
        self._ignore_endpoint = False

    def __eq__(self, other: "EndpointTest"):
        return self.method == other.method and self.path == other.path

    @property
    def is_v2_api(self) -> bool:
        return self._is_v2_api

    def set_is_v2_api(self, is_v2_api: bool):
        self._is_v2_api = is_v2_api

    def set_debug_response(self, debug_response: bool):
        self._debug_response = debug_response

    @property
    def method(self):
        return self._method

    @property
    def path(self):
        return self._path

    def set_replace_body(self, request_type: str, flag: bool):
        self._request_curators[request_type].set_replace_body(flag)

    def setup_request_curators(
        self, payloads: Optional[RequestPayloads], fixture: Optional[RequestFixture]
    ):
        self._request_curators = {
            "valid": RequestCurator(
                None if payloads is None else payloads.valid, fixture
            )
        }

    def set_expected_payload(
        self, access_role: AccessRole, expected_payload: Any
    ) -> "EndpointTest":
        self._response_validator.set_expected_payload(access_role, expected_payload)
        return self

    def set_expected_payload_type(
        self, access_role: AccessRole, expected_payload_type: Type
    ) -> "EndpointTest":
        self._response_validator.set_expected_payload_type(
            access_role, expected_payload_type
        )
        return self

    def set_expected_payloads(self, expected_payloads: ResponsePayloads):
        self._expected_payloads = expected_payloads
        # TODO: this needs to be changed in the base class to support valid, invalid and read_only responses!
        if expected_payloads.valid is not None:
            for access_role, payload in expected_payloads.valid.items():
                self.set_expected_payload(access_role, payload)

    def set_ignore_skip(self, access_role: AccessRole) -> "EndpointTest":
        self._response_validator.set_ignore_skip(access_role)
        return self

    def set_ignore_endpoint(self) -> "EndpointTest":
        self._ignore_endpoint = True
        return self

    def set_response_for_access_role(
        self, access_role: AccessRole, status_code: int
    ) -> "EndpointTest":
        self._response_validator.set_response_for_access_role(access_role, status_code)
        return self

    def prepare_request(
        self,
        endpoint: Endpoint,
        access_role: AccessRole,
        should_test_with_own_organization_of_test_user: bool,
    ):
        # TODO: 'valid' is for valid requests, 'invalid' and 'read_only' will be future requests
        curator = self._request_curators["valid"]
        curator.prepare_request(
            endpoint, access_role, should_test_with_own_organization_of_test_user
        )

    @staticmethod
    def get_headers_with_trimmed_auth_token(headers: Optional[Dict]):
        if headers is not None:
            authorization = headers.get("Authorization")
            if authorization is not None:
                authorization_list = authorization.split(" ")
                if len(authorization_list) == 2:
                    scheme, token = authorization_list
                    if len(token) > 50:
                        headers = deepcopy(headers)
                        trimmed_token = f"{token[:25]} ... {token[-25:]}"
                        headers["Authorization"] = f"{scheme} {trimmed_token}"
        return headers

    def _assert_correct_auth_token_was_used(
        self, endpoint: Endpoint, access_role: AccessRole
    ):
        authorization = endpoint.headers["Authorization"]
        token = authorization.split(" ")[1]
        test_user: APITestUser = api_test_state.get_test_user()
        expected_authorization = test_user.get_authorization_headers()["Authorization"]
        if authorization != expected_authorization:
            expired_authorization = None
            expired_authorization_headers = (
                test_user.get_previously_expired_authorization_headers()
            )
            if expired_authorization_headers is not None:
                expired_authorization = expired_authorization_headers["Authorization"]
            if expired_authorization is None or authorization != expired_authorization:
                assert authorization == expected_authorization
        token_data = login_manager.introspect_token(token)

        try:
            if test_user.use_v2_api():
                self._assert_auth_token_data_v2_api(test_user, token_data, access_role)
            else:
                raise AssertionError("V1 API exists no more")
        except AssertionError as e:
            print(f"Assertion failed, token data was: {token_data}")
            raise e

    def _assert_auth_token_data_v2_api(
        self, test_user: APITestUser, token_data: Dict, access_role: AccessRole
    ):
        assert access_role in AccessRole.get_v2_api_access_roles()
        assert token_data["username"] == test_user.get_authorization_data()["user"]
        user = test_user.get_user()
        assert token_data["email"] == user.contact_data.email_address

        membership_data = None
        organization_user_roles = None
        if test_user.get_organization() is not None:
            membership_data = token_data["organizations"].get(
                test_user.get_organization().organization_id
            )
            if membership_data is None:
                raise AssertionError(
                    "Token does not contain the users organization id "
                    f"'{test_user.get_organization().organization_id}' in the 'organizations' header:\n{token_data}"
                )
            organization_user_roles = membership_data.get("roles")
            if organization_user_roles is not None:
                organization_user_roles = [
                    OrganizationUserRole.from_token_value(r)
                    for r in organization_user_roles
                ]

        # Users with certain roles should be organization members:
        if access_role not in (
            AccessRole.ADMIN,
            AccessRole.MODERATOR,
            AccessRole.EMPAIA_INTERNATIONAL_ASSOCIATE,
            AccessRole.REGISTERED_USER,
        ):
            assert membership_data is not None

        auth_service_client_roles = (
            token_data["resource_access"].get("auth_service_client", {}).get("roles")
        )

        if access_role == AccessRole.ADMIN:
            assert "admin" in auth_service_client_roles
            self._assert_registered_user_membership(token_data)
        elif access_role == AccessRole.MODERATOR:
            assert "moderator" in auth_service_client_roles
            self._assert_registered_user_membership(token_data)
        elif access_role == AccessRole.EMPAIA_INTERNATIONAL_ASSOCIATE:
            assert "empaia_international_associate" in auth_service_client_roles
            self._assert_registered_user_membership(token_data)
        elif access_role == AccessRole.APP_MAINTAINER:
            assert OrganizationUserRole.APP_MAINTAINER in organization_user_roles
        elif access_role == AccessRole.DATA_MANAGER:
            assert OrganizationUserRole.DATA_MANAGER in organization_user_roles
        elif access_role == AccessRole.ORGANIZATION_MANAGER:
            assert OrganizationUserRole.MANAGER in organization_user_roles
        elif access_role == AccessRole.ORGANIZATION_MEMBER:
            assert len(organization_user_roles) == 0
        elif access_role == AccessRole.PATHOLOGIST:
            assert OrganizationUserRole.PATHOLOGIST in organization_user_roles
        else:
            assert access_role == AccessRole.REGISTERED_USER
            self._assert_registered_user_membership(token_data)

    @staticmethod
    def _assert_registered_user_membership(token_data: Dict):
        # some tests create organizations and the registered user will be the MANAGER:
        if len(token_data["organizations"]) == 1:
            assert (
                OrganizationUserRole.MANAGER.value
                in list(token_data["organizations"].values())[0]["roles"]
            )
        else:
            assert len(token_data["organizations"]) == 0

    def validate_request(
        self,
        endpoint: Endpoint,
        access_role: AccessRole,
        response: requests.Response,
        expected_responses: Optional[ExpectedResponses],
    ) -> Tuple[APIResultState, Union[int, str]]:
        if self._debug_response:
            print(f"Response: [{response.status_code}]: {response.content}")
        expected_status_code = self._get_expected_status_code(
            endpoint, access_role, expected_responses
        )
        api_result_state = self._get_initial_api_result_state(expected_responses)
        if api_result_state != APIResultState.SKIPPED:
            if expected_status_code != response.status_code:
                self._raise_unexpected_status_code_error(expected_status_code, response)
            else:
                if expected_responses is not None:
                    ResponsePayloadValidator(
                        response, expected_responses
                    ).validate_response_payload()
                self._validate_authorization(
                    endpoint, access_role, response, expected_responses
                )
        else:
            expected_status_code = ""
        if api_result_state == APIResultState.OK:
            if self._request_curators is not None:
                # TODO: 'valid' is for valid requests, 'invalid' and 'read_only' will be future requests
                curator = self._request_curators.get("valid")
                if curator is not None:
                    curator.validate_response(access_role, response)
        return api_result_state, expected_status_code

    def cleanup_after_request(
        self, access_role: AccessRole, response: requests.Response
    ):
        if self._request_curators is not None:
            # TODO: 'valid' is for valid requests, 'invalid' and 'read_only' will be future requests
            curator = self._request_curators.get("valid")
            if curator is not None:
                curator.cleanup_after_request(access_role, response)

    @staticmethod
    def _raise_unexpected_status_code_error(
        expected_status_code: int, response: requests.Response
    ):
        raise AssertionError(
            f"Received a response with status code '{response.status_code}', "
            f"but expected '{expected_status_code}'.\nContent:\n{response.content}"
        )

    @staticmethod
    def _get_initial_api_result_state(
        expected_responses: Optional[ExpectedResponses],
    ) -> APIResultState:
        state = APIResultState.OK
        if expected_responses is not None and expected_responses.should_ignore_skip():
            state = APIResultState.SKIPPED
        return state

    @staticmethod
    def _matches_endpoint(path, endpoint) -> bool:
        return path.startswith(f"{endpoint}/") or path == endpoint

    def _is_public_endpoint(self, path: str) -> bool:
        if self._is_v2_api:
            public_endpoints = ("/api/v2/public",)
        else:
            public_endpoints = ("/api/users", "/api/organization")
        result = False
        for public_endpoint in public_endpoints:
            if self._matches_endpoint(path, public_endpoint):
                result = True
                break
        return result

    def _get_expected_status_code(
        self,
        endpoint: Endpoint,
        access_role: AccessRole,
        expected_responses: Optional[ExpectedResponses],
    ) -> int:
        if access_role == AccessRole.ANONYMOUS:
            status_code = (
                http_codes.FORBIDDEN
                if self._is_public_endpoint(endpoint.path)
                else http_codes.UNAUTHORIZED  # pylint: disable=no-member
            )
        else:
            status_code = http_codes.FORBIDDEN  # pylint: disable=no-member
        if expected_responses is not None:
            is_user_of_own_organization = (
                api_test_state.get_test_user().should_test_with_own_organization()
            )
            status_code = expected_responses.get_expected_status_code(
                is_user_of_own_organization, status_code
            )
        return status_code

    def _validate_authorization(
        self,
        endpoint: Endpoint,
        access_role: AccessRole,
        response: requests.Response,
        expected_responses: ExpectedResponses,
    ):
        if response.status_code < http_codes.BAD_REQUEST:  # pylint: disable=no-member
            is_user_of_own_organization = (
                api_test_state.get_test_user().should_test_with_own_organization()
            )
            if not expected_responses.is_access_allowed(is_user_of_own_organization):
                allowed_access_roles = (
                    self._response_validator.get_allowed_access_roles(
                        is_user_of_own_organization
                    )
                )
                allowed_access_roles_str = "', '".join(
                    [r.value for r in allowed_access_roles]
                )
                raise AssertionError(
                    f"{endpoint.method.upper()} {endpoint.path} must not grant access to role "
                    f"'{access_role}', only the role(s) '{allowed_access_roles_str}' should be allowed"
                )

    def run_test(self, access_role):
        if self._ignore_endpoint:
            return
        api_result_state = APIResultState.ERROR
        response = None
        expected_status_code = ""
        endpoint = Endpoint(
            general_settings.auth_service_host, self._method, self._path
        )
        endpoint.set_is_v2_api(self._is_v2_api)
        try:
            expected_responses = self._response_validator.get_expected_responses_for_applying_access_roles(
                access_role
            )
            self.prepare_request(
                endpoint,
                access_role,
                api_test_state.get_test_user().should_test_with_own_organization(),
            )
            response = endpoint.send_request()
            try:
                self._assert_no_auth_header_for_anonymous_user(endpoint, access_role)
                api_result_state, expected_status_code = self.validate_request(
                    endpoint, access_role, response, expected_responses
                )
            finally:
                self.cleanup_after_request(access_role, response)
            if access_role != AccessRole.ANONYMOUS:
                # ensure that the correct auth token has been used for this access role
                self._assert_correct_auth_token_was_used(endpoint, access_role)
        finally:
            if api_result_state == APIResultState.ERROR:
                self.log_status_on_error(endpoint, response)
            api_test_state.get_api_results_manager().add_api_result(
                response,
                endpoint,
                api_result_state,
                expected_status_code,
                self._controller_name,
            )

    @staticmethod
    def _assert_no_auth_header_for_anonymous_user(endpoint: Endpoint, access_role):
        if access_role == AccessRole.ANONYMOUS:
            if endpoint.headers is not None and "Authorization" in endpoint.headers:
                raise AssertionError(
                    f"Current access role is {access_role.value}. "
                    f'The request should not contain an "Authorization" header'
                )

    def log_status_on_error(
        self, endpoint: Endpoint, response: Optional[requests.Response]
    ):
        user = None
        auth_headers = None
        access_role = api_test_state.get_current_access_role()
        if access_role is not AccessRole.ANONYMOUS:
            auth_headers = api_test_state.get_authorization_headers()
            if access_role is not AccessRole.SUPER_ADMIN:
                user = MyController().get_profile(auth_headers)
        debug_response = False
        self._log_user_v2_api_on_error(user, access_role, auth_headers, debug_response)
        self._log_request_on_error(endpoint)
        if response is not None:
            self._log_response_on_error(response, debug_response)

    @staticmethod
    def _log(msg, *args, add_prefix=True, **kwargs):
        if add_prefix:
            msg = f">>>>>> {msg}"
        print(msg, *args, **kwargs, file=sys.stderr)

    def _log_auth_header(self, auth_headers: Optional[Dict] = None):
        if auth_headers is not None:
            auth_token = auth_headers["Authorization"].split(" ")[1]
            self._log(
                f"auth_headers: {self.get_headers_with_trimmed_auth_token(auth_headers)}"
            )
            self._log(
                f"auth_token: {json.dumps(login_manager.introspect_token(auth_token), sort_keys=True)}"
            )

    def _log_user_v2_api_on_error(
        self,
        user: UserProfileEntity,
        access_role: AccessRole,
        auth_headers: Dict,
        debug_response: bool,
    ):
        if user is None:
            self._log(f"Logged in user [{access_role.value}]")
            self._log_auth_header(auth_headers)
        else:
            self._log(f"Logged in user [{access_role.value}]:")
            if debug_response:
                pprint(user)
            else:
                self._log_auth_header(auth_headers)
                self._log(f"   user id: {user.user_id}")
                self._log(f"   username: {user.username}")
                self._log(f"   email address: {user.contact_data.email_address}")
                self._log(
                    f"   user roles: {[user_role.value for user_role in user.user_roles]}"
                )
                self._log(f"   account state: {user.account_state.value}")
                self._log(
                    f"  (more error details are hidden, to see them set 'debug_response = True' in {__file__})"
                )

    def _log_request_on_error(self, endpoint: Endpoint):
        self._log("Request:")
        self._log(f"   [{endpoint.method.upper()}] {endpoint.path}")
        if endpoint.body is not None:
            self._log(f"   body: {endpoint.body}")
        if endpoint.headers is not None:
            self._log(
                f"   headers: {self.get_headers_with_trimmed_auth_token(endpoint.headers)}"
            )
        if endpoint.path_parameters is not None:
            self._log(f"   path_parameters: {endpoint.path_parameters}")
        if endpoint.query is not None:
            self._log(f"   query: {endpoint.query}")

    def _log_response_on_error(self, response: requests.Response, debug_response: bool):
        short_response = (
            type(response.content) in (str, bytes) and len(response.content) < 300
        )
        try:
            response_content = json.dumps(response.json(), sort_keys=True)
        except json.JSONDecodeError:
            response_content = response.content
        if short_response:
            self._log("Response:")
            self._log(f"   [{response.status_code}] {response_content}")
        else:
            if debug_response:
                self._log(f"Response: [{response.status_code}]")
                self._log(response_content, add_prefix=False)
            else:
                self._log("Response:")
                self._log(
                    f"    [{response.status_code}] <response content hidden, to see it "
                    f"set 'debug_response = True' in {__file__}>"
                )
