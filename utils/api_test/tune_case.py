import hypothesis.strategies
import schemathesis.hooks
import schemathesis.models


def make_tune_case(
    _: schemathesis.hooks.HookContext, strategy: hypothesis.strategies.SearchStrategy
):
    def tune_case(case: schemathesis.models.Endpoint):
        return case

    return strategy.map(tune_case)
