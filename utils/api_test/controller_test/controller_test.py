from collections import defaultdict

# from typing import bytearray, Dict, Generator, Optional, Set, Union
from typing import Dict, Generator, Optional, Set, Union

from ...api_test.endpoint_test.endpoint_test import EndpointTest
from ...api_test.endpoint_test.expected_response import (
    OrganizationMemberStatusCode,
)
from ...api_test.endpoint_test.request_fixture import RequestFixture
from ...api_test.endpoint_test.response_payload import (
    RequestPayloads,
    RequestPayloadsType,
    ResponsePayloads,
    ResponsePayloadsType,
)
from ...api_test.lazy_values import LazyGetter, LazyValue
from ...auth_service.access_role import AccessRole

AccessRoleResponses = Dict[AccessRole, Union[int, OrganizationMemberStatusCode]]


class ControllerTest:
    def __init__(self, base_path: str, allowed_access_roles: Set[AccessRole] = None):
        self._base_path = base_path
        self._endpoints_by_path_by_method = defaultdict(dict)
        self._allowed_access_roles = allowed_access_roles
        if allowed_access_roles is None:
            self._allowed_access_roles = AccessRole.get_v1_api_access_roles()
            self._allowed_access_roles.add(AccessRole.ALL)
            self._allowed_access_roles.add(AccessRole.ALL_REGISTERED_USERS)
        self.setup_endpoints()

    def setup_endpoints(self):
        self._setup_delete_endpoints()
        self._setup_get_endpoints()
        self._setup_post_endpoints()
        self._setup_put_endpoints()

    def delete(
        self,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.add_endpoint(
            "delete",
            path,
            responses,
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def get(
        self,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.add_endpoint(
            "get",
            path,
            responses,
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def post(
        self,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.add_endpoint(
            "post",
            path,
            responses,
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def put(
        self,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.add_endpoint(
            "put",
            path,
            responses,
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def _resolve_path(self, path: Optional[str]) -> str:
        resolved_path = self._base_path
        if path is not None:
            resolved_path += path
        return resolved_path

    def _add_new_endpoint_test(self, method: str, path: Optional[str]) -> EndpointTest:
        endpoint_tests_by_path = self._endpoints_by_path_by_method[method]
        resolved_path = self._resolve_path(path)
        if resolved_path in endpoint_tests_by_path:
            raise AssertionError(
                f"Another endpoint test was already added for [{method.upper()}] {resolved_path}"
            )
        controller_name = self.__class__.__name__
        if controller_name.endswith("Test"):
            controller_name = controller_name[: -len("Test")]
        endpoint_test = EndpointTest(method, resolved_path, controller_name)
        endpoint_tests_by_path[resolved_path] = endpoint_test
        return endpoint_test

    @staticmethod
    def _merge_payload(
        body: Optional[Dict] = None,
        headers: Optional[Dict] = None,
        path_parameters: Optional[Dict] = None,
        query: Optional[Dict] = None,
        payloads: Optional[RequestPayloadsType] = None,
    ) -> RequestPayloads:
        if any((body, headers, path_parameters, query)):
            valid_request_data = {
                "body": body,
                "headers": headers,
                "path_parameters": path_parameters,
                "query": query,
            }
            if payloads is None:
                payloads = {}
            else:
                # explicit payload attributes and valid payloads data is contradictory:
                assert payloads.get("valid") is None
            payloads["valid"] = valid_request_data
        if payloads is not None and not isinstance(payloads, LazyValue):
            payloads = RequestPayloads.model_validate(payloads)
        return payloads

    @staticmethod
    def _configure_new_endpoint_test(
        endpoint_test: EndpointTest,
        responses: Optional[AccessRoleResponses],
        payloads: Optional[RequestPayloads],
        expected_payloads: Optional[ResponsePayloadsType],
        fixture: Optional[RequestFixture],
    ):
        if responses is None:
            assert payloads is None
            assert fixture is None
        else:
            for access_role, status_code in responses.items():
                endpoint_test.set_response_for_access_role(access_role, status_code)
            endpoint_test.setup_request_curators(payloads, fixture)
        if expected_payloads is not None:
            expected_payloads = ResponsePayloads.model_validate(expected_payloads)
            endpoint_test.set_expected_payloads(expected_payloads)

    def add_endpoint(
        self,
        method: str,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        if responses is not None:
            access_roles = set(responses.keys())
            forbidden_access_roles = access_roles.difference(self._allowed_access_roles)
            if len(forbidden_access_roles) > 0:
                raise AssertionError(
                    f"These access roles are not allowed in this API version: {forbidden_access_roles}"
                )
        payloads = self._merge_payload(body, headers, path_parameters, query, payloads)
        endpoint_test = self._add_new_endpoint_test(method, path)
        self._configure_new_endpoint_test(
            endpoint_test, responses, payloads, expected_payloads, fixture
        )
        return endpoint_test

    def get_endpoint_test(self, method: str, path: str) -> Optional[EndpointTest]:
        endpoint_test: Optional[EndpointTest] = None
        if path.startswith(self._base_path):
            endpoints_by_path = self._endpoints_by_path_by_method[method.lower()]
            endpoint_test = endpoints_by_path.get(path)
        return endpoint_test

    def get_endpoint_tests(self) -> Generator[EndpointTest, None, None]:
        for endpoints_by_path in self._endpoints_by_path_by_method.values():
            yield from endpoints_by_path.values()

    def set_debug_responses(self, debug_responses: bool):
        for endpoint_test in self.get_endpoint_tests():
            endpoint_test.set_debug_response(debug_responses)

    def _setup_delete_endpoints(self):
        pass

    def _setup_get_endpoints(self):
        pass

    def _setup_post_endpoints(self):
        pass

    def _setup_put_endpoints(self):
        pass


class PublicEndpointControllerTest(ControllerTest):
    @staticmethod
    def get_all_users_access_role() -> AccessRole:
        return AccessRole.ALL

    @staticmethod
    def get_registered_users_access_role() -> AccessRole:
        return AccessRole.ALL_REGISTERED_USERS

    def super_admin_get(
        self,
        path: str,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.get(
            path,
            {AccessRole.SUPER_ADMIN: 200},
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def registered_users_get(
        self,
        path: str,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.get(
            path,
            {self.get_registered_users_access_role(): 200},
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def public_get(
        self,
        path: str,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        return self.get(
            path,
            {self.get_all_users_access_role(): 200},
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    def get(
        self,
        path: Optional[str],
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Union[bytearray, Dict, LazyGetter]] = None,
        headers: Optional[Union[Dict, LazyGetter]] = None,
        path_parameters: Optional[Union[Dict, LazyGetter]] = None,
        query: Optional[Union[Dict, LazyGetter]] = None,
        payloads: Optional[RequestPayloadsType] = None,
        expected_payloads: Optional[ResponsePayloadsType] = None,
        fixture: Optional[RequestFixture] = None,
    ) -> EndpointTest:
        if path_parameters is None:
            path_parameters = LazyGetter(self.get_path_parameters)
        return super().get(
            path,
            responses,
            body,
            headers,
            path_parameters,
            query,
            payloads,
            expected_payloads,
            fixture,
        )

    @staticmethod
    def get_path_parameters() -> Optional[Dict]:
        return None


class ControllerV2Test(ControllerTest):
    def __init__(self, base_path: str):
        allowed_access_roles = AccessRole.get_v2_api_access_roles()
        allowed_access_roles.add(AccessRole.ALL_V2)
        allowed_access_roles.add(AccessRole.ALL_REGISTERED_USERS_V2)
        super().__init__(base_path, allowed_access_roles)

    def _add_new_endpoint_test(self, method: str, path: Optional[str]) -> EndpointTest:
        endpoint_test = super()._add_new_endpoint_test(method, path)
        endpoint_test.set_is_v2_api(True)
        return endpoint_test


class PublicEndpointControllerV2Test(PublicEndpointControllerTest):
    def __init__(self, base_path: str):
        allowed_access_roles = AccessRole.get_v2_api_access_roles()
        allowed_access_roles.add(AccessRole.ALL_V2)
        allowed_access_roles.add(AccessRole.ALL_REGISTERED_USERS_V2)
        super().__init__(base_path, allowed_access_roles)

    @staticmethod
    def get_all_users_access_role() -> AccessRole:
        return AccessRole.ALL_V2

    @staticmethod
    def get_registered_users_access_role() -> AccessRole:
        return AccessRole.ALL_REGISTERED_USERS_V2

    def _add_new_endpoint_test(self, method: str, path: Optional[str]) -> EndpointTest:
        endpoint_test = super()._add_new_endpoint_test(method, path)
        endpoint_test.set_is_v2_api(True)
        return endpoint_test
