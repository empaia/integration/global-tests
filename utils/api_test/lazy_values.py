from typing import Any, Callable

import requests

from ..singletons import api_test_state, general_settings, login_manager


class LazyValue:
    def __init__(self, default_value: Any = None):
        self._default_value = default_value

    @property
    def value(self):
        v = self._get_value()
        if v is None:
            v = self._default_value
        return v

    def _get_value(self):
        raise NotImplementedError


class LazyGetter(LazyValue):
    def __init__(self, db_data_call: Callable, default_value=None):
        super().__init__(default_value)
        assert callable(db_data_call)
        self._db_data_call = db_data_call

    def _get_value(self):
        return self._db_data_call()


class LazyTestUserEmail(LazyValue):
    def _get_value(self) -> str:
        email = "no-such-user@test.empaia.org"
        if api_test_state.get_test_user().has_db_user():
            email = api_test_state.get_test_user().get_user().email
        return email


class NewUserPassword(LazyValue):
    def __init__(self, new_password: str):
        super().__init__()
        self._new_password = new_password
        self._count = 0

    def _get_value(self):
        # always return a unique password, because reusing an old password is not allowed, which the falsification would
        # do if we do not change the password here:
        self._count += 1
        return self.get_current_value()

    def get_current_value(self):
        return self._new_password + str(self._count)


class UserToken(LazyValue):
    def __init__(self, token_name: str, default_value=None, email: str = None):
        super().__init__(default_value)
        self._token_name = token_name
        self._email = email

    @staticmethod
    def _get_user_tokens_by_email(email: str):
        headers = login_manager.super_admin()
        r = requests.post(
            general_settings.auth_service_host + "api/super_admin/user_tokens",
            params={"emails": [email]},
            headers=headers,
        )
        r.raise_for_status()
        return r.json()

    def _get_user_email(self):
        return self._email

    def _get_user_tokens(self):
        user_tokens = None
        email = self._get_user_email()
        if email is not None:
            user_tokens_by_email = self._get_user_tokens_by_email(email)
            if len(user_tokens_by_email) > 0:
                user_tokens = user_tokens_by_email[0]
        return user_tokens

    def _get_value(self):
        tokens_by_name = self._get_user_tokens()
        return None if tokens_by_name is None else tokens_by_name[self._token_name]
