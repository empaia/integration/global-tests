import traceback
from typing import TYPE_CHECKING, Optional

from ..auth_service.access_role import AccessRole
from .api_results_manager import APIResultsManager

if TYPE_CHECKING:
    from ..settings import ComposeSettings
    from .api_test_user.api_test_user import APITestUser
    from .endpoint_test.endpoint_test import EndpointTest


class APITestState:
    def __init__(self, compose_settings: "ComposeSettings"):
        self._api_results_manager = APIResultsManager(self, compose_settings)
        self._current_access_role = AccessRole.ANONYMOUS
        self._authorization_data = None
        self._current_user_data = None
        self._test_user: Optional["APITestUser"] = None
        self._test_filter = None
        self._last_id = 0
        self._any_error_occurred = False

    def set_any_error_occurred(self):
        self._any_error_occurred = True

    @property
    def did_any_error_occur(self) -> bool:
        return self._any_error_occurred

    def use_v2_api(self) -> bool:
        return self._test_user.use_v2_api()

    def get_next_unique_id(self):
        self._last_id += 1
        return self._last_id

    def set_test_filter(self, test_filter):
        self._test_filter = test_filter

    @property
    def test_filter(self):
        return self._test_filter

    def set_test_user(self, test_user: "APITestUser"):
        self._test_user = test_user

    def get_test_user(self) -> Optional["APITestUser"]:
        return self._test_user

    def get_api_results_manager(self) -> APIResultsManager:
        return self._api_results_manager

    def get_current_access_role(self) -> AccessRole:
        return self._test_user.get_access_role()

    def _setup(
        self,
        access_role: AccessRole,
        test_with_own_organization: bool,
    ):
        self._test_user.setup(access_role, test_with_own_organization)

    @staticmethod
    def should_ignore_endpoint_test(
        endpoint_test: "EndpointTest",
        method_filter: str = None,
        endpoint_filter: str = None,
        use_v2_api: bool = False,
    ) -> bool:
        ignore_test = False
        if use_v2_api is not endpoint_test.is_v2_api:
            ignore_test = True
        if not ignore_test and method_filter is not None:
            if isinstance(method_filter, list):
                ignore_test = endpoint_test.method.lower() not in method_filter
            else:
                ignore_test = endpoint_test.method.lower() != method_filter
        if not ignore_test and endpoint_filter is not None:
            if isinstance(endpoint_filter, list):
                ignore_test = True
                for endpoint in endpoint_filter:
                    if endpoint_test.path.find(endpoint) >= 0:
                        ignore_test = False
            else:
                ignore_test = endpoint_test.path.find(endpoint_filter) < 0
        return ignore_test

    def _tear_down(self):
        self._api_results_manager.write_api_test_results_file(self.use_v2_api())
        self._api_results_manager.save_v2_openapi_definition()
        self._api_results_manager.assert_all_endpoints_have_been_tested(self)
        # Keep the test user if no error occurred, so that it can be inspected in keycloak for debugging:
        if not self._api_results_manager.has_any_error():
            self._test_user.cleanup()

    def get_authorization_headers(self):
        return self._test_user.get_authorization_headers()

    def update_password(self, password: str):
        self._test_user.update_password(password)

    def setup_teardown(
        self,
        access_role: AccessRole,
        test_with_own_organization: bool,
    ):
        has_exception = False
        try:
            self._setup(access_role, test_with_own_organization)
        except Exception as e:
            has_exception = True
            self.set_any_error_occurred()
            raise e
        else:
            yield
        finally:
            try:
                self._tear_down()
            except Exception as e:  # pylint: disable=W0703
                if has_exception:
                    traceback.print_exc()
                else:
                    raise e
