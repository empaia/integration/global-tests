from typing import Dict, Optional, Tuple

from ...auth_service.access_role import AccessRole
from ...auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    UserProfileEntity,
)
from ...singletons import api_test_state, login_manager


class BaseAPITestUserManager:
    def __init__(self):
        self._access_role: Optional[AccessRole] = None
        self._authorization_data: Optional[Dict] = None
        self._last_authorization_headers = None
        self._previously_expired_authorization_headers = None

    def _set_authorization_data(
        self, user: str, password: str, client_id: Optional[str] = None
    ):
        self._authorization_data = {
            "user": user,
            "password": password,
            "client_id": client_id,
        }

    def get_authorization_data(self) -> Optional[Dict]:
        return self._authorization_data

    def get_authorization_headers(self):
        if self._access_role == AccessRole.ANONYMOUS:
            authorization_headers = None
        else:
            authorization_headers = login_manager.user(**self.get_authorization_data())
            if (
                self._last_authorization_headers is not None
                and authorization_headers["Authorization"]
                != self._last_authorization_headers["Authorization"]
            ):
                self._previously_expired_authorization_headers = (
                    self._last_authorization_headers
                )
            self._last_authorization_headers = authorization_headers
        return authorization_headers

    def get_previously_expired_authorization_headers(self):
        return self._previously_expired_authorization_headers

    def get_auth_tokens(self) -> Optional[Tuple[str, Dict]]:
        if self._access_role != AccessRole.ANONYMOUS:
            return login_manager.get_user_tokens(**self.get_authorization_data())

    def cleanup(self):
        self._access_role = None
        if self._authorization_data is not None:
            login_manager.clear_cache_for_user(**self._authorization_data)
            self._authorization_data = None
        self._cleanup_data()

    def update_password(self, password: str):
        login_manager.update_password(
            self._authorization_data["user"],
            self._authorization_data["password"],
            password,
            self._authorization_data["client_id"],
        )
        self._authorization_data["password"] = password

    def _create_username(self, use_v2_api: bool = False) -> str:
        prefix = self._access_role.value.lower()
        # email address length is limited to 50 characters, so reduce the length:
        prefix = prefix.replace("organization", "orga")
        prefix = prefix.replace("registered_user", "reg_user")
        if api_test_state.get_test_user().should_test_with_own_organization():
            prefix += "-own-orga"
        return f"{prefix}{'-v2-api' if use_v2_api else ''}@tests.empaia.org"

    @staticmethod
    def _create_profile_picture_filename() -> str:
        return "sample.jpg"

    def _create_organization_name(self) -> str:
        return f"{self._access_role.value.lower()}-organization"

    def update_after_organization_membership_changed(
        self,
    ) -> Optional[UserProfileEntity]:
        user = self._update_user_after_organization_membership_changed()
        if self._authorization_data is not None:
            login_manager.clear_cache_for_user(**self._authorization_data)
        return user

    def _cleanup_data(self):
        raise NotImplementedError

    def create_user_for_access_role(self, access_role: AccessRole):
        self._access_role = access_role
        if access_role != AccessRole.ANONYMOUS:
            self._create_user()

    def _create_user(self):
        raise NotImplementedError

    def get_user(self) -> Optional[UserProfileEntity]:
        raise NotImplementedError

    def get_organization(self) -> Optional[ConfidentialOrganizationProfileEntity]:
        raise NotImplementedError

    def has_db_user(self) -> bool:
        raise NotImplementedError

    def _update_user_after_organization_membership_changed(
        self,
    ) -> Optional[UserProfileEntity]:
        raise NotImplementedError
