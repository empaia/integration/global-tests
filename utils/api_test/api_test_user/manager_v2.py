from typing import Optional

from ...auth_service.access_role import AccessRole
from ...auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ...auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    OrganizationCategory,
    OrganizationUserRole,
    PostUserRolesEntity,
    UserAccountState,
    UserProfileEntity,
    UserRole,
)
from ...auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from ...auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from ...auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from ...auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from ...auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ...singletons import api_test_state, login_manager
from .base_manager import BaseAPITestUserManager


class APITestUserManagerV2(BaseAPITestUserManager):
    def __init__(self):
        super().__init__()
        self._admin_controller = AdminController()
        self._manager_controller = ManagerController()
        self._moderator_controller = ModeratorController()
        self._my_controller = MyController()
        self._my_unapproved_organizations_controller = (
            MyUnapprovedOrganizationsController()
        )
        self._public_controller = PublicController()
        self._user: Optional[UserProfileEntity] = None
        self._organization: Optional[ConfidentialOrganizationProfileEntity] = None

    def _cleanup_data(self):
        if self._user is not None:
            self._admin_controller.delete_user(self._user.user_id)
            self._user = None
        if self._organization is not None:
            self._admin_controller.delete_organization(
                self._organization.organization_id
            )
            self._organization = None

    def _create_user(self):
        user_data = self._public_controller.create_post_user_entity(
            self._create_username(use_v2_api=True)
        )
        self._public_controller.register_new_user(user_data)
        self._public_controller.verify_registration(
            user_data.contact_data.email_address
        )
        self._set_authorization_data(
            user_data.contact_data.email_address, user_data.password
        )
        auth_headers = api_test_state.get_authorization_headers()
        self._my_controller.set_profile_picture(
            self._create_profile_picture_filename(), auth_headers
        )
        self._user = self._my_controller.get_profile(auth_headers)

        assert (
            self._user.account_state == UserAccountState.ENABLED_AND_NO_ACTIONS_REQUIRED
        )
        if self._access_role == AccessRole.MODERATOR:
            self._moderator_controller.set_user_roles(
                self._user.user_id,
                PostUserRolesEntity(roles=[UserRole.MODERATOR.value]),
            )
        elif self._access_role == AccessRole.ADMIN:
            self._admin_controller.grant_admin_privileges(self._user.user_id)
        if self._access_role in (
            AccessRole.APP_MAINTAINER,
            AccessRole.DATA_MANAGER,
            AccessRole.ORGANIZATION_MANAGER,
            AccessRole.ORGANIZATION_MEMBER,
            AccessRole.PATHOLOGIST,
        ):
            self._create_organization()
            if self._access_role != AccessRole.ORGANIZATION_MANAGER:
                self._join_organization()
        # Clear the cache to get an auth token with the correct user roles:
        login_manager.clear_cache_for_user(self._user.username)

    def _create_organization(self):
        email_address = f"{self._user.username.split('@')[0]}-org@test.empaia.org"
        # If the api test user should not be the organization manager, then the auth_headers are None
        # and the organization will be created by the admin account, which will be the organization manager:
        auth_headers = None
        if self._access_role == AccessRole.ORGANIZATION_MANAGER:
            auth_headers = self.get_authorization_headers()
        organization = (
            self._my_unapproved_organizations_controller.create_new_organization(
                email_address,
                auth_headers,
                organization_categories={
                    OrganizationCategory.APP_CUSTOMER,
                    OrganizationCategory.APP_VENDOR,
                },
            )
        )
        self._my_unapproved_organizations_controller.request_organization_approval(
            organization.organization_id, auth_headers
        )
        self._moderator_controller.accept_organization(organization.organization_id)
        # If the organization manager is the admin user, then clear the admin user token cache:
        if self._access_role != AccessRole.ORGANIZATION_MANAGER:
            login_manager.clear_cache_for_super_admin()
        self._organization = organization

    def _join_organization(self):
        organization_id = self._organization.organization_id
        auth_headers = self.get_authorization_headers()
        self._my_controller.request_membership(organization_id, auth_headers)
        membership_request = self._my_controller.get_membership_request(
            organization_id, auth_headers
        )
        self._manager_controller.accept_membership_request(
            organization_id, membership_request.membership_request_id
        )
        if self._access_role == AccessRole.APP_MAINTAINER:
            organization_user_roles = [OrganizationUserRole.APP_MAINTAINER]
        elif self._access_role == AccessRole.DATA_MANAGER:
            organization_user_roles = [OrganizationUserRole.DATA_MANAGER]
        elif self._access_role == AccessRole.ORGANIZATION_MEMBER:
            organization_user_roles = []
        elif self._access_role == AccessRole.PATHOLOGIST:
            organization_user_roles = [OrganizationUserRole.PATHOLOGIST]
        else:
            # AccessRole.ORGANIZATION_MANAGER is not handled here, because for that access role the test user is
            # the creator and automatically a member of the organization:
            raise AssertionError(f"Unhandled access role '{self._access_role}'")
        self._manager_controller.set_organization_user_roles(
            organization_id, self._user.user_id, organization_user_roles
        )

    def get_user(self) -> Optional[UserProfileEntity]:
        return self._user

    def get_organization(self) -> Optional[ConfidentialOrganizationProfileEntity]:
        return self._organization

    def has_db_user(self) -> bool:
        return False

    def _update_user_after_organization_membership_changed(
        self,
    ) -> Optional[UserProfileEntity]:
        if self._user is not None:
            self._user = self._my_controller.get_profile(
                self.get_authorization_headers()
            )
        return self._user
