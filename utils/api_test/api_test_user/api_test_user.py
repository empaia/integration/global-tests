from typing import Dict, Optional, Tuple

from ...auth_service.access_role import AccessRole
from ...auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    UserProfileEntity,
)
from .base_manager import BaseAPITestUserManager
from .manager_v2 import APITestUserManagerV2


class APITestUser:
    def __init__(self):
        self._manager: Optional[BaseAPITestUserManager] = None
        self._access_role: Optional[AccessRole] = None
        self._next_password_count = 0
        self._test_with_own_organization = False

    def use_v2_api(self) -> bool:
        return isinstance(self._manager, APITestUserManagerV2)

    def should_test_with_own_organization(self):
        return self._test_with_own_organization

    def get_password(self) -> Optional[str]:
        password = None
        if self._manager.get_authorization_data() is not None:
            password = self._manager.get_authorization_data()["password"]
        return password

    def get_next_password(self) -> Optional[str]:
        next_password = None
        if self._manager.get_authorization_data() is not None:
            self._next_password_count += 1
            next_password = f"{self._manager.get_authorization_data()['password']}_{self._next_password_count}"
        return next_password

    def get_authorization_data(self) -> Optional[Dict]:
        return self._manager.get_authorization_data()

    def get_previously_expired_authorization_headers(self):
        return self._manager.get_previously_expired_authorization_headers()

    def get_auth_tokens(self) -> Optional[Tuple[str, Dict]]:
        return self._manager.get_auth_tokens()

    def get_authorization_headers(self) -> Dict:
        return self._manager.get_authorization_headers()

    def get_access_role(self) -> Optional[AccessRole]:
        return self._access_role

    def get_user(self) -> Optional[UserProfileEntity]:
        return self._manager.get_user()

    def get_organization(self) -> Optional[ConfidentialOrganizationProfileEntity]:
        return self._manager.get_organization()

    def has_db_user(self) -> bool:
        return self._manager.has_db_user()

    def cleanup(self):
        if self._manager is not None:
            self._manager.cleanup()

    def setup(self, access_role: AccessRole, test_with_own_organization: bool):
        self.cleanup()
        self._manager = APITestUserManagerV2()
        self._access_role = access_role
        self._test_with_own_organization = test_with_own_organization
        self._manager.create_user_for_access_role(access_role)

    def update_password(self, password: str):
        self._manager.update_password(password)

    def update_after_organization_membership_changed(
        self,
    ) -> Optional[UserProfileEntity]:
        return self._manager.update_after_organization_membership_changed()
