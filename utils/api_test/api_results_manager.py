import collections
import copy
import hashlib
import json
import os
from collections import OrderedDict, defaultdict
from enum import Enum
from typing import (
    TYPE_CHECKING,
    DefaultDict,
    Dict,
    Generator,
    List,
    Optional,
    Tuple,
    Union,
)

import requests

from ..auth_service.access_role import AccessRole

if TYPE_CHECKING:
    from ..api_test.api_test_state import APITestState
    from ..settings import ComposeSettings
    from .endpoint_test.endpoint_test import EndpointTest


class APIResultState(Enum):
    ERROR = "ERROR"
    OK = "OK"
    SKIPPED = "SKIPPED"


APIResultData = Dict[str, str]


class APIResult:
    def __init__(
        self,
        status_code: Optional[Union[int, str]],
        path: str,
        method: str,
        state: APIResultState,
        expected_status: Union[int, str],
        access_role: AccessRole,
        controller_name: str,
    ):
        self.access_role = access_role
        self.method = method.upper()
        self.path = path
        self.status_code = "" if status_code is None else str(status_code)
        self.state = state
        self.expected_status = str(expected_status)
        self.controller_name = controller_name

    @property
    def is_v2_api(self):
        return self.path.startswith("/api/v2")

    def get_hashable_data(self) -> str:
        hashable_data = f"{self.state}"
        if self.state != APIResultState.SKIPPED:
            hashable_data += f"|{self.status_code}({self.expected_status})"
        return hashable_data

    def __eq__(self, other: "APIResult"):
        return (
            self.access_role == other.access_role
            and self.method == other.method
            and self.path == other.path
            and self.status_code == other.status_code
            and self.state == other.state
            and self.expected_status == other.expected_status
        )

    def has_same_status_code(self, other: "APIResult"):
        return (
            self.method == other.method
            and self.path == other.path
            and self.status_code == other.status_code
            and self.state == other.state
        )

    def json(self) -> APIResultData:
        return {
            "access_role": self.access_role.value,
            "method": self.method,
            "path": self.path,
            "status_code": self.status_code,
            "state": self.state.value,
            "expected_status": self.expected_status,
            "controller_name": self.controller_name,
        }

    @classmethod
    def from_json(cls, data: APIResultData) -> "APIResult":
        data["access_role"] = AccessRole(data["access_role"])
        data["state"] = APIResultState(data["state"])
        return APIResult(**data)


APIResultsDict = DefaultDict[
    str, DefaultDict[str, DefaultDict[AccessRole, DefaultDict[bool, APIResult]]]
]
APIResultsDataDict = DefaultDict[
    str, DefaultDict[str, DefaultDict[AccessRole, DefaultDict[bool, Dict[str, str]]]]
]


class APIResults:
    api_results_hash_prefix = "RESULTS HASH: "

    def __init__(
        self,
        api_test_state: "APITestState",
        access_role_order: List[AccessRole],
        cache_file: str,
    ):
        self._api_test_state = api_test_state
        self._api_results_by_method_by_endpoint: APIResultsDict = (
            self._create_api_results_dict()
        )
        self.__api_results_columns = OrderedDict(
            {"Method": len("DELETE"), "Endpoint": 0}
        )
        self._access_role_order = access_role_order
        self._cache_file = cache_file
        self._cached_api_results_by_method_by_endpoint = (
            self._read_api_results_from_cache_file()
        )

    @staticmethod
    def _create_api_results_dict() -> APIResultsDict:
        return defaultdict(lambda: defaultdict(lambda: defaultdict(defaultdict)))

    @staticmethod
    def _create_api_results_data_dict() -> APIResultsDataDict:
        return defaultdict(lambda: defaultdict(lambda: defaultdict(defaultdict)))

    def _read_api_results_from_cache_file(self) -> APIResultsDict:
        cached_api_results_by_method_by_endpoint = self._create_api_results_dict()
        if os.path.exists(self._cache_file):
            with open(self._cache_file, encoding="utf8") as f:
                try:
                    data = json.load(f)
                except Exception as e:  # pylint: disable=W0703
                    print(
                        "Skipping corrupt api results cache file: " + self._cache_file,
                        e,
                    )
                else:
                    self._read_api_results_from_json(
                        data, cached_api_results_by_method_by_endpoint
                    )
        return cached_api_results_by_method_by_endpoint

    def _read_api_results_from_json(
        self, data, cached_api_results_by_method_by_endpoint: APIResultsDict
    ):
        for (
            endpoint,
            method,
            access_role_value,
            is_own_organization_str,
            api_result_data,
        ) in self._iter_api_results(data):
            if is_own_organization_str == "true":
                is_own_organization = True
            elif is_own_organization_str == "false":
                is_own_organization = False
            else:
                raise AssertionError(
                    "is_own_organization dict key should be boolean 'true'/'false', "
                    f"but it is '{is_own_organization_str}'"
                )
            access_role = AccessRole(access_role_value)
            cached_api_results_by_method_by_endpoint[endpoint][method][access_role][
                is_own_organization
            ] = APIResult.from_json(api_result_data)

    @property
    def _api_results_columns(self):
        d = self.__api_results_columns.copy()
        for access_role in self._access_role_order:
            s = self._get_access_role_column_title(access_role)
            d[s] = max(len(":construction: <sup><sub>501</sub></sup>"), len(s))
        return d

    @property
    def api_results_by_method_by_endpoint(self) -> APIResultsDict:
        api_results_by_method_by_endpoint = copy.deepcopy(
            self._api_results_by_method_by_endpoint
        )
        for (
            endpoint,
            method,
            access_role,
            is_own_organization,
            api_result,
        ) in self._iter_api_results(self._cached_api_results_by_method_by_endpoint):
            if (
                is_own_organization
                not in api_results_by_method_by_endpoint[endpoint][method][access_role]
            ):
                api_results_by_method_by_endpoint[endpoint][method][access_role][
                    is_own_organization
                ] = api_result
        return api_results_by_method_by_endpoint

    @staticmethod
    def _iter_api_results(
        api_results_by_method_by_endpoint: APIResultsDict,
    ) -> Generator[
        Tuple[
            str,
            str,
            Union[str, AccessRole],
            Union[str, bool],
            Union[APIResult, APIResultData],
        ],
        None,
        None,
    ]:
        for (
            endpoint,
            api_results_by_method,
        ) in api_results_by_method_by_endpoint.items():
            for method, api_results_by_access_role in api_results_by_method.items():
                for (
                    access_role,
                    api_results_by_own_organization,
                ) in api_results_by_access_role.items():
                    for (
                        is_own_organization,
                        api_result,
                    ) in api_results_by_own_organization.items():
                        yield endpoint, method, access_role, is_own_organization, api_result

    @property
    def api_results_columns(self):
        return self._api_results_columns

    def add_api_result(self, api_result: APIResult):
        test_user = self._api_test_state.get_test_user()
        access_role = test_user.get_access_role()
        is_user_of_own_organization = test_user.should_test_with_own_organization()

        d = self._api_results_by_method_by_endpoint[api_result.path][api_result.method][
            access_role
        ]
        if is_user_of_own_organization in d:
            raise AssertionError(
                f"API result of '{api_result.method.upper()} {api_result.path}' for "
                f"'{access_role.get_extended_name(is_user_of_own_organization)}' is already set"
            )
        d[is_user_of_own_organization] = api_result
        assert self.has_api_result(
            api_result.path, api_result.method, access_role, is_user_of_own_organization
        )

    @staticmethod
    def _get_access_role_column_title(access_role: AccessRole):
        words = access_role.value.split("_")
        words = [w[0] + w[1:].lower() for w in words]
        return " ".join(words)

    def _fill_string(self, s: str, column: str):
        length = self._api_results_columns[column]
        return s.ljust(length)

    @staticmethod
    def _add_color_(text: str, color: str):
        return "$`\\textcolor{%s}{\\text{%s}}`$" % (color, text)

    def _get_hashable_data(
        self, api_results: Dict[AccessRole, Dict[bool, APIResult]]
    ) -> str:
        hashable_data = ""
        for access_role in self._access_role_order:
            hashable_data += f"{access_role.value}:"
            api_results_by_is_own_organization = api_results.get(access_role)
            if api_results_by_is_own_organization is not None:
                for is_user_of_own_organization in (False, True):
                    api_result = api_results_by_is_own_organization.get(
                        is_user_of_own_organization
                    )
                    if api_result is None:
                        hashable_data += "NO_RESULT"
                    else:
                        hashable_data += api_result.get_hashable_data()
                    hashable_data += "|"
        return hashable_data + "\n"

    def _format_api_result_line(
        self, api_results: DefaultDict[AccessRole, DefaultDict[bool, APIResult]]
    ) -> Tuple[List[str], str]:
        line_data = []
        line_data_2 = []
        access_roles_with_no_results = []
        add_own_organization_status_codes = False
        is_first_access_role = True
        controller_name = "Unknown"
        for access_role in self._access_role_order:
            api_results_by_is_own_organization = api_results.get(access_role)
            if api_results_by_is_own_organization is not None:
                api_result = api_results_by_is_own_organization.get(
                    False
                )  # is_user_of_own_organization=False
                api_result_own_organization = api_results_by_is_own_organization.get(
                    True
                )  # is_user_of_own_organization=True
                if is_first_access_role:
                    is_first_access_role = False
                    method = self._fill_string(api_result.method, "Method")
                    line_data.append(method)
                    line_data_2.append(len(method) * " ")
                    endpoint = self._fill_string(
                        api_result.path.replace("_", r"\_"), "Endpoint"
                    )
                    line_data.append(endpoint)
                    as_organization_member = self._fill_string(
                        ":busts_in_silhouette: (organization creator or member access)",
                        "Endpoint",
                    )
                    line_data_2.append(as_organization_member)
                    for access_role_without_result in access_roles_with_no_results:
                        line_data.append(
                            self._format_api_result_cell(
                                None,
                                add_icon=False,
                                access_role=access_role_without_result,
                            )
                        )
                if api_result:
                    controller_name = api_result.controller_name
                    cell = self._format_api_result_cell(api_result, add_icon=True)
                    line_data.append(cell)
                    # Show api_result_own_organization only if it differs from the api_result of related to a
                    # foreign organization:
                    if api_result_own_organization is not None:
                        if api_result != api_result_own_organization:
                            add_own_organization_status_codes = True
                        cell_own_organization = self._format_api_result_cell(
                            api_result_own_organization, add_icon=True
                        )
                    else:
                        cell_own_organization = self._format_api_result_cell(
                            None, add_icon=False, access_role=access_role
                        )
                    line_data_2.append(cell_own_organization)
            else:
                if is_first_access_role:
                    access_roles_with_no_results.append(access_role)
                else:
                    line_data.append(
                        self._format_api_result_cell(
                            None, add_icon=False, access_role=access_role
                        )
                    )
        lines = ["| " + " | ".join(line_data) + " |"]
        if add_own_organization_status_codes:
            lines.append("| " + " | ".join(line_data_2) + " |")
        return lines, controller_name

    def _format_api_result_cell(
        self,
        api_result: Optional[APIResult],
        add_icon: bool,
        access_role: Optional[AccessRole] = None,
    ) -> str:
        status_code = "" if api_result is None else api_result.status_code
        column_title = self._get_access_role_column_title(
            api_result.access_role if access_role is None else access_role
        )
        cell = f"<sup><sub>{status_code}</sub></sup>"
        if api_result is not None:
            expected_status = api_result.expected_status
            if api_result.state != APIResultState.OK:
                cell += f" ({api_result.state.value}"
                if len(expected_status) > 0:
                    cell += f", expected {expected_status}"
                cell += ")"
            if api_result.state == APIResultState.ERROR:
                cell = self._add_color_(cell, "red")
                if add_icon:
                    cell = f":boom: {cell}"
            elif api_result.state == APIResultState.SKIPPED:
                if add_icon:
                    cell = f":fast_forward: {cell}"
            elif api_result.state == APIResultState.OK:
                if add_icon:
                    emojis_by_status_code = {
                        "200": "earth_africa",
                        "201": "earth_africa",
                        "204": "earth_africa",
                        "400": "x",
                        "401": "lock",
                        "403": "lock",
                        "404": "question",
                        "405": "lock",
                        "409": "zap",
                        "500": "boom",
                        "501": "construction",
                    }
                    emoji = emojis_by_status_code.get(status_code, "")
                    if emoji:
                        cell = f":{emoji}: {cell}"
        cell = self._fill_string(cell, column_title)
        return cell

    def _fill_column_title(self, column: str):
        length = self._api_results_columns[column]
        title = column.ljust(length).replace("_", " ")
        separator = "-" * len(title)
        return title, separator

    def _format_api_result_header(self) -> List[str]:
        titles, separator = zip(
            *[self._fill_column_title(column) for column in self._api_results_columns]
        )
        titles = "| " + " | ".join(titles) + " |"
        separator = "|-" + "-|-".join(separator) + "-|"
        return [titles, separator]

    def _write_api_results_to_cache_file(
        self, api_results_by_method_by_endpoint: APIResultsDict
    ):
        with open(self._cache_file, "w", encoding="utf8") as f:
            api_results_by_method_by_endpoint_to_cache = (
                self._create_api_results_data_dict()
            )
            for (
                endpoint,
                method,
                access_role,
                is_own_organization,
                api_result,
            ) in self._iter_api_results(api_results_by_method_by_endpoint):
                access_role_value = access_role.value
                assert isinstance(is_own_organization, bool)
                api_results_by_method_by_endpoint_to_cache[endpoint][method][
                    access_role_value
                ][is_own_organization] = api_result.json()
            json.dump(api_results_by_method_by_endpoint_to_cache, f)

    def write_api_test_results_file(self, file_name: str):
        api_results_by_method_by_endpoint = self.api_results_by_method_by_endpoint
        self._write_api_results_to_cache_file(api_results_by_method_by_endpoint)
        caption = "API Auth Test Results"
        api_results_hash_data = ""

        header_lines = self._format_api_result_header()
        lines_by_controller = defaultdict(list)
        endpoints = list(api_results_by_method_by_endpoint)
        endpoints.sort()

        max_endpoint_length = 0
        for endpoint in endpoints:
            max_endpoint_length = max(
                len(endpoint.replace("_", r"\n")), max_endpoint_length
            )
        self.__api_results_columns["Endpoint"] = max(
            max_endpoint_length, self.__api_results_columns["Endpoint"]
        )

        for endpoint in endpoints:
            api_results_hash_data += f"{endpoint}|"
            api_results_by_method = api_results_by_method_by_endpoint[endpoint]
            methods = list(api_results_by_method.keys())
            methods.sort()
            for method in methods:
                api_results_hash_data += f"{method}|"
                api_results = api_results_by_method[method]
                result_lines, controller_name = self._format_api_result_line(
                    api_results
                )
                lines_by_controller[controller_name].append(result_lines[0])
                if len(result_lines) == 2:
                    # result for own organization is available, add it in a row below
                    lines_by_controller[controller_name].append(result_lines[1])
                api_results_hash_data += self._get_hashable_data(api_results)

        m = hashlib.sha256()
        m.update(api_results_hash_data.encode("utf-8"))
        api_results_hash = m.hexdigest()
        with open(file_name, "w", encoding="utf-8") as f:
            f.write(caption + "\n")
            f.write("=" * len(caption) + "\n\n")
            controller_names = list(lines_by_controller.keys())
            controller_names.sort()
            is_first_controller_name = True
            for controller_name in controller_names:
                if is_first_controller_name:
                    is_first_controller_name = False
                else:
                    f.write("\n\n")
                controller_name_with_spaces = (
                    self._insert_spaces_into_camel_case_string(controller_name)
                )
                f.write(controller_name_with_spaces + "\n")
                f.write(len(controller_name_with_spaces) * "-" + "\n\n")
                f.write("\n".join(header_lines) + "\n")
                lines = lines_by_controller[controller_name]
                f.write("\n".join(lines) + "\n")
            f.write(f"\n{self.api_results_hash_prefix}{api_results_hash}")

    @staticmethod
    def _insert_spaces_into_camel_case_string(s: str):
        r = ""
        last_c_was_lower = False
        for c in s:
            if c.isupper():
                if last_c_was_lower:
                    r += " "
                    last_c_was_lower = False
            else:
                last_c_was_lower = True
            r += c
        return r

    def has_api_result(
        self,
        path: str,
        method: str,
        access_role: AccessRole,
        is_user_of_own_organization: bool,
    ):
        api_results = (
            self._api_results_by_method_by_endpoint.get(path, {})
            .get(method.upper(), {})
            .get(access_role, {})
            .get(is_user_of_own_organization)
        )
        return api_results is not None

    def has_cached_api_result(
        self,
        path: str,
        method: str,
        access_role: AccessRole,
        is_user_of_own_organization: bool,
    ):
        api_results = (
            self._cached_api_results_by_method_by_endpoint.get(path, {})
            .get(method.upper(), {})
            .get(access_role, {})
            .get(is_user_of_own_organization)
        )
        return api_results is not None


class APIResultsManager:
    _access_role_order_v1 = [
        AccessRole.ANONYMOUS,
        AccessRole.REGISTERED_USER,
        AccessRole.ORGANIZATION_MEMBER,
        AccessRole.ORGANIZATION_ADMIN,
        AccessRole.ADMIN_USER,
        AccessRole.SUPER_ADMIN,
    ]

    _access_role_order_v2 = [
        AccessRole.ANONYMOUS,
        AccessRole.REGISTERED_USER,
        AccessRole.APP_MAINTAINER,
        AccessRole.DATA_MANAGER,
        AccessRole.PATHOLOGIST,
        AccessRole.ORGANIZATION_MANAGER,
        AccessRole.MODERATOR,
        AccessRole.ADMIN,
    ]

    _openapi_definition_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "..",
        "..",
        "..",
        "docs",
        "openapi_definition.json",
    )

    def __init__(
        self, api_test_state: "APITestState", compose_settings: "ComposeSettings"
    ):
        self._api_test_state = api_test_state
        v2_api_results_cache_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "api_results_manager.v2_api.cache",
        )
        self._v2_api_results = APIResults(
            api_test_state, self._access_role_order_v2, v2_api_results_cache_file
        )
        self._has_any_error = False
        self._endpoints_to_be_tested: Dict[
            AccessRole, Dict[bool, List["EndpointTest"]]
        ] = collections.defaultdict(lambda: collections.defaultdict(list))
        self._compose_settings = compose_settings

    def has_any_error(self) -> bool:
        return self._has_any_error

    def write_api_test_results_file(self, use_v2_api: bool):
        file_name = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "..",
            "..",
            "..",
            f"docs/api_auth_test_results_{'v2' if use_v2_api else 'vUNKNOWN'}.md",
        )
        if use_v2_api:
            self._v2_api_results.write_api_test_results_file(file_name)
        else:
            raise AssertionError("Only V2 API is supported")

    def has_api_result(
        self,
        endpoint_test: "EndpointTest",
        access_role: AccessRole,
        is_user_of_own_organization: bool,
    ) -> bool:
        return self._v2_api_results.has_api_result(
            endpoint_test.path,
            endpoint_test.method,
            access_role,
            is_user_of_own_organization,
        ) or self._v2_api_results.has_cached_api_result(
            endpoint_test.path,
            endpoint_test.method,
            access_role,
            is_user_of_own_organization,
        )

    def add_api_result(
        self,
        response: requests.Response,
        endpoint_test: "EndpointTest",
        state: APIResultState,
        expected_status: Union[int, str],
        controller_name: str,
    ):
        test_user = self._api_test_state.get_test_user()
        access_role = test_user.get_access_role()

        api_result = APIResult(
            None if response is None else response.status_code,
            endpoint_test.path,
            endpoint_test.method,
            state,
            expected_status,
            access_role,
            controller_name,
        )
        if state == APIResultState.ERROR:
            self._has_any_error = True

        if api_result.is_v2_api:
            self._v2_api_results.add_api_result(api_result)
        else:
            raise AssertionError("Non-V2 API result not handled")

    def assert_all_endpoints_have_been_tested(self, api_test_state: "APITestState"):
        test_user = api_test_state.get_test_user()
        access_role = test_user.get_access_role()
        for _, endpoints_to_be_tested_by_access_role in self._endpoints_to_be_tested[
            access_role
        ].items():
            for endpoint_test in endpoints_to_be_tested_by_access_role:
                if endpoint_test._ignore_endpoint:
                    continue
                if not self.has_any_error() and not api_test_state.did_any_error_occur:
                    if self._needs_api_result(
                        api_test_state, endpoint_test
                    ) and not self.has_api_result(
                        endpoint_test,
                        access_role,
                        test_user.should_test_with_own_organization(),
                    ):
                        msg = []
                        # pylint: disable=W0212
                        api_results_by_method_by_endpoint = {}
                        api_results_by_method_by_endpoint.update(
                            self._v2_api_results.api_results_by_method_by_endpoint
                        )
                        for ep in api_results_by_method_by_endpoint:
                            _d0 = api_results_by_method_by_endpoint[ep]
                            for mt in _d0:
                                _d1 = _d0[mt]
                                for ar in _d1:
                                    _d2 = _d1[ar]
                                    for with_own_orga in _d2:
                                        r = _d2[with_own_orga]
                                        msg.append(
                                            f"[{mt}] {ep} {ar.value} {with_own_orga}: {r.state}"
                                        )
                        msg = "\n- ".join(msg)
                        raise AssertionError(
                            f"Endpoint '{endpoint_test.method.upper()} {endpoint_test.path}' has not been tested "
                            f"for {access_role.get_extended_name(test_user.should_test_with_own_organization())}.\n"
                            f"Tested endpoints are:\n- {msg}.\n"
                            f"Test filter: {api_test_state.test_filter.method} {api_test_state.test_filter.endpoint} "
                            f"{test_user.use_v2_api()}"
                            f"\n {self.has_any_error()} {api_test_state.did_any_error_occur}"
                        )

    def expect_api_test_results_for_endpoint_test(
        self,
        endpoint_test: "EndpointTest",
        access_role: AccessRole,
        test_with_own_organization: bool,
    ):
        d = self._endpoints_to_be_tested
        if endpoint_test in d[access_role][test_with_own_organization]:
            raise AssertionError(
                f"Endpoint [{endpoint_test.method.upper()}] {endpoint_test.path} "
                f"already tested for access role {access_role.value}"
                f"{' (own organization)' if test_with_own_organization else ''}"
            )
        d[access_role][test_with_own_organization].append(endpoint_test)

    @staticmethod
    def _needs_api_result(
        api_test_state: "APITestState", endpoint_test: "EndpointTest"
    ) -> bool:
        test_user = api_test_state.get_test_user()
        return not api_test_state.should_ignore_endpoint_test(
            endpoint_test,
            api_test_state.test_filter.method,
            api_test_state.test_filter.endpoint,
            test_user.use_v2_api(),
        )

    def save_v2_openapi_definition(self):
        url = self._compose_settings.aaa_swagger_server_url + "/v3/api-docs/v2"
        self.load_openapi_definition_insert_checksum_and_save_it(url)

    @classmethod
    def load_openapi_definition_insert_checksum_and_save_it(cls, url: str):
        openapi_definition = requests.get(url).json()
        data = json.dumps(openapi_definition, indent=4, sort_keys=True)
        m = hashlib.sha256()
        m.update(data.encode("utf-8"))
        checksum = m.hexdigest()
        openapi_definition["info"]["checksum"] = checksum
        with open(cls._openapi_definition_file, "w", encoding="utf-8") as f:
            json.dump(openapi_definition, f, indent=4, sort_keys=True)
