import os
import shutil
import sys
from typing import List, Optional

import hypothesis.strategies
import pytest
import schemathesis.hooks

from ...data_generator_utils import (
    datagen_wipe,
    wait_for_auth_service_to_be_available,
)
from ...tests.auth_service.permission_tests.controller_tests import (
    ControllerTests,
)
from .. import utils_resources
from ..auth_service.access_role import AccessRole
from ..singletons import api_test_state, general_settings
from . import tune_case
from .api_test_user.api_test_user import APITestUser
from .test_filter import APITestFilter


def assert_all_data_was_wiped():
    datagen_wipe.assert_all_data_was_wiped()


def wipe_all_data():
    datagen_wipe.wipe_auth_service_data()
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        shutil.rmtree(utils_resources.get_data_generator_output_directory())


controller_tests = ControllerTests()


def setup_api_state(
    access_role_names: Optional[List[str]],
    endpoint: Optional[str],
    method: Optional[str],
    repeat_last_failed_request: bool,
    skip_schemathesis: bool,
    api_version: str,
    test_functional_only: bool,
    test_data_migration_only: bool,
    test_keycloak_initialize_only: bool,
):
    if not test_keycloak_initialize_only:
        wait_for_auth_service_to_be_available()
        if general_settings.drop_all_data_before_running_tests:
            wipe_all_data()
        assert_all_data_was_wiped()
    access_roles = (
        None
        if access_role_names is None
        else [AccessRole[name.upper()] for name in access_role_names]
    )
    test_filter = APITestFilter(
        access_roles,
        endpoint,
        method,
        repeat_last_failed_request,
        skip_schemathesis,
        api_version,
        test_functional_only,
        test_data_migration_only,
        test_keycloak_initialize_only,
    )
    api_test_state.set_test_filter(test_filter)
    api_test_state.set_test_user(APITestUser())


def schema_hook(
    context: schemathesis.hooks.HookContext,
    strategy: hypothesis.strategies.SearchStrategy,
):
    return tune_case.make_tune_case(context, strategy)


def should_skip_api_permission_tests(use_v2_api: bool) -> bool:
    should_skip = True
    if api_test_state.test_filter.run_v1_api_permission_test and not use_v2_api:
        should_skip = False
    elif api_test_state.test_filter.run_v2_api_permission_test and use_v2_api:
        should_skip = False
    return should_skip


def make_api_tests(
    test_module_name: str,
    access_role: AccessRole,
    test_with_own_organization: bool = False,
    use_v2_api: bool = False,
):
    if not should_skip_api_permission_tests(
        False
    ) and not should_skip_api_permission_tests(True):
        # probably test-keycloak-initialize-only or test-functional-only. return to not prevent the permission test
        # setup through schemathesis
        return
    api_version_to_test = api_test_state.test_filter.api_version
    api_version = "v2" if use_v2_api else "v1"
    if (
        api_test_state.test_filter.access_roles is None
        or any(
            [
                ar in api_test_state.test_filter.access_roles
                for ar in AccessRole.get_applying_access_roles(
                    access_role, use_v2_api=use_v2_api
                )
            ]
        )
    ) and (api_version_to_test == api_version or api_version_to_test in (None, "all")):
        module = sys.modules[test_module_name]

        def module_setup_teardown_fixture():
            yield from api_test_state.setup_teardown(
                access_role, test_with_own_organization
            )

        module.module_setup_teardown_fixture = pytest.fixture(
            scope="module", autouse=True
        )(module_setup_teardown_fixture)

        if (
            not api_test_state.test_filter.skip_schemathesis
            and not should_skip_api_permission_tests(use_v2_api)
        ):
            module.schema = schemathesis.from_uri(
                f"{general_settings.auth_service_host}{general_settings.auth_service_openapi_url_suffix.lstrip('/')}",
                validate_schema=False,
            )

            module.schema_hook = module.schema.hooks.register("before_generate_case")(
                schema_hook
            )

            setattr(
                module,
                f"test_api_conformity_for_{access_role.value.lower()}",
                make_test_function(module.schema, use_v2_api),
            )

        if not should_skip_api_permission_tests(use_v2_api):
            for test_function in controller_tests.get_test_functions(
                access_role,
                api_test_state.test_filter.method,
                api_test_state.test_filter.endpoint,
                use_v2_api,
            ):
                assert getattr(module, test_function.name, None) is None
                setattr(
                    module,
                    test_function.name,
                    test_function.wrap(api_test_state.test_filter),
                )


def make_api_tests_v2(
    test_module_name: str,
    access_role: AccessRole,
    test_with_own_organization: bool = False,
):
    make_api_tests(
        test_module_name, access_role, test_with_own_organization, use_v2_api=True
    )


def make_test_function(schema, use_v2_api: bool):
    def test_function(case: schemathesis.models.Case):
        access_role = api_test_state.get_current_access_role()
        test_user = api_test_state.get_test_user()
        assert test_user.use_v2_api() == use_v2_api
        test_with_own_organization = test_user.should_test_with_own_organization()
        try:
            controller_tests.assert_endpoint_will_be_tested(
                case.method,
                case.path,
                access_role,
                test_with_own_organization,
            )
            # We can add more checks when the API gets more standard conform
            # selected_checks = (checks.status_code_conformance, )
            # case.call_and_validate(checks=selected_checks)
        except Exception as e:
            api_test_state.set_any_error_occurred()
            api_test_state.test_filter.update_last_failed_request(access_role, case)
            raise e
        else:
            api_test_state.test_filter.remove_last_failed_request_file()

    hypothesis_test_function = hypothesis.settings(
        phases=[hypothesis.Phase.generate],
        max_examples=1,
        suppress_health_check=(hypothesis.HealthCheck.too_slow,),
    )(test_function)
    endpoint = api_test_state.test_filter.endpoint
    if endpoint is None:
        endpoint = "^/api/v2/" if use_v2_api else "^/api/(?!v2/)"
    return schema.parametrize(
        method=api_test_state.test_filter.method, endpoint=endpoint
    )(hypothesis_test_function)
