import difflib
import json
import unicodedata
from typing import Any, Dict, Optional, Tuple

from .singletons import api_test_state


def get_dict_diff(a: Dict, a_title: str, b: Dict, b_title: str) -> Optional[str]:
    diff = None
    a = json.dumps(a, sort_keys=True, indent=2)
    b = json.dumps(b, sort_keys=True, indent=2)
    if len(a) != len(b) or a != b:
        diff = difflib.unified_diff(a.splitlines(), b.splitlines(), a_title, b_title)
        diff = "\n".join(list(diff))
    return diff


def assert_dicts_are_equal(a: Dict, a_title: str, b: Dict, b_title: str):
    diff = get_dict_diff(a, a_title, b, b_title)
    if diff is not None:
        a = json.dumps(a, sort_keys=True, indent=2)
        b = json.dumps(b, sort_keys=True, indent=2)
        raise AssertionError(
            f"Data differs:\n\n{a_title}: {a}\n\n{b_title}: {b}\n\nDiff:\n{diff}"
        )


def assert_dicts_are_not_equal(a: Dict, a_title: str, b: Dict, b_title: str):
    diff = get_dict_diff(a, a_title, b, b_title)
    if diff is None:
        raise AssertionError(
            f"Data does not differs:\n\n{a_title} == {b_title}:\n\n{a}"
        )


def assert_test_user_token_contains_organization(organization_id: str):
    is_member, decoded_token = _is_test_user_organization_member(organization_id)
    if not is_member:
        raise AssertionError(
            f"Token of API test user does not include the organization: {decoded_token}"
        )


def assert_test_user_token_does_not_contain_organization(organization_id: str):
    is_member, decoded_token = _is_test_user_organization_member(organization_id)
    if is_member:
        raise AssertionError(
            f"Token of API test user should not include the organization: {decoded_token}"
        )


def _is_test_user_organization_member(organization_id: str) -> Tuple[bool, Dict]:
    test_user = api_test_state.get_test_user()
    _, decoded_token = test_user.get_auth_tokens()
    return organization_id in decoded_token["organizations"], decoded_token


def assert_equal(a: Any, b: Any):
    if a != b:
        raise AssertionError(f"{a} != {b}")


def normalize_name(name):
    name = str(replace_special_characters(name))
    for wildcard in [" ", ".", ","]:
        name.replace(wildcard, "_")

    name.lower()
    return name


def replace_special_characters(text):
    result_str = ""

    for char in text:
        if unicodedata.category(char) == "Ll":
            # ff the character is a lowercase letter, keep it as is
            result_str += char
        else:
            # not an ascii lower case letter > try to normalize it to its ascii form
            normalized_char = (
                unicodedata.normalize("NFKD", char)
                .encode("ASCII", "ignore")
                .decode("utf-8")
            )
            result_str += normalized_char

    return result_str
