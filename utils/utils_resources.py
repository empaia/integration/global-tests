import json
import os
from pathlib import Path
from typing import Dict, List, Union

from .singletons import general_settings

g_data_generator_output_directory = os.path.abspath("./data_generator_output")


def resolve_file_name_in_resource_path(file_name: str) -> Path:
    path = Path(file_name).expanduser()
    if not path.is_absolute():
        path = (general_settings.resources_path / file_name).expanduser()
    return path


def resolve_resource_file_name(file_name: str) -> Path:
    path = Path(file_name).expanduser()
    if not path.is_absolute():
        path = Path().absolute().joinpath(file_name)
    return path


def get_json_data(file_name: str):
    data_path = resolve_file_name_in_resource_path(file_name)
    with open(data_path, encoding="UTF-8") as f:
        data = json.load(f)
    return data


def write_to_json_file(file_name: str, data: dict):
    file_name = resolve_file_name_in_resource_path(file_name)
    file_name.unlink(missing_ok=True)
    file_name.parent.mkdir(exist_ok=True, parents=True)
    with open(file_name, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def write_to_file_lines(file_name: str, data: list):
    file_name = resolve_file_name_in_resource_path(file_name)
    file_name.unlink(missing_ok=True)
    file_name.parent.mkdir(exist_ok=True, parents=True)
    with open(file_name, "w", encoding="utf-8") as f:
        for line in data:
            f.write("%s\n" % line)


def read_binary_file(file_name: str) -> bytes:
    file_name = resolve_file_name_in_resource_path(file_name)
    with open(file_name, "rb") as f:
        return f.read()


def get_data_generator_output_directory() -> str:
    return g_data_generator_output_directory


def write_data_generator_output_to_json_file(
    file_name: str, data: Union[Dict, List], out_dir: str = None
):
    if not out_dir:
        out_dir = get_data_generator_output_directory()
    file_name = os.path.join(out_dir, file_name)
    write_to_json_file(file_name, data)


def resolve_data_generator_output_file_name(file_name: str) -> Path:
    path = Path(file_name).expanduser()
    if not path.is_absolute():
        path = (Path(get_data_generator_output_directory()) / file_name).expanduser()
    return path
