import datetime
import re
from base64 import urlsafe_b64encode
from collections import defaultdict
from hashlib import sha256
from random import choices
from string import ascii_letters, digits
from typing import Dict, Optional, Tuple
from urllib.parse import parse_qs, urlparse

import jwt
import requests
from bs4 import BeautifulSoup
from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class LoginBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=".env", env_prefix="login_", extra="allow"
    )


class ModeSettings(LoginBaseSettings):
    auth_mode: str = "keycloak"


class EmpaiaAuthSettings(LoginBaseSettings):
    idp_url: AnyHttpUrl = None

    aaa_client_id: str = ""
    aaa_client_secret: str = ""

    portal_client_id: str = ""
    super_admin_name: str = ""
    super_admin_password: str = ""


Tokens = Tuple[str, Dict]


class BasicAuthData:
    def __init__(
        self,
        settings: EmpaiaAuthSettings,
        client_id: str,
        verbose_requests: bool,
    ):
        self._settings = settings
        self._verbose_requests = verbose_requests
        self._client_id = client_id
        self._tokens = None
        self._best_before = None
        self._well_known = None

    @property
    def tokens(self) -> Tokens:
        if self._tokens is None or datetime.datetime.now() >= self._best_before:
            self.retrieve_new_tokens()
        return self._tokens

    def retrieve_new_tokens(self) -> Tokens:
        self._tokens = self._fetch_new_tokens()
        self._best_before = self._calc_best_before(self._tokens)
        return self._tokens

    @staticmethod
    def _decode(token):
        return jwt.decode(token, options={"verify_signature": False})

    @staticmethod
    def _calc_best_before(tokens):
        _, decoded_token = tokens
        exp = datetime.datetime.fromtimestamp(decoded_token["exp"])
        iat = datetime.datetime.fromtimestamp(decoded_token["iat"])
        delta = exp - iat
        best_before = datetime.datetime.now() + delta / 2
        return best_before

    @property
    def well_known(self):
        if self._well_known is not None:
            return self._well_known
        idp_url = str(self._settings.idp_url).rstrip("/")
        well_known_url = f"{idp_url}/.well-known/openid-configuration"
        r = requests.get(well_known_url)
        r.raise_for_status()
        self._well_known = r.json()
        return self._well_known

    @property
    def token_url(self):
        return self.well_known["token_endpoint"]

    @property
    def auth_url(self):
        return self.well_known["authorization_endpoint"]

    @property
    def introspect_url(self):
        return self.well_known["introspection_endpoint"]

    def _fetch_new_tokens(self) -> Tokens:
        raise NotImplementedError


class ClientCredentialsFlowAuthData(BasicAuthData):
    def __init__(
        self,
        settings: EmpaiaAuthSettings,
        client_id: str,
        client_secret: str,
        verbose_requests: bool,
    ):
        super().__init__(settings, client_id, verbose_requests)
        self._client_secret = client_secret

    def _fetch_new_tokens(self) -> Tokens:
        return self._client_credentials_flow(self._client_id, self._client_secret)

    def _client_credentials_flow(self, client_id, client_secret) -> Tokens:
        data = {
            "grant_type": "client_credentials",
            "client_id": client_id,
            "client_secret": client_secret,
        }
        r = requests.post(self.token_url, data=data)
        data = r.json()
        token = data["access_token"]
        return token, self._decode(token)


class AccessFlowAuthData(BasicAuthData):
    def __init__(
        self,
        settings: EmpaiaAuthSettings,
        client_id: str,
        user: str,
        password: str,
        verbose_requests: bool,
    ):
        super().__init__(settings, client_id, verbose_requests)
        self._user = user
        self._password = password

    def update_password(self, password: str):
        self._password = password

    def _fetch_new_tokens(self) -> Tokens:
        return self._access_code_flow(self._client_id, self._user, self._password)

    def _access_code_flow(self, client_id, user_name, user_password):
        # retreive first access code
        state = "".join(choices(ascii_letters + digits, k=16))
        code_verifier = "".join(choices(ascii_letters + digits + "-._~", k=128))
        m = sha256()
        m.update(code_verifier.encode("ascii"))
        code_challenge = urlsafe_b64encode(m.digest()).decode("ascii")
        code_challenge = code_challenge.rstrip("=")

        params = {
            "client_id": client_id,
            "response_type": "code",
            "redirect_uri": "http://localhost",
            "state": state,
            "code_challenge": code_challenge,
            "code_challenge_method": "S256",
        }
        headers = {"Content-type": "application/json"}
        r = requests.get(
            self.auth_url, params=params, headers=headers, allow_redirects=False
        )
        if self._verbose_requests:
            print("Retrieve First Access Code:", r.status_code)

        assert r.status_code == 200
        cookie = r.headers["Set-Cookie"]

        matches = re.findall('"loginAction": "(.*?)"', r.text)
        if matches and len(matches) > 0:
            form_action = matches[0]
        else:
            soup = BeautifulSoup(r.text, "html.parser")
            kc_form_wrapper = soup.find_all(id="kc-form-wrapper")
            assert len(kc_form_wrapper) == 1
            form_action = kc_form_wrapper[0].form.get("action")

        # retrieve second access code
        r = requests.post(
            url=form_action,
            data={
                "username": user_name,
                "password": user_password,
            },
            headers={"Cookie": cookie},
            allow_redirects=False,
        )

        if self._verbose_requests:
            print("Retrieve Second Access Code:", r.status_code)
        if r.status_code != 302:
            raise AssertionError(
                f"Expected status code 302, but got {r.status_code}:\nContent:\n{r.content}"
            )
        location = urlparse(r.headers["Location"])
        params = parse_qs(location.query)

        # retrieve access token
        data = {
            "grant_type": "authorization_code",
            "client_id": client_id,
            "code": params["code"][0],
            "redirect_uri": "http://localhost",
            "code_verifier": code_verifier,
        }
        r = requests.post(self.token_url, data=data)
        r.raise_for_status()
        data = r.json()
        token = data["access_token"]

        return token, self._decode(token)


class LoginManager:
    def __init__(
        self,
        verbose_requests: bool,
        keycloak_user: str = None,
        keycloak_password: str = None,
    ):
        mode_settings = ModeSettings()
        self.auth_mode = mode_settings.auth_mode
        self.settings = EmpaiaAuthSettings()
        self.verbose_requests = verbose_requests
        self._auth_service_client_data: Optional[ClientCredentialsFlowAuthData] = None
        # user AccessFlowAuthData by auth mode, client id, user name and password
        self._user_data: Optional[
            Dict[str, Dict[str, Dict[str, AccessFlowAuthData]]]
        ] = None
        self.clear_cache()
        self.keycloak_user = keycloak_user
        self.keycloak_password = keycloak_password

    def clear_cache(self):
        self._auth_service_client_data = ClientCredentialsFlowAuthData(
            self.settings,
            self.settings.aaa_client_id,
            self.settings.aaa_client_secret,
            self.verbose_requests,
        )
        self._user_data = defaultdict(lambda: defaultdict(dict))

    def clear_cache_for_user(
        self, user: str, password: str = None, client_id: str = None
    ):
        if client_id is None:
            client_id = self.settings.portal_client_id
        user_data_by_user_name = self._user_data[client_id]
        if user in user_data_by_user_name:
            if password is None:
                del user_data_by_user_name[user]
            else:
                user_data_by_password = user_data_by_user_name[user]
                if password in user_data_by_password:
                    del user_data_by_password[password]

    def get_client_credentials_flow_tokens(
        self, client_id: str, client_secret: str
    ) -> Tokens:
        auth_data = ClientCredentialsFlowAuthData(
            self.settings,
            client_id,
            client_secret,
            self.verbose_requests,
        )
        return auth_data.retrieve_new_tokens()

    def get_access_flow_tokens(
        self, client_id: str, user_name: str, password: str
    ) -> Tokens:
        auth_data = AccessFlowAuthData(
            self.settings,
            client_id,
            user_name,
            password,
            self.verbose_requests,
        )
        return auth_data.retrieve_new_tokens()

    @property
    def auth_service_client_tokens(self) -> Tokens:
        return self._auth_service_client_data.tokens

    def get_user_tokens(
        self, user: str, password: str, client_id: str = None
    ) -> Tokens:
        return self._get_user_auth_data(
            user, password, client_id, clear_cache=False
        ).tokens

    @property
    def super_admin_tokens(self) -> Tokens:
        return self.get_user_tokens(
            self.settings.super_admin_name,
            self.settings.super_admin_password,
            self.settings.portal_client_id,
        )

    def _create_auth_header(self, auth_data: BasicAuthData) -> Dict:
        token, decoded_token = auth_data.tokens
        return {
            "user-id": decoded_token["sub"],
            "Authorization": f"Bearer {token}",
        }

    @property
    def admin_cli_header(self) -> Tokens:
        realm = str(self.settings.idp_url).split("/")[-1]
        idp_url_master = str(self.settings.idp_url).replace(realm, "master").rstrip("/")
        well_known_url = f"{idp_url_master}/.well-known/openid-configuration"
        r = requests.get(well_known_url)
        r.raise_for_status()
        well_known = r.json()
        token_endpoint = well_known["token_endpoint"]
        data = {
            "client_id": "admin-cli",
            "username": self.keycloak_user,
            "password": self.keycloak_password,
            "grant_type": "password",
        }
        r = requests.post(token_endpoint, data=data)
        r.raise_for_status()
        data = r.json()
        token = data["access_token"]
        return {
            "Authorization": f"Bearer {token}",
        }

    def _create_auth_header(self, auth_data: BasicAuthData) -> Dict:
        token, decoded_token = auth_data.tokens
        return {
            "user-id": decoded_token["sub"],
            "Authorization": f"Bearer {token}",
        }

    def _get_user_auth_data(
        self, user: str, password: str, client_id: str, clear_cache: bool
    ) -> AccessFlowAuthData:
        if client_id is None:
            client_id = self.settings.portal_client_id
        if clear_cache:
            self.clear_cache_for_user(user, password, client_id)
        user_data = self._user_data[client_id][user].get(password)
        if user_data is None:
            user_data = AccessFlowAuthData(
                self.settings,
                client_id,
                user,
                password,
                self.verbose_requests,
            )
            self._user_data[client_id][user][password] = user_data
        return user_data

    def update_password(
        self, user: str, old_password: str, new_password: str, client_id: str = None
    ):
        if client_id is None:
            client_id = self.settings.portal_client_id
        user_data = self._user_data[client_id][user].get(old_password)
        if user_data is not None:
            user_data.update_password(new_password)
            self._user_data[client_id][user][new_password] = user_data
            del self._user_data[client_id][user][old_password]

    def user(
        self, user: str, password: str, client_id: str = None, clear_cache: bool = False
    ) -> Dict:
        user_data = self._get_user_auth_data(user, password, client_id, clear_cache)
        return self._create_auth_header(user_data)

    def get_user_token_and_header(
        self, user: str, password: str, client_id: str = None, clear_cache: bool = False
    ) -> Dict:
        user_data = self._get_user_auth_data(user, password, client_id, clear_cache)
        return user_data.tokens, self._create_auth_header(user_data)

    def clear_cache_for_super_admin(self):
        self.clear_cache_for_user(
            self.settings.super_admin_name,
            self.settings.super_admin_password,
            self.settings.portal_client_id,
        )

    def super_admin(self) -> Dict:
        return self.user(
            self.settings.super_admin_name,
            self.settings.super_admin_password,
            self.settings.portal_client_id,
        )

    def auth_service_client(self) -> Dict:
        return self._create_auth_header(self._auth_service_client_data)

    def introspect_token(self, token: str) -> Dict:
        headers = self.super_admin()
        data = {
            "client_id": self.settings.aaa_client_id,
            "client_secret": self.settings.aaa_client_secret,
            "token": token,
        }
        r = requests.post(
            self._auth_service_client_data.introspect_url, data=data, headers=headers
        )
        r.raise_for_status()
        return r.json()
