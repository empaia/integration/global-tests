import argparse
import csv
import datetime
from pathlib import Path

from tests.utils import singletons, utils_resources
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.mps import utils_mps


def read_csv_to_json(filepath: Path):
    data_dict = []
    with open(filepath, encoding="utf-8") as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler, delimiter=";")
        for row in csv_reader:
            if row["Company"] == "":
                break
            data_dict.append(row)
    return data_dict


def create_clearance(default_admin_header, post_clearance, post_clearance_item):
    r = utils_mps.generic_post(
        "/v1/admin/clearances",
        json=post_clearance,
        headers=default_admin_header,
        raise_status=True,
    )
    resp = r.json()
    clearance_id = resp["id"]

    r = utils_mps.generic_post(
        f"/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item,
        headers=default_admin_header,
        raise_status=True,
    )
    resp = r.json()
    return clearance_id, resp


def import_clearances(clearance_filepath: Path, headers):
    clearance_dict = read_csv_to_json(clearance_filepath)

    for clearance_record in clearance_dict:
        organization_name = clearance_record["Company"]
        organization_id = (
            PublicController()
            .get_organization_by_name(organization_name)
            .organization_id
        )

        clearance = {"organization_id": organization_id, "is_hidden": False}

        tags = []
        for tag_group in [
            "TISSUE",
            "PROCEDURE",
            "INDICATION",
            "ANALYSIS",
            "STAIN",
            "CLEARANCE",
            "IMAGE_TYPE",
        ]:
            for tag_name in str(clearance_record[tag_group]).split(","):
                sanitized_tag = tag_name.strip()
                if sanitized_tag == "":
                    continue
                tags.append(
                    {"tag_group": tag_group, "tag_name": tag_name.lstrip().rstrip()}
                )

        desc = clearance_record["Description (optional)"]
        date_precision = str(clearance_record["product_release_at_precision"]).strip()

        release_date_timestamp = 0
        if date_precision in ["DATE", "MONTH_YEAR", "QUARTER", "YEAR"]:
            release_date_timestamp = int(
                datetime.datetime.strptime(
                    clearance_record["product_release_at"], "%Y-%m-%d"
                ).timestamp()
            )

        info_source_type = str(clearance_record["Source"]).strip()
        clearance_item = {
            "product_name": clearance_record["Algorithm/Product name"],
            "tags": tags,
            "description": {
                "EN": desc,
                "DE": desc,
            },  # DE is set to english description for now!
            "product_release_at_precision": date_precision,
            "product_release_at": release_date_timestamp,
            "info_updated_at": int(
                datetime.datetime.now().timestamp()
            ),  # TODO: should be set by csv
            "info_provider": "EMPAIA",
            "info_source_type": str(clearance_record["Source"]).strip(),
        }

        if info_source_type == "PUBLIC_WEBSITE":
            references = str(clearance_record["Source content/References"]).split(",")
            source_content = [{"reference_url": ref} for ref in references]
            clearance_item["info_source_content"] = source_content
        _, _ = create_clearance(headers, clearance, clearance_item)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("clearance_file", help="Path to clearance csv file")
    args = parser.parse_args()

    clearance_file = utils_resources.resolve_resource_file_name(args.clearance_file)

    headers = singletons.login_manager.super_admin()

    import_clearances(clearance_file, headers)
