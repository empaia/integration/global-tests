import argparse
from enum import Enum

from tests.utils import singletons, utils_resources
from tests.utils.mps import utils_mps


class LicenseUpdateState(str, Enum):
    ACTIVATE = "activate"
    REVOKE = "revoke"


def get_all_listed_app_ids():
    r = utils_mps.generic_put(
        "/v1/public/portal-apps/query",
        json={},
        raise_status=True,
    )
    resp = r.json()
    app_ids = [app["id"] for app in resp["items"]]
    return app_ids


def get_licenses_for_orga(organization_id: str, headers):
    license_query = {"organizations": [organization_id]}
    r = utils_mps.generic_put(
        "/v1/admin/licenses/query",
        json=license_query,
        headers=headers,
        raise_status=True,
    )
    return r.json()["items"]


def post_new_license(organization_id: str, portal_app_id: str, headers):
    post_license = {
        "items": [{"organization_id": organization_id, "portal_app_id": portal_app_id}]
    }

    _ = utils_mps.generic_post(
        "/v1/admin/licenses",
        json=post_license,
        headers=headers,
        raise_status=True,
    )


def update_license_state(
    license_id: str, license_update_state: LicenseUpdateState, headers
):
    _ = utils_mps.generic_put(
        f"/v1/admin/licenses/{license_id}/{license_update_state.value}",
        headers=headers,
        raise_status=True,
    )


def update_licenses(license_file, headers):
    licenses = utils_resources.get_json_data(license_file)

    for orga in licenses.keys():
        lic = licenses[orga]
        if singletons.general_settings.verbose_requests:
            print(f"Updating license for organization {orga}: {lic}")

        listed_p_app_ids = get_all_listed_app_ids()

        existing_licenses_for_orga = get_licenses_for_orga(
            lic["organization_id"], headers
        )

        p_app_ids_to_license = []
        p_app_ids_to_unlicense = []

        # all listed app
        if lic["all_portal_apps"] is True:
            p_app_ids_to_license += listed_p_app_ids

        # apps specifically (may be DELISTED)
        if lic["specific_portal_apps"] and len(lic["specific_portal_apps"]) > 0:
            p_app_ids_to_license += lic["specific_portal_apps"]

        # apps explicitly to unlicense
        if lic["exclude_portal_apps"] and len(lic["exclude_portal_apps"]) > 0:
            p_app_ids_to_license = set(p_app_ids_to_license) - set(
                lic["exclude_portal_apps"]
            )

        p_app_ids_to_unlicense = list(set(listed_p_app_ids) - set(p_app_ids_to_license))
        p_app_ids_to_unlicense += lic["exclude_portal_apps"]

        # UNLISTED app might have been added in the past but should no longer be licensed
        for el in existing_licenses_for_orga:
            if el["portal_app_id"] not in listed_p_app_ids:
                if el["portal_app_id"] not in p_app_ids_to_license:
                    p_app_ids_to_unlicense.append(el["portal_app_id"])

        # add / activate
        for portal_app_id in p_app_ids_to_license:
            existing_license = [
                el
                for el in existing_licenses_for_orga
                if el["portal_app_id"] == portal_app_id
            ]

            # license exists, but inactive > activate
            if (
                existing_license
                and len(existing_license) > 0
                and not bool(existing_license[0]["active"])
            ):
                lic_id = existing_license[0]["id"]
                update_license_state(lic_id, LicenseUpdateState.ACTIVATE, headers)

            # license does not exist > post new license
            if not existing_license:
                post_new_license(lic["organization_id"], portal_app_id, headers)

        # revoke
        for portal_app_id in p_app_ids_to_unlicense:
            existing_license = [
                el
                for el in existing_licenses_for_orga
                if el["portal_app_id"] == portal_app_id
            ]

            # license exists, but not active > revoke
            if (
                existing_license
                and len(existing_license) > 0
                and bool(existing_license[0]["active"])
            ):
                lic_id = existing_license[0]["id"]
                update_license_state(lic_id, LicenseUpdateState.REVOKE, headers)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("license_file", help="Path to app license file")
    args = parser.parse_args()

    license_file = utils_resources.resolve_resource_file_name(args.license_file)

    headers = singletons.login_manager.super_admin()

    update_licenses(license_file, headers)
