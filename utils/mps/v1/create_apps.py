import argparse
import json
from io import BytesIO
from pathlib import Path
from typing import Callable
from uuid import uuid4

from ... import singletons, utils_resources
from .. import utils_mps


def put_media_to_portal_app(
    media_file,
    portal_app_id: str,
    app_view_id: str,
    mps_headers: dict,
    media_purpose: str,
    index: int,
    media_metadata,
):
    with open(media_file, "rb") as fh:
        file_buffer = BytesIO(fh.read())
        files = {"media_file": ("filename", file_buffer, "image/png")}
        data = {"media_metadata": json.dumps(media_metadata)}
        r = utils_mps.generic_put(
            f"/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{media_purpose}/{index}",
            data=data,
            files=files,
            headers=mps_headers,
        )
        if singletons.general_settings.verbose_requests:
            print(r.json())
        assert r.status_code == 200


def put_app_ui_bundle(app_ui_bundle_file, app_id: str, mps_headers: dict):
    with open(app_ui_bundle_file, "rb") as fh:
        file_buffer = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", file_buffer, "application/zip")}
        r = utils_mps.generic_put(
            f"/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=mps_headers
        )
        if singletons.general_settings.verbose_requests:
            print(r.json())
        assert r.status_code == 200


def populate_media_data(
    app: dict, portal_app_id: str, app_view_id: str, mps_headers: dict
):
    media_files = app["media_data"]
    workflow_file = app["workflow_file"]
    portal_app_data_dir = app["portal_app_dir"]
    workflows = app["workflows"]
    # app peek
    if (
        "img_marketplace_before" in media_files
        and media_files["img_marketplace_before"].exists()
    ):
        data = {
            "caption": {"EN": "Peek before caption"},
            "alternative_text": {"EN": "Peek before alt text"},
        }
        put_media_to_portal_app(
            media_files["img_marketplace_before"],
            portal_app_id,
            app_view_id,
            mps_headers,
            "peek",
            0,
            data,
        )
    if (
        "img_marketplace_after" in media_files
        and media_files["img_marketplace_after"].exists()
    ):
        data = {
            "caption": {"EN": "Peek after caption"},
            "alternative_text": {"EN": "Peek after alt text"},
        }
        put_media_to_portal_app(
            media_files["img_marketplace_after"],
            portal_app_id,
            app_view_id,
            mps_headers,
            "peek",
            1,
            data,
        )
    # app banner
    if "img_slider_before" in media_files and media_files["img_slider_before"].exists():
        data = {
            "caption": {"EN": "Image banner before caption"},
            "alternative_text": {"EN": "Image banner before alt text"},
        }
        put_media_to_portal_app(
            media_files["img_slider_before"],
            portal_app_id,
            app_view_id,
            mps_headers,
            "banner",
            1,
            data,
        )
    if "img_slider_after" in media_files and media_files["img_slider_after"].exists():
        data = {
            "caption": {"EN": "Image banner after caption"},
            "alternative_text": {"EN": "Image banner after alt text"},
        }
        put_media_to_portal_app(
            media_files["img_slider_after"],
            portal_app_id,
            app_view_id,
            mps_headers,
            "banner",
            0,
            data,
        )

    if workflow_file.exists():
        for i, wf in enumerate(workflows):
            wf_file_path = portal_app_data_dir.joinpath(wf["file"])
            data = {
                "caption": {"EN": wf["en"], "DE": wf["de"]},
                "alternative_text": {
                    "EN": f"Workflow {i} alt text en",
                    "DE": f"Workflow {i} alt text de",
                },
            }
            put_media_to_portal_app(
                wf_file_path,
                portal_app_id,
                app_view_id,
                mps_headers,
                "workflow",
                i,
                data,
            )


def get_app_ead(app_data: dict, file: Path):
    app_dir = file.parent.joinpath(app_data["rel_path"]).absolute()
    ead_file = app_dir.joinpath("ead.json")
    ead_data = utils_resources.get_json_data(ead_file)
    return ead_data


def gather_app_data(
    app_rel_path: str, portal_app_data: dict, file: Path, app_reg_dir: str
):
    # ead
    # pprint(portal_app_data)
    app_dir = file.parent.joinpath(app_rel_path).absolute()
    ead_file = app_dir.joinpath("ead.json")
    ead_data = utils_resources.get_json_data(ead_file)
    app_ui_bundle_file = app_dir.joinpath("app_ui_bundle.zip")
    app_ui_file = app_dir.joinpath("app_ui_config.json")
    app_ui_config = {}
    if app_ui_file.exists():
        app_ui_config = utils_resources.get_json_data(app_ui_file)

    # Exception for Sample App UI
    app_ui_url = None
    if ead_data["namespace"] == "org.empaia.vendor.sample_frontend_app_01.v1":
        app_ui_url = "http://sample-app:80"

    app_reg_file_content = utils_resources.get_json_data(app_reg_dir)
    registry_url = app_reg_file_content[ead_data["namespace"]]

    # tags
    raw_tags = portal_app_data["tags"]
    tags = []
    for tag_group in raw_tags.keys():
        for tag in raw_tags[tag_group]:
            tags.append({"tag_group": tag_group, "tag_name": tag})

    # image data and workflow
    portal_app_dir = app_dir
    if "rel_path" in portal_app_data:
        portal_app_dir = file.parent.joinpath(portal_app_data["rel_path"]).absolute()
    portal_app_data_dir = portal_app_dir.joinpath("portal_data")
    media_data = {}
    media_data["img_marketplace_before"] = portal_app_data_dir.joinpath(
        "marketplace_before.png"
    )
    media_data["img_marketplace_after"] = portal_app_data_dir.joinpath(
        "marketplace_after.png"
    )
    media_data["img_slider_before"] = portal_app_data_dir.joinpath("slider_before.png")
    media_data["img_slider_after"] = portal_app_data_dir.joinpath("slider_after.png")
    workflow_file = portal_app_data_dir.joinpath("workflow.json")
    workflows = None
    app_description = {
        "en": "app description placeholder",
        "de": "App Beschreibung Platzhalter",
    }
    if workflow_file.exists():
        workflow_data = utils_resources.get_json_data(workflow_file)
        app_description = workflow_data["description"]
        workflows = workflow_data["workflows"]
        for wf in workflows:
            wf["file"] = portal_app_data_dir.joinpath(wf["file"])

    # active app
    active_app = {
        "ead": ead_data,
        "registry_image_url": registry_url,
    }
    if app_ui_url:
        active_app["app_ui_url"] = app_ui_url
    if app_ui_config:
        active_app["app_ui_configuration"] = app_ui_config

    app_data = {
        "portal_app_dir": portal_app_data_dir,
        "namespace": ead_data["namespace"],
        "active_app": active_app,
        "tags": tags,
        "app_description": app_description,
        "app_ui_bundle_file": app_ui_bundle_file,
        "media_data": media_data,
        "workflow_file": workflow_file,
        "workflows": workflows,
    }

    return app_data


def get_app_mapping_for_namespace(
    orga_apps_mapping: dict,
    namespace: dict,
    vendor_data: dict,
    orga_id_from_orga_name_fn: Callable,
):
    # read app ids and organizations from mapping file if exists
    portal_app_id = None
    app_id = None
    orga_id = None
    if orga_apps_mapping and namespace in orga_apps_mapping:
        orga_data = orga_apps_mapping[namespace]
        portal_app_id = orga_data.get("portal_app_id")
        app_id = orga_data.get("app_id")
        orga_id = orga_data.get("keycloak_id")
    if orga_id is None:
        organization_name = vendor_data["organization"]
        orga_id = orga_id_from_orga_name_fn(organization_name)
    return {
        "portal_app_id": portal_app_id,
        "app_id": app_id,
        "organization_id": orga_id,
    }


def build_post_portal_app_single_active_app(
    app: dict,
    app_name: str,
    api_version: str = None,
    listing_status: str = "LISTED",
):
    app_json = {
        "organization_id": app["organization_id"],
        "status": listing_status,
        "details": {
            "name": app_name,
            "description": app["app_description"],
        },
        "active_apps": {
            api_version: app["active_app"],
        },
        "tags": app["tags"],
    }
    params = {}
    if app["portal_app_id"] is not None:
        params = {"external_ids": True}
        app_json["active_apps"][api_version]["id"] = (
            app["app_id"] if app["app_id"] else str(uuid4())
        )
        app_json["id"] = app["portal_app_id"]

    return app_json, params


def build_post_portal_app(
    active_app_data: dict,
    app_name: str,
    listing_status: str,
):
    external_ids = False
    most_recent_active_app = None
    active_app_v1 = None
    active_app_v2 = None
    active_app_v3 = None
    if active_app_data["v3"] is not None:
        most_recent_active_app = active_app_data["v3"]
        active_app_v3 = active_app_data["v3"]["active_app"]
        if active_app_data["v3"]["app_id"] is not None:
            active_app_v3["id"] = active_app_data["v3"]["app_id"]
            external_ids = True
    if active_app_data["v2"] is not None:
        most_recent_active_app = active_app_data["v2"]
        active_app_v2 = active_app_data["v2"]["active_app"]
        if active_app_data["v2"]["app_id"] is not None:
            active_app_v2["id"] = active_app_data["v2"]["app_id"]
            external_ids = True
    if active_app_data["v1"] is not None:
        most_recent_active_app = active_app_data["v1"]
        active_app_v1 = active_app_data["v1"]["active_app"]
        if active_app_data["v1"]["app_id"] is not None:
            active_app_v1["id"] = active_app_data["v1"]["app_id"]
            external_ids = True

    app_json = {
        "organization_id": most_recent_active_app["organization_id"],
        "status": listing_status,
        "details": {
            "name": app_name,
            "description": most_recent_active_app["app_description"],
        },
        "active_apps": {
            "v1": active_app_v1,
            "v2": active_app_v2,
            "v3": active_app_v3,
        },
        "tags": most_recent_active_app["tags"],
    }

    if most_recent_active_app["portal_app_id"]:
        app_json["id"] = most_recent_active_app["portal_app_id"]
        external_ids = True

    params = {}
    if external_ids:
        params = {"external_ids": True}

    return app_json, params


def post_admin_portal_app(
    portal_app_json: dict, portal_app_id: str, params: dict, mps_headers: dict
):
    if portal_app_id:
        r = utils_mps.generic_get(
            f"/v1/public/portal-apps/{portal_app_id}",
            headers=mps_headers,
            raise_status=False,
        )
        if r.status_code != 404:
            params.update({"existing_portal_app": True})

    r = utils_mps.generic_post(
        "/v1/admin/portal-apps",
        json=portal_app_json,
        params=params,
        headers=mps_headers,
        raise_status=False,
    )

    return r


def apps_file_to_mps(
    filename: str,
    mps_headers: dict,
    orga_id_from_orga_name_fn: Callable,
    app_reg_dir: str,
    orga_apps_mapping_filename: str = None,
):
    file = Path(filename).absolute()
    apps_file_data = utils_resources.get_json_data(file)

    # get old apps from v0 mps
    orga_apps_mapping = None
    if orga_apps_mapping_filename:
        orga_app_file = Path(orga_apps_mapping_filename).absolute()
        orga_apps_mapping = utils_resources.get_json_data(orga_app_file)

    list_namespaces = []
    list_namespace_duplicates = {}

    for _app_vendor_key, vendor_data in apps_file_data.items():
        for portal_app_key, portal_app_data in vendor_data["portal_apps"].items():
            portal_app_id = None
            legacy_app_data = {"v1": [], "v2": [], "v3": []}
            active_app_data = {"v1": None, "v2": None, "v3": None}
            portal_app_data_status = portal_app_data.get(
                "status", "LISTED"
            )  # LISTED if not explicitly otherwise

            # Gather app data
            for api_version in ["v1", "v2", "v3"]:
                if api_version in portal_app_data["apps"]:
                    if "legacy" in portal_app_data["apps"][api_version]:
                        legacy_apps = []
                        for raw_legacy_app in portal_app_data["apps"][api_version][
                            "legacy"
                        ]:
                            app_data = gather_app_data(
                                raw_legacy_app["rel_path"],
                                portal_app_data,
                                file,
                                app_reg_dir,
                            )
                            app_orga_ids = get_app_mapping_for_namespace(
                                orga_apps_mapping,
                                app_data["namespace"],
                                vendor_data,
                                orga_id_from_orga_name_fn,
                            )
                            if app_orga_ids["portal_app_id"]:
                                portal_app_id = app_orga_ids["portal_app_id"]
                            app_data.update(app_orga_ids)
                            legacy_apps.append(app_data)

                            if (
                                app_data["namespace"] in list_namespaces
                                and app_data["namespace"]
                                not in list_namespace_duplicates.keys()
                            ):
                                list_namespace_duplicates[
                                    app_data["namespace"]
                                ] = app_orga_ids["app_id"]
                            list_namespaces.append(app_data["namespace"])

                        legacy_app_data[api_version].extend(legacy_apps)
                    if "active" in portal_app_data["apps"][api_version]:
                        app_data = gather_app_data(
                            portal_app_data["apps"][api_version]["active"]["rel_path"],
                            portal_app_data,
                            file,
                            app_reg_dir,
                        )
                        app_orga_ids = get_app_mapping_for_namespace(
                            orga_apps_mapping,
                            app_data["namespace"],
                            vendor_data,
                            orga_id_from_orga_name_fn,
                        )
                        if app_orga_ids["portal_app_id"]:
                            portal_app_id = app_orga_ids["portal_app_id"]
                        app_data.update(app_orga_ids)
                        active_app_data[api_version] = app_data

                        if (
                            app_data["namespace"] in list_namespaces
                            and app_data["namespace"]
                            not in list_namespace_duplicates.keys()
                        ):
                            list_namespace_duplicates[
                                app_data["namespace"]
                            ] = app_orga_ids["app_id"]
                        list_namespaces.append(app_data["namespace"])

            if portal_app_id is None:
                portal_app_id = str(uuid4())

            # Post legacy apps
            for api_version in ["v1", "v2", "v3"]:
                if legacy_app_data[api_version] != []:
                    for app in legacy_app_data[api_version]:
                        app["portal_app_id"] = portal_app_id
                        (
                            portal_app_json,
                            params,
                        ) = build_post_portal_app_single_active_app(
                            app, portal_app_key, api_version, portal_app_data_status
                        )
                        r = post_admin_portal_app(
                            portal_app_json, portal_app_id, params, mps_headers
                        )
                        resp = r.json()
                        if r.status_code == 400:
                            print(r.json()["detail"])
                            continue

                        assert r.status_code == 200
                        resp = r.json()
                        portal_app_id = resp["id"]
                        app_view_id = resp["active_app_views"][api_version]["id"]
                        app_id = resp["active_app_views"][api_version]["app"]["id"]
                        populate_media_data(
                            app, portal_app_id, app_view_id, mps_headers
                        )

                        app_ui_bundle_file = Path(app["app_ui_bundle_file"])
                        if app_ui_bundle_file.exists():
                            put_app_ui_bundle(app_ui_bundle_file, app_id, mps_headers)

            # Post active apps
            portal_app_json, params = build_post_portal_app(
                active_app_data, portal_app_key, portal_app_data_status
            )
            portal_app_json["id"] = portal_app_id

            # Workaround for aignostics app with same backend app twice
            # TODO: Refactor admin routes that they can handle this more comfortable!!
            post_app_v2 = None
            if (
                portal_app_json["active_apps"]["v1"]
                and portal_app_json["active_apps"]["v1"]["ead"]["namespace"]
                in list_namespace_duplicates.keys()
            ):
                post_app_v2 = {
                    "details": portal_app_json["details"],
                    "tags": portal_app_json["tags"],
                    "app": {
                        "id": list_namespace_duplicates.get(
                            portal_app_json["active_apps"]["v1"]["ead"]["namespace"]
                        )
                    },
                }
                del portal_app_json["active_apps"]["v2"]

            r = post_admin_portal_app(
                portal_app_json, portal_app_id, params, mps_headers
            )
            if r.status_code == 400:
                print(r.json()["detail"])
                continue

            assert r.status_code == 200

            if post_app_v2:
                params = {"api_version": "v2", "existing_app": True}
                r = utils_mps.generic_post(
                    f"/v1/admin/portal-apps/{portal_app_id}/app-views",
                    json=post_app_v2,
                    params=params,
                    headers=mps_headers,
                    raise_status=False,
                )
                assert r.status_code == 200

            r = utils_mps.generic_get(
                f"/v1/admin/portal-apps/{portal_app_id}",
                headers=mps_headers,
                raise_status=True,
            )
            resp = r.json()
            portal_app_id = resp["id"]

            for api_version in ["v1", "v2", "v3"]:
                if (
                    resp["active_app_views"][api_version]
                    and active_app_data[api_version]
                ):
                    app_view_id = resp["active_app_views"][api_version]["id"]
                    app_id = resp["active_app_views"][api_version]["app"]["id"]
                    populate_media_data(
                        active_app_data[api_version],
                        portal_app_id,
                        app_view_id,
                        mps_headers,
                    )

                    if active_app_data[api_version] and "app_ui_bundle_file" in dict(
                        active_app_data[api_version]
                    ):
                        app_ui_bundle_file = Path(
                            dict(active_app_data[api_version])["app_ui_bundle_file"]
                        )
                        if app_ui_bundle_file.exists():
                            put_app_ui_bundle(app_ui_bundle_file, app_id, mps_headers)

            params = {"status": portal_app_data_status, "comment": "Reviewed by admin"}

            r = utils_mps.generic_put(
                f"/v1/admin/portal-apps/{portal_app_id}/status",
                params=params,
                headers=mps_headers,
                raise_status=False,
            )
            resp = r.json()
            assert r.status_code == 200


def main(orga_id_from_orga_name_fn: Callable):
    parser = argparse.ArgumentParser()
    parser.add_argument("apps_file", help="Path to apps json file.")
    parser.add_argument("app_mapping_file", help="Path to app mapping file")
    parser.add_argument(
        "app_namespace_registry_file", help="Path to app namespace registry file"
    )
    args = parser.parse_args()

    apps_file = args.apps_file

    orga_apps_mapping_file = utils_resources.resolve_resource_file_name(
        args.app_mapping_file
    )

    headers = singletons.login_manager.super_admin()

    apps_file_to_mps(
        apps_file,
        headers,
        orga_id_from_orga_name_fn,
        args.app_namespace_registry_file,
        orga_apps_mapping_filename=orga_apps_mapping_file,
    )
