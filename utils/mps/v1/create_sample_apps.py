import argparse
from pathlib import Path
from typing import Callable

from ... import singletons, utils_resources
from .create_apps import apps_file_to_mps


def main(orga_id_from_orga_name_fn: Callable):
    parser = argparse.ArgumentParser()
    parser.add_argument("app_mapping_file", help="Path to app mapping file")
    parser.add_argument(
        "app_namespace_registry_file", help="Path to app namespace registry file"
    )
    args = parser.parse_args()

    orga_apps_mapping_file = utils_resources.resolve_resource_file_name(
        args.app_mapping_file
    )

    headers = singletons.login_manager.super_admin()

    apps_file = (
        Path(__file__)
        .parents[4]
        .joinpath(
            "sample_apps", "sample_apps", "meta_data", "portal_app_meta_data.json"
        )
    )

    apps_file_to_mps(
        apps_file,
        headers,
        orga_id_from_orga_name_fn,
        args.app_namespace_registry_file,
        orga_apps_mapping_filename=orga_apps_mapping_file,
    )
