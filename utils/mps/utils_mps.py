import requests

from ..singletons import general_settings, login_manager


def generic_post(
    url: str,
    data: str = None,
    json: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    files: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{str(general_settings.mps_host).rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.post(
        url=url, headers=headers, data=data, json=json, params=params, files=files
    )
    if general_settings.verbose_requests or r.status_code >= 400:
        print("POST ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_get(
    url: str,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{str(general_settings.mps_host).rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    if general_settings.verbose_requests:
        print(f"Headers: {headers}")
    r = requests.get(url=url, headers=headers, params=params)
    if general_settings.verbose_requests or r.status_code >= 400:
        print("GET ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_put(
    url: str,
    data: str = None,
    json: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    files: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{str(general_settings.mps_host).rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.put(
        url=url, headers=headers, data=data, json=json, params=params, files=files
    )
    if general_settings.verbose_requests or r.status_code >= 400:
        print("PUT ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_delete(
    url: str,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{str(general_settings.mps_host).rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    print(headers)
    r = requests.delete(url=url, headers=headers, params=params)
    if general_settings.verbose_requests or r.status_code >= 400:
        print("DELETE ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def create_vault_approle(role_id: str, secret_id: str, engine: str):
    VAULT_BASE_URL = general_settings.vault_host
    ROOT_TOKEN = general_settings.vault_root_token
    ROOT_HEADERS = {"X-Vault-Token": ROOT_TOKEN}

    policy_path = f"{engine}/data/*"
    policy_capabilities = '{capabilities = ["read"]}'
    policy_name = f"read-{engine}"
    data = {
        "name": policy_name,
        "policy": f'path "{policy_path}" {policy_capabilities}',
    }
    r = requests.put(
        f"{VAULT_BASE_URL}/v1/sys/policies/acl/{policy_name}",
        headers=ROOT_HEADERS,
        json=data,
    )
    assert r.status_code == 204

    # enable app role
    data = {"type": "approle"}
    r = requests.post(
        f"{VAULT_BASE_URL}/v1/sys/auth/approle", headers=ROOT_HEADERS, json=data
    )

    # create app role
    role_name = "marketplace-service"
    data = {"token_policies": [policy_name]}
    r = requests.post(
        f"{VAULT_BASE_URL}/v1/auth/approle/role/{role_name}",
        headers=ROOT_HEADERS,
        json=data,
    )
    assert r.status_code == 204

    # create custom app role_id
    data = {"role_id": role_id}
    r = requests.post(
        f"{VAULT_BASE_URL}/v1/auth/approle/role/{role_name}/role-id",
        headers=ROOT_HEADERS,
        json=data,
    )
    assert r.status_code == 204

    # create custom secret
    data = {"secret_id": secret_id}
    r = requests.post(
        f"{VAULT_BASE_URL}/v1/auth/approle/role/{role_name}/custom-secret-id",
        headers=ROOT_HEADERS,
        json=data,
    )
    if r.status_code == 500:
        # secret id already exists
        assert "is already registered" in r.json()["errors"][0]
    else:
        assert r.status_code == 200
