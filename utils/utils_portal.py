# from pprint import pprint

import requests

from .singletons import (
    compose_settings,
    general_settings,
    login_manager,
)


def generic_get(
    url: str,
    add_headers: dict = None,
    params: dict = None,
    raise_status: bool = True,
):
    headers = login_manager.super_admin()
    url = f"{compose_settings.aaa_frontend_auth_url.rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.get(url=url, headers=headers, params=params)
    if general_settings.verbose_requests or r.status_code >= 400:
        print("GET ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_post(
    url: str,
    data: str = None,
    json: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    files: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{compose_settings.aaa_frontend_auth_url.rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.post(
        url=url, headers=headers, data=data, json=json, params=params, files=files
    )
    if general_settings.verbose_requests or r.status_code >= 400:
        print("POST ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_put(
    url: str,
    data: str = None,
    json: dict = None,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    files: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{compose_settings.aaa_frontend_auth_url.rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.put(
        url=url, headers=headers, data=data, json=json, params=params, files=files
    )
    if general_settings.verbose_requests or r.status_code >= 400:
        print("PUT ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r


def generic_delete(
    url: str,
    add_headers: dict = None,
    params: dict = None,
    headers: dict = None,
    raise_status: bool = True,
):
    if headers is None:
        headers = login_manager.super_admin()
    url = f"{compose_settings.aaa_frontend_auth_url.rstrip('/')}{url}"
    if add_headers:
        for k, v in add_headers.items():
            headers[k] = v
    r = requests.delete(url=url, headers=headers, params=params)
    if general_settings.verbose_requests or r.status_code >= 400:
        print(headers)
        print("DELETE ", url)
        print("status: ", r.status_code)
        print("content: ", r.content)
    if raise_status:
        r.raise_for_status()
    return r
