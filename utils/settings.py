from pathlib import Path

from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class ComposeSettings(BaseSettings):
    aaa_frontend_auth_url: str = ""
    aaa_client_id: str = ""
    aaa_client_secret: str = ""
    aaa_swagger_server_url: str = ""
    mps_vault_url: AnyHttpUrl = ""
    mps_vault_role_id: str = ""
    mps_vault_secret_id: str = ""
    keycloak_user: str = ""
    keycloak_password: str = ""

    model_config = SettingsConfigDict(env_file=".env", extra="allow")


class GeneralSettings(BaseSettings):
    auth_service_openapi_url_suffix: str = "/v3/api-docs"
    vault_openapi_url_suffix: str = "/v3/api-docs"
    auth_service_host: AnyHttpUrl = None
    mps_host: AnyHttpUrl = None
    eves_host: AnyHttpUrl = None
    maildev_host: AnyHttpUrl = None
    vault_host: AnyHttpUrl = None
    allow_data_wipe: bool = False
    vault_create_approle: bool = False
    vault_root_token: str = ""
    resources_path: Path = Path(__file__).parent.parent / "resources"
    verbose_requests: bool = False
    drop_all_data_before_running_tests: bool = False

    model_config = SettingsConfigDict(
        env_file=".env", env_prefix="pytest_", extra="allow"
    )


# Note: values must be optional as organizations have different sets of clients
# e.g. COMPUTE_PROVIDER can only have JES, while AI_CUSTOMER most likely has no JES
class PlatformSettings(BaseSettings):
    idp_url: AnyHttpUrl = None
    wbs_organization_id: str = ""
    wbs_client_id: str = ""
    wbs_client_secret: str = ""
    jes_client_id: str = ""
    jes_client_secret: str = ""
    as_client_id: str = ""
    as_client_secret: str = ""
    wbc_client_id: str = ""
    patho_user_name: str = ""
    patho_user_password: str = ""
    mdc_client_id: str = ""
    mta_user_name: str = ""
    mta_user_password: str = ""

    model_config = SettingsConfigDict(
        env_file=".env", env_prefix="login_", extra="allow"
    )
