import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.singletons import general_settings, login_manager

from ...utils import api_auth_check

EXCLUDED_PATHS = []
MPS_URL = general_settings.mps_host


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_accept_customer_wbs(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).workbench_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": org_id_correct,
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=True,
        openapi_suffix="openapi.json",
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_rejected_customer_wbs_wrong_header(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).workbench_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": "1234",  # wrong ID
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=False,
        openapi_suffix="openapi.json",
        expected_reject_code=403,
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_rejected_customer_wbs_wrong_organizations(
    create_orga_and_clients_test_data,
):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]
    org_id_incorrect = orga_data[1]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_incorrect).workbench_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": org_id_correct,
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=False,
        openapi_suffix="openapi.json",
        expected_reject_code=403,
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_accept_customer_aps(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[1]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).app_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": org_id_correct,
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=True,
        openapi_suffix="openapi.json",
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_rejected_customer_aps_wrong_organization_id_header(
    create_orga_and_clients_test_data,
):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).app_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": "1234",
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=False,
        openapi_suffix="openapi.json",
        expected_reject_code=403,
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_accept_customer_jes(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).job_execution_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": org_id_correct,
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=True,
        openapi_suffix="openapi.json",
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_rejected_customer_jes_wrong_organization_id_header(
    create_orga_and_clients_test_data,
):
    orga_data = create_orga_and_clients_test_data
    org_id_correct = orga_data[0]["organization"]["organization_id"]  # test_orga_1 id
    org_id_false = orga_data[1]["organization"]["organization_id"]  # test_orga_2 id

    client_credentials = (
        AdminController().get_client_credentials(org_id_correct).job_execution_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": org_id_false,
        "Authorization": f"Bearer {token}",
    }

    api_auth_check(
        MPS_URL,
        headers,
        ["config"],
        url_suffix="/v1/customer/",
        auth_expected=False,
        openapi_suffix="openapi.json",
        expected_reject_code=403,
    )
