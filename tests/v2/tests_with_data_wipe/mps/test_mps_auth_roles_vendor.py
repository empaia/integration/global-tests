# # TODO: currently vendor routes are disabled
# import pytest
# import requests

# from uuid import uuid4
# from tests.utils.auth_service.api_controllers.v2.client_controller import (
#     ClientControllerV2,
# )
# from tests.utils.singletons import general_settings, login_manager

# from ...utils import api_auth_check

# EXCLUDED_PATHS = []
# MPS_URL = general_settings.mps_host


# @pytest.mark.skipif(
#     login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
# )
# def test_mps_auth_reject_vendor():
#     headers = login_manager.super_admin()
#     for api in ["vendor"]:
#         api_auth_check(
#             MPS_URL,
#             headers,
#             EXCLUDED_PATHS,
#             url_suffix=f"/v1/{api}/",
#             auth_expected=True,
#             openapi_suffix="openapi.json",
#         )
