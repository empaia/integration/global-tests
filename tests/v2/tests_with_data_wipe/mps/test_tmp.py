import pytest

from tests.utils.singletons import general_settings, login_manager

EXCLUDED_PATHS = []
MPS_URL = general_settings.mps_host


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test02(create_orga_and_clients_test_data):
    _orga_data = create_orga_and_clients_test_data
    _token, _decoded_token = login_manager.get_access_flow_tokens(
        client_id="portal_client",
        user_name="admin_1@pp2.empaia.org",
        password="Password123!",
    )
