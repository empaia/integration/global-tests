import pytest
import requests

# from tests.utils.auth_service.api_controllers.v2.client_controller import (
#     ClientControllerV2,
# )
from tests.utils.singletons import general_settings, login_manager

# from ...utils import api_auth_check

EXCLUDED_PATHS = []
MPS_URL = general_settings.mps_host


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_accept_product_provider(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    orga_1_id = orga_data[-2]["organization"]["organization_id"]
    password = orga_data[-2]["organization_users"][0]["password"]
    user_name = orga_data[-2]["organization_users"][0]["user"]["contact_data"][
        "email_address"
    ]

    client_id = login_manager.settings.portal_client_id
    print(client_id)
    print(user_name)
    print(password)

    token, decoded_token = login_manager.get_access_flow_tokens(
        client_id=client_id, user_name=user_name, password=password
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": orga_1_id,
        "Authorization": f"Bearer {token}",
    }

    r = requests.get(f"{MPS_URL}v1/product-provider/clearances", headers=headers)
    assert r.status_code != 401 or r.status_code != 403


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_reject_product_provider_wrong_org(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    orga_1_id = orga_data[-1]["organization"]["organization_id"]  # other orga
    password = orga_data[-2]["organization_users"][0]["password"]
    user_name = orga_data[-2]["organization_users"][0]["user"]["contact_data"][
        "email_address"
    ]

    client_id = login_manager.settings.portal_client_id

    print(client_id)
    print(user_name)
    print(password)

    token, decoded_token = login_manager.get_access_flow_tokens(
        client_id=client_id, user_name=user_name, password=password
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": orga_1_id,
        "Authorization": f"Bearer {token}",
    }

    r = requests.get(f"{MPS_URL}v1/product-provider/clearances", headers=headers)
    assert r.status_code == 403


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_reject_product_provider_super_admin_not_in_org(
    create_orga_and_clients_test_data,
):
    orga_data = create_orga_and_clients_test_data
    orga_1_id = orga_data[-1]["organization"]["organization_id"]  # other orga

    client_id = login_manager.settings.portal_client_id
    user_name = login_manager.settings.super_admin_name
    password = login_manager.settings.super_admin_password

    print(client_id)
    print(user_name)
    print(password)

    token, decoded_token = login_manager.get_access_flow_tokens(
        client_id=client_id, user_name=user_name, password=password
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": orga_1_id,
        "Authorization": f"Bearer {token}",
    }

    r = requests.get(f"{MPS_URL}v1/product-provider/clearances", headers=headers)
    assert r.status_code == 403
