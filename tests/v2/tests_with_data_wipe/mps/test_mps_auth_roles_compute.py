import pytest
import requests

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.singletons import general_settings, login_manager

from ...utils import api_auth_check

EXCLUDED_PATHS = []
MPS_URL = general_settings.mps_host


def test_mps_auth_accept_compute_provider_jes(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    orga_1_id = orga_data[0]["organization"]["organization_id"]
    orga_cp_id = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(orga_cp_id).job_execution_service
    )

    token, _ = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "Authorization": f"Bearer {token}",
    }

    r = requests.get(
        f"{MPS_URL}v1/compute/organizations/{orga_1_id}/apps/b10648a7-340d-43fc-a2d9-4d91cc86f33f",
        headers=headers,
    )
    assert r.status_code != 401 or r.status_code != 403


def test_mps_auth_reject_compute_provider_no_token():
    api_auth_check(
        MPS_URL,
        {},
        [],
        url_suffix="/v1/compute/",
        auth_expected=False,
        expected_reject_code=403,
        openapi_suffix="openapi.json",
    )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_reject_customer_jes_token(create_orga_and_clients_test_data):
    orga_data = create_orga_and_clients_test_data
    orga_1_id = orga_data[0]["organization"]["organization_id"]

    client_credentials = (
        AdminController().get_client_credentials(orga_1_id).job_execution_service
    )

    token, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client_credentials.client_id, client_secret=client_credentials.secret
    )

    headers = {
        "user-id": decoded_token["sub"],
        "organization-id": orga_1_id,
        "Authorization": f"Bearer {token}",
    }

    r = requests.get(
        f"{MPS_URL}v1/compute/organizations/{orga_1_id}/apps/b10648a7-340d-43fc-a2d9-4d91cc86f33f",
        headers=headers,
    )
    assert r.status_code == 403
