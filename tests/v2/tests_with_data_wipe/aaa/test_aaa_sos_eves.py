from pathlib import Path
from time import sleep

import pytest
import requests
import sseclient

from tests.data_generator_utils import (
    datagen_organization,
    datagen_user,
)
from tests.utils import singletons
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationUserRole,
)

user_file = (
    Path(__file__).parents[4].joinpath("resources/global_deployment/test_user.json")
)


def test_create_orga_send_mail(create_orga_and_clients_test_data):
    r = requests.get(f"{singletons.general_settings.maildev_host}email")
    assert r.status_code == 200
    initial_mails = r.json()

    for mail in initial_mails:
        assert mail["html"]
        assert mail["text"]
        assert str(mail["subject"]).startswith("EMPAIA | ")
        assert mail["envelope"]["from"]["address"] == "test-noreply@empaia.org"

    user_profile, post_user_data = datagen_user.create_user_from_resource_file(
        user_file
    )
    orga_name = create_orga_and_clients_test_data[0]["organization"][
        "organization_name"
    ]

    datagen_organization.add_user_to_organization(
        user_profile,
        post_user_data,
        orga_name,
        organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
    )

    sleep(1)
    r = requests.get(f"{singletons.general_settings.maildev_host}email")
    assert r.status_code == 200
    updated_mails = r.json()

    # four new mails should be sent
    # 1. + 2. Approval requests to admins
    # 3. Membership approval to test_user
    # 4. User role has been altered for test user
    assert len(initial_mails) == len(updated_mails) - 4

    # mails are not always ordered correctly, especially -2 and -3 as they have different receivers
    for mail in [
        updated_mails[len(updated_mails) - 1],
        updated_mails[len(updated_mails) - 2],
        updated_mails[len(updated_mails) - 3],
    ]:
        assert (
            (
                mail["subject"]
                == "EMPAIA | Organization membership role has been altered"
                and mail["envelope"]["from"]["address"] == "test-noreply@empaia.org"
                and mail["envelope"]["to"][0]["address"] == "test_user@empaia.org"
            )
            or (
                mail["subject"] == "EMPAIA | Approved – organization membership"
                and mail["envelope"]["from"]["address"] == "test-noreply@empaia.org"
                and mail["envelope"]["to"][0]["address"] == "test_user@empaia.org"
            )
            or (
                mail["subject"] == "EMPAIA | Action required – organization membership"
                and mail["envelope"]["from"]["address"] == "test-noreply@empaia.org"
            )
        )


def test_subscribe_to_eves_and_receive_sse_as_orga_manager(
    create_orga_and_clients_test_data,
):
    user_id = create_orga_and_clients_test_data[0]["organization_users"][0]["user"][
        "user_id"
    ]
    orga_name = create_orga_and_clients_test_data[0]["organization"][
        "organization_name"
    ]

    sse_headers = {
        "user-id": user_id,
        "Accept": "text/event-stream",
    }

    r = requests.get(
        f"{singletons.general_settings.eves_host}v1/events",
        headers=sse_headers,
        stream=True,
    )
    r.raise_for_status()
    client = sseclient.SSEClient(r)

    user_profile, post_user_data = datagen_user.create_user_from_resource_file(
        user_file, allow_existing_user=False, recreate_user=True
    )
    datagen_organization.add_user_to_organization(
        user_profile,
        post_user_data,
        orga_name,
        organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
    )

    events = []

    sleep(1)
    for event in client.events():
        if event.event == "ping":
            continue

        events.append(event.event)
        if len(events) == 3:
            break

    assert "MEMBERSHIP_REQUEST" in events
    assert "NOTIFICATION" in events


def test_subscribe_to_eves_and_receive_sse_as_user_joining_orga(
    create_orga_and_clients_test_data,
):
    user_profile, post_user_data = datagen_user.create_user_from_resource_file(
        user_file, allow_existing_user=False, recreate_user=True
    )
    orga_name = create_orga_and_clients_test_data[1]["organization"][
        "organization_name"
    ]

    sse_headers = {
        "user-id": user_profile.user_id,
        "Accept": "text/event-stream",
    }

    r = requests.get(
        f"{singletons.general_settings.eves_host}v1/events",
        headers=sse_headers,
        stream=True,
    )
    r.raise_for_status()
    client = sseclient.SSEClient(r)

    datagen_organization.add_user_to_organization(
        user_profile,
        post_user_data,
        orga_name,
        organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
    )

    events = []

    sleep(1)
    for event in client.events():
        if event.event == "ping":
            continue

        events.append(event.event)
        if len(events) == 3:
            break

    assert "MEMBERSHIP_REQUEST" in events
    assert "NOTIFICATION" in events


@pytest.mark.skip("Needs investigation: Runs locally but fails in CI")
def test_subscribe_to_eves_and_receive_sse_as_super_admin(
    create_orga_and_clients_test_data,
):
    super_admin_sse_header = singletons.login_manager.super_admin()
    super_admin_sse_header["Accept"] = "text/event-stream"

    r = requests.get(
        f"{singletons.general_settings.eves_host}v1/events",
        headers=super_admin_sse_header,
        stream=True,
    )
    r.raise_for_status()
    client = sseclient.SSEClient(r)

    events = ["UNAPPROVED_ORGANIZATION", "NOTIFICATION", "MEMBERSHIP_REQUEST"]

    for event in client.events():
        if event.event == "ping":
            continue
        if len(events) == 0:
            break

        if event.event in events:
            events.remove(event.event)

    assert True
