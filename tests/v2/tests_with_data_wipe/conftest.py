from pathlib import Path

import pytest
import requests

from tests.data_generator_utils import (
    datagen_configure_deployments,
    datagen_organization,
)
from tests.data_generators.auth_service.v2 import (
    wipe_all_auth_service_data,
)
from tests.utils import singletons

orga_file = (
    Path(__file__).parents[3].joinpath("resources/global_deployment/test_orgas.json")
)
configuration_file = (
    Path(__file__)
    .parents[3]
    .joinpath("resources/global_deployment/test_deployment_configurations.json")
)


@pytest.fixture(scope="session", autouse=True)
def create_orga_and_clients_test_data(request):
    wipe_all_auth_service_data.wipe_auth_service_data()

    orgas_and_users = (
        datagen_organization.create_organizations_and_users_from_resource_file(
            orga_file, recreate_entities=True
        )
    )
    datagen_configure_deployments.configure_deployments_from_resource_file(
        configuration_file
    )

    def tear_down():
        wipe_all_auth_service_data.wipe_auth_service_data()

    request.addfinalizer(tear_down)

    return orgas_and_users


@pytest.fixture(scope="session", autouse=True)
def clean_up_mails():
    r = requests.delete(f"{singletons.general_settings.maildev_host}email/all")
    assert r.status_code == 200
