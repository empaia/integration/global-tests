import json

import requests

APP_ID = "test_app_id"


def api_auth_check(
    base_url,
    headers,
    excluded_paths,
    url_suffix="",
    openapi_suffix="/openapi.json",
    auth_expected=True,
    expected_reject_code=401,
    scope_id=None,
):
    suffix = f"{url_suffix}{openapi_suffix}".lstrip("/")
    r = requests.get(f"{base_url}{suffix}")
    openapi = json.loads(r.content)
    if headers:
        expected_status_code = "!= 401" if auth_expected else f"{expected_reject_code}"
    else:
        expected_status_code = f"{expected_reject_code}"
    for path, ops in openapi["paths"].items():
        excluded = False
        excluded = any(p in path for p in excluded_paths)
        if excluded:
            continue
        path = path.replace("scope_id", str(scope_id))
        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{str(base_url).rstrip('/')}{url_suffix.rstrip('/')}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url, headers=headers)
            print(
                f"AUTH - '{op}': '{url}', - STATUS CODE: '{r.status_code}' - EXPECTED: '{expected_status_code}'"
            )
            print(f"Response: {r.content}")
            if auth_expected:
                assert (
                    r.status_code != 401 and r.status_code != 403
                )  # currently not working and r.status_code != 500
            else:
                assert r.status_code == expected_reject_code
