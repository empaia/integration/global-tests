import pytest

from tests.utils.singletons import general_settings, login_manager

from ..utils import api_auth_check

EXCLUDED_PATHS = []
MPS_URL = general_settings.mps_host


# ACCEPTED #


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_accepted_super_admin():
    headers = login_manager.super_admin()
    for api in ["public", "admin"]:
        api_auth_check(
            MPS_URL,
            headers,
            EXCLUDED_PATHS,
            url_suffix=f"/v1/{api}/",
            auth_expected=True,
            openapi_suffix="openapi.json",
        )


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_reject_super_admin():
    headers = login_manager.super_admin()
    for api in ["customer", "product-provider", "vendor", "reviewer"]:
        api_auth_check(
            MPS_URL,
            headers,
            EXCLUDED_PATHS,
            url_suffix=f"/v1/{api}/",
            auth_expected=False,
            expected_reject_code=403,  # super admin is not in organzation and must not access these routes
            openapi_suffix="openapi.json",
        )


# NO TOKEN #
@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_mps_auth_rejected_no_token():
    for api in ["customer", "product-provider", "vendor", "reviewer", "admin"]:
        api_auth_check(
            MPS_URL,
            None,
            EXCLUDED_PATHS,
            url_suffix=f"/v1/{api}/",
            auth_expected=False,
            expected_reject_code=403,
            openapi_suffix="openapi.json",
        )


def test_public_api_no_token():
    api_auth_check(
        MPS_URL,
        None,
        EXCLUDED_PATHS,
        expected_reject_code=200,
        url_suffix="/v1/public/",
        openapi_suffix="openapi.json",
    )
