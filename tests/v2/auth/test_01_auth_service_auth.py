import pytest

from tests.utils.singletons import general_settings, login_manager

from ..utils import api_auth_check

AUTH_SERVICE_URL = general_settings.auth_service_host


# ACCEPTED #


# ACCEPTED AUTH - Super Admin
def test_auth_service_auth_accepted_super_admin():
    headers = login_manager.super_admin()
    api_auth_check(
        AUTH_SERVICE_URL,
        headers,
        [],
        openapi_suffix=f"{general_settings.auth_service_openapi_url_suffix}/v2",
    )


# NO TOKEN #


@pytest.mark.skipif(
    login_manager.auth_mode == "off", reason="auth_mode is set to 'off'"
)
def test_auth_service_auth_rejected_no_token():
    EXCLUDED_PATHS = ["public", "domain"]
    api_auth_check(
        AUTH_SERVICE_URL,
        None,
        EXCLUDED_PATHS,
        auth_expected=False,
        expected_reject_code=401,
        openapi_suffix=general_settings.auth_service_openapi_url_suffix,
    )
