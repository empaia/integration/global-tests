import json
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import MagicMock

import pytest

from ...data_generator_utils import (
    wait_for_auth_service_to_be_available,
)
from ...keycloak_initialize.empaia_keycloak_admin import (
    EmpaiaKeycloakAdmin,
)
from ...keycloak_initialize.global_clients_migrator import (
    GlobalClientsMigrator,
)
from ...utils import tools
from ...utils.singletons import api_test_state
from .test_keycloak_initialize import (
    FileBackup,
    assert_no_empaia_realm,
    cleanup_empaia_realm,
    get_auth_service_initializer,
    run_command,
    run_keycloak_initialize,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_keycloak_initialize_test,
    reason="Skipping keycloak_initialize.py tests",
)


def setup_teardown():
    # setup here
    assert_no_empaia_realm()
    yield
    # tear down here
    cleanup_empaia_realm()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_migrate_global_clients():
    test_deployment_directory = (
        Path(os.path.abspath(os.path.dirname(__file__)))
        / "../../resources/test/test_deployment"
    )
    os.chdir(test_deployment_directory)

    prepare_test_deployment(test_deployment_directory)
    root_directory = Path(os.path.abspath(os.path.dirname(__file__))) / "../../.."
    # we need to temporarily create a new-style .initialize.env file with old client data, because
    # keycloak_initialize.py can no more read the old-style initialize.env file:
    with (
        test_deployment_directory
        / "original_files/new_style_initialize_with_old_client_data.env"
    ).open() as f:
        content = f.read()

    initialize_env_backup = FileBackup(root_directory / ".initialize.env", content)
    try:
        run_keycloak_initialize()
        keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
        # initially, auth service client has old client id in keycloak:
        auth_service_client_data = keycloak_admin.get_client_by_client_id(
            "org.empaia.prod.aaa"
        )
        assert auth_service_client_data is not None
        assert auth_service_client_data["name"] == "AAA Service"
    finally:
        initialize_env_backup.restore()

    # start auth service container:
    with (root_directory / ".env").open() as f:
        content = f.read().replace(
            "AAA_CLIENT_ID=auth_service_client", "AAA_CLIENT_ID=org.empaia.prod.aaa"
        )
    env_backup = FileBackup(root_directory / ".env", content)
    try:
        run_command(
            ["docker", "compose", "--profile", "pytest", "up", "-d"], root_directory
        )
        wait_for_auth_service_to_be_available()

        # create global_services organization and clients:
        run_command(
            [
                "python",
                "-m",
                "tests.data_generators.auth_service.v2.create_organizations",
                test_deployment_directory / "global_services_organizations.json",
            ],
            root_directory,
        )
    finally:
        # stop services and restart keycloak again:
        run_command(
            ["docker", "compose", "--profile", "pytest", "stop", "auth-service"],
            root_directory,
        )
        env_backup.restore()
    # migrate global clients:
    run_keycloak_initialize(
        "--migrate-global-clients",
        "--dummy-organization-name",
        "global_services",
        "--clients-file",
        test_deployment_directory / "global_services_clients.json",
        exec_directory=str(test_deployment_directory),
    )
    keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
    # auth service client still has old client id in keycloak:
    auth_service_client_data = keycloak_admin.get_client_by_client_id(
        "org.empaia.prod.aaa"
    )
    assert auth_service_client_data is not None
    assert auth_service_client_data["name"] == "AAA Service"
    # update the global clients and verify the renaming
    run_keycloak_initialize(
        "--update-global-clients", exec_directory=str(test_deployment_directory)
    )
    auth_service_client_data = keycloak_admin.get_client_by_client_id(
        "org.empaia.prod.aaa"
    )
    assert auth_service_client_data is None
    auth_service_client_data = keycloak_admin.get_client_by_client_id(
        "auth_service_client"
    )
    assert auth_service_client_data is not None
    assert auth_service_client_data["name"] == "Auth Service Client"


def prepare_test_deployment(test_deployment_directory: Path):
    for path in test_deployment_directory.iterdir():
        if path.is_file():
            os.remove(path)
    copy_test_deployment_files(test_deployment_directory, test_deployment_directory)


def copy_test_deployment_files(test_deployment_directory: Path, target_directory: Path):
    original_files_directory = test_deployment_directory / "original_files"
    for file_path in original_files_directory.iterdir():
        if file_path.name != "new_style_initialize_with_old_client_data.env":
            shutil.copyfile(file_path, target_directory / file_path.name)
    shutil.copyfile(target_directory / "sample.env", target_directory / ".env")
    shutil.copyfile(
        target_directory / "sample.initialize.env",
        target_directory / ".initialize.env",
    )


OLD_CLIENTS_FILE_CONTENT = {
    "global_services": {
        "clients": {"jes": {}, "sss": {}, "vs": {}},
        "internal_mappings": [{"from": "jes", "to": "sss"}],
        "external_mappings": [],
    }
}

# The expected new clients should have the vs and sss removed. vs has no replacement, sss is replaced by the
# global marketplace service. jes remains as organization client.
EXPECTED_NEW_CLIENTS_FILE_CONTENT = {
    "global_services": {
        "clients": {
            "jes": {"client_service_roles": ["SERVICE_COMPUTE_PROVIDER"]},
        },
        "internal_mappings": [],
        "external_mappings": [],
    }
}


def create_clients_file(clients_file: Path):
    with clients_file.open("w") as f:
        json.dump(OLD_CLIENTS_FILE_CONTENT, f, indent=4)


def test_global_clients_migrator():
    test_deployment_directory = (
        Path(os.path.abspath(os.path.dirname(__file__)))
        / "../../resources/test/test_deployment"
    )

    keycloak_admin = MagicMock(spec=EmpaiaKeycloakAdmin)
    keycloak_admin.get_client_by_client_id.return_value = {"root_url": "${authBaseUrl}"}
    migrator = GlobalClientsMigrator(keycloak_admin)
    dummy_organization_name = "global_services"
    with tempfile.TemporaryDirectory() as temp_directory:
        deployment_directory = Path(temp_directory)
        copy_test_deployment_files(test_deployment_directory, deployment_directory)
        clients_file = deployment_directory / "global_services_clients.json"
        create_clients_file(clients_file)
        migrator.migrate(
            dummy_organization_name,
            clients_file,
            deployment_directory,
        )

        old_clients_file = Path(f"{clients_file}.old")
        with old_clients_file.open(encoding="utf8") as f:
            old_json = json.load(f)
        tools.assert_dicts_are_equal(
            old_json,
            "actual old clients",
            OLD_CLIENTS_FILE_CONTENT,
            "expected old clients",
        )

        with clients_file.open() as f:
            new_json = json.load(f)
        tools.assert_dicts_are_equal(
            new_json,
            "actual new clients",
            EXPECTED_NEW_CLIENTS_FILE_CONTENT,
            "expected new clients",
        )
