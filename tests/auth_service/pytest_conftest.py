from _pytest.config import Config
from _pytest.config.argparsing import Parser

from tests.utils.api_test import api_tests


def pytest_addoption(parser: Parser):
    parser.addoption("--access-role", action="append")
    parser.addoption("--endpoint", action="append")
    parser.addoption("--method", action="append")
    parser.addoption("--repeat-last-failed-request", action="store_true")
    parser.addoption("--skip-schemathesis", action="store_true")
    parser.addoption("--api-version", action="store")
    parser.addoption("--test-functional-only", action="store_true")
    parser.addoption("--test-data-migration-only", action="store_true")
    parser.addoption("--test-keycloak-initialize-only", action="store_true")


def get_config_option(config, name, default):
    value = config.getoption(name, default=default)
    if value is None:
        value = default
    return value


def pytest_configure(config: Config):
    api_tests.setup_api_state(
        access_role_names=config.getoption("access_role", default=None),
        endpoint=config.getoption("endpoint", default=None),
        method=config.getoption("method", default=None),
        repeat_last_failed_request=get_config_option(
            config, "repeat_last_failed_request", default=False
        ),
        skip_schemathesis=get_config_option(config, "skip_schemathesis", default=False),
        api_version=get_config_option(config, "api_version", default="all"),
        test_functional_only=get_config_option(
            config, "test_functional_only", default=False
        ),
        test_data_migration_only=get_config_option(
            config, "test_data_migration_only", default=False
        ),
        test_keycloak_initialize_only=get_config_option(
            config, "test_keycloak_initialize_only", default=False
        ),
    )
