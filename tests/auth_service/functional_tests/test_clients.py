import pytest

from tests.data_generator_utils.datagen_user import create_user
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
    PostOrganizationDeploymentConfiguration,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils.datagen_wipe import (
    assert_all_data_was_wiped,
)
from .utils.data_creation_utils import (
    change_organization_categories,
    create_organization,
    create_user,
    join_organization,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    assert_all_data_was_wiped()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_client_access_without_organization_category():
    my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()

    organization = my_unapproved_organizations_controller.create_new_organization(
        "test-organization@test.empaia.org", auth_headers=login_manager.super_admin()
    )
    try:
        my_unapproved_organizations_controller.request_organization_approval(
            organization.organization_id, auth_headers=login_manager.super_admin()
        )
        ModeratorController().accept_organization(organization.organization_id)
        credentials = AdminController().get_client_credentials(
            organization.organization_id
        )
        for client in vars(credentials):
            client_credential = getattr(credentials, client)
            assert client_credential is None
    finally:
        AdminController().delete_organization(organization.organization_id)


def test_client_access_organization_app_customer_category():
    my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()

    organization = my_unapproved_organizations_controller.create_new_organization(
        "test-organization@test.empaia.org",
        auth_headers=login_manager.super_admin(),
        organization_categories=[OrganizationCategory.APP_CUSTOMER],
    )
    try:
        organization_id = organization.organization_id
        my_unapproved_organizations_controller.request_organization_approval(
            organization_id, auth_headers=login_manager.super_admin()
        )
        ModeratorController().accept_organization(organization.organization_id)
        credentials = AdminController().get_client_credentials(organization_id)

        # All clients should be enabled
        for client in vars(credentials):
            if client == "workbench_client" or client == "data_management_client":
                continue  # skip frontend clients as these use credentials flow

            client_credential = getattr(credentials, client)

            # should not have compute provider JES credentials
            if client == "compute_provider_job_execution_service":
                assert client_credential is None
                continue

            _, decoded_token = login_manager.get_client_credentials_flow_tokens(
                client_credential.client_id, client_credential.secret
            )

            aud = decoded_token["aud"]
            aud = [aud] if not isinstance(aud, list) else aud
            aud.remove("account")  # this does not need to be checked

            orgas = decoded_token["organizations"]

            if client == "workbench_service":
                assert len(aud) == 4
                assert "marketplace_service_client" in aud
                assert f"{organization_id}.idms" in aud
                assert f"{organization_id}.mds" in aud
                assert (
                    f"{organization_id}.jes" in aud
                )  # no external jes, so jes client is enabled
                assert organization_id in orgas
                assert ["service-app-reader"] == orgas[organization_id]["roles"]
            if client == "medical_data_service":
                assert len(aud) == 0
                assert organization_id in orgas
                assert [] == orgas[organization_id]["roles"]
            if client == "id_mapper_service":
                assert len(aud) == 0
                assert organization_id in orgas
                assert [] == orgas[organization_id]["roles"]
            if client == "upload_service":
                assert len(aud) == 0
                assert organization_id in orgas
                assert [] == orgas[organization_id]["roles"]
            if client == "app_service":
                assert len(aud) == 2
                assert "marketplace_service_client" in aud
                assert f"{organization_id}.mds" in aud
                assert organization_id in orgas
                assert (
                    sorted(["service-app-config-reader", "service-app-reader"])
                    == orgas[organization_id]["roles"]
                )
            if client == "job_execution_service":
                assert len(aud) == 1
                assert "marketplace_service_client" in aud
                assert organization_id in orgas
                assert sorted(["service-app-reader"]) == orgas[organization_id]["roles"]
    finally:
        AdminController().delete_organization(organization.organization_id)


def test_client_access_organization_app_compute_provider():
    my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()

    organization = my_unapproved_organizations_controller.create_new_organization(
        "test-organization@test.empaia.org",
        auth_headers=login_manager.super_admin(),
        organization_categories=[OrganizationCategory.COMPUTE_PROVIDER],
    )
    try:
        organization_id = organization.organization_id
        my_unapproved_organizations_controller.request_organization_approval(
            organization_id, auth_headers=login_manager.super_admin()
        )
        ModeratorController().accept_organization(organization.organization_id)
        credentials = AdminController().get_client_credentials(organization_id)

        # All clients should be enabled
        for client in vars(credentials):
            client_credential = getattr(credentials, client)
            if client == "compute_provider_job_execution_service":
                _, decoded_token = login_manager.get_client_credentials_flow_tokens(
                    client_credential.client_id, client_credential.secret
                )

                aud = decoded_token["aud"]
                orgas = decoded_token["organizations"]
                assert len(aud) == 2
                assert "marketplace_service_client" in aud
                assert f"{organization_id}.cpjes" not in aud
                assert organization_id in orgas
                assert "service-compute-provider" in orgas[organization_id]["roles"]
                assert "service-app-reader" in orgas[organization_id]["roles"]
            else:
                assert client_credential is None
    finally:
        AdminController().delete_organization(organization.organization_id)


def test_configure_clients():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization = create_organization(
        "test-organization@test.empaia.org", auth_headers=manager_auth_headers
    )
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )

    try:
        _ = join_organization(
            organization_id=organization.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        ManagerController().set_organization_user_roles(
            organization_id=organization.organization_id,
            target_user_id=user.user_id,
            organization_user_roles=[OrganizationUserRole.DATA_MANAGER],
            auth_headers=manager_auth_headers,
        )

        try:
            configuration = AdminController().get_organization_deployment_configuration(
                organization.organization_id
            )
            assert False
        except AssertionError as err:
            # should fail as there is no deployment configuration yet
            assert "No valid deployment configuration for organization" in str(err)

        post_config = PostOrganizationDeploymentConfiguration(
            allow_localhost_web_clients=False,
            allow_http=True,
            workbench_deployment_domain="https://lab1.empaia.demo",
            data_manager_deployment_domain="https://lab1.empaia.demo",
            workbench_redirect_uris=[
                "https://lab1.empaia.demo/wbc2",
                "https://lab1.empaia.demo/wbc3",
                "http://localhost",
            ],
            workbench_web_origins=[
                "https://lab1.empaia.demo",
            ],
            data_manager_redirect_uris=[
                "https://lab1.empaia.demo/mds-ui",
                "http://localhost",
            ],
            data_manager_web_origins=[
                "https://lab1.empaia.demo",
            ],
        )
        try:
            configuration = AdminController().put_organization_deployment_configuration(
                organization.organization_id, post_config
            )
            assert False
        except AssertionError as err:
            assert "contains localhost but is not allowed" in str(err)

        post_config.allow_localhost_web_clients = True

        configuration = AdminController().put_organization_deployment_configuration(
            organization.organization_id, post_config
        )
        assert configuration.organization_id == organization.organization_id
        assert (
            post_config.allow_localhost_web_clients
            == configuration.allow_localhost_web_clients
        )
        assert (
            post_config.workbench_deployment_domain
            == configuration.workbench_deployment_domain
        )
        assert (
            post_config.data_manager_deployment_domain
            == configuration.data_manager_deployment_domain
        )
        assert sorted(post_config.workbench_redirect_uris) == sorted(
            configuration.workbench_redirect_uris
        )
        assert sorted(post_config.workbench_web_origins) == sorted(
            configuration.workbench_web_origins
        )
        assert sorted(post_config.data_manager_redirect_uris) == sorted(
            configuration.data_manager_redirect_uris
        )
        assert sorted(post_config.data_manager_web_origins) == sorted(
            configuration.data_manager_web_origins
        )

        configuration_2 = AdminController().get_organization_deployment_configuration(
            organization.organization_id
        )
        assert configuration == configuration_2

        credentials = AdminController().get_client_credentials(
            organization.organization_id
        )

        _, decoded_token_mdc = login_manager.get_access_flow_tokens(
            client_id=credentials.data_management_client.client_id,
            user_name=post_user.contact_data.email_address,
            password=post_user.password,
        )
        _, decoded_token_wbc = login_manager.get_access_flow_tokens(
            client_id=credentials.workbench_client.client_id,
            user_name=post_user.contact_data.email_address,
            password=post_user.password,
        )

        # check allowed origins in data manger access token
        assert sorted(decoded_token_mdc["allowed-origins"]) == sorted(
            post_config.data_manager_web_origins
        )
        assert decoded_token_mdc["organizations"][organization.organization_id][
            "roles"
        ] == ["data-manager"]
        assert sorted(decoded_token_wbc["allowed-origins"]) == sorted(
            post_config.workbench_web_origins
        )
        assert decoded_token_mdc["organizations"][organization.organization_id][
            "roles"
        ] == ["data-manager"]

        ManagerController().set_organization_user_roles(
            organization_id=organization.organization_id,
            target_user_id=user.user_id,
            organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
            auth_headers=manager_auth_headers,
        )

        # check allowed origins in workbench access token
        _, decoded_token_mdc = login_manager.get_access_flow_tokens(
            client_id=credentials.data_management_client.client_id,
            user_name=post_user.contact_data.email_address,
            password=post_user.password,
        )
        _, decoded_token_wbc = login_manager.get_access_flow_tokens(
            client_id=credentials.workbench_client.client_id,
            user_name=post_user.contact_data.email_address,
            password=post_user.password,
        )

        assert sorted(decoded_token_mdc["allowed-origins"]) == sorted(
            post_config.data_manager_web_origins
        )
        assert decoded_token_mdc["organizations"][organization.organization_id][
            "roles"
        ] == ["pathologist"]
        assert sorted(decoded_token_wbc["allowed-origins"]) == sorted(
            post_config.workbench_web_origins
        )
        assert decoded_token_mdc["organizations"][organization.organization_id][
            "roles"
        ] == ["pathologist"]
    finally:
        AdminController().delete_organization(organization.organization_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_user(manager.user_id)


def test_make_organization_external_compute_provider():
    m_data_customer, m_customer, m_customer_auth_headers = create_user(
        "test-manager-customer@empaia.org"
    )
    m_data_cp, m_cp, m_cp_auth_headers = create_user(
        "test-manager-compute-provider@empaia.org"
    )
    customer_organization = create_organization(
        "test-orga-1@empaia.org",
        m_customer_auth_headers,
        organization_categories=[OrganizationCategory.APP_CUSTOMER],
    )
    compute_provider_org = create_organization(
        "test-orga-1@empaia.org", m_cp_auth_headers, organization_categories=[]
    )
    compute_provider_organization_2 = create_organization(
        "test-orga-1@empaia.org",
        m_cp_auth_headers,
        organization_categories=[OrganizationCategory.COMPUTE_PROVIDER],
    )
    m_customer_auth_headers = login_manager.user(
        m_data_customer.contact_data.email_address,
        m_data_customer.password,
        clear_cache=True,
    )
    m_cp_auth_headers = login_manager.user(
        m_data_cp.contact_data.email_address, m_data_cp.password, clear_cache=True
    )

    try:
        try:
            AdminController().set_organization_compute_provider_jes(
                organization_id=customer_organization.organization_id,
                compute_provider_organization_id=compute_provider_org.organization_id,
            )
            assert False
        except AssertionError as err:
            err_str = f"JES of organization {compute_provider_org.organization_id} is not eligible to serve as an external JES"
            missing_role = "missing COMPUTE_PROVIDER category"
            assert err_str in str(err)
            assert missing_role in str(err)

        change_organization_categories(
            organization_id=compute_provider_org.organization_id,
            organization_categories=[OrganizationCategory.COMPUTE_PROVIDER],
            manager_auth_headers=m_cp_auth_headers,
        )

        r = AdminController().set_organization_compute_provider_jes(
            organization_id=customer_organization.organization_id,
            compute_provider_organization_id=compute_provider_org.organization_id,
        )
        assert r.status_code == 204

        credentials = AdminController().get_client_credentials(
            organization_id=customer_organization.organization_id
        )
        _, decoded_token = login_manager.get_client_credentials_flow_tokens(
            credentials.workbench_service.client_id,
            credentials.workbench_service.secret,
        )
        # both JES audiences should exist
        assert f"{compute_provider_org.organization_id}.cpjes" in decoded_token["aud"]
        assert f"{customer_organization.organization_id}.jes" in decoded_token["aud"]

        # set another compute provider
        r = AdminController().set_organization_compute_provider_jes(
            organization_id=customer_organization.organization_id,
            compute_provider_organization_id=compute_provider_organization_2.organization_id,
        )
        assert r.status_code == 204

        credentials = AdminController().get_client_credentials(
            organization_id=customer_organization.organization_id
        )
        _, decoded_token = login_manager.get_client_credentials_flow_tokens(
            credentials.workbench_service.client_id,
            credentials.workbench_service.secret,
        )
        assert (
            f"{compute_provider_org.organization_id}.cpjes" not in decoded_token["aud"]
        )
        assert (
            f"{compute_provider_organization_2.organization_id}.cpjes"
            in decoded_token["aud"]
        )
        assert f"{customer_organization.organization_id}.jes" in decoded_token["aud"]
    finally:
        AdminController().delete_organization(customer_organization.organization_id)
        AdminController().delete_organization(compute_provider_org.organization_id)
        AdminController().delete_organization(
            compute_provider_organization_2.organization_id
        )
        AdminController().delete_user(m_customer.user_id)
        AdminController().delete_user(m_cp.user_id)


def test_remove_external_compute_provider():
    m_data_customer, m_customer, m_customer_auth_headers = create_user(
        "test-manager-customer@empaia.org"
    )
    m_data_cp, m_cp, m_cp_auth_headers = create_user(
        "test-manager-compute-provider@empaia.org"
    )
    customer_organization = create_organization(
        "test-orga-1@empaia.org",
        m_customer_auth_headers,
        organization_categories=[OrganizationCategory.APP_CUSTOMER],
    )
    compute_provider_organization = create_organization(
        "test-orga-1@empaia.org",
        m_cp_auth_headers,
        organization_categories=[OrganizationCategory.COMPUTE_PROVIDER],
    )
    m_customer_auth_headers = login_manager.user(
        m_data_customer.contact_data.email_address,
        m_data_customer.password,
        clear_cache=True,
    )
    m_cp_auth_headers = login_manager.user(
        m_data_cp.contact_data.email_address, m_data_cp.password, clear_cache=True
    )

    try:
        r = AdminController().set_organization_compute_provider_jes(
            organization_id=customer_organization.organization_id,
            compute_provider_organization_id=compute_provider_organization.organization_id,
        )
        assert r.status_code == 204

        credentials = AdminController().get_client_credentials(
            organization_id=customer_organization.organization_id
        )
        _, decoded_token = login_manager.get_client_credentials_flow_tokens(
            credentials.workbench_service.client_id,
            credentials.workbench_service.secret,
        )
        assert (
            f"{compute_provider_organization.organization_id}.cpjes"
            in decoded_token["aud"]
        )
        assert f"{customer_organization.organization_id}.jes" in decoded_token["aud"]

        r = AdminController().remove_compute_provider_jes_from_organization(
            organization_id=customer_organization.organization_id,
        )
        assert r.status_code == 204

        credentials = AdminController().get_client_credentials(
            organization_id=customer_organization.organization_id
        )
        _, decoded_token = login_manager.get_client_credentials_flow_tokens(
            credentials.workbench_service.client_id,
            credentials.workbench_service.secret,
        )
        assert (
            f"{compute_provider_organization.organization_id}.cpjes"
            not in decoded_token["aud"]
        )
        assert f"{customer_organization.organization_id}.jes" in decoded_token["aud"]
    finally:
        AdminController().delete_organization(customer_organization.organization_id)
        AdminController().delete_organization(
            compute_provider_organization.organization_id
        )
        AdminController().delete_user(m_customer.user_id)
        AdminController().delete_user(m_cp.user_id)
