from typing import Dict, List, Optional, Tuple

from tests.utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    MembershipRequestEntity,
    OrganizationCategoriesEntity,
    OrganizationCategory,
    OrganizationUserRole,
    PostUserEntity,
    RegisteredUserEntity,
)

# from tests.utils.auth_service.api_controllers.v2.manager_auth_headers import
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.auth_service.api_controllers.v2.organization_controller import (
    OrganizationControllerV2,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.singletons import login_manager


def create_organization(
    email_address: str,
    auth_headers: Dict,
    organization_categories=[OrganizationCategory.APP_CUSTOMER],
) -> ConfidentialOrganizationProfileEntity:
    organization = MyUnapprovedOrganizationsController().create_new_organization(
        email_address=email_address,
        auth_headers=auth_headers,
        organization_categories=organization_categories,
    )
    MyUnapprovedOrganizationsController().request_organization_approval(
        organization_id=organization.organization_id, auth_headers=auth_headers
    )
    ModeratorController().accept_organization(
        organization_id=organization.organization_id
    )
    return ModeratorController().get_organization(
        organization_id=organization.organization_id
    )


def create_user(
    email_address: str,
) -> Tuple[PostUserEntity, Optional[RegisteredUserEntity], Dict]:
    post_user = PublicController().register_new_user(email_address)
    PublicController().verify_registration(email_address)
    user_profile = ModeratorController().get_user_by_username(email_address)
    auth_headers = login_manager.user(
        email_address, post_user.password, clear_cache=True
    )
    return post_user, user_profile, auth_headers


def get_user_auth_header(user_name: str, password: str) -> Dict:
    return login_manager.user(user_name, password, clear_cache=True)


def join_organization(
    organization_id: str,
    user_auth_headers: Optional[Dict] = None,
    manager_auth_headers: Optional[Dict] = None,
) -> MembershipRequestEntity:
    my_controller = MyController()
    my_controller.request_membership(organization_id, auth_headers=user_auth_headers)
    membership_request = my_controller.get_membership_request(
        organization_id, auth_headers=user_auth_headers
    )
    membership_requests_orga_1 = ManagerController().get_membership_requests(
        organization_id, auth_headers=manager_auth_headers
    )
    assert len(membership_requests_orga_1.membership_requests) == 1
    ManagerController().accept_membership_request(
        organization_id,
        membership_request.membership_request_id,
        auth_headers=manager_auth_headers,
    )
    return ManagerController().get_membership_request(
        organization_id=organization_id,
        user_id=user_auth_headers["user-id"],
        auth_headers=manager_auth_headers,
    )


def change_organization_categories(
    organization_id: str,
    organization_categories: List[OrganizationCategory],
    manager_auth_headers: Optional[Dict] = None,
) -> OrganizationCategoriesEntity:
    ManagerController().request_organization_category(
        organization_id=organization_id,
        organization_categories=organization_categories,
        auth_headers=manager_auth_headers,
    )

    category_requests = ModeratorController().get_organization_category_requests()
    return ModeratorController().accept_category_request(
        category_request_id=category_requests.organization_category_requests[
            0
        ].organization_category_request_id
    )


def change_organization_user_role(
    organization_id: str,
    organization_user_roles: List[OrganizationUserRole],
    user_auth_headers: Optional[Dict] = None,
    manager_auth_headers: Optional[Dict] = None,
):
    _ = OrganizationControllerV2().request_organization_user_roles(
        organization_id=organization_id,
        organization_user_roles=organization_user_roles,
        auth_headers=user_auth_headers,
    )
    organization_role_requests = (
        OrganizationControllerV2().get_organization_user_role_requests(
            organization_id=organization_id,
            auth_headers=user_auth_headers,
        )
    )
    last_role_request_index = (
        len(organization_role_requests.organization_user_role_requests) - 1
    )
    return ManagerController().accept_organization_user_role_request(
        organization_id=organization_id,
        role_request_id=organization_role_requests.organization_user_role_requests[
            last_role_request_index
        ].organization_user_role_request_id,
        auth_headers=manager_auth_headers,
    )
