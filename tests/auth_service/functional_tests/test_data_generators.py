import os.path
import shutil
import subprocess
from typing import Optional

import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.singletons import api_test_state, login_manager
from tests.utils.utils_resources import (
    resolve_file_name_in_resource_path,
)

from ....data_generator_utils import datagen_wipe
from ....utils import utils_resources
from ....utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def assert_no_data_generator_output_file_exists():
    # there should be no files in the data generator output directory, because this test will
    # delete generated files there:
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        assert (
            len(os.listdir(utils_resources.get_data_generator_output_directory())) == 0
        )


def setup_teardown():
    # setup here
    assert_no_data_generator_output_file_exists()
    yield
    # tear down here
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        shutil.rmtree(utils_resources.get_data_generator_output_directory())


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


admin_user_file = resolve_file_name_in_resource_path("test_super_admin.json")
deployment_configuration_file = resolve_file_name_in_resource_path(
    "test_deployment_configurations.json"
)
orga_file = resolve_file_name_in_resource_path("test_organizations.json")
test_user_file = resolve_file_name_in_resource_path("test_user.json")

data_generator_scripts_directory = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../..",
    "..",
    "data_generators",
    "v2_api",
)

exec_directory = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../../..")
)


def run_data_generator(data_generator_module: str, resource_file_name: Optional[str]):
    pwd = os.path.abspath(os.getcwd())
    try:
        cmd = [
            "python",
            "-m",
            f"tests.data_generators.auth_service.v2.{data_generator_module}",
        ]
        if resource_file_name is not None:
            cmd += [resource_file_name]
        os.chdir(exec_directory)
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(f"Output of '{data_generator_module} {resource_file_name}' is:\n{output}")
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf8")
        print(f"Output of '{data_generator_module} {resource_file_name}' is:\n{output}")
        raise e
    finally:
        os.chdir(pwd)


def delete_users(user_ids):
    admin_controller = AdminController()
    for user_id in user_ids:
        admin_controller.delete_user(user_id)


def test_create_user():
    user_ids_before = set(datagen_wipe.get_all_user_ids())
    try:
        run_data_generator("create_user", "test_user.json")
    finally:
        user_ids_after = set(datagen_wipe.get_all_user_ids())
        created_user_ids = user_ids_after.difference(user_ids_before)
        if len(created_user_ids) > 0:
            delete_users(created_user_ids)
        datagen_wipe.assert_all_data_was_wiped()


def test_create_user_v2_api_model():
    user_ids_before = set(datagen_wipe.get_all_user_ids())
    try:
        run_data_generator("create_user", "test_user_v2_api_model.json")
    finally:
        user_ids_after = set(datagen_wipe.get_all_user_ids())
        created_user_ids = user_ids_after.difference(user_ids_before)
        if len(created_user_ids) > 0:
            delete_users(created_user_ids)
        datagen_wipe.assert_all_data_was_wiped()


def delete_organizations(organization_ids):
    admin_controller = AdminController()
    for organization_id in organization_ids:
        admin_controller.delete_organization(organization_id)


def test_create_organizations():
    admin_controller = AdminController()
    organization_ids_before = set(admin_controller.get_all_organization_ids())
    user_ids_before = set(datagen_wipe.get_all_user_ids())
    try:
        run_data_generator("create_organizations", "test_organizations.json")
        run_data_generator(
            "configure_deployments", "test_deployment_configurations.json"
        )
    finally:
        user_ids_after = set(datagen_wipe.get_all_user_ids())
        organization_ids_after = set(admin_controller.get_all_organization_ids())
        created_organization_ids = organization_ids_after.difference(
            organization_ids_before
        )
        if len(created_organization_ids) > 0:
            delete_organizations(created_organization_ids)
        created_user_ids = user_ids_after.difference(user_ids_before)
        if len(created_user_ids) > 0:
            delete_users(created_user_ids)
        datagen_wipe.assert_all_data_was_wiped()


def test_create_organizations_v2_api_model():
    admin_controller = AdminController()
    organization_ids_before = set(admin_controller.get_all_organization_ids())
    user_ids_before = set(datagen_wipe.get_all_user_ids())
    try:
        run_data_generator(
            "create_organizations", "test_organizations_v2_api_model.json"
        )
        run_data_generator(
            "configure_deployments", "test_deployment_configurations_v2_api_model.json"
        )
        _verify_client_service_user_account_not_in_organization_member_list()
    finally:
        user_ids_after = set(datagen_wipe.get_all_user_ids())
        organization_ids_after = set(admin_controller.get_all_organization_ids())
        created_organization_ids = organization_ids_after.difference(
            organization_ids_before
        )
        try:
            _verifyOrganizationProfileEntitiesIncludeLogoUrls()
        finally:
            if len(created_organization_ids) > 0:
                delete_organizations(created_organization_ids)
            created_user_ids = user_ids_after.difference(user_ids_before)
            if len(created_user_ids) > 0:
                delete_users(created_user_ids)
            datagen_wipe.assert_all_data_was_wiped()


def _verify_client_service_user_account_not_in_organization_member_list():
    login_manager.clear_cache_for_super_admin()
    organization = PublicController().get_organization_by_name("test_orga_1")
    manager_controller = ManagerController()
    expected_user_emails = (
        "empaia_admin@empaia.org",
        "manager_1@testorga1.empaia.org",
        "resigning_user_1@testorga1.empaia.org",
        "user_1@testorga1.empaia.org",
    )
    users = manager_controller.get_users(organization.organization_id, None).users
    for user in users:
        assert user.email_address is not None
        assert user.email_address in expected_user_emails
    assert len(users) == len(expected_user_emails)


def _verifyOrganizationProfileEntitiesIncludeLogoUrls():
    organization_id = None
    moderator_controller = ModeratorController()
    for organization in moderator_controller.get_organizations().organizations:
        if organization.organization_name == "test_orga_1":
            organization_id = organization.organization_id
            break
    assert organization_id is not None

    organization = moderator_controller.get_organization(organization_id)
    assert organization.logo_url is not None
    assert organization.resized_logo_urls is not None

    public_organization = PublicController().get_organization(organization_id)
    assert public_organization.logo_url is not None
    assert public_organization.resized_logo_urls is not None

    my_organizations = MyController().get_organizations()
    found = False
    for my_organization in my_organizations.organizations:
        if organization_id == my_organization.organization_id:
            found = True
            assert my_organization.logo_url is not None
            assert my_organization.resized_logo_urls is not None
            break
    assert found

    found = False
    for admin_organization in (
        AdminController().get_organizations(0, 9999).organizations
    ):
        if admin_organization.organization_id == organization_id:
            found = True
            assert admin_organization.logo_url is not None
            assert admin_organization.resized_logo_urls is not None
            break
    assert found
