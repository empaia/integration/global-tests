from typing import Callable, Dict

import pytest

from ....data_generator_utils import datagen_wipe
from ....utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from ....utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    PostOrganizationContactDataEntity,
    PostOrganizationDetailsEntity,
    UnapprovedConfidentialOrganizationProfileEntity,
)
from ....utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from ....utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from ....utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ....utils.singletons import api_test_state, login_manager

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    login_manager.clear_cache_for_super_admin()
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def expect_cannot_request_organization_approval(
    unapproved_organization: UnapprovedConfidentialOrganizationProfileEntity,
    user_auth_headers: Dict,
):
    my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()
    assert (
        my_unapproved_organizations_controller.can_request_organization_approval(
            unapproved_organization.organization_id, auth_headers=user_auth_headers
        )
        is False
    )
    try:
        my_unapproved_organizations_controller.request_organization_approval(
            unapproved_organization.organization_id, user_auth_headers
        )
    except AssertionError as e:
        if 'should not fail: 422 b\'{"error":"Invalid data","errors"' not in str(e):
            raise e
    else:
        raise AssertionError(
            "Approval request of organization with invalid data did not fail"
        )

    try:
        ModeratorController().accept_organization(
            unapproved_organization.organization_id
        )
    except AssertionError as e:
        if 'should not fail: 400 b\'{"error":"Bad Request"' not in str(
            e
        ) or "Organization cannot be approved, because the account state is REQUIRES_ACTIVATION" not in str(
            e
        ):
            raise e
    else:
        raise AssertionError(
            "Accepting approval request of unapproved organization should not be possible"
        )


def expect_organization_cannot_be_modified(
    controller_func: Callable,
    *controller_func_args,
):
    try:
        controller_func(*controller_func_args)
    except AssertionError as e:
        if 'should not fail: 400 b\'{"error":"Bad Request"' not in str(
            e
        ) or '"error_description":"Organization cannot be modified"' not in str(e):
            raise e
    else:
        raise AssertionError("Organization should not be modifiable")


def test_create_unapproved_organization_without_data():
    moderator_controller = ModeratorController()
    post_user = PublicController().register_new_user("test-user@empaia.org")
    user = ModeratorController().get_unapproved_user_by_username(
        post_user.contact_data.email_address  # pylint: disable=E1101
    )
    assert user is not None
    unapproved_organization = None
    try:
        PublicController().verify_registration(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )

        unapproved_user = moderator_controller.get_unapproved_user_by_username(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )
        assert unapproved_user is None
        user_auth_headers = login_manager.user(
            user.username, post_user.password, clear_cache=True  # pylint: disable=E1101
        )

        my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()
        unapproved_organization = (
            my_unapproved_organizations_controller.create_new_organization_without_data(
                user_auth_headers
            )
        )

        expect_cannot_request_organization_approval(
            unapproved_organization, user_auth_headers
        )

        my_unapproved_organizations_controller.set_organization_name(
            unapproved_organization.organization_id,
            "My Test ü+ßäö$§% Organization",
            user_auth_headers,
        )
        my_unapproved_organizations_controller.set_organization_name(
            unapproved_organization.organization_id,
            "My Test §% Organization",
            user_auth_headers,
        )
        expect_cannot_request_organization_approval(
            unapproved_organization, user_auth_headers
        )

        contact_data = PostOrganizationContactDataEntity.model_validate(
            {
                "country_code": "GB",
                "email_address": "my-test-organization@empaia.org",
                "place_name": "Londinium",
                "street_name": "Trafalgar Square",
                "street_number": "42a",
                "zip_code": "9873-en",
                "is_email_address_public": False,
                "is_fax_number_public": False,
                "is_phone_number_public": True,
                "phone_number": "9900123",
                "website": "",
            }
        )
        my_unapproved_organizations_controller.set_organization_contact_data(
            unapproved_organization.organization_id, contact_data, user_auth_headers
        )
        # organization can be approved without details like description or categories being set
        # categories is by default an empty list
        assert (
            my_unapproved_organizations_controller.can_request_organization_approval(
                unapproved_organization.organization_id, auth_headers=user_auth_headers
            )
            is True
        )

        details = PostOrganizationDetailsEntity.model_validate(
            {
                "categories": [OrganizationCategory.APP_CUSTOMER],
                "description_de": "",
                "description_en": "",
                "is_user_count_public": False,
            }
        )
        my_unapproved_organizations_controller.set_organization_details(
            unapproved_organization.organization_id, details, user_auth_headers
        )

        assert (
            my_unapproved_organizations_controller.can_request_organization_approval(
                unapproved_organization.organization_id, auth_headers=user_auth_headers
            )
            is True
        )

        my_unapproved_organizations_controller.request_organization_approval(
            unapproved_organization.organization_id, user_auth_headers
        )

        # it should not be possible to change the organization until it is approved:
        expect_organization_cannot_be_modified(
            my_unapproved_organizations_controller.set_organization_name,
            unapproved_organization.organization_id,
            "My Test Organization",
            user_auth_headers,
        )
    finally:
        if unapproved_organization is not None:
            AdminController().delete_organization(
                unapproved_organization.organization_id
            )
        AdminController().delete_user(user.user_id)


def test_delete_unapproved_organization():
    moderator_controller = ModeratorController()
    post_user = PublicController().register_new_user("test-user@empaia.org")
    user = ModeratorController().get_unapproved_user_by_username(
        post_user.contact_data.email_address  # pylint: disable=E1101
    )
    assert user is not None
    unapproved_organization = None
    try:
        PublicController().verify_registration(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )

        unapproved_user = moderator_controller.get_unapproved_user_by_username(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )
        assert unapproved_user is None
        user_auth_headers = login_manager.user(
            user.username, post_user.password, clear_cache=True  # pylint: disable=E1101
        )

        my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()
        unapproved_organization = (
            my_unapproved_organizations_controller.create_new_organization_without_data(
                user_auth_headers
            )
        )

        r = my_unapproved_organizations_controller.delete_unapproved_organization(
            unapproved_organization.organization_id, user_auth_headers
        )
        assert r.status_code == 204
    finally:
        AdminController().delete_user(user.user_id)
