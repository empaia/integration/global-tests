import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    RequestState,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from .utils.data_creation_utils import create_organization, create_user

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_accept_category_change_request():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.APP_VENDOR],
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REQUESTED
        )

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 0

        category_requests = ManagerController().get_organization_category_requests(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.ACCEPTED
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_deny_category_request():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.APP_VENDOR],
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REQUESTED
        )

        ModeratorController().deny_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id,
            reviewer_comment="category request not allowed",
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 0

        category_requests = ManagerController().get_organization_category_requests(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REJECTED
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_revoke_category_request():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.APP_VENDOR],
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REQUESTED
        )

        ManagerController().revoke_category_request(
            organization_id=organization_1.organization_id,
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id,
            auth_headers=manager_auth_headers,
        )

        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 0

        category_requests = ManagerController().get_organization_category_requests(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert len(category_requests.organization_category_requests) == 1
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REVOKED
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_advanced_organization_category():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        initial_category_request = [
            OrganizationCategory.APP_VENDOR,
            OrganizationCategory.APP_CUSTOMER,
            OrganizationCategory.COMPUTE_PROVIDER,
            OrganizationCategory.PRODUCT_PROVIDER,
        ]
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=initial_category_request,
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert category_requests.organization_category_requests[
            0
        ].existing_categories == [OrganizationCategory.APP_CUSTOMER]
        assert set(
            category_requests.organization_category_requests[0].requested_categories
        ) == set(
            [
                OrganizationCategory.APP_VENDOR,
                OrganizationCategory.COMPUTE_PROVIDER,
                OrganizationCategory.PRODUCT_PROVIDER,
            ]
        )
        assert (
            category_requests.organization_category_requests[0].retracted_categories
            == []
        )
        assert (
            category_requests.organization_category_requests[0].request_state
            == RequestState.REQUESTED
        )

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )

        organization = ManagerController().get_organization(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert OrganizationCategory.APP_VENDOR in organization.details.categories
        assert OrganizationCategory.APP_CUSTOMER in organization.details.categories
        assert OrganizationCategory.COMPUTE_PROVIDER in organization.details.categories
        assert OrganizationCategory.PRODUCT_PROVIDER in organization.details.categories

        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.APP_VENDOR],
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert set(
            category_requests.organization_category_requests[0].existing_categories
        ) == set(initial_category_request)
        assert (
            category_requests.organization_category_requests[0].requested_categories
            == []
        )
        assert set(
            category_requests.organization_category_requests[0].retracted_categories
        ) == set(
            [
                OrganizationCategory.APP_CUSTOMER,
                OrganizationCategory.COMPUTE_PROVIDER,
                OrganizationCategory.PRODUCT_PROVIDER,
            ]
        )

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )

        organization = ManagerController().get_organization(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert OrganizationCategory.APP_VENDOR in organization.details.categories
        assert OrganizationCategory.APP_CUSTOMER not in organization.details.categories
        assert (
            OrganizationCategory.COMPUTE_PROVIDER not in organization.details.categories
        )
        assert (
            OrganizationCategory.PRODUCT_PROVIDER not in organization.details.categories
        )

        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[],
            auth_headers=manager_auth_headers,
        )

        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1
        assert category_requests.organization_category_requests[
            0
        ].existing_categories == [OrganizationCategory.APP_VENDOR]
        assert (
            category_requests.organization_category_requests[0].requested_categories
            == []
        )
        assert category_requests.organization_category_requests[
            0
        ].retracted_categories == [OrganizationCategory.APP_VENDOR]

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )

        organization = ManagerController().get_organization(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert organization.details.categories == []
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_manager_changes_organization_category():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization(
        "test-orga-1@empaia.org", manager_auth_headers, organization_categories=[]
    )
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        initial_category_request = [
            OrganizationCategory.APP_CUSTOMER,
        ]
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=initial_category_request,
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )

        organization = ManagerController().get_organization(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert OrganizationCategory.APP_CUSTOMER in organization.details.categories
        assert OrganizationCategory.APP_VENDOR not in organization.details.categories
        assert (
            OrganizationCategory.COMPUTE_PROVIDER not in organization.details.categories
        )
        assert (
            OrganizationCategory.PRODUCT_PROVIDER not in organization.details.categories
        )

        ModeratorController().set_category_request(
            organization_id=organization_1.organization_id,
            categories=[OrganizationCategory.COMPUTE_PROVIDER],
        )
        manager_auth_headers = login_manager.user(
            manager_data.contact_data.email_address,
            manager_data.password,
            clear_cache=True,
        )

        organization = ManagerController().get_organization(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert OrganizationCategory.COMPUTE_PROVIDER in organization.details.categories
        assert OrganizationCategory.APP_CUSTOMER not in organization.details.categories
        assert OrganizationCategory.APP_VENDOR not in organization.details.categories
        assert (
            OrganizationCategory.PRODUCT_PROVIDER not in organization.details.categories
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization_1.organization_id)
