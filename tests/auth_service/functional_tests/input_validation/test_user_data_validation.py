import json
import os
from typing import Dict

import pytest

from .....data_generator_utils import datagen_user, datagen_wipe
from .....utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from .....utils.auth_service.api_controllers.v2.entity_models import (
    LanguageCode,
    PostUserEntity,
)
from .....utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from .....utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from .....utils.singletons import api_test_state, login_manager

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


def load_users_data() -> Dict:
    data_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "valid_data_with_special_characters.json",
    )
    with open(data_file, encoding="utf8") as f:
        data = json.load(f)
    for user in data["users"]:
        datagen_user.set_post_user_default_values(user)
    return data["users"]


def raise_if_not_invalid_request_error(e: Exception, expected_errors: str):
    if f'400 b\'{{"error":"Invalid Request","errors":{expected_errors}' not in str(e):
        raise e


def _test_invalid_request(
    user: PostUserEntity, expected_errors: str, error_message: str
):
    try:
        PublicController().register_new_user(user)
    except AssertionError as e:
        raise_if_not_invalid_request_error(e, expected_errors)
    else:
        raise AssertionError(error_message)


def _test_invalid_user_email_address(email_address: str, error_message: str):
    user_data = PublicController.create_post_user_entity(email_address)
    _test_invalid_request(
        user_data, '{"contactData.emailAddress":"Invalid email address"}', error_message
    )


def test_accepted_email_address_assertion_triggers():
    # Test to verify that the helper function asserts when an invalid email address is accepted by the auth service:
    msg = "Valid email address accepted"
    try:
        _test_invalid_user_email_address("user@empaia.org", msg)
    except AssertionError as e:
        assert str(e) == msg
    else:
        assert False


def test_invalid_email_address_username_longer_than_64_characters():
    _test_invalid_user_email_address(
        f"{'a'*65}@empaia.org",
        "Email address with a username length > 64 was not rejected",
    )


def test_invalid_email_address_domain_longer_than_63_characters():
    _test_invalid_user_email_address(
        f"admin@{'a'*64}.org",
        "Email address with a domain length > 63 was not rejected",
    )


def test_invalid_email_address_username_only():
    _test_invalid_user_email_address(
        "username", "Email address with a local part only was not rejected"
    )


def test_invalid_email_address_without_domain():
    _test_invalid_user_email_address(
        "admin@.org", "Email address without domain was not rejected"
    )


def test_invalid_email_address_without_tld():
    _test_invalid_user_email_address(
        "admin@empaia", "Email address without top-level domain was not rejected"
    )


def test_invalid_email_address_without_username():
    _test_invalid_user_email_address(
        "@empaia.org", "Email address without username was not rejected"
    )


def test_invalid_email_address_invalid_tld():
    _test_invalid_user_email_address(
        "admin@empaia.d", "Email address with invalid top-level domain was not rejected"
    )
    _test_invalid_user_email_address(
        "admin@empaia.bad",
        "Email address with invalid top-level domain was not rejected",
    )
    _test_invalid_user_email_address(
        "admin@empaia.home",
        "Email address with invalid top-level domain was not rejected",
    )


def _test_invalid_user_password(
    password: str, expected_errors: str, error_message: str
):
    user = PublicController.create_post_user_entity("user@test.empaia.org")
    user.password = password
    _test_invalid_request(user, expected_errors, error_message)


def test_invalid_user_password_too_short():
    # see application-production.yml, in section 'password-complexity':
    _test_invalid_user_password(
        "aA1!",
        '{"password":"Password must be 12 or more characters in length."}',
        "Password with too few characters should be rejected",
    )


def test_invalid_user_password_too_long():
    # see application-production.yml, in section 'password-complexity':
    maximum_length = 32
    _test_invalid_user_password(
        "a" * (maximum_length - 3) + "aA1!",
        '{"password":"Password must be no more than 32 characters in length."}',
        "Password with too many characters should be rejected",
    )


def test_invalid_user_password_with_spaces():
    # see application-production.yml, in section 'password-complexity':
    minimum_length = 12
    _test_invalid_user_password(
        " " * minimum_length,
        '{"password":"Password must contain 1 or more uppercase characters.\\\\n'
        "Password must contain 1 or more lowercase characters.\\\\n"
        "Password must contain 1 or more digit characters.\\\\n"
        "Password must contain 1 or more special characters.\\\\n"
        'Password contains a whitespace character."}',
        "Password with whitespaces should be rejected",
    )
    _test_invalid_user_password(
        "a" * minimum_length + "A 1!",
        '{"password":"Password contains a whitespace character."}',
        "Password with whitespaces should be rejected",
    )
    _test_invalid_user_password(
        " " + "a" * minimum_length + "A1!",
        '{"password":"Password contains a whitespace character."}',
        "Password with whitespaces should be rejected",
    )
    _test_invalid_user_password(
        "a" * minimum_length + "A1! ",
        '{"password":"Password contains a whitespace character."}',
        "Password with whitespaces should be rejected",
    )


def test_invalid_user_password_too_few_uppercase_characters():
    # see application-production.yml, in section 'password-complexity':
    minimum_required_uppercase_characters = 1
    _test_invalid_user_password(
        "aaaaaaaaaa1!",
        f'{{"password":"Password must contain {minimum_required_uppercase_characters} or more uppercase characters."}}',
        "Password with too few uppercase characters should be rejected",
    )


def test_invalid_user_password_too_few_lowercase_characters():
    # see application-production.yml, in section 'password-complexity':
    minimum_required_lowercase_characters = 1
    _test_invalid_user_password(
        "AAAAAAAAAA1!",
        f'{{"password":"Password must contain {minimum_required_lowercase_characters} or more lowercase characters."}}',
        "Password with too few uppercase characters should be rejected",
    )


def test_invalid_user_password_too_few_digit_characters():
    # see application-production.yml, in section 'password-complexity':
    minimum_required_digits = 1
    _test_invalid_user_password(
        "aaaaaaAAAAAA!",
        f'{{"password":"Password must contain {minimum_required_digits} or more digit characters."}}',
        "Password with too few uppercase characters should be rejected",
    )


def test_invalid_user_password_too_few_special_characters():
    # see application-production.yml, in section 'password-complexity':
    minimum_required_special_characters = 1
    _test_invalid_user_password(
        "aaaaaaAAAAAA1",
        f'{{"password":"Password must contain {minimum_required_special_characters} or more special characters."}}',
        "Password with too few uppercase characters should be rejected",
    )


def test_user_data_with_special_characters():
    users_data = load_users_data()
    _test_and_verify_user_0(users_data[0])
    _test_and_verify_user_1(users_data[1])


def _create_user_and_return_auth_headers(user_data: Dict):
    post_user_entity = PostUserEntity.model_validate(user_data)
    public_controller = PublicController()
    public_controller.register_new_user(post_user_entity)
    public_controller.verify_registration(post_user_entity.contact_data.email_address)
    return login_manager.user(
        post_user_entity.contact_data.email_address, post_user_entity.password
    )


def _test_and_verify_user_0(user_data: Dict):
    auth_headers = _create_user_and_return_auth_headers(user_data)

    user = MyController().get_profile(auth_headers=auth_headers)
    assert user.username == "aellis-maria@virtual-charite.empaia.org"
    assert user.contact_data.email_address == "aellis-maria@virtual-charite.empaia.org"
    assert user.details.first_name == "Älliss-Maria"
    assert user.details.last_name == "Bobson Bod-é"
    assert user.details.title == ""
    assert user.language_code == LanguageCode.DE
    assert user.user_roles == []

    AdminController().delete_user(user.user_id)


def _test_and_verify_user_1(user_data: Dict):
    auth_headers = _create_user_and_return_auth_headers(user_data)

    user = MyController().get_profile(auth_headers=auth_headers)
    assert user.username == "bob_bobson@virtual-charite.empaia.org"
    assert user.contact_data.email_address == "bob_bobson@virtual-charite.empaia.org"
    assert user.details.first_name == "Bob Maria"
    assert user.details.last_name == "Bobson"
    assert user.details.title == "Input Validation Challenger"
    assert user.language_code == LanguageCode.EN
    assert user.user_roles == []

    AdminController().delete_user(user.user_id)


def test_user_data_title_with_special_characters():
    users_data = load_users_data()
    user_data = users_data[0]
    for title in ("Dr.", "Dipl-Inf."):
        user_data["title"] = title
        login_manager.clear_cache()
        auth_headers = _create_user_and_return_auth_headers(user_data)
        user = MyController().get_profile(auth_headers=auth_headers)
        AdminController().delete_user(user.user_id)


def test_keycloak_id_validation():
    try:
        AdminController().delete_user("target_user_id")
    except AssertionError as e:
        if '422 b\'{"error":"Invalid data"' not in str(
            e
        ) or "Invalid Keycloak ID" not in str(e):
            raise e
    try:
        AdminController().delete_user("sdfe45-234gg-345-43344-!")
    except AssertionError as e:
        if '422 b\'{"error":"Invalid data"' not in str(
            e
        ) or "Invalid Keycloak ID" not in str(e):
            raise e
