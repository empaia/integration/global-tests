import json
import os.path
from typing import Dict

import pytest

from .....data_generator_utils import datagen_organization, datagen_wipe
from .....utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from .....utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    OrganizationCategory,
    PostOrganizationEntity,
)
from .....utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from .....utils.singletons import api_test_state

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


def load_organization_data() -> Dict:
    data_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "valid_data_with_special_characters.json",
    )
    with open(data_file, encoding="utf8") as f:
        data = json.load(f)
    organization_data = data["organization"]
    datagen_organization.set_post_organization_default_values(organization_data)
    return organization_data


def test_create_organization_with_special_characters():
    my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()
    organization_data = load_organization_data()
    organization = (
        my_unapproved_organizations_controller.create_new_organization_from_data(
            PostOrganizationEntity.model_validate(organization_data)
        )
    )
    assert organization.organization_name == "Virtual Charité"
    assert (
        organization.contact_data.email_address
        == "facilitator@virtual-charite.empaia.org"
    )
    assert (
        organization.contact_data.department
        == organization_data["contact_data"]["department"]
    )
    assert organization.contact_data.street_name == "Charitéplatz"
    assert organization.contact_data.street_number == "42a"
    assert (
        organization.contact_data.place_name == "Schlößhausen bei ~\\(^_°}/~ Groß-Dorf"
    )
    assert organization.contact_data.zip_code == "12345"
    assert organization.contact_data.country_code == "DE"
    assert organization.contact_data.phone_number == "+0171 123-456"
    assert organization.contact_data.website == "https://virtual-charite.empaia.org/"
    assert (
        organization.details.description_de
        == organization_data["details"]["description_de"]
    )
    assert organization.details.categories == [OrganizationCategory.APP_CUSTOMER]
    AdminController().delete_organization_and_members_only_in_given_organization(
        organization.organization_id
    )


def raise_if_not_invalid_organization_data_error(e: Exception, expected_errors: str):
    if f'400 b\'{{"error":"Invalid Request","errors":{expected_errors}' not in str(e):
        raise e


def _test_invalid_organization_data(
    organization_data: Dict, expected_errors: str, error_message: str
) -> ConfidentialOrganizationProfileEntity:
    organization = None
    try:
        organization = (
            MyUnapprovedOrganizationsController().create_new_organization_from_data(
                PostOrganizationEntity.model_validate(organization_data)
            )
        )
    except AssertionError as e:
        raise_if_not_invalid_organization_data_error(e, expected_errors)
    else:
        raise AssertionError(error_message)
    return organization


def test_organization_description_too_long():
    maximum_length_of_description = 750
    organization_data = load_organization_data()
    description_de = organization_data["details"]["description_de"]
    characters_available = maximum_length_of_description - len(description_de)
    description_de += (characters_available + 1) * "E"
    organization_data["details"]["description_de"] = description_de
    expected_errors = '{"details.descriptionGerman":"size must be between 0 and 750"}'
    _test_invalid_organization_data(
        organization_data,
        expected_errors,
        f"Too long description_de not rejected: '{description_de}'",
    )


def _test_valid_organization_data(organization_data: Dict):
    organization = (
        MyUnapprovedOrganizationsController().create_new_organization_from_data(
            PostOrganizationEntity.model_validate(organization_data)
        )
    )
    AdminController().delete_organization_and_members_only_in_given_organization(
        organization.organization_id
    )


def test_valid_website_urls():
    organization_data = load_organization_data()
    for protocol in ("http://", "https://"):
        for path in ("", "/main/index.html"):
            for server in ("", "www.", "charite."):
                organization_data["contact_data"][
                    "website"
                ] = f"{protocol}{server}empaia.org{path}"
                _test_valid_organization_data(organization_data)


def test_invalid_website_url():
    organization_data = load_organization_data()
    for invalid_url in (
        "charite",
        "charite..org",
        "http:charite.empaia.org",
        "https:charite.empaia.org",
        "http:/charite.empaia.org",
        "https:/charite.empaia.org",
        "http://charite..org",
        "https://charite..org",
        "http://.empaia.org",
        "https://.empaia.org",
    ):
        organization_data["contact_data"]["website"] = invalid_url
        expected_errors = '{"contactData.website":"Invalid URL"}'
        _test_invalid_organization_data(
            organization_data,
            expected_errors,
            f"Invalid website url not rejected: '{invalid_url}'",
        )


def test_sql_injection_attempt_in_organization_description_is_rejected():
    organization_data = load_organization_data()
    for injection in ("'); DELETE FROM users; --", " OR 1 == 1;"):
        description_de = organization_data["details"]["description_de"]
        organization_data["details"]["description_de"] = (
            description_de[: -len(injection)] + injection
        )
        organization = (
            MyUnapprovedOrganizationsController().create_new_organization_from_data(
                PostOrganizationEntity.model_validate(organization_data)
            )
        )
        # Assert that the description_de still contains the SQL injection, which means it was escaped and had not
        # effect on the SQL insert statement:
        assert (
            organization.details.description_de
            == organization_data["details"]["description_de"]
        )
        AdminController().delete_organization_and_members_only_in_given_organization(
            organization.organization_id
        )
