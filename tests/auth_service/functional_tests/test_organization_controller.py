from uuid import uuid4

import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.auth_service.api_controllers.v2.organization_controller import (
    OrganizationControllerV2,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from .utils.data_creation_utils import (
    create_organization,
    create_user,
    get_user_auth_header,
    join_organization,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_default_organization():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    organization_2 = create_organization("test-orga-2@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        _ = join_organization(
            organization_id=organization_2.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        # by default there should e no default organization
        my_controller = MyController()
        resp = my_controller.get_default_active_organization_request(
            auth_headers=user_auth_headers
        )
        assert resp.status_code == 200
        assert resp.content == b'{"organization_id":null}'

        # put empty organization id
        resp = my_controller.put_default_active_organization_request(
            default_active_organization_id=None,
            auth_headers=user_auth_headers,
        )
        assert resp.status_code == 200
        assert resp.content == b'{"organization_id":null}'

        # put non exisiting organization id
        resp = my_controller.put_default_active_organization_request(
            default_active_organization_id=str(uuid4()),
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert resp.status_code == 400
        assert (
            resp.json()["error_description"]
            == "Cannot set default active organization: user is not a member"
        )

        # set default active orga
        resp = my_controller.put_default_active_organization_request(
            default_active_organization_id=organization_2.organization_id,
            auth_headers=user_auth_headers,
        )
        assert resp.status_code == 200
        assert resp.json()["organization_id"] == organization_2.organization_id

        resp = my_controller.get_default_active_organization_request(
            auth_headers=user_auth_headers
        )
        assert resp.status_code == 200
        assert resp.json()["organization_id"] == organization_2.organization_id

        # delete default active orga
        resp = my_controller.delete_default_active_organization(
            auth_headers=user_auth_headers
        )
        assert resp.status_code == 204
        assert resp.content == b""

        resp = my_controller.get_default_active_organization_request(
            auth_headers=user_auth_headers
        )
        assert resp.status_code == 200
        assert resp.content == b'{"organization_id":null}'

        resp = my_controller.put_default_active_organization_request(
            default_active_organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert resp.status_code == 200
        assert resp.json()["organization_id"] == organization_1.organization_id

        resp = my_controller.get_default_active_organization_request(
            auth_headers=user_auth_headers
        )
        assert resp.status_code == 200
        assert resp.json()["organization_id"] == organization_1.organization_id
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)
        AdminController().delete_organization(organization_2.organization_id)


def test_leave_organization():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    post_user2, user2, user_auth_headers2 = create_user("test-user2@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers2,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header 2
        user_auth_headers2 = get_user_auth_header(
            post_user2.contact_data.email_address, post_user2.password
        )

        # assign test user 1 the manager role
        OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.MANAGER,
            ],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )

        ManagerController().accept_organization_user_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=manager_auth_headers,
        )

        # check if users can leave orga
        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert resp.json()["can_leave"] is True
        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers2,
        )
        assert resp.json()["can_leave"] is True
        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )
        assert resp.json()["can_leave"] is True

        # 2nd manager leaves organization should work
        OrganizationControllerV2().leave(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )

        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
            allowed_errors=[404],
        )
        assert resp.status_code == 404
        assert (
            resp.json()["error_description"]
            == "You are not a member of the organization"
        )
        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert resp.json()["can_leave"] is False
        resp = OrganizationControllerV2().can_leave(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers2,
        )
        assert resp.json()["can_leave"] is True

        # last manager tries to leave organization should fail
        resp = OrganizationControllerV2().leave(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert resp.status_code == 400
        assert (
            resp.json()["error_description"]
            == "Last manager in organization must not leave the organization"
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_user(user2.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_my_unapproved_organizations():
    post_user1, user1, user_auth_headers1 = create_user("test-user1@empaia.org")
    post_user2, user2, user_auth_headers2 = create_user("test-user2@empaia.org")
    try:
        my_unapproved_organizations_controller = MyUnapprovedOrganizationsController()
        resp = my_unapproved_organizations_controller.get_unapproved_organizations(
            page=0, page_size=999, auth_headers=user_auth_headers1
        )
        assert resp.total_organizations_count == 0
        assert len(resp.organizations) == 0

        _ = MyUnapprovedOrganizationsController().create_new_organization(
            email_address=post_user1.contact_data.email_address,
            auth_headers=user_auth_headers1,
            organization_categories=[OrganizationCategory.APP_CUSTOMER],
        )

        resp = my_unapproved_organizations_controller.get_unapproved_organizations(
            page=0, page_size=999, auth_headers=user_auth_headers1
        )
        assert resp.total_organizations_count == 1
        assert len(resp.organizations) == 1

        _ = MyUnapprovedOrganizationsController().create_new_organization(
            email_address=post_user2.contact_data.email_address,
            auth_headers=user_auth_headers2,
            organization_categories=[OrganizationCategory.APP_CUSTOMER],
        )

        resp = my_unapproved_organizations_controller.get_unapproved_organizations(
            page=0, page_size=999, auth_headers=user_auth_headers1
        )
        assert resp.total_organizations_count == 1
        assert len(resp.organizations) == 1

    finally:
        AdminController().delete_user(user1.user_id)
        AdminController().delete_user(user2.user_id)
