import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationUserRole,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from .utils.data_creation_utils import (
    create_organization,
    create_user,
    get_user_auth_header,
    join_organization,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


def test_leave_organization():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        ManagerController().set_organization_user_roles(
            organization_id=organization_1.organization_id,
            target_user_id=user.user_id,
            organization_user_roles=[
                OrganizationUserRole.DATA_MANAGER,
                OrganizationUserRole.PATHOLOGIST,
            ],
            auth_headers=manager_auth_headers,
        )

        # update user token
        _ = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
        _, access_token_before = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )

        assert (
            access_token_before["organizations"][organization_1.organization_id]
            is not None
        )
        assert (
            "data-manager"
            in access_token_before["organizations"][organization_1.organization_id][
                "roles"
            ]
        )
        assert (
            "pathologist"
            in access_token_before["organizations"][organization_1.organization_id][
                "roles"
            ]
        )

        ManagerController().remove_user_from_organization(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=manager_auth_headers,
        )

        # update user token
        _ = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        _, access_token_after = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )

        assert access_token_after["organizations"] == {}
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)
