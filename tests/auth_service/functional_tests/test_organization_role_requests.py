import os
import shutil

import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
    RequestState,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.organization_controller import (
    OrganizationControllerV2,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from ....utils import utils_resources
from .utils.data_creation_utils import (
    create_organization,
    create_user,
    get_user_auth_header,
    join_organization,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def assert_no_data_generator_output_file_exists():
    # there should be no files in the data generator output directory, because this test will
    # delete generated files there:
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        assert (
            len(os.listdir(utils_resources.get_data_generator_output_directory())) == 0
        )


def setup_teardown():
    # setup here
    assert_no_data_generator_output_file_exists()
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        shutil.rmtree(utils_resources.get_data_generator_output_directory())


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_accept_organization_member_requests_role_change():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
        # try request unallowed role for app customer
        response = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.APP_MAINTAINER],
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert response.status_code == 400
        assert (
            response.json()["error_description"]
            == "The organization categories do not allow the requested organization user roles for this organization"
        )

        # try request allowed role
        response = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.MANAGER],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )

        # try to open a new role request that should fail
        response = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.APP_MAINTAINER,
                OrganizationUserRole.DATA_MANAGER,
            ],
            auth_headers=user_auth_headers,
            allowed_errors=[409],
        )
        assert response.status_code == 409
        assert (
            response.json()["error_description"]
            == "An open role request for this organization and user already exists. Revoke open request first"
        )

        ManagerController().accept_organization_user_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=manager_auth_headers,
        )

        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.ACCEPTED
        )

        member_profile = OrganizationControllerV2().get_user_profile(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert OrganizationUserRole.MANAGER in member_profile.organization_user_roles
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_deny_organization_member_requests_role_change():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.MANAGER],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )

        ManagerController().deny_organization_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            reviewer_comment="role request denied",
            auth_headers=manager_auth_headers,
        )

        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REJECTED
        )

        member_profile = OrganizationControllerV2().get_user_profile(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert (
            OrganizationUserRole.MANAGER not in member_profile.organization_user_roles
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_revoke_organization_member_requests_role_change():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.MANAGER],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )

        OrganizationControllerV2().revoke_organization_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=user_auth_headers,
        )

        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REVOKED
        )

        member_profile = OrganizationControllerV2().get_user_profile(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert (
            OrganizationUserRole.MANAGER not in member_profile.organization_user_roles
        )

        # try to request a new role change
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 2
        for role_request in organization_role_requests.organization_user_role_requests:
            if role_request == RequestState.REQUESTED:
                assert role_request.pathologist
            if role_request == RequestState.REVOKED:
                assert role_request.manager
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_request_multiple_user_roles():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        # role app maintainer should not be allowed for category app customer
        resp = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.MANAGER,
                OrganizationUserRole.APP_MAINTAINER,
                OrganizationUserRole.PATHOLOGIST,
            ],
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert resp.status_code == 400
        assert (
            resp.json()["error_description"]
            == "The organization categories do not allow the requested organization user roles for this organization"
        )

        # manager and pathologist are valid and should be changed
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.MANAGER,
                OrganizationUserRole.PATHOLOGIST,
            ],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )

        ManagerController().accept_organization_user_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=manager_auth_headers,
        )

        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.ACCEPTED
        )

        member_profile = OrganizationControllerV2().get_user_profile(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert OrganizationUserRole.MANAGER in member_profile.organization_user_roles
        assert (
            OrganizationUserRole.PATHOLOGIST in member_profile.organization_user_roles
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_request_unallowed_user_roles():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        # role clearance maintainer should not be allowed for category app customer
        resp = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.CLEARANCE_MAINTAINER,
            ],
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert resp.status_code == 400
        assert (
            resp.json()["error_description"]
            == "The organization categories do not allow the requested organization user roles for this organization"
        )

        # change category to product provider
        ManagerController().request_organization_category(
            organization_id=organization_1.organization_id,
            organization_categories=[
                OrganizationCategory.APP_VENDOR,
                OrganizationCategory.PRODUCT_PROVIDER,
            ],
            auth_headers=manager_auth_headers,
        )
        category_requests = ModeratorController().get_organization_category_requests()
        assert len(category_requests.organization_category_requests) == 1

        ModeratorController().accept_category_request(
            category_request_id=category_requests.organization_category_requests[
                0
            ].organization_category_request_id
        )

        # pathologist is not a valid role for app customer/product porvider and should be rejected
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.CLEARANCE_MAINTAINER,
                OrganizationUserRole.PATHOLOGIST,
            ],
            auth_headers=user_auth_headers,
            allowed_errors=[400],
        )
        assert resp.status_code == 400
        assert (
            resp.json()["error_description"]
            == "The organization categories do not allow the requested organization user roles for this organization"
        )

        # app and clearance maintainer are both valid roles for app customer/product porvider and should be approved
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.CLEARANCE_MAINTAINER,
                OrganizationUserRole.APP_MAINTAINER,
            ],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )

        ManagerController().accept_organization_user_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=manager_auth_headers,
        )

        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.ACCEPTED
        )

        member_profile = OrganizationControllerV2().get_user_profile(
            target_user_id=user.user_id,
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
        assert (
            OrganizationUserRole.CLEARANCE_MAINTAINER
            in member_profile.organization_user_roles
        )
        assert (
            OrganizationUserRole.APP_MAINTAINER
            in member_profile.organization_user_roles
        )

    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_advanced_user_roles():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )
        # get updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        initial_role_request = [
            OrganizationUserRole.MANAGER,
            OrganizationUserRole.PATHOLOGIST,
            OrganizationUserRole.DATA_MANAGER,
        ]

        # manager and pathologist are valid and should be changed
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=initial_role_request,
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 1
        assert (
            organization_role_requests.organization_user_role_requests[0].request_state
            == RequestState.REQUESTED
        )
        assert (
            organization_role_requests.organization_user_role_requests[0].existing_roles
            == []
        )
        assert set(
            organization_role_requests.organization_user_role_requests[
                0
            ].requested_roles
        ) == set(initial_role_request)
        assert (
            organization_role_requests.organization_user_role_requests[
                0
            ].retracted_roles
            == []
        )

        ManagerController().accept_organization_user_role_request(
            organization_id=organization_1.organization_id,
            role_request_id=organization_role_requests.organization_user_role_requests[
                0
            ].organization_user_role_request_id,
            auth_headers=manager_auth_headers,
        )

        # retract all roles except pathologist
        _ = OrganizationControllerV2().request_organization_user_roles(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.PATHOLOGIST],
            auth_headers=user_auth_headers,
        )
        organization_role_requests = (
            OrganizationControllerV2().get_organization_user_role_requests(
                organization_id=organization_1.organization_id,
                auth_headers=user_auth_headers,
            )
        )
        assert len(organization_role_requests.organization_user_role_requests) == 2
        assert (
            organization_role_requests.organization_user_role_requests[1].request_state
            == RequestState.REQUESTED
        )
        assert set(
            organization_role_requests.organization_user_role_requests[1].existing_roles
        ) == set(initial_role_request)
        assert (
            organization_role_requests.organization_user_role_requests[
                1
            ].requested_roles
            == []
        )
        assert set(
            organization_role_requests.organization_user_role_requests[
                1
            ].retracted_roles
        ) == set([OrganizationUserRole.MANAGER, OrganizationUserRole.DATA_MANAGER])

    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)
