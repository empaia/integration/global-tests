import os
import shutil
import subprocess
import typing
from typing import Dict, List, Optional, Sequence, Tuple

import pytest

from tests.tests.auth_service.functional_tests.utils.data_creation_utils import (
    create_user,
    join_organization,
)
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
    PostUserEntity,
    RegisteredUserEntity,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from ....utils import utils_resources
from ....utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)

exec_directory = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../..", "..", "..")
)


def assert_no_data_generator_output_file_exists():
    # there should be no files in the data generator output directory, because this test will
    # delete generated files there:
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        assert (
            len(os.listdir(utils_resources.get_data_generator_output_directory())) == 0
        )


def setup_teardown():
    # setup here
    assert_no_data_generator_output_file_exists()
    yield
    # tear down here
    if os.path.exists(utils_resources.get_data_generator_output_directory()):
        shutil.rmtree(utils_resources.get_data_generator_output_directory())


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def run_data_generator(
    data_generator_module: str,
    resource_file_name: str,
    extra_args: Optional[List[str]] = None,
):
    pwd = os.path.abspath(os.getcwd())
    try:
        os.chdir(exec_directory)
        cmd = [
            "python",
            "-m",
            f"tests.data_generators.auth_service.{data_generator_module}",
            resource_file_name,
        ]
        if extra_args is not None:
            cmd += extra_args
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT).decode("utf8")
        print(f"Output of '{data_generator_module} {resource_file_name}' is:\n{output}")
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf8")
        print(f"Output of '{data_generator_module} {resource_file_name}' is:\n{output}")
        raise e
    finally:
        os.chdir(pwd)


def create_organization_user_with_role(
    i: int, user_role: Optional[OrganizationUserRole], organization_id: str
) -> Tuple[PostUserEntity, RegisteredUserEntity, Dict]:
    email = f"test_orga_user_{i}@empaia.org"
    post_user, user, user_auth_headers = create_user(email)

    manager_auth_headers = login_manager.super_admin()
    join_organization(organization_id, user_auth_headers, manager_auth_headers)
    if user_role is not None:
        ManagerController().set_organization_user_roles(
            organization_id, user.user_id, [user_role], manager_auth_headers
        )
    user_auth_headers = get_updated_auth_headers(post_user, user)
    return post_user, user, user_auth_headers


def get_updated_auth_headers(
    post_user: PostUserEntity, user: RegisteredUserEntity
) -> Dict:
    return login_manager.user(user.username, post_user.password, clear_cache=True)


def get_allowed_user_roles(organization_id: str) -> List[OrganizationUserRole]:
    organization = next(
        filter(
            lambda o: organization_id == o.organization_id,
            AdminController().get_organizations(0, 9999).organizations,
        )
    )

    user_roles = []

    if OrganizationCategory.APP_CUSTOMER in organization.details.categories:
        user_roles.extend(
            [
                OrganizationUserRole.DATA_MANAGER,
                OrganizationUserRole.MANAGER,
                OrganizationUserRole.PATHOLOGIST,
            ]
        )
    if OrganizationCategory.APP_VENDOR in organization.details.categories:
        user_roles.append(OrganizationUserRole.APP_MAINTAINER)

    return user_roles


def test_configure_deployments():
    admin_controller = AdminController()
    organization_ids_before = set(admin_controller.get_all_organization_ids())
    user_ids_before = set(datagen_wipe.get_all_user_ids())

    run_data_generator(
        "v2.create_organizations", "test_organizations_v2_api_model.json"
    )
    login_manager.clear_cache_for_super_admin()

    clients = admin_controller.get_all_clients()
    # all relevant clients are initially created but not configured
    assert (
        len(clients) == 2 * 8 + 1
    )  # each APP_CUSTOMER organization (2) has 8 clients + one COMPUTE_PROVODER organization with cpjes client
    for client in clients:
        assert len(client.redirect_uris) == 0
        assert len(client.web_origins) == 0

    try:
        run_data_generator(
            "v2.configure_deployments",
            "test_deployment_configurations_v2_api_model.json",
        )

        clients = admin_controller.get_all_clients()
        validated_clients = set()
        for client in clients:
            if client.name.endswith("dmc"):
                assert len(client.redirect_uris) > 0
                assert len(client.web_origins) > 0
                for redirect_uri in client.redirect_uris:
                    assert (
                        redirect_uri.endswith("/data/")
                        or redirect_uri.endswith("/mds-api/*")
                        or "localhost" in redirect_uri
                    )
                for web_origin in client.web_origins:
                    assert web_origin.endswith("/data/") or "localhost" in web_origin
                validated_clients.add(client.name)
            elif client.name.endswith("wbc"):
                assert len(client.redirect_uris) > 0
                assert len(client.web_origins) > 0
                for redirect_uri in client.redirect_uris:
                    assert (
                        redirect_uri.endswith("/wbc2/")
                        or redirect_uri.endswith("/wbc3/")
                        or redirect_uri.endswith("/wbs-api/*")
                        or "localhost" in redirect_uri
                    )
                for web_origin in client.web_origins:
                    assert (
                        web_origin.endswith("/wbc2/")
                        or web_origin.endswith("/wbc3/")
                        or "localhost" in web_origin
                    )
                validated_clients.add(client.name)
            else:
                assert len(client.redirect_uris) == 0
                assert len(client.web_origins) == 0

        assert "dmc" in validated_clients or "wbc" in validated_clients

        organization_ids_after = set(admin_controller.get_all_organization_ids())
        created_organization_ids = organization_ids_after.difference(
            organization_ids_before
        )

        user_by_organization_id_by_role = create_users_for_each_organization_user_role(
            created_organization_ids
        )
        verify_updating_client_access(user_by_organization_id_by_role)
    finally:
        user_ids_after = set(datagen_wipe.get_all_user_ids())
        organization_ids_after = set(admin_controller.get_all_organization_ids())
        created_organization_ids = organization_ids_after.difference(
            organization_ids_before
        )
        if len(created_organization_ids) > 0:
            for organization_id in created_organization_ids:
                admin_controller.delete_organization(organization_id)
        created_user_ids = user_ids_after.difference(user_ids_before)
        if len(created_user_ids) > 0:
            for user_id in created_user_ids:
                admin_controller.delete_user(user_id)
        datagen_wipe.assert_all_data_was_wiped()


def create_users_for_each_organization_user_role(created_organization_ids):
    user_by_organization_id_by_role = {}
    i = 1
    for organization_id in created_organization_ids:
        users_by_role = {}
        user_by_organization_id_by_role[organization_id] = users_by_role
        for user_role in get_allowed_user_roles(organization_id):
            users_by_role[user_role] = create_organization_user_with_role(
                i, user_role, organization_id
            )
            i += 1
    return user_by_organization_id_by_role


def get_allowed_audiences_by_user_role_by_organization_id_from_user_access_tokens(
    users_by_role_by_organization_id: Dict[
        str,
        Dict[OrganizationUserRole, Tuple[PostUserEntity, RegisteredUserEntity, Dict]],
    ]
) -> Dict[str, Dict[OrganizationUserRole, List[str]]]:
    login_manager.clear_cache()
    allowed_audiences_by_user_role_by_organization_id = {}
    for organization_id in users_by_role_by_organization_id:
        allowed_audiences_by_user_role = {}
        allowed_audiences_by_user_role_by_organization_id[
            organization_id
        ] = allowed_audiences_by_user_role
        users_by_role = users_by_role_by_organization_id[organization_id]
        for user_role, (post_user, user, _) in users_by_role.items():
            _, decoded_token = login_manager.get_user_tokens(
                user.username, post_user.password
            )
            allowed_audiences_by_user_role[user_role] = typing.cast(
                List[str], decoded_token["aud"]
            )
    return allowed_audiences_by_user_role_by_organization_id


def get_expected_audiences(
    organization_id: str, client_types: Sequence[str], user_role: OrganizationUserRole
):
    audiences = (
        [f"{organization_id}.{client_type}" for client_type in client_types]
        # all users that log-in via portal_client (login_manager default) get the auth_service_client and
        # event_service_client audiences:
        + ["auth_service_client", "event_service_client"]
    )
    if user_role == OrganizationUserRole.APP_MAINTAINER:
        audiences.append("marketplace_service_client")
    return sorted(audiences)


def verify_updating_client_access(
    users_by_role_by_organization_id: Dict[
        str,
        Dict[OrganizationUserRole, Tuple[PostUserEntity, RegisteredUserEntity, Dict]],
    ],
):
    test_orga_1_id = (
        PublicController().get_organization_by_name("test_orga_1").organization_id
    )
    test_orga_2_id = (
        PublicController().get_organization_by_name("test_orga_2").organization_id
    )
    expected_client_audiences_for_by_user_role = {
        OrganizationUserRole.DATA_MANAGER: ["mds", "idms", "us"],
        OrganizationUserRole.PATHOLOGIST: ["wbs"],
        OrganizationUserRole.MANAGER: [],
        OrganizationUserRole.APP_MAINTAINER: [],
        OrganizationUserRole.CLEARANCE_MAINTAINER: [],
    }
    actual_allowed_audiences_by_user_role_by_organization_id = (
        get_allowed_audiences_by_user_role_by_organization_id_from_user_access_tokens(
            users_by_role_by_organization_id
        )
    )
    for organization_id in (test_orga_1_id, test_orga_2_id):
        actual_allowed_audiences_by_user_role = (
            actual_allowed_audiences_by_user_role_by_organization_id[organization_id]
        )
        for user_role in get_allowed_user_roles(organization_id):
            actual_aud = actual_allowed_audiences_by_user_role[user_role]
            if isinstance(actual_aud, str):
                actual_aud = typing.cast(List[str], [actual_aud])
            actual_aud.remove("account")
            expected_aud = get_expected_audiences(
                organization_id,
                expected_client_audiences_for_by_user_role.get(user_role, []),
                user_role,
            )

            assert sorted(actual_aud) == expected_aud
            print(
                f"User role {user_role.value} of organization {organization_id} "
                f"grants audience {actual_aud} before updating clients"
            )
            # TODO: service account
