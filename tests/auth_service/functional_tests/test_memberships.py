from typing import Dict

import pytest

from tests.tests.auth_service.functional_tests.utils.data_creation_utils import (
    create_organization,
    create_user,
    join_organization,
)
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    DefaultActiveOrganization,
    RequestState,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_get_empty_memberships():
    my_controller = MyController()
    _, user, auth_headers = create_user("test-user@empaia.org")
    try:
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 0
    finally:
        AdminController().delete_user(user.user_id)


def test_get_memberships_only_request_exists():
    my_controller = MyController()
    _, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    _, user, auth_headers = create_user("test-user@empaia.org")
    organization = create_organization("test-orga@empaia.org", manager_auth_headers)
    try:
        my_controller.request_membership(
            organization.organization_id, auth_headers=auth_headers
        )
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 1
        assert memberships.memberships[0].membership_request_id is not None
        assert memberships.memberships[0].membership_request_id > 0
        assert (
            memberships.memberships[0].organization_id == organization.organization_id
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization.organization_id)


def test_get_memberships_only_active_membership_exists():
    my_controller = MyController()
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    user_data, user, auth_headers = create_user("test-user@empaia.org")
    organization = create_organization("test-orga@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        my_controller.request_membership(
            organization.organization_id, auth_headers=auth_headers
        )
        membership_request = my_controller.get_membership_request(
            organization.organization_id, auth_headers=auth_headers
        )
        ManagerController().accept_membership_request(
            organization.organization_id,
            membership_request.membership_request_id,
            auth_headers=manager_auth_headers,
        )
        auth_headers = login_manager.user(
            user_data.contact_data.email_address, user_data.password, clear_cache=True
        )
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 1
        assert memberships.memberships[0].membership_request_id is None
        assert (
            memberships.memberships[0].organization_id == organization.organization_id
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization.organization_id)


def test_get_memberships_request_and_active_membership_exist():
    my_controller = MyController()
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    _, user, auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    organization_2 = create_organization("test-orga-2@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        my_controller.request_membership(
            organization_1.organization_id, auth_headers=auth_headers
        )
        my_controller.request_membership(
            organization_2.organization_id, auth_headers=auth_headers
        )
        membership_request = my_controller.get_membership_request(
            organization_1.organization_id, auth_headers=auth_headers
        )
        membership_requests_orga_1 = ManagerController().get_membership_requests(
            organization_1.organization_id, auth_headers=manager_auth_headers
        )
        assert len(membership_requests_orga_1.membership_requests) == 1
        membership_requests_orga_2 = ManagerController().get_membership_requests(
            organization_2.organization_id, auth_headers=manager_auth_headers
        )
        assert len(membership_requests_orga_2.membership_requests) == 1

        ManagerController().accept_membership_request(
            organization_1.organization_id,
            membership_request.membership_request_id,
            auth_headers=manager_auth_headers,
        )
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 2
        membership_1 = [
            m
            for m in memberships.memberships
            if m.organization_id == organization_1.organization_id
        ][0]
        membership_2 = [
            m
            for m in memberships.memberships
            if m.organization_id == organization_2.organization_id
        ][0]
        assert membership_1.membership_request_id is None
        assert membership_2.membership_request_id is not None

        membership_requests_orga_1 = ManagerController().get_membership_requests(
            organization_1.organization_id, auth_headers=manager_auth_headers
        )
        assert len(membership_requests_orga_1.membership_requests) == 0
        membership_requests_orga_2 = ManagerController().get_membership_requests(
            organization_2.organization_id, auth_headers=manager_auth_headers
        )
        assert len(membership_requests_orga_2.membership_requests) == 1
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)
        AdminController().delete_organization(organization_2.organization_id)


def test_revoke_membership_request():
    my_controller = MyController()
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    _, user, auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        my_controller.request_membership(
            organization_1.organization_id, auth_headers=auth_headers
        )
        membership_request = my_controller.get_membership_request(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert membership_request.request_state == RequestState.REQUESTED
        my_controller.revoke_membership_request(
            membership_requests_id=membership_request.membership_request_id,
            auth_headers=auth_headers,
        )
        membership_request = my_controller.get_membership_request(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert membership_request.request_state == RequestState.REVOKED
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 0
        membership_requests = my_controller.get_membership_requests(
            auth_headers=auth_headers
        )
        assert len(membership_requests.membership_requests) == 1
        assert (
            membership_requests.membership_requests[0].request_state
            == RequestState.REVOKED
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_deny_membership_request():
    my_controller = MyController()
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    _, user, auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        my_controller.request_membership(
            organization_1.organization_id, auth_headers=auth_headers
        )
        membership_request = my_controller.get_membership_request(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert membership_request.request_state == RequestState.REQUESTED
        ManagerController().deny_membership_request(
            organization_id=organization_1.organization_id,
            membership_request_id=membership_request.membership_request_id,
            reviewer_comment="not allowed to access orga",
            auth_headers=manager_auth_headers,
        )
        membership_request = my_controller.get_membership_request(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert membership_request.request_state == RequestState.REJECTED
        memberships = my_controller.get_memberships(auth_headers=auth_headers)
        assert len(memberships.memberships) == 0
        membership_requests = my_controller.get_membership_requests(
            auth_headers=auth_headers
        )
        assert len(membership_requests.membership_requests) == 1
        assert (
            membership_requests.membership_requests[0].request_state
            == RequestState.REJECTED
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def change_default_active_organization(
    organization: ConfidentialOrganizationProfileEntity, user_auth_headers: Dict
) -> DefaultActiveOrganization:
    default_active_organization = MyController().put_default_active_organization(
        organization.organization_id, auth_headers=user_auth_headers
    )
    assert default_active_organization.organization_id == organization.organization_id
    default_active_organization = MyController().get_default_active_organization(
        auth_headers=user_auth_headers
    )
    assert default_active_organization.organization_id == organization.organization_id
    return default_active_organization


def test_default_active_organization():
    my_controller = MyController()
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    _, user, auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    organization_2 = create_organization("test-orga-2@empaia.org", manager_auth_headers)
    organization_3 = create_organization("test-orga-3@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        # Assert that initially no default active organization is set
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert default_active_organization.organization_id is None

        # Join organization 1 and assert that joining does not automatically makes it the default active
        join_organization(
            organization_1.organization_id, auth_headers, manager_auth_headers
        )

        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert default_active_organization.organization_id is None

        # Make organization 1 the default active
        default_active_organization = my_controller.put_default_active_organization(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_1.organization_id
        )
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_1.organization_id
        )

        # Try making organization 2 the default active, it should fail because the user is no member
        try:
            my_controller.put_default_active_organization(
                organization_2.organization_id, auth_headers=auth_headers
            )
        except AssertionError as e:
            if 'should not fail: 400 b\'{"error":"Bad Request"' not in str(
                e
            ) or '"error_description":"Cannot set default active organization: user is not a member"' not in str(
                e
            ):
                raise e
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_1.organization_id
        )

        # Join organization 2 and make it the default active
        join_organization(
            organization_2.organization_id, auth_headers, manager_auth_headers
        )

        default_active_organization = my_controller.put_default_active_organization(
            organization_2.organization_id, auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_2.organization_id
        )
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_2.organization_id
        )

        # Join organization 3, delete it, organization 2 should still be the default active
        join_organization(
            organization_3.organization_id, auth_headers, manager_auth_headers
        )

        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_2.organization_id
        )

        # Unset the default active organization
        default_active_organization = my_controller.put_default_active_organization(
            None, auth_headers=auth_headers
        )
        assert default_active_organization.organization_id is None
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert default_active_organization.organization_id is None

        # Make organization 1 the default active, delete it, and expect that the default active organization is set
        # to None
        default_active_organization = my_controller.put_default_active_organization(
            organization_1.organization_id, auth_headers=auth_headers
        )
        assert (
            default_active_organization.organization_id
            == organization_1.organization_id
        )

        AdminController().delete_organization(organization_1.organization_id)
        organization_1 = None
        default_active_organization = my_controller.get_default_active_organization(
            auth_headers=auth_headers
        )
        assert default_active_organization.organization_id is None

    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        if organization_1 is not None:
            AdminController().delete_organization(organization_1.organization_id)
        AdminController().delete_organization(organization_2.organization_id)
        AdminController().delete_organization(organization_3.organization_id)
