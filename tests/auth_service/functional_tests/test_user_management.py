import pytest

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
    PostUserRolesEntity,
    UserRole,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.auth_service.api_controllers.v2.organization_controller import (
    OrganizationControllerV2,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.singletons import api_test_state, login_manager

from ....data_generator_utils import datagen_wipe
from ..functional_tests.utils.data_creation_utils import (
    change_organization_categories,
    change_organization_user_role,
    create_organization,
    create_user,
    get_user_auth_header,
    join_organization,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def setup_teardown():
    # setup here
    yield
    # tear down here
    datagen_wipe.assert_all_data_was_wiped()


def test_newly_registered_user_requires_email_verification():
    post_user = PublicController().register_new_user("test-user@empaia.org")
    user = ModeratorController().get_unapproved_user_by_username(
        post_user.contact_data.email_address  # pylint: disable=E1101
    )
    assert user is not None
    try:
        PublicController().verify_registration(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )
        unapproved_user = ModeratorController().get_unapproved_user_by_username(
            post_user.contact_data.email_address  # pylint: disable=E1101
        )
        assert unapproved_user is None
    finally:
        AdminController().delete_user(user.user_id)


def test_user_deletes_own_account():
    my_controller = MyController()
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")

    my_controller.delete_account(auth_headers=user_auth_headers)
    try:
        ModeratorController().get_user_profile(target_user_id=user.user_id)
    except AssertionError as e:
        assert "User was not found" in str(e)

    try:
        # try to login again with credentials of delete user
        _updated_user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )
    except AssertionError as e:
        assert "Expected status code 302, but got 200:" in str(e)


def test_user_access_token():
    post_user, user, _ = create_user("test-user@empaia.org")

    try:
        _, decoded_token = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert "auth_service_client" in decoded_token["aud"]
        assert "event_service_client" in decoded_token["aud"]
        assert "marketplace_service_client" not in decoded_token["aud"]

        ModeratorController().set_user_roles(
            user.user_id, PostUserRolesEntity(roles=[UserRole.MODERATOR.name])
        )
        login_manager.clear_cache_for_user(post_user.contact_data.email_address)
        _, decoded_token_2 = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert "auth_service_client" in decoded_token["aud"]
        assert "event_service_client" in decoded_token_2["aud"]
        assert "marketplace_service_client" in decoded_token_2["aud"]

        ModeratorController().set_user_roles(
            user.user_id, PostUserRolesEntity(roles=[])
        )
        login_manager.clear_cache_for_user(post_user.contact_data.email_address)
        _, decoded_token_3 = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert "auth_service_client" in decoded_token["aud"]
        assert "event_service_client" in decoded_token_3["aud"]
        assert "marketplace_service_client" not in decoded_token_3["aud"]
    finally:
        AdminController().delete_user(user.user_id)


def test_user_profile_entity():
    _, user, user_auth_headers = create_user("test-user@empaia.org")

    try:
        my_controller = MyController()
        user_profile = my_controller.get_profile(user_auth_headers)
        assert user_profile.details.last_name is not None
        assert user_profile.details.first_name is not None

        user_profile_mod = ModeratorController().get_user_profile(
            target_user_id=user.user_id
        )
        assert user_profile.details == user_profile_mod.details
    finally:
        AdminController().delete_user(user.user_id)


def test_admin_tries_to_delete_own_account():
    admin_auth_headers = login_manager.super_admin()
    my_controller = MyController()
    try:
        my_controller.delete_account(auth_headers=admin_auth_headers)
    except AssertionError as e:
        assert (
            "Account deletion for user with role ADMIN or MODERATOR not allowed"
            in str(e)
        )


def test_last_manager_tries_to_delete_own_account():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    my_controller = MyController()
    try:
        my_controller.delete_account(auth_headers=manager_auth_headers)
    except AssertionError as e:
        assert (
            "Account deletion not allowed. User is the last manager in charge for one of its organization"
            in str(e)
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization.organization_id)


def test_moderator_tries_to_delete_last_manager_in_orga_account():
    _, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    try:
        ModeratorController().delete_user_account(target_user_id=manager.user_id)
    except AssertionError as e:
        assert (
            "Account deletion not allowed. User is the last manager in charge for one of its organization"
            in str(e)
        )
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_organization(organization.organization_id)


def test_delete_user_with_draft_orga():
    post_user, _, user_auth_headers = create_user("test-user@empaia.org")
    organization = MyUnapprovedOrganizationsController().create_new_organization(
        email_address=post_user.contact_data.email_address,
        auth_headers=user_auth_headers,
        organization_categories=[OrganizationCategory.APP_CUSTOMER],
    )
    my_controller = MyController()
    my_controller.delete_account(auth_headers=user_auth_headers)
    try:
        ModeratorController().get_unapproved_organization(organization.organization_id)
    except AssertionError as e:
        assert "Organization not found" in str(e)


def test_last_manager_multiple_organizations_tries_to_delete_own_account():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    organization_2 = create_organization("test-orga-2@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )

    # can't delete because user is last manager in two organizations
    my_controller = MyController()
    try:
        my_controller.delete_account(auth_headers=manager_auth_headers)
    except AssertionError as e:
        assert (
            "Account deletion not allowed. User is the last manager in charge for one of its organization"
            in str(e)
        )

    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    # join orga 1 and request manager role
    user_auth_headers = join_orga_and_request_manager_role(
        organization_1, post_user, user_auth_headers, manager_auth_headers
    )

    # can't delete because user is still last manager in one organizations
    try:
        my_controller.delete_account(auth_headers=manager_auth_headers)
    except AssertionError as e:
        assert (
            "Account deletion not allowed. User is the last manager in charge for one of its organization"
            in str(e)
        )

    # join orga 2 and request manager role
    user_auth_headers = join_orga_and_request_manager_role(
        organization_2, post_user, user_auth_headers, manager_auth_headers
    )

    # manager can finally delete itself
    my_controller.delete_account(auth_headers=manager_auth_headers)
    try:
        ModeratorController().get_user_profile(target_user_id=manager.user_id)
    except AssertionError as e:
        assert "User was not found" in str(e)
    finally:
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)
        AdminController().delete_organization(organization_2.organization_id)


def join_orga_and_request_manager_role(
    organization_1, post_user, user_auth_headers, manager_auth_headers
):
    _ = join_organization(
        organization_id=organization_1.organization_id,
        user_auth_headers=user_auth_headers,
        manager_auth_headers=manager_auth_headers,
    )
    user_auth_headers = get_user_auth_header(
        post_user.contact_data.email_address, post_user.password
    )
    _ = OrganizationControllerV2().request_organization_user_roles(
        organization_id=organization_1.organization_id,
        organization_user_roles=[OrganizationUserRole.MANAGER],
        auth_headers=user_auth_headers,
    )
    organization_role_requests = (
        OrganizationControllerV2().get_organization_user_role_requests(
            organization_id=organization_1.organization_id,
            auth_headers=user_auth_headers,
        )
    )
    ManagerController().accept_organization_user_role_request(
        organization_id=organization_1.organization_id,
        role_request_id=organization_role_requests.organization_user_role_requests[
            0
        ].organization_user_role_request_id,
        auth_headers=manager_auth_headers,
    )

    return user_auth_headers


def test_validate_token_mapping_on_category_change():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = change_organization_categories(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.PRODUCT_PROVIDER],
            manager_auth_headers=manager_auth_headers,
        )

        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        # updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        # assign clearance maintainer role
        _ = change_organization_user_role(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.CLEARANCE_MAINTAINER],
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        # get user tokens
        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )

        assert "auth_service_client" in decoded_token["aud"]
        assert "event_service_client" in decoded_token["aud"]
        assert "marketplace_service_client" in decoded_token["aud"]

        assert organization_1.organization_id in decoded_token["organizations"]
        assert (
            "clearance-maintainer"
            in decoded_token["organizations"][organization_1.organization_id]["roles"]
        )

        _ = change_organization_categories(
            organization_id=organization_1.organization_id,
            organization_categories=[OrganizationCategory.APP_CUSTOMER],
            manager_auth_headers=manager_auth_headers,
        )
        # updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        _ = change_organization_user_role(
            organization_id=organization_1.organization_id,
            organization_user_roles=[OrganizationUserRole.DATA_MANAGER],
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )

        assert f"{organization_1.organization_id}.idms" in decoded_token["aud"]
        assert f"{organization_1.organization_id}.mds" in decoded_token["aud"]
        assert f"{organization_1.organization_id}.us" in decoded_token["aud"]
        assert (
            "data-manager"
            in decoded_token["organizations"][organization_1.organization_id]["roles"]
        )
        assert (
            len(decoded_token["organizations"][organization_1.organization_id]["roles"])
            == 1
        )

        _ = change_organization_categories(
            organization_id=organization_1.organization_id,
            organization_categories=[],
            manager_auth_headers=manager_auth_headers,
        )

        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )
        assert f"{organization_1.organization_id}.idms" not in decoded_token["aud"]
        assert f"{organization_1.organization_id}.mds" not in decoded_token["aud"]
        assert f"{organization_1.organization_id}.us" not in decoded_token["aud"]
        assert "marketplace_service_client" not in decoded_token["aud"]
        assert (
            len(decoded_token["organizations"][organization_1.organization_id]["roles"])
            == 0
        )

        _ = change_organization_categories(
            organization_id=organization_1.organization_id,
            organization_categories=[
                OrganizationCategory.APP_CUSTOMER,
                OrganizationCategory.APP_VENDOR,
            ],
            manager_auth_headers=manager_auth_headers,
        )
        # updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        _ = change_organization_user_role(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.APP_MAINTAINER,
                OrganizationUserRole.PATHOLOGIST,
            ],
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )

        assert f"{organization_1.organization_id}.wbs" in decoded_token["aud"]
        assert f"{organization_1.organization_id}.idms" not in decoded_token["aud"]
        assert f"{organization_1.organization_id}.mds" not in decoded_token["aud"]
        assert f"{organization_1.organization_id}.us" not in decoded_token["aud"]
        assert "marketplace_service_client" in decoded_token["aud"]
        roles = decoded_token["organizations"][organization_1.organization_id]["roles"]
        assert len(roles) == 2
        assert "app-maintainer" in roles
        assert "pathologist" in roles
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_validate_token_role_mapping_on_moderator_category_drop():
    manager_data, manager, manager_auth_headers = create_user("test-manager@empaia.org")
    post_user, user, user_auth_headers = create_user("test-user@empaia.org")
    organization_1 = create_organization("test-orga-1@empaia.org", manager_auth_headers)
    manager_auth_headers = login_manager.user(
        manager_data.contact_data.email_address, manager_data.password, clear_cache=True
    )
    try:
        _ = change_organization_categories(
            organization_id=organization_1.organization_id,
            organization_categories=[
                OrganizationCategory.APP_CUSTOMER,
                OrganizationCategory.PRODUCT_PROVIDER,
            ],
            manager_auth_headers=manager_auth_headers,
        )

        _ = join_organization(
            organization_id=organization_1.organization_id,
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        # updated auth header
        user_auth_headers = get_user_auth_header(
            post_user.contact_data.email_address, post_user.password
        )

        # assign clearance maintainer role
        _ = change_organization_user_role(
            organization_id=organization_1.organization_id,
            organization_user_roles=[
                OrganizationUserRole.MANAGER,
                OrganizationUserRole.DATA_MANAGER,
                OrganizationUserRole.PATHOLOGIST,
                OrganizationUserRole.CLEARANCE_MAINTAINER,
            ],
            user_auth_headers=user_auth_headers,
            manager_auth_headers=manager_auth_headers,
        )

        # get user tokens
        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )

        roles = decoded_token["organizations"][organization_1.organization_id]["roles"]
        assert len(roles) == 4
        assert "manager" in roles
        assert "data-manager" in roles
        assert "pathologist" in roles
        assert "clearance-maintainer" in roles

        ModeratorController().set_category_request(
            organization_id=organization_1.organization_id,
            categories=[OrganizationCategory.PRODUCT_PROVIDER],
        )

        user_auth_headers, decoded_token = relogin_and_get_user_token(
            post_user=post_user
        )

        roles = decoded_token["organizations"][organization_1.organization_id]["roles"]
        assert len(roles) == 2
        assert "manager" in roles
        assert "clearance-maintainer" in roles
    finally:
        AdminController().delete_user(manager.user_id)
        AdminController().delete_user(user.user_id)
        AdminController().delete_organization(organization_1.organization_id)


def test_global_role_empaia_international_associate():
    post_user, user, _ = create_user("test-user@empaia.org")

    try:
        _, decoded_token = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert "auth_service_client" not in decoded_token["resource_access"]
        assert "medical_device_companion_client" not in decoded_token["aud"]

        ModeratorController().set_user_roles(
            user.user_id,
            PostUserRolesEntity(roles=[UserRole.EMPAIA_INTERNATIONAL_ASSOCIATE.name]),
        )
        login_manager.clear_cache_for_user(post_user.contact_data.email_address)
        _, decoded_token_2 = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert (
            "empaia-international-associate"
            in decoded_token_2["resource_access"]["auth_service_client"]["roles"]
        )
        assert "medical_device_companion_client" in decoded_token_2["aud"]

        ModeratorController().set_user_roles(
            user.user_id, PostUserRolesEntity(roles=[])
        )
        login_manager.clear_cache_for_user(post_user.contact_data.email_address)
        _, decoded_token_3 = login_manager.get_user_tokens(
            post_user.contact_data.email_address, post_user.password
        )
        assert "auth_service_client" not in decoded_token_3["resource_access"]
        assert "medical_device_companion_client" not in decoded_token_3["aud"]
    finally:
        AdminController().delete_user(user.user_id)


def relogin_and_get_user_token(post_user):
    user_auth_headers = get_user_auth_header(
        post_user.contact_data.email_address, post_user.password
    )
    _, decoded_token = login_manager.get_user_tokens(
        post_user.contact_data.email_address, post_user.password
    )
    return user_auth_headers, decoded_token
