import copy
import json
import os
import subprocess
from http import HTTPStatus
from pathlib import Path
from typing import Dict, List, Optional, Sequence, Type, Union

import pytest
from keycloak import KeycloakGetError

from ...data_generators.keycloak_initialize import (
    AuthServiceInitializer,
)
from ...keycloak_initialize.client_creator import (
    Client,
    ClientCreator,
    FrontendClient,
    FrontendClientCreator,
    ServiceClient,
    ServiceClientCreator,
)
from ...keycloak_initialize.empaia_keycloak_admin import (
    EmpaiaKeycloakAdmin,
)
from ...keycloak_initialize.empaia_realm_creator import (
    EMPAIA_REALM_NAME,
)
from ...keycloak_initialize.global_clients_manager import ClientLists
from ...keycloak_initialize.settings import InitializeSettings
from ...utils import tools
from ...utils.singletons import api_test_state, login_manager

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_keycloak_initialize_test,
    reason="Skipping keycloak_initialize.py tests",
)


data_generator_scripts_directory = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../..",
    "..",
    "data_generators",
    "v2_api",
)


def exists_empaia_realm() -> bool:
    keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
    return _exists_empaia_realm(keycloak_admin)


def _exists_empaia_realm(keycloak_admin: EmpaiaKeycloakAdmin) -> bool:
    realm_representation = None
    try:
        realm_representation = keycloak_admin.get_realm(EMPAIA_REALM_NAME)
    except KeycloakGetError as e:
        if e.response_code != HTTPStatus.NOT_FOUND:
            raise
    return realm_representation is not None


def assert_no_empaia_realm():
    if exists_empaia_realm():
        raise AssertionError(
            f"test_keycloak_initialize requires that the '{EMPAIA_REALM_NAME}' realm does not exist in keycloak"
        )


def cleanup_empaia_realm():
    keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
    if _exists_empaia_realm(keycloak_admin):
        keycloak_admin.delete_realm(EMPAIA_REALM_NAME)


def get_path_to_env_file(env_file_name: str) -> Path:
    return Path(os.path.dirname(__file__)) / f"../../../{env_file_name}"


initialize_settings = InitializeSettings(
    _env_file=get_path_to_env_file(".initialize.env")
)


def get_auth_service_initializer() -> AuthServiceInitializer:
    initializer = AuthServiceInitializer(initialize_settings)
    initializer.switch_realm(EMPAIA_REALM_NAME)
    return initializer


def setup_teardown():
    # setup here
    assert_no_empaia_realm()
    yield
    # tear down here
    cleanup_empaia_realm()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def run_command(cmd: List[str], exec_directory: Path, *args):
    pwd = os.path.abspath(os.getcwd())
    cmd = cmd.copy()
    cmd.extend(args)
    try:
        os.chdir(exec_directory)
        print(f"Running '{cmd}' in '{exec_directory}'")
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(f"Output of '{cmd}' is:\n{output.decode('utf8')}")
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf8")
        print(f"Output of '{cmd}' is:\n{output}")
        raise e
    finally:
        os.chdir(pwd)


def run_keycloak_initialize(*args, exec_directory: Optional[Union[Path, str]] = None):
    root_directory = os.path.abspath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../..")
    )
    if exec_directory is None:
        exec_directory = root_directory
    else:
        os.environ["PYTHONPATH"] = (
            root_directory + os.pathsep + os.environ.get("PYTHONPATH", "")
        )
    run_command(
        ["python", "-m", "tests.data_generators.keycloak_initialize"],
        Path(exec_directory),
        *args,
    )


class FileBackup:
    def __init__(self, file_path: Path, file_content: str):
        self._file_path = file_path
        self._backup_file_path = Path(f"{file_path}.backup")
        if self._backup_file_path.is_file():
            os.remove(self._backup_file_path)
        if os.path.isfile(self._file_path):
            self._file_path.rename(self._backup_file_path)
        with open(file_path, "w", encoding="utf8") as f:
            f.write(file_content)

    def restore(self):
        os.remove(self._file_path)
        if self._backup_file_path.is_file():
            self._backup_file_path.rename(self._file_path)


def run_keycloak_initialize_and_backup_initialize_env(initialize_env: str, *args):
    env_file_backup = FileBackup(get_path_to_env_file(".env"), ENV)
    initialize_env_file_backup = FileBackup(
        get_path_to_env_file(".initialize.env"), initialize_env
    )
    try:
        run_keycloak_initialize(*args)
    finally:
        env_file_backup.restore()
        initialize_env_file_backup.restore()


def query_client_secret(client: ServiceClient) -> str:
    keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
    keycloak_client = keycloak_admin.get_client_by_client_id(client.client_id)
    secret_data = keycloak_admin.get_client_secrets(keycloak_client["id"])
    assert secret_data["type"] == "secret"
    return secret_data["value"]


def filter_actual_data_for_comparison_with_expected_data(
    actual_client_data: Dict, expected_data_keys: Sequence[str]
) -> Dict:
    actual_data: Dict = {}
    for key in expected_data_keys:
        actual_data[key] = actual_client_data[key]
        if key in ("defaultClientScopes", "redirectUris"):
            actual_data[key].sort()
    return actual_data


def get_expected_client_data(client: Client, client_creator_class: Type[ClientCreator]):
    expected_data = client_creator_class.create_client_data(client)
    expected_data["defaultClientScopes"] = sorted(
        ["web-origins", "acr", "roles", "profile", "email"] + ["organization"]
    )
    if "redirectUris" in expected_data:
        expected_data["redirectUris"].sort()
    return expected_data


def verify_client_data(client: Client, client_creator_class: Type[ClientCreator]):
    keycloak_admin = get_auth_service_initializer().get_keycloak_admin_interface()
    actual_data = keycloak_admin.get_client_by_client_id(client.client_id)
    if actual_data is None:
        raise AssertionError(
            f"Client with clientId '{client.client_id}' does not exist"
        )
    expected_data = get_expected_client_data(client, client_creator_class)
    actual_data = filter_actual_data_for_comparison_with_expected_data(
        actual_data, list(expected_data.keys())
    )
    tools.assert_dicts_are_equal(
        expected_data,
        "expected client data",
        actual_data,
        "actual client data",
    )
    for key, value in expected_data.items():
        assert key in actual_data
        assert actual_data[key] == value


def verify_frontend_client(client: FrontendClient):
    verify_client_data(client, FrontendClientCreator)


def verify_service_client(client: ServiceClient):
    verify_client_data(client, ServiceClientCreator)
    service_client_secret = client.secret
    if service_client_secret is None:
        # password is left empty, keycloak will have generated it, so query it:
        service_client_secret = query_client_secret(client)
    _, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client.client_id, service_client_secret
    )
    if client.audiences is not None:
        for expected_aud in client.audiences:
            if expected_aud not in decoded_token["aud"]:
                raise AssertionError(
                    f"Client '{client.client_id}' misses '{expected_aud}' in 'aud' claim"
                )


def verify_global_clients(client_settings: Dict):
    clients = ClientLists.model_validate(client_settings)
    for frontend_client in clients.frontends:
        verify_frontend_client(frontend_client)
    for service_client in clients.services:
        verify_service_client(service_client)


def test_keycloak_initialize():
    run_keycloak_initialize_and_backup_initialize_env(INITIALIZE_ENV)
    verify_global_clients(INITIAL_CLIENTS)
    run_keycloak_initialize_and_backup_initialize_env(
        UPDATED_ENV, "--update-global-clients"
    )
    verify_global_clients(UPDATED_CLIENTS)


ENV = """
LOGIN_IDP_URL=http://localhost:8080/auth/realms/empaia
LOGIN_PORTAL_CLIENT_ID=portal_client
LOGIN_SUPER_ADMIN_NAME=empaia_admin@empaia.org
LOGIN_SUPER_ADMIN_PASSWORD=password
LOGIN_AAA_CLIENT_ID=auth_service_client
LOGIN_AAA_CLIENT_SECRET=as_secret
"""


INITIAL_CLIENTS = {
    "frontends": [
        {
            "client_id": "portal_client",
            "name": "Portal Client",
            "base_url": "https://portal.empaia.demo",
            "redirect_uris": [
                "https://portal.empaia.demo",
                "https://lab1.empaia.demo",
                "http://localhost",
                "https://localhost",
            ],
            "web_origins": [
                "https://portal.empaia.demo,http://localhost,https://localhost"
            ],
        },
        {
            "client_id": "swagger",
            "name": "Swagger Client",
            "redirect_uris": [
                "http://localhost:8765/docs/swagger-ui/oauth2-redirect.html"
            ],
            "web_origins": ["+"],
        },
    ],
    "services": [
        {
            "client_id": "auth_service_client",
            "name": "Auth Service",
            "audiences": ["shout_out_service_client"],
            "root_url": "${authBaseUrl}",
            "secret": "as_secret",
        },
        {
            "client_id": "event_service_client",
            "name": "Event Service",
            "secret": "eves_secret",
        },
        {
            "client_id": "marketplace_service_client",
            "name": "Marketplace Service",
            "audiences": ["shout_out_service_client"],
            "secret": "mps_secret",
        },
        {
            "client_id": "shout_out_service_client",
            "name": "Shout Out Service",
            "secret": "sos_secret",
        },
    ],
}

ENV_TEMPLATE = """
ADMIN_NAME=empaia_admin
ADMIN_EMAIL_ADDRESS=empaia_admin@empaia.org
ADMIN_PASSWORD=password

AUTH_SERVICE_CLIENT_ID=auth_service_client

KEYCLOAK_FRONTEND_URL=http://localhost:8080/auth/
KEYCLOAK_USER=keycloak
KEYCLOAK_PASSWORD=password

PORTAL_CLIENT_ID=portal_client

CLIENTS='{client_settings}'
"""

INITIALIZE_ENV = ENV_TEMPLATE.format(client_settings=json.dumps(INITIAL_CLIENTS))


def create_updated_client_settings():
    updated_settings = copy.deepcopy(INITIAL_CLIENTS)
    portal_client = updated_settings["frontends"][0]

    # test data for updating a frontend client:
    portal_client.update(
        {
            "client_id": "new_portal_client",
            "old_client_id": "portal_client",
            "name": "New Portal Client",
            "base_url": "https://new-portal.empaia.demo",
            "redirect_uris": ["https://new-portal.empaia.demo"],
            "web_origins": ["https://new-portal.empaia.demo"],
        }
    )

    # test data for updating a service client:
    auth_service_client = updated_settings["services"][0]
    auth_service_client.update(
        {
            "client_id": "org.empaia.prod.aaa",
            "name": "Old Name",
            "old_client_id": "auth_service_client",
            "root_url": "http://localhost",
        }
    )

    # test data for adding a service client which gets a random secret:
    updated_settings["services"].append(
        {"client_id": "new_service_client", "name": "New Service"}
    )

    # removing a client is not supported, this should be done via the keycloak admin console

    return updated_settings


UPDATED_CLIENTS = create_updated_client_settings()

UPDATED_ENV = ENV_TEMPLATE.format(client_settings=json.dumps(UPDATED_CLIENTS))
