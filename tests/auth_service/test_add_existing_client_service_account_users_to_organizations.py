import copy
import os
import time
from pathlib import Path
from typing import Callable, Dict, List, Optional, Set

import pytest
from keycloak import KeycloakAuthenticationError

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)

from ...data_generator_utils import (
    wait_for_auth_service_to_be_available,
)
from ...keycloak_initialize.empaia_keycloak_admin import (
    EmpaiaKeycloakAdmin,
)
from ...utils.auth_service.api_controllers.v2.entity_models import (
    ClientEntity,
    PublicOrganizationProfileEntity,
)
from ...utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from ...utils.singletons import api_test_state
from .test_keycloak_initialize import (
    assert_no_empaia_realm,
    cleanup_empaia_realm,
    get_auth_service_initializer,
    run_command,
    run_keycloak_initialize,
)

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_keycloak_initialize_test,
    reason="Skipping keycloak_initialize.py tests",
)


def setup_teardown():
    # setup here
    assert_no_empaia_realm()
    yield
    # tear down here
    cleanup_empaia_realm()


setup_teardown_fixture = pytest.fixture(scope="module", autouse=True)(setup_teardown)


def test_add_existing_client_service_account_users_to_organizations():
    helper = Helper()
    helper.create_empaia_realm_and_start_auth_service()

    try:
        helper.create_organization_and_clients()

        client_service_account_users = helper.get_client_service_account_users()
        # new client service accounts users are already organization members:
        helper.assert_client_service_account_user_organization_memberships(
            membership_expected=True,
            client_service_account_users=client_service_account_users,
        )
    finally:
        # DEBUG
        # helper.print_auth_service_logs()
        helper.stop_auth_service()

    # remove organization clients from organization groups in keycloak:
    helper.remove_service_client_account_users_from_organization_groups(
        client_service_account_users
    )

    # verify that clients were removed from their organization:
    helper.assert_client_service_account_user_organization_memberships(
        membership_expected=False,
        client_service_account_users=client_service_account_users,
    )

    # fake that upgrading to auth service v0.1.5 is necessary:
    # (WARNING: this might fail if future auth service client upgrades (0.1.6, 0.1.7, ...) exist and do not expect that
    # the data is actually up-to-date, except for the organization membership of service client account users)
    # helper.run_auth_service_upgrade("0.1.4")
    # helper.refresh_keycloak_admin()

    # verify that clients were added to their organization again:
    # helper.assert_client_service_account_user_organization_memberships(
    #    membership_expected=True,
    #    client_service_account_users=client_service_account_users,
    # )


class Helper:
    organization_file_name = "test_organizations_v2_api_model.json"
    organization_deployment_configuration_file = (
        "test_deployment_configurations_v2_api_model.json"
    )
    organization_name = "test_orga_1"

    def __init__(self):
        self._auth_service_client_support: Optional[AuthServiceClientSupport] = None
        self._downgraded_auth_service_client_version: Optional[str] = None
        self._keycloak_admin = (
            get_auth_service_initializer().get_keycloak_admin_interface()
        )
        self._organization: Optional[PublicOrganizationProfileEntity] = None
        self._root_directory = (
            Path(os.path.abspath(os.path.dirname(__file__))) / "../../.."
        )

    def refresh_keycloak_admin(self):
        self._keycloak_admin = (
            get_auth_service_initializer().get_keycloak_admin_interface()
        )
        self._auth_service_client_support.update_keycloak_admin(self._keycloak_admin)

    def create_empaia_realm_and_start_auth_service(self):
        run_keycloak_initialize(exec_directory=self._root_directory)
        self.run_docker_compose("up", "-d")
        wait_for_auth_service_to_be_available()
        self._auth_service_client_support = AuthServiceClientSupport(
            self._keycloak_admin
        )

    def print_auth_service_logs(self):
        print("### Minio Service Init Logs:")
        self.run_docker_compose("logs", "-t", "minio-service-init")
        print("### Auth Service Logs:")
        self.run_docker_compose("logs", "-t", "auth-service")

    def stop_auth_service(self):
        self.run_docker_compose("stop", "auth-service")
        time.sleep(3)

    def create_organization_and_clients(self):
        self.run_data_generator("create_organizations", self.organization_file_name)
        self.run_data_generator(
            "configure_deployments", self.organization_deployment_configuration_file
        )
        self._organization = PublicController().get_organization_by_name(
            self.organization_name
        )

    def run_docker_compose(self, *cmd):
        run_command(
            ["docker", "compose", "--profile", "pytest"] + list(cmd),
            self._root_directory,
        )

    def run_data_generator(self, name: str, resource_file_name: str):
        run_command(
            [
                "python",
                "-m",
                f"tests.data_generators.auth_service.v2.{name}",
                resource_file_name,
            ],
            self._root_directory,
        )

    def run_auth_service_upgrade(self, downgrade_version: str):
        self.set_auth_service_client_version(downgrade_version)
        # start auth service to run the upgrade to fix the memberships:
        self.run_docker_compose("up", "-d")
        wait_for_auth_service_to_be_available()
        self.refresh_keycloak_admin()
        self.assert_auth_service_client_was_upgraded()

    def set_auth_service_client_version(self, version: str):
        def func():
            self._downgraded_auth_service_client_version = version
            assert (
                self._auth_service_client_support.query_auth_service_version()
                != version
            )
            self._auth_service_client_support.set_auth_service_client_version(version)
            assert (
                self._auth_service_client_support.query_auth_service_version()
                == version
            )

        return self.retry_on_authentication_error_with_refreshed_keycloak_admin(func)

    def assert_auth_service_client_was_upgraded(self):
        assert self._downgraded_auth_service_client_version is not None
        assert (
            self._auth_service_client_support.query_auth_service_version()
            != self._downgraded_auth_service_client_version
        )

    def get_organization_group_member_ids(self) -> Set[str]:
        def func():
            return set(
                [
                    member["id"]
                    for member in self._keycloak_admin.get_group_members(
                        self._organization.organization_id
                    )
                ]
            )

        return self.retry_on_authentication_error_with_refreshed_keycloak_admin(func)

    def get_clients_by_client_id(self) -> Dict[str, ClientEntity]:
        clients_by_client_id: Dict[str, ClientEntity] = {}
        client_entities = AdminController().get_organization_clients(
            self._organization.organization_id
        )

        for client in client_entities:

            def func(*args):
                return self._keycloak_admin.get_client(*args)

            keycloak_client = (
                self.retry_on_authentication_error_with_refreshed_keycloak_admin(
                    func, client.keycloak_id
                )
            )

            if keycloak_client["serviceAccountsEnabled"]:
                clients_by_client_id[client.client_id] = client

        return clients_by_client_id

    def get_client_service_account_users(self) -> List[Dict]:
        client_service_account_users = []
        clients_by_client_id = self.get_clients_by_client_id()
        for client in clients_by_client_id.values():

            def func(*args):
                return self._keycloak_admin.get_client_service_account_user(*args)

            user = self.retry_on_authentication_error_with_refreshed_keycloak_admin(
                func, client.keycloak_id
            )
            assert user is not None
            client_service_account_users.append(user)
        return client_service_account_users

    def remove_service_client_account_users_from_organization_groups(
        self, client_service_account_users: List[Dict]
    ):
        for user in client_service_account_users:

            def func(*args):
                self._keycloak_admin.group_user_remove(*args)

            self.retry_on_authentication_error_with_refreshed_keycloak_admin(
                func, user["id"], self._organization.organization_id
            )

    def assert_client_service_account_user_organization_memberships(
        self, membership_expected: bool, client_service_account_users: List[Dict]
    ):
        organization_group_member_ids = self.get_organization_group_member_ids()
        for user in client_service_account_users:
            if membership_expected:
                if user["id"] not in organization_group_member_ids:
                    raise AssertionError(
                        f"Client service account user '{user['username']}' with id' {user['id']}' "
                        "should be an organization member, but is not."
                    )
            else:
                if user["id"] in organization_group_member_ids:
                    raise AssertionError(
                        f"Client service account user '{user['username']}' with id' {user['id']}' "
                        "should not be an organization member, but is."
                    )

    def retry_on_authentication_error_with_refreshed_keycloak_admin(
        self, func: Callable, *args, **kwargs
    ):
        try:
            return func(*args, **kwargs)
        except KeycloakAuthenticationError:
            self.refresh_keycloak_admin()
        return func(*args, **kwargs)


class AuthServiceClientSupport:
    auth_service_client_id = "auth_service_client"
    auth_service_version_key = "auth_service_version"

    def __init__(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin
        self._auth_service_client = keycloak_admin.get_client_by_client_id(
            self.auth_service_client_id
        )
        self._auth_service_client_id = self._auth_service_client["id"]
        self._auth_service_client_user = keycloak_admin.get_client_service_account_user(
            self._auth_service_client_id
        )
        self._auth_service_client_user_id = self._auth_service_client_user["id"]

    def update_keycloak_admin(self, keycloak_admin: EmpaiaKeycloakAdmin):
        self._keycloak_admin = keycloak_admin

    def set_auth_service_client_version(self, version: str):
        attributes = copy.deepcopy(self._auth_service_client_user["attributes"])
        assert self.auth_service_version_key in attributes
        attributes[self.auth_service_version_key] = [version]
        self._keycloak_admin.update_user(
            self._auth_service_client_user_id, {"attributes": attributes}
        )

    def query_auth_service_version(self) -> str:
        user = self._keycloak_admin.get_client_service_account_user(
            self._auth_service_client_id
        )
        return user["attributes"][self.auth_service_version_key][0]
