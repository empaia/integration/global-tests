import traceback
from typing import Callable, List

from tests.utils.api_test.endpoint_test.endpoint_test import (
    EndpointTest,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.singletons import api_test_state

from .v2.admin_controller import AdminControllerTest
from .v2.domain_controller import DomainControllerTest
from .v2.manager_controller import ManagerControllerTest
from .v2.moderator_controller import ModeratorControllerTest
from .v2.my_controller import MyControllerTest
from .v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsControllerTest,
)
from .v2.organization_controller import OrganizationControllerV2Test
from .v2.public_controller import PublicControllerTest


class ControllerTests:
    def __init__(self):
        self._controller_tests = [
            AdminControllerTest(),
            DomainControllerTest(),
            ManagerControllerTest(),
            ModeratorControllerTest(),
            MyControllerTest(),
            MyUnapprovedOrganizationsControllerTest(),
            OrganizationControllerV2Test(),
            PublicControllerTest(),
        ]

    def assert_endpoint_will_be_tested(
        self,
        method: str,
        path: str,
        access_role: AccessRole,
        test_with_own_organization: bool,
    ):
        found = False
        for ct in self._controller_tests:
            endpoint_test = ct.get_endpoint_test(method, path)
            if endpoint_test is not None:
                found = True
                api_test_state.get_api_results_manager().expect_api_test_results_for_endpoint_test(
                    endpoint_test, access_role, test_with_own_organization
                )
                break
        if not found:
            raise AssertionError(f"No test for endpoint found: {method} {path}")

    def make_test_function(
        self,
        access_role: AccessRole,
        method_filter: str = None,
        endpoint_filter: str = None,
    ):
        test_functions = self._make_test_functions(
            access_role, method_filter, endpoint_filter
        )

        def test(subtests):
            for endpoint, test_function in test_functions:
                with subtests.test(
                    "test_endpoint",
                    method=endpoint.method.upper(),
                    path=endpoint.path,
                    access_role=access_role.value,
                ):
                    try:
                        test_function()
                    except Exception:  # pylint: disable=broad-except
                        traceback.print_exc()
                        break

        return test

    def set_test_functions(
        self,
        module,
        access_role: AccessRole,
        method_filter: str = None,
        endpoint_filter: str = None,
    ):
        for test_name, test_function in self._make_test_functions(
            access_role, method_filter, endpoint_filter
        ):
            setattr(module, test_name, test_function)

    def get_test_functions(
        self,
        access_role: AccessRole,
        method_filter: str = None,
        endpoint_filter: str = None,
        use_v2_api: bool = False,
    ):
        return self._make_test_functions(
            access_role, method_filter, endpoint_filter, use_v2_api
        )

    def _make_test_functions(
        self,
        access_role: AccessRole,
        method_filter: str = None,
        endpoint_filter: str = None,
        use_v2_api: bool = False,
    ) -> List[Callable]:
        test_functions = []
        for ct in self._controller_tests:
            for endpoint_test in ct.get_endpoint_tests():
                if not api_test_state.should_ignore_endpoint_test(
                    endpoint_test, method_filter, endpoint_filter, use_v2_api
                ):
                    test_functions.append(TestFunction(endpoint_test, access_role))
        return test_functions


class TestFunction:
    def __init__(self, endpoint_test: EndpointTest, access_role: AccessRole):
        self._endpoint_test = endpoint_test
        self._access_role = access_role
        path_name = (
            endpoint_test.path.lower()
            .replace("/", "_")
            .replace("{", "")
            .replace("}", "")
        )
        self._name = f"test_{access_role.value.lower()}_{endpoint_test.method.upper()}_{path_name}"

    @property
    def access_role(self):
        return self._access_role

    @property
    def endpoint_test(self):
        return self._endpoint_test

    @property
    def name(self):
        return self._name

    def __call__(self):
        try:
            self._endpoint_test.run_test(self._access_role)
        finally:
            api_test_state.test_filter.update_last_failed_request(
                self.access_role, self.endpoint_test
            )

    def wrap(self, test_filter: "APITestFilter"):
        def wrapper():
            try:
                self()
            except Exception as e:
                api_test_state.set_any_error_occurred()
                test_filter.update_last_failed_request(
                    api_test_state.get_current_access_role(), self.endpoint_test
                )
                raise e
            else:
                test_filter.remove_last_failed_request_file()

        return wrapper
