import json
from typing import Collection, Dict, List, Optional, Set, Tuple, Union

import requests

from tests.utils import tools
from tests.utils.api_test.endpoint_test.request_data import RequestData
from tests.utils.api_test.endpoint_test.request_data_injector import (
    RequestDataInjector,
)
from tests.utils.api_test.endpoint_test.request_fixture import (
    RequestFixture,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    ConfidentialOrganizationProfileEntity,
    MembershipRequestEntity,
    OrganizationAccountState,
    OrganizationCategory,
    OrganizationCategoryRequestEntity,
    OrganizationUserRole,
    OrganizationUserRoleRequestEntity,
    PostMembershipRequestEntity,
    PostOrganizationUserRoleRequestEntity,
    PostUserEntity,
    UserProfileEntity,
)
from tests.utils.auth_service.api_controllers.v2.manager_controller import (
    ManagerController,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.auth_service.api_controllers.v2.organization_controller import (
    OrganizationControllerV2,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.singletons import api_test_state, login_manager

__all__ = [
    "CreateInactiveOrganizationFixture",
    "CreateOrganizationCategoryRequestFixture",
    "CreateOrganizationFixture",
    "CreateOrganizationUserRoleRequestFixture",
    "CreateUnapprovedOrganizationFixture",
    "DeleteUnapprovedOrganization",
    "ExternalUserRequestMembershipInOrganizationFixture",
    "InjectExternalUserMembershipRequest",
    "InjectMemberAuthHeaders",
    "InjectMembershipRequestIdIntoPathParameters",
    "InjectMemberUserIdIntoPathParameters",
    "InjectOrganizationCategoryRequestIdIntoPathParameters",
    "InjectOrganizationIdIntoBody",
    "InjectOrganizationIdIntoHeader",
    "InjectOrganizationIdIntoPathParameters",
    "InjectOrganizationUserRoleRequestBody",
    "InjectOrganizationUserRoleRequestIdIntoPathParameters",
    "InjectPostMembershipRequestEntity",
    "MemberRequestOrganizationUserRoleInOrganizationFixture",
    "RequestMembershipInOrganizationFixture",
]


class DeleteUnapprovedOrganization(RequestFixture):
    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        super().cleanup_request_data(request_data, access_role, response)
        if response.status_code < 400:
            organization = ConfidentialOrganizationProfileEntity.model_validate(
                response.json()
            )
            auth_headers = api_test_state.get_authorization_headers()
            MyUnapprovedOrganizationsController().delete_unapproved_organization(
                organization.organization_id, auth_headers=auth_headers
            )


class BaseOrganizationFixture(RequestFixture):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__(request_data_injectors=request_data_injectors)
        self._admin_controller = AdminController()
        self._manager_controller = ManagerController()
        self._moderator_controller = ModeratorController()
        self._my_controller = MyController()
        self._my_unapproved_organizations_controller = (
            MyUnapprovedOrganizationsController()
        )
        self._organization: Optional[ConfidentialOrganizationProfileEntity] = None
        self._public_controller = PublicController()

    def get_organization(self) -> Optional[ConfidentialOrganizationProfileEntity]:
        return self._organization


UserTuple = Tuple[PostUserEntity, Dict, UserProfileEntity]


class CreateUnapprovedOrganizationFixture(BaseOrganizationFixture):
    _last_creator_id = 0
    _last_organization_id = 0

    def __init__(
        self,
        request_will_delete_organization: bool = False,
        request_approval: bool = False,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        add_logo: bool = False,
        creator_should_be_api_test_user: bool = False,
        organization_categories: Optional[Set[OrganizationCategory]] = None,
    ):
        super().__init__(request_data_injectors=request_data_injectors)
        self._request_approval = request_approval
        self._add_logo = add_logo
        self._request_will_delete_organization = request_will_delete_organization
        self._creator: Optional[UserProfileEntity] = None
        self._creator_user_data: Optional[PostUserEntity] = None
        self._creator_auth_headers: Optional[Dict] = None
        self._creator_should_be_api_test_user = creator_should_be_api_test_user
        self._organization_categories = organization_categories or set()

    def _get_required_organization_categories(self) -> Set[OrganizationCategory]:
        return set(self._organization_categories)

    @property
    def is_api_test_user_the_creator(self):
        return (
            self.should_test_with_own_organization_of_test_user
            and self._creator_should_be_api_test_user
        )

    def _get_created_user_tuple(self) -> UserTuple:
        if self._creator_user_data is None:
            CreateUnapprovedOrganizationFixture._last_creator_id += 1
            r = self.create_user(
                f"organization-creator-{self._last_creator_id}@example.empaia.org"
            )
            self._creator_user_data = r[0]
            self._creator_auth_headers = r[1]
            self._creator = r[2]
        return self._creator_user_data, self._creator_auth_headers, self._creator

    def get_creator(self) -> UserProfileEntity:
        if self.is_api_test_user_the_creator:
            creator = api_test_state.get_test_user().get_user()
        else:
            creator = self._get_created_user_tuple()[2]
        return creator

    def get_creator_auth_headers(self) -> Dict:
        if self.is_api_test_user_the_creator:
            auth_headers = api_test_state.get_test_user().get_authorization_headers()
        else:
            auth_headers = self._get_created_user_tuple()[1]
        return auth_headers

    def create_user(self, email_address: str) -> UserTuple:
        user_data = self._public_controller.register_new_user(email_address)
        self._public_controller.verify_registration(
            user_data.contact_data.email_address
        )
        auth_headers = self._my_controller.get_auth_headers(
            user_data.contact_data.email_address, user_data.password
        )
        user = self._my_controller.get_profile(auth_headers)
        return user_data, auth_headers, user

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        auth_headers = self.get_creator_auth_headers()
        CreateUnapprovedOrganizationFixture._last_organization_id += 1
        self._organization = (
            self._my_unapproved_organizations_controller.create_new_organization(
                f"my-new-orga{self._last_organization_id}@test.empaia.org",
                auth_headers,
                organization_categories=self._get_required_organization_categories(),
            )
        )
        if self._add_logo:
            self._my_unapproved_organizations_controller.set_organization_logo(
                self._organization.organization_id,
                "sample.jpg",
                auth_headers=auth_headers,
            )
        if self._request_approval:
            self._my_unapproved_organizations_controller.request_organization_approval(
                self._organization.organization_id, auth_headers
            )
            self._organization = self._my_unapproved_organizations_controller.get_unapproved_organization(
                self._organization.organization_id, auth_headers
            )
        assert self._organization is not None
        return self._organization

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        super().cleanup_request_data(request_data, access_role, response)
        if self._organization is not None:
            if (
                not self._request_will_delete_organization
                or self._admin_controller.exists_organization(
                    self._organization.organization_id
                )
            ):
                self.update_creator_data()
                auth_headers = self.get_creator_auth_headers()
                is_organization_unapproved = False
                # update organization data, because the account state may have changed:
                self._update_organization()
                if (
                    self._organization.account_state
                    == OrganizationAccountState.AWAITING_ACTIVATION
                ):
                    self._my_unapproved_organizations_controller.revoke_organization_approval(
                        self._organization.organization_id, auth_headers=auth_headers
                    )
                    is_organization_unapproved = True
                elif (
                    self._organization.account_state
                    == OrganizationAccountState.REQUIRES_ACCOUNT_ACTIVATION
                ):
                    is_organization_unapproved = True
                if is_organization_unapproved:
                    self._my_unapproved_organizations_controller.delete_unapproved_organization(
                        self._organization.organization_id, auth_headers=auth_headers
                    )
                else:
                    self._admin_controller.delete_organization(
                        self._organization.organization_id
                    )
        assert not self._admin_controller.exists_organization(
            self._organization.organization_id
        )
        if self._creator is not None:
            self._cleanup_creator()
        self._organization = None

    def _cleanup_creator(self):
        self._admin_controller.delete_user(self._creator.user_id)
        self._creator_user_data = None
        self._creator = None
        self._creator_auth_headers = None

    def _update_organization(self):
        self._organization = self._moderator_controller.get_organization(
            self._organization.organization_id
        )

    def update_creator_data(self):
        if self.is_api_test_user_the_creator:
            api_test_state.get_test_user().update_after_organization_membership_changed()
        else:
            login_manager.clear_cache_for_user(
                self._creator_user_data.contact_data.email_address,
                self._creator_user_data.password,
            )
            self._creator_auth_headers = login_manager.user(
                self._creator_user_data.contact_data.email_address,
                self._creator_user_data.password,
            )
            self._creator = self._my_controller.get_profile(self._creator_auth_headers)


class CreateOrganizationFixture(CreateUnapprovedOrganizationFixture):
    _last_external_user_id = 0
    _last_member_id = 0

    def __init__(
        self,
        add_logo: bool = False,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        create_external_user_count: int = 0,
        create_member_count_by_role: Dict[OrganizationUserRole, int] = None,
        organization_categories: Optional[Set[OrganizationCategory]] = None,
    ):
        super().__init__(
            request_data_injectors=request_data_injectors,
            request_approval=True,
            add_logo=add_logo,
            organization_categories=organization_categories,
        )
        self._create_external_user_count = create_external_user_count
        self._external_users: Optional[List[UserTuple]] = None
        self._create_member_count_by_role = create_member_count_by_role
        self._members_by_organization_user_role: Optional[
            Dict[OrganizationUserRole, List[UserTuple]]
        ] = None

    def _should_add_api_test_user_as_member(self, access_role: AccessRole):
        member_roles = (
            AccessRole.APP_MAINTAINER,
            AccessRole.DATA_MANAGER,
            AccessRole.ORGANIZATION_MANAGER,
            AccessRole.ORGANIZATION_MEMBER,
            AccessRole.PATHOLOGIST,
        )
        return (
            access_role in member_roles
            and self.should_test_with_own_organization_of_test_user
            and not self._creator_should_be_api_test_user
        )

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        super().create_request_data(request_data, access_role)
        self._moderator_controller.accept_organization(
            self._organization.organization_id
        )
        self.update_creator_data()
        self._update_organization()
        self._create_external_users()
        self._create_members()
        if self._should_add_api_test_user_as_member(access_role):
            self._add_api_test_user_as_member()
            tools.assert_test_user_token_contains_organization(
                self._organization.organization_id
            )
        return self._organization

    def _create_external_users(self):
        if self._create_external_user_count > 0:
            for _ in range(self._create_external_user_count):
                self.create_external_user()

    def create_external_user(self) -> UserTuple:
        if self._external_users is None:
            self._external_users = []
        CreateOrganizationFixture._last_external_user_id += 1
        user_tuple = self.create_user(
            f"ext-user-{self._last_external_user_id}@test.empaia.org"
        )
        self._external_users.append(user_tuple)
        return user_tuple

    @staticmethod
    def _map_organization_user_roles_to_required_organization_categories(
        organization_user_roles: Collection[OrganizationUserRole],
    ) -> Set[OrganizationCategory]:
        required_categories = set()
        if (
            OrganizationUserRole.DATA_MANAGER in organization_user_roles
            or OrganizationUserRole.PATHOLOGIST in organization_user_roles
        ):
            required_categories.add(OrganizationCategory.APP_CUSTOMER)
        if OrganizationUserRole.APP_MAINTAINER in organization_user_roles:
            required_categories.add(OrganizationCategory.APP_VENDOR)
        return required_categories

    def _get_required_organization_categories(self) -> Set[OrganizationCategory]:
        access_role = api_test_state.get_test_user().get_access_role()
        organization_user_roles = set()
        if self._should_add_api_test_user_as_member(access_role):
            organization_user_roles.update(
                self._get_organization_user_roles_from_access_role(access_role)
            )
        if self._create_member_count_by_role is not None:
            organization_user_roles.update(self._create_member_count_by_role.keys())
        required_categories = super()._get_required_organization_categories()
        required_categories.update(
            self._map_organization_user_roles_to_required_organization_categories(
                organization_user_roles
            )
        )
        return required_categories

    def _create_members(self):
        if self._create_member_count_by_role is not None:
            self._members_by_organization_user_role = {}
            for organization_user_role in self._create_member_count_by_role:
                members = []
                for _ in range(
                    self._create_member_count_by_role[organization_user_role]
                ):
                    CreateOrganizationFixture._last_member_id += 1
                    user_tuple = self.create_user(
                        f"{organization_user_role.value.lower()}-{self._last_member_id}@test.empaia.org"
                    )
                    member_tuple = self.add_user_as_member(user_tuple)
                    members.append(member_tuple)
                self._members_by_organization_user_role[
                    organization_user_role
                ] = members

    def _add_api_test_user_as_member(self):
        test_user = api_test_state.get_test_user()
        organization_id = self.get_organization().organization_id
        member_auth_headers = test_user.get_authorization_headers()
        self._my_controller.request_membership(organization_id, member_auth_headers)
        creator_auth_headers = self.get_creator_auth_headers()
        membership_request = self._manager_controller.get_membership_request(
            organization_id, test_user.get_user().user_id, creator_auth_headers
        )
        self._manager_controller.accept_membership_request(
            organization_id,
            membership_request.membership_request_id,
            creator_auth_headers,
        )
        organization_user_roles = self._get_organization_user_roles_from_access_role(
            test_user.get_access_role()
        )
        self._manager_controller.set_organization_user_roles(
            organization_id,
            test_user.get_user().user_id,
            organization_user_roles,
            creator_auth_headers,
        )
        test_user.update_after_organization_membership_changed()

    @staticmethod
    def _get_organization_user_roles_from_access_role(
        access_role: AccessRole,
    ) -> List[OrganizationUserRole]:
        if access_role == AccessRole.APP_MAINTAINER:
            organization_user_roles = [OrganizationUserRole.APP_MAINTAINER]
        elif access_role == AccessRole.DATA_MANAGER:
            organization_user_roles = [OrganizationUserRole.DATA_MANAGER]
        elif access_role == AccessRole.ORGANIZATION_MANAGER:
            organization_user_roles = [OrganizationUserRole.MANAGER]
        elif access_role == AccessRole.ORGANIZATION_MEMBER:
            organization_user_roles = []
        elif access_role == AccessRole.PATHOLOGIST:
            organization_user_roles = [OrganizationUserRole.PATHOLOGIST]
        else:
            raise AssertionError(f"Unhandled access role '{access_role}'")
        return organization_user_roles

    def add_user_as_member(self, user_tuple: UserTuple) -> UserTuple:
        organization_id = self.get_organization().organization_id
        member_auth_headers = user_tuple[1]
        self._my_controller.request_membership(organization_id, member_auth_headers)
        creator_auth_headers = self.get_creator_auth_headers()
        membership_request = self._manager_controller.get_membership_request(
            organization_id, user_tuple[2].user_id, creator_auth_headers
        )
        self._manager_controller.accept_membership_request(
            organization_id,
            membership_request.membership_request_id,
            creator_auth_headers,
        )
        member_auth_headers = login_manager.user(
            user_tuple[0].contact_data.email_address,
            user_tuple[0].password,
            clear_cache=True,
        )
        member = self._my_controller.get_profile(member_auth_headers)
        return user_tuple[0], member_auth_headers, member

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        if self._external_users is not None:
            for user_tuple in self._external_users:
                self._admin_controller.delete_user(user_tuple[2].user_id)
            self._external_users = None
        if self._members_by_organization_user_role is not None:
            for user_tuple_list in self._members_by_organization_user_role.values():
                for user_tuple in user_tuple_list:
                    self._admin_controller.delete_user(user_tuple[2].user_id)
            self._members_by_organization_user_role = None
        organization_id = self._organization.organization_id
        was_api_test_user_added_as_member = self._should_add_api_test_user_as_member(
            access_role
        )
        if was_api_test_user_added_as_member:
            tools.assert_test_user_token_contains_organization(organization_id)
        super().cleanup_request_data(request_data, access_role, response)
        if was_api_test_user_added_as_member:
            api_test_state.get_test_user().update_after_organization_membership_changed()
            tools.assert_test_user_token_does_not_contain_organization(organization_id)

    def get_external_user_auth_headers(self, external_user_index: int) -> Dict:
        return self._external_users[external_user_index][1]

    def get_member(
        self, organization_user_role: OrganizationUserRole, member_index: int
    ) -> UserProfileEntity:
        return self._members_by_organization_user_role[organization_user_role][
            member_index
        ][2]

    def get_member_auth_headers(
        self, organization_user_role: OrganizationUserRole, member_index: int
    ) -> Dict:
        return self._members_by_organization_user_role[organization_user_role][
            member_index
        ][1]


class CreateInactiveOrganizationFixture(CreateOrganizationFixture):
    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        super().create_request_data(request_data, access_role)
        assert self._organization.account_state == OrganizationAccountState.ACTIVE
        self._moderator_controller.deactivate_organization(
            self._organization.organization_id
        )
        self._update_organization()
        assert self._organization.account_state == OrganizationAccountState.INACTIVE
        return self._organization


class InjectExternalUserAuthHeaders(RequestDataInjector):
    def __init__(self, external_user_index: int = 0):
        self._external_user_index = external_user_index

    def get_authorization_headers(
        self, fixture: CreateOrganizationFixture
    ) -> Optional[Dict]:
        return fixture.get_external_user_auth_headers(self._external_user_index)


class InjectExternalUserMembershipRequest(InjectExternalUserAuthHeaders):
    def get_body(
        self, fixture: CreateOrganizationFixture
    ) -> PostMembershipRequestEntity:
        return PostMembershipRequestEntity(
            organization_id=fixture.get_organization().organization_id
        )


class InjectMemberUserIdIntoPathParameters(RequestDataInjector):
    def __init__(
        self, organization_user_role: OrganizationUserRole, member_index: int = 0
    ):
        self._organization_user_role = organization_user_role
        self._member_index = member_index

    def get_path_parameters(self, fixture: CreateOrganizationFixture) -> Dict:
        return {
            "target_user_id": fixture.get_member(
                self._organization_user_role, self._member_index
            ).user_id
        }


class InjectMemberAuthHeaders(RequestDataInjector):
    def __init__(
        self, organization_user_role: OrganizationUserRole, member_index: int = 0
    ):
        self._organization_user_role = organization_user_role
        self._member_index = member_index

    def get_authorization_headers(self, fixture: CreateOrganizationFixture) -> Dict:
        return fixture.get_member_auth_headers(
            self._organization_user_role, self._member_index
        )


class InjectPostMembershipRequestEntity(RequestDataInjector):
    def get_authorization_headers(
        self, fixture: CreateOrganizationFixture
    ) -> Optional[Dict]:
        return api_test_state.get_authorization_headers()

    def get_body(
        self, fixture: CreateOrganizationFixture
    ) -> PostMembershipRequestEntity:
        return PostMembershipRequestEntity(
            organization_id=fixture.get_organization().organization_id
        )


class InjectOrganizationIdIntoBody(RequestDataInjector):
    def get_body(self, fixture: BaseOrganizationFixture) -> Dict:
        return {"organization_id": str(fixture.get_organization().organization_id)}


class InjectOrganizationIdIntoHeader(RequestDataInjector):
    def get_headers(self, fixture: BaseOrganizationFixture) -> Dict:
        return {"organization-id": str(fixture.get_organization().organization_id)}


class InjectOrganizationIdIntoPathParameters(RequestDataInjector):
    def get_path_parameters(self, fixture: BaseOrganizationFixture) -> Dict:
        return {"organization_id": str(fixture.get_organization().organization_id)}


class InjectOrganizationUserRoleRequestBody(RequestDataInjector):
    def get_body(self, fixture: BaseOrganizationFixture) -> Dict:
        if api_test_state.get_current_access_role() == AccessRole.APP_MAINTAINER:
            return json.loads(
                PostOrganizationUserRoleRequestEntity(
                    requested_roles=[OrganizationUserRole.MANAGER.value]
                ).model_dump_json()
            )
        else:
            return json.loads(
                PostOrganizationUserRoleRequestEntity(
                    requested_roles=[OrganizationUserRole.APP_MAINTAINER.value]
                ).model_dump_json()
            )


class RequestMembershipInOrganizationFixture(CreateOrganizationFixture):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__(request_data_injectors=request_data_injectors)
        self._membership_request: Optional[MembershipRequestEntity] = None

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        organization = super().create_request_data(request_data, access_role)
        auth_headers = api_test_state.get_authorization_headers()
        self._my_controller.request_membership(
            organization.organization_id, auth_headers=auth_headers
        )
        self._membership_request = self._my_controller.get_membership_request(
            organization.organization_id, auth_headers=auth_headers
        )
        return organization

    @property
    def membership_request(self) -> Optional[MembershipRequestEntity]:
        return self._membership_request


class ExternalUserRequestMembershipInOrganizationFixture(CreateOrganizationFixture):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__(
            create_external_user_count=1, request_data_injectors=request_data_injectors
        )
        self._membership_request: Optional[MembershipRequestEntity] = None

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        organization = super().create_request_data(request_data, access_role)
        auth_headers = self.get_external_user_auth_headers(0)
        self._my_controller.request_membership(
            organization.organization_id, auth_headers=auth_headers
        )
        self._membership_request = self._my_controller.get_membership_request(
            organization.organization_id, auth_headers=auth_headers
        )
        assert organization is not None
        return organization

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        # A HTTP error means that either denying or accepting the membership request has expectedly failed. Deny it
        # now to delete the membership request from the DB:
        if response.status_code == 403:
            auth_headers = self.get_creator_auth_headers()
            self._manager_controller.deny_membership_request(
                self._organization.organization_id,
                self._membership_request.membership_request_id,
                reviewer_comment="cleanup",
                auth_headers=auth_headers,
            )
        super().cleanup_request_data(request_data, access_role, response)

    @property
    def membership_request(self) -> Optional[MembershipRequestEntity]:
        return self._membership_request


class InjectMembershipRequestIdIntoPathParameters(RequestDataInjector):
    def get_path_parameters(
        self,
        fixture: Union[
            RequestMembershipInOrganizationFixture,
            ExternalUserRequestMembershipInOrganizationFixture,
        ],
    ):
        test_user = api_test_state.get_test_user()
        if test_user.get_access_role() != AccessRole.ANONYMOUS:
            membership_request_id = fixture.membership_request.membership_request_id
        else:
            membership_request_id = 1
        return {"membership_request_id": str(membership_request_id)}


class InjectCreatorMembershipRequestIdIntoPathParameter(RequestDataInjector):
    def get_authorization_headers(
        self, fixture: RequestMembershipInOrganizationFixture
    ) -> Optional[Dict]:
        return api_test_state.get_authorization_headers()

    def get_path_parameters(self, fixture: RequestMembershipInOrganizationFixture):
        return {
            "membership_request_id": str(
                fixture.membership_request.membership_request_id
            )
        }


class CreateOrganizationUserRoleRequestFixture(CreateOrganizationFixture):
    def __init__(
        self,
        add_logo: bool = False,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        create_external_user_count: int = 0,
        create_member_count_by_role: Dict[OrganizationUserRole, int] = None,
    ):
        super().__init__(
            add_logo,
            request_data_injectors,
            create_external_user_count,
            create_member_count_by_role,
        )
        self._organization_user_role_request: Optional[
            OrganizationUserRoleRequestEntity
        ] = None

    def _get_required_organization_categories(self) -> Set[OrganizationCategory]:
        required_categories = super()._get_required_organization_categories()
        role = self.get_organization_user_role_to_request()
        required_categories.update(
            self._map_organization_user_roles_to_required_organization_categories(
                [role]
            )
        )
        return required_categories

    @property
    def organization_user_role_request(
        self,
    ) -> Optional[OrganizationUserRoleRequestEntity]:
        return self._organization_user_role_request

    @property
    def organization_user_role_request_id(self) -> int:
        return (
            self._organization_user_role_request.organization_user_role_request_id
            if self._organization_user_role_request is not None
            else -1
        )

    def get_role_requesting_member(self) -> UserProfileEntity:
        if self.should_test_with_own_organization_of_test_user:
            member = api_test_state.get_test_user().get_user()
        else:
            member = self.get_creator()
        return member

    def get_role_request_auth_headers(self) -> Dict:
        if self.should_test_with_own_organization_of_test_user:
            auth_headers = api_test_state.get_authorization_headers()
        else:
            auth_headers = self.get_creator_auth_headers()
        return auth_headers

    def get_organization_user_role_to_request(self) -> OrganizationUserRole:
        if api_test_state.get_current_access_role() == AccessRole.APP_MAINTAINER:
            organization_user_role = OrganizationUserRole.DATA_MANAGER
        else:
            organization_user_role = OrganizationUserRole.APP_MAINTAINER
        return organization_user_role

    def assert_member_does_not_already_have_requested_role(
        self,
        organization_user_role: OrganizationUserRole,
        organization: ConfidentialOrganizationProfileEntity,
    ):
        found = False
        role_requesting_member = self.get_role_requesting_member()
        auth_headers = self.get_creator_auth_headers()
        for member in (
            OrganizationControllerV2()
            .get_users(organization.organization_id, auth_headers)
            .users
        ):
            if member.user_id == role_requesting_member.user_id:
                assert organization_user_role not in member.organization_user_roles
                found = True
                break
        assert found

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        organization = super().create_request_data(request_data, access_role)
        auth_headers = self.get_role_request_auth_headers()
        organization_user_role = self.get_organization_user_role_to_request()
        if access_role != AccessRole.REGISTERED_USER:
            # a registered user is not a member, so the user cannot request an organization user role:
            self.assert_member_does_not_already_have_requested_role(
                organization_user_role, organization
            )
            organization_controller = OrganizationControllerV2()
            organization_controller.request_organization_user_roles(
                organization_id=organization.organization_id,
                organization_user_roles=[organization_user_role],
                auth_headers=auth_headers,
            )
            self._organization_user_role_request = (
                organization_controller.find_organization_user_role_request(
                    organization_id=organization.organization_id,
                    auth_headers=auth_headers,
                )
            )
        return organization


class MemberRequestOrganizationUserRoleInOrganizationFixture(
    CreateOrganizationUserRoleRequestFixture
):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__(
            create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
            request_data_injectors=request_data_injectors,
        )

    def get_role_requesting_member(self) -> UserProfileEntity:
        return self.get_member(OrganizationUserRole.PATHOLOGIST, 0)

    def get_role_request_auth_headers(self) -> Dict:
        return self.get_member_auth_headers(OrganizationUserRole.PATHOLOGIST, 0)

    def get_organization_user_role_to_request(self) -> OrganizationUserRole:
        return OrganizationUserRole.APP_MAINTAINER


class InjectOrganizationUserRoleRequestIdIntoPathParameters(RequestDataInjector):
    def get_path_parameters(self, fixture: CreateOrganizationUserRoleRequestFixture):
        return {"role_request_id": str(fixture.organization_user_role_request_id)}


class CreateOrganizationCategoryRequestFixture(CreateOrganizationFixture):
    def __init__(
        self,
        add_logo: bool = False,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        create_external_user_count: int = 0,
        create_member_count_by_role: Dict[OrganizationUserRole, int] = None,
    ):
        super().__init__(
            add_logo,
            request_data_injectors,
            create_external_user_count,
            create_member_count_by_role,
        )
        self._organization_category_request: Optional[
            OrganizationCategoryRequestEntity
        ] = None

    @property
    def organization_category_request(
        self,
    ) -> Optional[OrganizationCategoryRequestEntity]:
        return self._organization_category_request

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> ConfidentialOrganizationProfileEntity:
        organization = super().create_request_data(request_data, access_role)
        auth_headers = self.get_creator_auth_headers()
        if access_role == AccessRole.APP_MAINTAINER:
            # if a member is an app-maintainer, then the initial category is APP_VENDOR and we can request APP_CUSTOMER
            # instead:
            organization_category = OrganizationCategory.APP_CUSTOMER
        else:
            organization_category = OrganizationCategory.APP_VENDOR
        manager_controller = ManagerController()
        manager_controller.request_organization_category(
            organization_id=organization.organization_id,
            organization_categories=[organization_category],
            auth_headers=auth_headers,
        )
        self._organization_category_request = (
            manager_controller.find_open_organization_category_request(
                organization_id=organization.organization_id,
                auth_headers=auth_headers,
            )
        )
        return organization


class InjectOrganizationCategoryRequestIdIntoPathParameters(RequestDataInjector):
    def get_path_parameters(self, fixture: CreateOrganizationCategoryRequestFixture):
        organization_category_request_id = (
            fixture.organization_category_request.organization_category_request_id
        )
        return {"category_request_id": str(organization_category_request_id)}
