from typing import Dict, List, Optional

import requests
from pydantic import BaseModel

from tests.utils import tools
from tests.utils.api_test.endpoint_test.request_data import RequestData
from tests.utils.api_test.endpoint_test.request_data_injector import (
    RequestDataInjector,
)
from tests.utils.api_test.endpoint_test.request_fixture import (
    RequestFixture,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    PostUserContactDataEntity,
    PostUserDetailsEntity,
    RegisteredUserEntity,
    UserContactDataEntity,
    UserDetailsEntity,
    UserProfileEntity,
)
from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)
from tests.utils.auth_service.api_controllers.v2.my_controller import (
    MyController,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PostUserEntity,
    PublicController,
)
from tests.utils.singletons import api_test_state, login_manager

__all__ = [
    "CreateInactiveUserFixture",
    "CreateUserFixture",
    "InjectUserAuthHeaders",
    "InjectUserEMailAddressAndWrongEMailVerificationTokenIntoQuery",
    "InjectUserEMailAddressIntoQuery",
    "InjectUserKeycloakIdIntoPathParameters",
    "InjectUserKeycloakIdIntoQuery",
    "RegisterNewUserFixture",
    "StaticUserFixture",
    "UpdateContactDataAndAssertSuccess",
    "UpdateUserDetailsAndAssertSuccess",
]


class RegisterNewUserFixture(RequestFixture):
    _last_user_id = 0

    def __init__(
        self,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        user_may_already_be_deleted: bool = False,
    ):
        super().__init__(request_data_injectors)
        self._admin_controller = AdminController()
        self._moderator_controller = ModeratorController()
        self._public_controller = PublicController()
        self._post_user_data: Optional[PostUserEntity] = None
        self._user_may_already_be_deleted = user_may_already_be_deleted
        self._user: Optional[RegisteredUserEntity] = None
        self._user_profile: Optional[UserProfileEntity] = None

    def get_user(self) -> Optional[UserProfileEntity]:
        return self._user

    def get_user_profile(self) -> Optional[UserProfileEntity]:
        if self._user_profile is None:
            if self._user is not None:
                try:
                    self._user_profile = self._moderator_controller.get_user_profile(
                        self._user.user_id
                    )
                except AssertionError as e:
                    if '404 b\'{"error":"Not found"' in str(
                        e
                    ) and "Approved user was not found" in str(e):
                        self._user_profile = (
                            self._moderator_controller.get_unapproved_user_profile(
                                self._user.user_id
                            )
                        )
                    else:
                        raise e
        return self._user_profile

    def get_post_user_data(self) -> Optional[PostUserEntity]:
        return self._post_user_data

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> RegisteredUserEntity:
        RegisterNewUserFixture._last_user_id += 1
        self._post_user_data = self._public_controller.create_post_user_entity(
            f"new_user_{self._last_user_id}@example.empaia.org"
        )
        self._public_controller.register_new_user(self._post_user_data)
        self._user = self._moderator_controller.get_unapproved_user_by_username(
            self._post_user_data.contact_data.email_address
        )
        assert self._user is not None
        return self._user

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        # TODO: if the super admin controller can delete a registered user that was not verified, then do it here!
        # if self._user_may_already_be_deleted:
        pass


class CreateUserFixture(RegisterNewUserFixture):
    def __init__(
        self,
        picture_filename: str = None,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        user_may_already_be_deleted: bool = False,
    ):
        super().__init__(request_data_injectors, user_may_already_be_deleted)
        self._picture_filename = picture_filename

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> RegisteredUserEntity:
        super().create_request_data(request_data, access_role)
        email_address = self.get_post_user_data().contact_data.email_address
        self._public_controller.verify_registration(email_address)
        self._user = self._moderator_controller.get_user_by_username(email_address)
        assert self._user is not None
        auth_headers = login_manager.user(
            self.get_post_user_data().contact_data.email_address,
            self.get_post_user_data().password,
        )
        if self._picture_filename is not None:
            MyController().set_profile_picture(self._picture_filename, auth_headers)
        return self._user

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        # self._user_controller.delete_user(self._user, user_may_already_be_deleted=self._user_may_already_be_deleted)
        self._admin_controller.delete_user(self._user.user_id)
        self._user = None


class CreateInactiveUserFixture(CreateUserFixture):
    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> RegisteredUserEntity:
        super().create_request_data(request_data, access_role)
        self._moderator_controller.deactivate_user(self._user.user_id)
        email_address = self.get_post_user_data().contact_data.email_address
        self._user = self._moderator_controller.get_user_by_username(email_address)
        assert self._user is not None
        return self._user


class StaticUserFixture(CreateUserFixture):
    __user: Optional[RegisteredUserEntity] = None
    __password: Optional[str] = None

    def __init__(
        self,
        picture_filename: str = None,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
        user_may_already_be_deleted: bool = False,
    ):
        super().__init__(
            picture_filename, request_data_injectors, user_may_already_be_deleted
        )
        self._password: Optional[str] = None

    def create_request_data(
        self, request_data: RequestData, access_role: AccessRole
    ) -> RegisteredUserEntity:
        # create the user only once and do not clean it up
        if self.__user is None:
            self.__user = super().create_request_data(request_data, access_role)
            self.__password = self._password
        else:
            self._user = self.__user
            self._password = self.__password
        return self.__user

    def cleanup_request_data(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        # do not clean the user up
        pass


class InjectUserEMailAddressIntoQuery(RequestDataInjector):
    def get_query(self, fixture: RegisterNewUserFixture):
        return {
            "email_address": [fixture.get_user_profile().contact_data.email_address]
        }


class InjectUserEMailAddressAndWrongEMailVerificationTokenIntoQuery(
    RequestDataInjector
):
    def get_query(self, fixture: RegisterNewUserFixture):
        return {
            "email_address": fixture.get_post_user_data().contact_data.email_address,
            "token": "invalid_token",
        }


class InjectUserKeycloakIdIntoPathParameters(RequestDataInjector):
    def __init__(self, path_parameter_name: str = None):
        self._path_parameter_name = (
            path_parameter_name if path_parameter_name is not None else "target_user_id"
        )

    def get_path_parameters(self, fixture: CreateUserFixture):
        return {self._path_parameter_name: fixture.get_user().user_id}


class InjectUserAuthHeaders(RequestDataInjector):
    def get_authorization_headers(self, fixture: CreateUserFixture):
        auth_headers = None
        if api_test_state.get_test_user().get_access_role() != AccessRole.ANONYMOUS:
            auth_headers = login_manager.user(
                fixture.get_post_user_data().contact_data.email_address,
                fixture.get_post_user_data().password,
            )
        return auth_headers


class InjectUserKeycloakIdIntoQuery(RequestDataInjector):
    def get_query(self, fixture: CreateUserFixture):
        return {"target_user_id": fixture.get_user().user_id}


class BaseUpdateUserDataAndAssertSuccessFixture(RequestFixture):
    def __init__(
        self,
        entity_title: str,
        request_data_injectors: Optional[List[RequestDataInjector]] = None,
    ):
        super().__init__(request_data_injectors=request_data_injectors)
        self._original_data = None
        self._new_data = None
        self._entity_title = entity_title

    def create_new_user_data(self):
        raise NotImplementedError

    def get_user_data(self, user: UserProfileEntity) -> BaseModel:
        raise NotImplementedError

    def restore_original_data(self, auth_headers: Dict):
        raise NotImplementedError

    def get_current_user_data(self) -> BaseModel:
        auth_headers = api_test_state.get_authorization_headers()
        user = MyController().get_profile(auth_headers=auth_headers)
        return self.get_user_data(user)

    def make_new_data_comparable_to_original_data(
        self, new_data: Dict, original_data: BaseModel  # pylint: disable=W0613
    ) -> Dict:
        return new_data

    def create_request_data(self, request_data: RequestData, access_role: AccessRole):
        super().create_request_data(request_data, access_role)
        if access_role != AccessRole.ANONYMOUS:
            self._original_data = self.get_current_user_data()
            self._new_data = self.create_new_user_data().model_dump()
            new_data_dict = self.make_new_data_comparable_to_original_data(
                self._new_data, self._original_data
            )
            request_data.body = self._new_data
            tools.assert_dicts_are_not_equal(
                new_data_dict,
                f"Updated {self._entity_title}",
                self._original_data.model_dump(),
                "fOriginal {self._entity_title}",
            )

    def cleanup_after_request(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        super().cleanup_after_request(request_data, access_role, response)
        if access_role != AccessRole.ANONYMOUS:
            # Restore contact data:
            auth_headers = api_test_state.get_authorization_headers()
            self.restore_original_data(auth_headers)
            self.assert_user_data_was_updated(self._original_data.model_dump())

    def validate_response(
        self,
        request_data: RequestData,
        access_role: AccessRole,
        response: requests.Response,
    ):
        super().validate_response(request_data, access_role, response)
        if access_role != AccessRole.ANONYMOUS:
            self.assert_user_data_was_updated(self._new_data)

    def assert_user_data_was_updated(self, expected_contact_data: BaseModel):
        current_data = self.get_current_user_data()
        expected_contact_data_dict = self.make_new_data_comparable_to_original_data(
            expected_contact_data, self._original_data
        )
        tools.assert_dicts_are_equal(
            expected_contact_data_dict,
            f"Expected {self._entity_title}",
            current_data.model_dump(),
            f"Current {self._entity_title}",
        )


class UpdateContactDataAndAssertSuccess(BaseUpdateUserDataAndAssertSuccessFixture):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__("Contact Data", request_data_injectors=request_data_injectors)

    def make_new_data_comparable_to_original_data(
        self, new_data: Dict, original_data: UserContactDataEntity
    ) -> Dict:
        new_data["email_address"] = self._original_data.email_address
        return new_data

    def get_user_data(self, user: UserProfileEntity) -> UserContactDataEntity:
        return user.contact_data

    def create_new_user_data(self) -> PostUserContactDataEntity:
        return PostUserContactDataEntity(
            country_code="BG",
            phone_number="0815",
            place_name="Utopia",
            street_name="Main Street",
            street_number="13",
            zip_code="98765",
        )

    def restore_original_data(self, auth_headers: Dict):
        MyController().set_contact_data(self._original_data, auth_headers=auth_headers)


class UpdateUserDetailsAndAssertSuccess(BaseUpdateUserDataAndAssertSuccessFixture):
    def __init__(
        self, request_data_injectors: Optional[List[RequestDataInjector]] = None
    ):
        super().__init__("User Details", request_data_injectors=request_data_injectors)

    def get_user_data(self, user: UserProfileEntity) -> UserDetailsEntity:
        return user.details

    def create_new_user_data(self):
        return PostUserDetailsEntity(
            first_name="New First Name", last_name="New Last Name", title="Dr."
        )

    def restore_original_data(self, auth_headers: Dict):
        MyController().set_details(self._original_data, auth_headers=auth_headers)
