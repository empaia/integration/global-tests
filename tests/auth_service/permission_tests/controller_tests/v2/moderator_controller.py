import json

from tests.utils.api_test.controller_test import ControllerV2Test
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationAccountState,
    OrganizationAccountStateQueryEntity,
    OrganizationCategoriesEntity,
    OrganizationCategory,
    OrganizationUserRole,
    PostReviewerComment,
    PostUserRolesEntity,
    UserRole,
)

from ..fixtures.v2 import (
    CreateInactiveOrganizationFixture,
    CreateInactiveUserFixture,
    CreateOrganizationCategoryRequestFixture,
    CreateOrganizationFixture,
    CreateUnapprovedOrganizationFixture,
    CreateUserFixture,
    InjectMemberUserIdIntoPathParameters,
    InjectOrganizationCategoryRequestIdIntoPathParameters,
    InjectOrganizationIdIntoPathParameters,
    InjectUserKeycloakIdIntoPathParameters,
    RegisterNewUserFixture,
)


class ModeratorControllerTest(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/moderator")

    def _setup_delete_endpoints(self):
        self.delete(
            "/unapproved-users/{unapproved_user_id}/account",
            responses={AccessRole.MODERATOR: 204},
            fixture=RegisterNewUserFixture(
                request_data_injectors=[
                    InjectUserKeycloakIdIntoPathParameters("unapproved_user_id")
                ]
            ),
        )
        self.delete(
            "/users/{target_user_id}/account",
            responses={AccessRole.MODERATOR: 204},
            fixture=CreateUserFixture(
                request_data_injectors=[
                    InjectUserKeycloakIdIntoPathParameters("target_user_id")
                ]
            ),
        )

    def _setup_get_endpoints(self):
        self.get(
            "/category-requests",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateOrganizationCategoryRequestFixture(),
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/organizations",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/organizations/{organization_id}",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateInactiveOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.get(
            "/unapproved-organizations/{organization_id}/profile",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateUnapprovedOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.get(
            "/unapproved-users",
            responses={AccessRole.MODERATOR: 200},
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/unapproved-users/{unapproved_user_id}/profile",
            responses={AccessRole.MODERATOR: 200},
            fixture=RegisterNewUserFixture(
                request_data_injectors=[
                    InjectUserKeycloakIdIntoPathParameters("unapproved_user_id")
                ]
            ),
        )
        self.get(
            "/users",
            responses={AccessRole.MODERATOR: 200},
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/users/{target_user_id}/organizations",
            responses={AccessRole.MODERATOR: 200},
            query={"page": 0, "page_size": 5},
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectMemberUserIdIntoPathParameters(
                        OrganizationUserRole.PATHOLOGIST
                    )
                ],
            ),
        )
        self.get(
            "/users/{target_user_id}/profile",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )

    def _setup_post_endpoints(self):
        pass

    def _setup_put_endpoints(self):
        self.put(
            "/unapproved-organizations/query",
            body=json.loads(
                OrganizationAccountStateQueryEntity(
                    organizationAccountStates=[
                        OrganizationAccountState.AWAITING_ACTIVATION,
                        OrganizationAccountState.REQUIRES_ACCOUNT_ACTIVATION,
                    ]
                ).model_dump_json()
            ),
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateUnapprovedOrganizationFixture(),
            query={"page": 0, "page_size": 5},
        )
        for action in ("accept", "deny"):
            body = {}
            if action == "deny":
                body = json.loads(
                    PostReviewerComment(
                        reviewer_comment="not allowed"
                    ).model_dump_json()
                )
            self.put(
                "/category-requests/{category_request_id}/" + action,
                body=body,
                responses={AccessRole.MODERATOR: 200},
                fixture=CreateOrganizationCategoryRequestFixture(
                    request_data_injectors=[
                        InjectOrganizationCategoryRequestIdIntoPathParameters()
                    ]
                ),
            )
            self.put(
                "/unapproved-organizations/{organization_id}/" + action,
                body=body,
                responses={AccessRole.MODERATOR: 204},
                fixture=CreateUnapprovedOrganizationFixture(
                    request_approval=True,
                    request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                ),
            )
        self.put(
            "/organizations/{organization_id}/activate",
            responses={AccessRole.MODERATOR: 204},
            fixture=CreateInactiveOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.put(
            "/organizations/{organization_id}/categories",
            responses={AccessRole.MODERATOR: 204},
            body=json.loads(
                OrganizationCategoriesEntity(
                    categories=[
                        OrganizationCategory.APP_CUSTOMER,
                        OrganizationCategory.APP_VENDOR,
                    ]
                ).model_dump_json()
            ),
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.put(
            "/organizations/{organization_id}/deactivate",
            responses={AccessRole.MODERATOR: 204},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.put(
            "/users/{target_user_id}/activate",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateInactiveUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )
        self.put(
            "/users/{target_user_id}/deactivate",
            responses={AccessRole.MODERATOR: 200},
            fixture=CreateUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )
        self.put(
            "/users/{target_user_id}/roles",
            responses={AccessRole.MODERATOR: 200},
            body=PostUserRolesEntity(roles=[UserRole.MODERATOR.value]).model_dump(),
            fixture=CreateUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )
