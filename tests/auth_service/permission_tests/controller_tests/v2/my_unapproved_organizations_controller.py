import json
from typing import Dict, Optional

from tests.utils.api_test.controller_test import ControllerV2Test
from tests.utils.api_test.controller_test.controller_test import (
    AccessRoleResponses,
)
from tests.utils.api_test.endpoint_test.expected_response import (
    OrganizationMemberStatusCode,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationAccountState,
    OrganizationAccountStateQueryEntity,
    OrganizationCategory,
    PostOrganizationContactDataEntity,
    PostOrganizationDetailsEntity,
    PostOrganizationEntity,
)
from tests.utils.multipart_file_request import (
    make_multipart_file_request,
)

from ..fixtures.v2 import (
    CreateUnapprovedOrganizationFixture,
    DeleteUnapprovedOrganization,
    InjectOrganizationIdIntoPathParameters,
)


class MyUnapprovedOrganizationsControllerTest(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/my/unapproved-organizations")

    def _setup_delete_endpoints(self):
        self.delete(
            "/{organization_id}",
            responses={
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=204
                )
            },
            fixture=CreateUnapprovedOrganizationFixture(
                request_will_delete_organization=True,
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                creator_should_be_api_test_user=True,
            ),
        )

    def _setup_get_endpoints(self):
        self.get(
            "/{organization_id}",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            fixture=CreateUnapprovedOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )
        self.get(
            "/{organization_id}/can-request-approval",
            responses={
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateUnapprovedOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                request_approval=False,
                creator_should_be_api_test_user=True,
            ),
        )

    def _setup_post_endpoints(self):
        self.post(
            "",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 201},
            body=json.loads(self._get_post_organization().model_dump_json()),
            fixture=DeleteUnapprovedOrganization(),
        )

    def _setup_put_endpoints(self):
        self.put(
            "/query",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            body=json.loads(
                OrganizationAccountStateQueryEntity(
                    organizationAccountStates=[
                        OrganizationAccountState.AWAITING_ACTIVATION,
                        OrganizationAccountState.REQUIRES_ACCOUNT_ACTIVATION,
                    ]
                ).model_dump_json()
            ),
            query={"page": 0, "page_size": 5},
        )
        self.put_for_unapproved_organization(
            "/contact",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=PostOrganizationContactDataEntity.create_entity_with_default_values(
                email_address="test@empaia.org", street_name="New street"
            ).model_dump(),
        )
        self.put_for_unapproved_organization(
            "/details",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=PostOrganizationDetailsEntity.create_entity_with_default_values(
                description_de="New description_de", description_en="New description_en"
            ).model_dump(),
        )
        logo_put_request = make_multipart_file_request("sample.jpg", "logo")
        self.put_for_unapproved_organization(
            "/logo",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=204
                )
            },
            headers={**logo_put_request.headers},
            body=logo_put_request.body,
        )
        self.put_for_unapproved_organization(
            "/name",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body={"organization_name": "New name"},
        )
        self.put_for_unapproved_organization(
            "/revoke-approval-request",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            request_approval=True,
        )
        self.put_for_unapproved_organization(
            "/request-approval",
            {
                AccessRole.ALL_REGISTERED_USERS_V2: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
        )

    def put_for_unapproved_organization(
        self,
        path: str,
        responses: Optional[AccessRoleResponses] = None,
        body: Optional[Dict] = None,
        query: Optional[Dict] = None,
        headers: Optional[Dict] = None,
        request_approval: Optional[bool] = False,
    ):
        self.put(
            "/{organization_id}" + path,
            responses=responses,
            body=body,
            query=query,
            headers=headers,
            fixture=CreateUnapprovedOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                request_approval=request_approval,
                creator_should_be_api_test_user=True,
            ),
        )

    @staticmethod
    def _get_post_organization() -> PostOrganizationEntity:
        return PostOrganizationEntity(
            categories=[],
            contact_data=PostOrganizationContactDataEntity(
                department="headquarter",
                country_code="DK",
                email_address="test-organization@empaia.org",
                fax_number="0185-4711",
                is_email_address_public=True,
                is_fax_number_public=True,
                is_phone_number_public=False,
                phone_number="199-13",
                place_name="capital",
                street_name="mainstreet",
                street_number="13c)",
                website="",
                zip_code="12345",
            ),
            details=PostOrganizationDetailsEntity(
                categories={OrganizationCategory.APP_CUSTOMER},
                description_de="ORG DESCR" + "-" * 741,
                description_en="ORG DESCR" + "-" * 741,
                is_user_count_public=True,
                organization_roles=["HOSPITAL"],
            ),
            organization_name="my-new-organization",
        )
