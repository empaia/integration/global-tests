import json

from tests.utils.api_test.controller_test import (
    PublicEndpointControllerV2Test,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)

from ..fixtures.v2 import (
    CreateOrganizationFixture,
    InjectOrganizationIdIntoPathParameters,
    InjectUserEMailAddressAndWrongEMailVerificationTokenIntoQuery,
    RegisterNewUserFixture,
)


class PublicControllerTest(PublicEndpointControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/public")

    def _setup_get_endpoints(self):
        self.public_get("/organizations")
        self.public_get(
            "/organizations/{organization_id}",
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )

    def _setup_post_endpoints(self):
        self.post(
            "/register-new-user",
            responses={AccessRole.ALL_V2: 204},
            body=json.loads(
                PublicController()
                .create_post_user_entity("some-new-user@example.empaia.org")
                .model_dump_json()
            ),  # TODO: if possible, delete new user that was not verified in fixture
        )
        self.post(
            "/verify-registration",
            responses={AccessRole.ALL_V2: 403},
            fixture=RegisterNewUserFixture(
                request_data_injectors=[
                    InjectUserEMailAddressAndWrongEMailVerificationTokenIntoQuery()
                ]
            ),
        )
