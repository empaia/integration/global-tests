from tests.utils.api_test.controller_test.controller_test import (
    PublicEndpointControllerV2Test,
)


class DomainControllerTest(PublicEndpointControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/domain")

    def _setup_get_endpoints(self):
        self.public_get("/countries")
        self.public_get(
            "/countries/{iso_3166_alpha_2_code}",
            path_parameters={"iso_3166_alpha_2_code": "DE"},
        )
