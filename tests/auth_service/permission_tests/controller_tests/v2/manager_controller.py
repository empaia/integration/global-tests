import json

from tests.utils.api_test.controller_test import ControllerV2Test
from tests.utils.api_test.endpoint_test.expected_response import (
    OrganizationMemberStatusCode,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
    PostOrganizationCategoryRequestEntity,
    PostOrganizationDetailsEntity,
    PostOrganizationNameEntity,
    PostOrganizationUserRolesEntity,
    PostReviewerComment,
)
from tests.utils.auth_service.api_controllers.v2.my_unapproved_organizations_controller import (
    MyUnapprovedOrganizationsController,
)
from tests.utils.multipart_file_request import (
    make_multipart_file_request,
)

from ..fixtures.v2 import (
    CreateOrganizationCategoryRequestFixture,
    CreateOrganizationFixture,
    CreateOrganizationUserRoleRequestFixture,
    ExternalUserRequestMembershipInOrganizationFixture,
    InjectMembershipRequestIdIntoPathParameters,
    InjectMemberUserIdIntoPathParameters,
    InjectOrganizationCategoryRequestIdIntoPathParameters,
    InjectOrganizationIdIntoHeader,
    InjectOrganizationUserRoleRequestIdIntoPathParameters,
    MemberRequestOrganizationUserRoleInOrganizationFixture,
)


class ManagerControllerTest(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/manager/organization")

    def _setup_delete_endpoints(self):
        pass

    def _setup_get_endpoints(self):
        self.get(
            "/membership-requests",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/role-requests",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationUserRoleRequestFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/users",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        self.get(
            "/users/{target_user_id}/profile",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberUserIdIntoPathParameters(
                        OrganizationUserRole.PATHOLOGIST
                    ),
                ],
            ),
        )
        self.get(
            "/profile",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        self.get(
            "/category-requests",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationCategoryRequestFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()],
            ),
            query={"page": 0, "page_size": 9999},
        )

    def _setup_post_endpoints(self):
        pass

    def _setup_put_endpoints(self):
        for action in ("accept", "deny"):
            body = {}
            if action == "deny":
                body = json.loads(
                    PostReviewerComment(
                        reviewer_comment="not allowed"
                    ).model_dump_json()
                )
            self.put(
                "/membership-requests/{membership_request_id}/" + action,
                body=body,
                responses={
                    AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                        foreign=403, own=200
                    )
                },
                fixture=ExternalUserRequestMembershipInOrganizationFixture(
                    request_data_injectors=[
                        InjectOrganizationIdIntoHeader(),
                        InjectMembershipRequestIdIntoPathParameters(),
                    ]
                ),
            )
            self.put(
                "/role-requests/{role_request_id}/" + action,
                body=body,
                responses={
                    AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                        foreign=403, own=200
                    )
                },
                fixture=MemberRequestOrganizationUserRoleInOrganizationFixture(
                    request_data_injectors=[
                        InjectOrganizationIdIntoHeader(),
                        InjectOrganizationUserRoleRequestIdIntoPathParameters(),
                    ]
                ),
            )
        self.put(
            "/profile/name",
            {
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=json.loads(
                PostOrganizationNameEntity(
                    organization_name="Test Organization"
                ).model_dump_json()
            ),
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        self.put(
            "/profile/contact",
            {
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=MyUnapprovedOrganizationsController.create_new_organization_data(
                email_address="test@empaia.org"
            ).model_dump(),
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        self.put(
            "/category-requests",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()],
            ),
            body=json.loads(
                PostOrganizationCategoryRequestEntity(
                    requested_organization_categories=[
                        OrganizationCategory.APP_VENDOR.value
                    ]
                ).model_dump_json()
            ),
        )
        self.put(
            "/category-requests/{category_request_id}/revoke",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationCategoryRequestFixture(
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectOrganizationCategoryRequestIdIntoPathParameters(),
                ],
            ),
        )
        self.put(
            "/profile/details",
            {
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=PostOrganizationDetailsEntity(
                description_de="New description_de",
                description_en="New description_en",
                categories=[],
            ).model_dump(),
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        logo_put_request = make_multipart_file_request("sample.jpg", "logo")
        self.put(
            "/profile/logo",
            {
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            headers={**logo_put_request.headers},
            body=logo_put_request.body,
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()]
            ),
        )
        self.put(
            "/users/{target_user_id}/roles",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                )
            },
            body=json.loads(
                PostOrganizationUserRolesEntity(
                    roles=[OrganizationUserRole.APP_MAINTAINER]
                ).model_dump_json()
            ),
            fixture=CreateOrganizationFixture(
                organization_categories={OrganizationCategory.APP_VENDOR},
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberUserIdIntoPathParameters(
                        OrganizationUserRole.PATHOLOGIST
                    ),
                ],
            ),
        )
        self.put(
            "/users/{target_user_id}/remove",
            responses={
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=204
                )
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberUserIdIntoPathParameters(
                        OrganizationUserRole.PATHOLOGIST
                    ),
                ],
            ),
        )
