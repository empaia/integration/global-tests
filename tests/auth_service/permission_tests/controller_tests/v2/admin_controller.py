import json

from tests.tests.auth_service.permission_tests.controller_tests.fixtures.v2.organization_fixtures import (
    CreateOrganizationFixture,
)
from tests.utils.api_test.controller_test import ControllerV2Test
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationAccountState,
    OrganizationCategory,
    PostComputeProvider,
    PostOrganizationDeploymentConfiguration,
)

from ..fixtures.v2 import (
    CreateUnapprovedOrganizationFixture,
    CreateUserFixture,
    InjectOrganizationIdIntoPathParameters,
    InjectUserEMailAddressIntoQuery,
    InjectUserKeycloakIdIntoPathParameters,
    RegisterNewUserFixture,
)


class AdminControllerTest(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/admin")

    def _setup_delete_endpoints(self):
        self.delete(
            "/organizations/{organization_id}",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateUnapprovedOrganizationFixture(
                request_will_delete_organization=True,
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
            ),
        )
        self.delete(
            "/organizations/{organization_id}/members_only_in_this_organization",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateUnapprovedOrganizationFixture(
                request_will_delete_organization=True,
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
            ),
        )
        self.delete(
            "/users/{target_user_id}/account",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateUserFixture(
                user_may_already_be_deleted=True,
                request_data_injectors=[
                    InjectUserKeycloakIdIntoPathParameters("target_user_id")
                ],
            ),
        )
        self.delete(
            "/organizations/{organization_id}/deployment/compute-provider-jes",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                organization_categories=[OrganizationCategory.APP_CUSTOMER],
            ),
        )

    def _setup_get_endpoints(self):
        self.get(
            "/generate-email-verification-token",
            responses={AccessRole.ADMIN: 200},
            fixture=RegisterNewUserFixture(
                request_data_injectors=[InjectUserEMailAddressIntoQuery()]
            ),
            query={"token_lifetime_ms": 300},
        )
        self.get(
            "/organizations",
            responses={AccessRole.ADMIN: 200},
            query={
                "account_states": [OrganizationAccountState.ACTIVE.value],
                "page": 0,
                "page_size": 1,
            },
        )
        self.get(
            "/clients",
            responses={AccessRole.ADMIN: 200},
            query={
                "page": 0,
                "page_size": 1,
            },
        )
        self.get(
            "/organizations/{organization_id}/clients",
            responses={AccessRole.ADMIN: 200},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
            query={
                "page": 0,
                "page_size": 1,
            },
        )
        self.get(
            "/organizations/{organization_id}/deployment/config",
            responses={AccessRole.ADMIN: 404},  # TODO: inject deployment config before
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
            ),
        )
        self.get(
            "/organizations/{organization_id}/deployment/client-credentials",
            responses={AccessRole.ADMIN: 200},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
        )

    def _setup_put_endpoints(self):
        self.put(
            "/users/{target_user_id}/grant_admin_privileges",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )
        self.put(
            "/users/{target_user_id}/revoke_admin_privileges",
            responses={AccessRole.ADMIN: 204},
            fixture=CreateUserFixture(
                request_data_injectors=[InjectUserKeycloakIdIntoPathParameters()]
            ),
        )
        self.put(
            "/organizations/{organization_id}/deployment/config",
            responses={AccessRole.ADMIN: 200},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()],
                organization_categories=[OrganizationCategory.APP_CUSTOMER],
            ),
            body=json.loads(
                PostOrganizationDeploymentConfiguration().model_dump_json()
            ),
        )
        # will be covered by functional test
        self.put(
            "/organizations/{organization_id}/deployment/compute-provider-jes",
            responses={AccessRole.ADMIN: 404},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoPathParameters()]
            ),
            body=json.loads(
                PostComputeProvider(
                    compute_provider_organization_id="dummy-id"
                ).model_dump_json()
            ),
        )
