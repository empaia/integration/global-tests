from tests.utils.api_test.controller_test.controller_test import (
    ControllerV2Test,
)
from tests.utils.api_test.endpoint_test.expected_response import (
    OrganizationMemberStatusCode,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationCategory,
    OrganizationUserRole,
)

from ..fixtures.v2 import (
    CreateOrganizationFixture,
    CreateOrganizationUserRoleRequestFixture,
    InjectMemberAuthHeaders,
    InjectMemberUserIdIntoPathParameters,
    InjectOrganizationIdIntoHeader,
    InjectOrganizationUserRoleRequestBody,
    InjectOrganizationUserRoleRequestIdIntoPathParameters,
)


class OrganizationControllerV2Test(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/organization")

    def _setup_delete_endpoints(self):
        pass

    def _setup_get_endpoints(self):
        self.get(
            "/users",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[InjectOrganizationIdIntoHeader()],
            ),
        )
        self.get(
            "/users/{target_user_id}/profile",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberUserIdIntoPathParameters(
                        OrganizationUserRole.PATHOLOGIST
                    ),
                ],
            ),
        )
        self.get(
            "/role-requests",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationUserRoleRequestFixture(
                request_data_injectors=[InjectOrganizationIdIntoHeader()],
            ),
            query={"page": 0, "page_size": 9999},
        )
        self.get(
            "/can-leave",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberAuthHeaders(OrganizationUserRole.PATHOLOGIST),
                ],
            ),
        )

    def _setup_put_endpoints(self):
        self.put(
            "/leave",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=204
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=204
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=204
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=204
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=204
                ),
            },
            fixture=CreateOrganizationFixture(
                create_member_count_by_role={OrganizationUserRole.PATHOLOGIST: 1},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectMemberAuthHeaders(OrganizationUserRole.PATHOLOGIST),
                ],
            ),
        )
        self.put(
            "/role-requests",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationFixture(
                organization_categories={OrganizationCategory.APP_VENDOR},
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectOrganizationUserRoleRequestBody(),
                ],
            ),
        )
        self.put(
            "/role-requests/{role_request_id}/revoke",
            responses={
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=403, own=200
                ),
            },
            fixture=CreateOrganizationUserRoleRequestFixture(
                request_data_injectors=[
                    InjectOrganizationIdIntoHeader(),
                    InjectOrganizationUserRoleRequestIdIntoPathParameters(),
                ],
            ),
        )
