import json

from tests.tests.auth_service.permission_tests.controller_tests.fixtures.v2.organization_fixtures import (
    InjectCreatorMembershipRequestIdIntoPathParameter,
)
from tests.utils.api_test.controller_test import ControllerV2Test
from tests.utils.api_test.endpoint_test.expected_response import (
    OrganizationMemberStatusCode,
)
from tests.utils.auth_service.access_role import AccessRole
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    LanguageCode,
    LanguageEntity,
)
from tests.utils.multipart_file_request import (
    make_multipart_file_request,
)

from ..fixtures.v2 import (
    CreateOrganizationFixture,
    CreateUserFixture,
    InjectOrganizationIdIntoBody,
    InjectPostMembershipRequestEntity,
    InjectUserAuthHeaders,
    RequestMembershipInOrganizationFixture,
    UpdateContactDataAndAssertSuccess,
    UpdateUserDetailsAndAssertSuccess,
)


class MyControllerTest(ControllerV2Test):
    def __init__(self):
        super().__init__("/api/v2/my")

    def _setup_delete_endpoints(self):
        self.delete(
            "/profile/picture", responses={AccessRole.ALL_REGISTERED_USERS_V2: 204}
        )
        self.delete(
            "/default-active-organization",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 204},
        )
        self.delete(
            "/account",
            responses={
                AccessRole.ANONYMOUS: 401,
                AccessRole.ALL_REGISTERED_USERS_V2: 204,
            },
            fixture=CreateUserFixture(
                request_data_injectors=[
                    InjectUserAuthHeaders(),
                ]
            ),
        ).set_ignore_endpoint()  # will fail all subsequent tests

    def _setup_get_endpoints(self):
        self.get(
            "/default-active-organization",
            responses={
                AccessRole.ALL_REGISTERED_USERS_V2: 200,
                AccessRole.APP_MAINTAINER: 200,
                AccessRole.DATA_MANAGER: 200,
                AccessRole.PATHOLOGIST: 200,
                AccessRole.ORGANIZATION_MANAGER: 200,
                AccessRole.ORGANIZATION_MEMBER: 200,
            },
        )
        self.get(
            "/membership-requests",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            query={"page": 0, "page_size": 5},
        )
        self.get(
            "/memberships",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectPostMembershipRequestEntity()]
            ),
        )
        self.get("/profile", responses={AccessRole.ALL_REGISTERED_USERS_V2: 200})
        self.get("/organizations", responses={AccessRole.ALL_REGISTERED_USERS_V2: 200})

    def _setup_post_endpoints(self):
        self.post(
            "/membership-requests",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 201},
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectPostMembershipRequestEntity()]
            ),
        )

    def _setup_put_endpoints(self):
        self.put(
            "/profile/contact",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            fixture=UpdateContactDataAndAssertSuccess(),
        )
        self.put(
            "/default-active-organization",
            responses={
                AccessRole.ALL_REGISTERED_USERS_V2: 400,
                AccessRole.APP_MAINTAINER: OrganizationMemberStatusCode(
                    foreign=400, own=200
                ),
                AccessRole.DATA_MANAGER: OrganizationMemberStatusCode(
                    foreign=400, own=200
                ),
                AccessRole.PATHOLOGIST: OrganizationMemberStatusCode(
                    foreign=400, own=200
                ),
                AccessRole.ORGANIZATION_MANAGER: OrganizationMemberStatusCode(
                    foreign=400, own=200
                ),
                AccessRole.ORGANIZATION_MEMBER: OrganizationMemberStatusCode(
                    foreign=400, own=200
                ),
            },
            fixture=CreateOrganizationFixture(
                request_data_injectors=[InjectOrganizationIdIntoBody()]
            ),
        )
        self.put(
            "/profile/details",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            fixture=UpdateUserDetailsAndAssertSuccess(),
        )
        self.put(
            "/profile/language",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            body=json.loads(
                LanguageEntity(language_code=LanguageCode.DE).model_dump_json()
            ),
        )
        picture_put_request = make_multipart_file_request("sample.jpg", "picture")
        self.put(
            "/profile/picture",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 204},
            headers={**picture_put_request.headers},
            body=picture_put_request.body,
        )
        self.put(
            "/membership-requests/{membership_request_id}/revoke",
            responses={AccessRole.ALL_REGISTERED_USERS_V2: 200},
            fixture=RequestMembershipInOrganizationFixture(
                request_data_injectors=[
                    InjectCreatorMembershipRequestIdIntoPathParameter()
                ]
            ),
        )
