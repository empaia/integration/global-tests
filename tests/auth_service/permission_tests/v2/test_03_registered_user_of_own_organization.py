from tests.utils.api_test.api_tests import make_api_tests_v2
from tests.utils.auth_service.access_role import AccessRole

make_api_tests_v2(__name__, AccessRole.REGISTERED_USER, test_with_own_organization=True)
