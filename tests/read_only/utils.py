import json

import requests

# 500 and 501 only temporary
EXPECTED_RESPONSE_CODES = [200, 204, 400, 403, 404, 413, 415, 422, 500, 501]


def api_check(base_url, headers, url_suffix="", openapi_suffix="/openapi.json"):
    suffix = f"{url_suffix}{openapi_suffix}".lstrip("/")
    r = requests.get(f"{base_url}{suffix}")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{base_url}{url_suffix}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url, headers=headers)
            print(f"API TEST - '{op}': '{url}', - STATUS CODE: '{r.status_code}'")
            print(r.content)
            assert r.status_code in EXPECTED_RESPONSE_CODES
