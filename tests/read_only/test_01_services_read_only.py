import pytest

from ...utils.singletons import (
    api_test_state,
    general_settings,
    login_manager,
)
from .utils import api_check

pytestmark = pytest.mark.skipif(
    not api_test_state.test_filter.run_functional_tests,
    reason="Skipping functional tests",
)


def test_auth_service_read_only():
    headers = login_manager.super_admin()
    api_check(
        general_settings.auth_service_host,
        headers,
        openapi_suffix=general_settings.auth_service_openapi_url_suffix,
    )
