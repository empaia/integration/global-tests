# isort: skip_file
# somehow global-deployment makes a new line here
import pytest
import requests
from confidential_apps.confidential_apps.mindpeak.ihc_cell_detection.v1 import (  # pylint: disable=E0401
    ead,
)
from tests.utils.singletons import compose_settings


@pytest.mark.skipif(
    compose_settings.mps_vault_url != "http://empaia-vault.charite.de",
    reason="Only for central vault we can assure to have mindpeak secrets",
)
def test_vault_get_mindpeak_secret():
    # login with app role
    data = {
        "role_id": compose_settings.mps_vault_role_id,
        "secret_id": compose_settings.mps_vault_secret_id,
    }
    r = requests.post(
        f"{compose_settings.mps_vault_url}/v1/auth/approle/login", json=data
    )
    client_token = r.json()["auth"]["client_token"]

    app_namespace = ead["namespace"]
    url = f"{compose_settings.mps_vault_url}/v1/secret/data/{app_namespace}"
    headers = {"X-Vault-Token": client_token, "X-Vault-Request": "true"}
    r = requests.get(url, headers=headers)
    print(f"VAULT - GET {url}:")
    print("r.status_code:", r.status_code)
    print("r.text:", r.text)
    r.raise_for_status()
    assert r.status_code == 200
