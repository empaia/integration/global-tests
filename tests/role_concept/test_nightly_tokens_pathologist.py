from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_audiences_for_role_with_client,
)
from tests.utils.settings import PlatformSettings

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_wbc_token_exclude_1():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.wbc_client_id,
        include_audiences=[],
        exclude_audiences=[
            "auth_service_client",
            "event_service_client",
            "marketplace_service_client",
        ],
        prepend_org_id_to_audiences=False,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_wbc_token_exclude_2():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.wbc_client_id,
        include_audiences=[],
        exclude_audiences=[
            ".us",
            ".idms",
            ".mds",
        ],
        prepend_org_id_to_audiences=True,
    )


# ##############
# ### Row 20 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_wbc_token_include_1():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.wbc_client_id,
        include_audiences=[".wbs"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=True,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
@pytest.mark.skip(
    reason="platform audiencies are currently always included, no matter the client_id. TODO: change"
)
def test_check_pathologist_portal_client_token_exclude_1():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    _platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client="portal_client",
        include_audiences=[],
        exclude_audiences=[
            ".us",
            ".mds",
            ".idms",
            ".wbs",
        ],
        prepend_org_id_to_audiences=True,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_portal_client_token_exclude_2():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    _platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client="portal_client",
        include_audiences=[],
        exclude_audiences=["marketplace_service_client"],
        prepend_org_id_to_audiences=False,
    )


# #################
# ### Row 2, 14 ###
# #################
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_portal_client_token_include_1():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    _platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client="portal_client",
        include_audiences=[
            "auth_service_client",
            "event_service_client",
        ],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_mdc_token_exclude_1():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.mdc_client_id,
        include_audiences=[],
        exclude_audiences=[
            "auth_service_client",
            "event_service_client",
            "marketplace_service_client",
        ],
        prepend_org_id_to_audiences=False,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_pathologist_mdc_token_exclude_2():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.mdc_client_id,
        include_audiences=[],
        exclude_audiences=[
            ".mds",
            ".us",
            ".idms",
        ],
        prepend_org_id_to_audiences=True,
    )


@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
@pytest.mark.skip(
    reason="platform audiencies are currently always included, no matter the client_id. TODO: change"
)
def test_check_pathologist_mdc_token_exclude_3():
    orga_out_file = DATA_OUT_PATH.joinpath("silver_organisations_silver.json")
    env_file = DATA_OUT_PATH.joinpath("silver_organisations_silver_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="PATHOLOGIST",
        client=platform_settings.mdc_client_id,
        include_audiences=[],
        exclude_audiences=[
            ".wbs",
        ],
        prepend_org_id_to_audiences=True,
    )
