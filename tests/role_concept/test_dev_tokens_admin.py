from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_admin_portal_client_token,
)

CWD = Path.cwd()


# #################
# ### Row 2, 14 ###
# #################
# #####################
# ### Row 6, 12, 13 ###
# #####################
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_admin_portal_client_token():
    check_admin_portal_client_token()
