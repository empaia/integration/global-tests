from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_audiences_for_role_with_client,
)

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


# #################
# ### Row 2, 14 ###
# #################
# ##############
# ### Row 11 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_app_maintainer_portal_client_token_include_1():
    orga_out_file = DATA_OUT_PATH.joinpath("empaia_apps_organisation_EMPAIA Apps.json")
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="APP_MAINTAINER",
        client="portal_client",
        include_audiences=[
            "auth_service_client",
            "event_service_client",
            "marketplace_service_client",
        ],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )
