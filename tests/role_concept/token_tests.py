from pathlib import Path

from tests.utils import utils_resources
from tests.utils.singletons import login_manager

CWD = Path.cwd()


def get_credentials_for_role(orga_out_file: str, orga_role: str):
    orga_out_file = Path(orga_out_file)
    assert orga_out_file.is_file()

    orga_out_data = utils_resources.get_json_data(orga_out_file)
    for item in orga_out_data["organization_users"]:
        user_name = item["user"]["contact_data"]["email_address"]
        user_password = item["password"]
        if orga_role in item["organization_user_roles"]:
            return user_name, user_password
    return None, None


def check_audiences_for_role_with_client(
    orga_out_file: str,
    role: str,
    client: str,
    include_audiences: list,
    exclude_audiences: list,
    prepend_org_id_to_audiences: bool,
):
    user_name, password = get_credentials_for_role(orga_out_file, role)
    assert user_name

    _, d_token = login_manager.get_access_flow_tokens(
        client_id=client,
        user_name=user_name,
        password=password,
    )
    org_id = list(d_token["organizations"].keys())[0]
    if role:
        role_norm = role.replace("_", "-").lower()
        token_roles_norm = [
            r.lower() for r in d_token["organizations"][org_id]["roles"]
        ]
        print(role_norm, token_roles_norm)
        assert role_norm in token_roles_norm
    for aud in include_audiences:
        if prepend_org_id_to_audiences:
            aud = org_id + aud
        assert aud in d_token["aud"]
    for aud in exclude_audiences:
        if prepend_org_id_to_audiences:
            aud = org_id + aud
        assert aud not in d_token["aud"]


def check_service_audiences_for_role_with_client(
    env_file: str,
    role: str,
    client: str,
    secret: str,
    include_audiences: list,
    exclude_audiences: list,
    prepend_org_id_to_audiences: bool,
):
    _, d_token = login_manager.get_client_credentials_flow_tokens(
        client_id=client,
        client_secret=secret,
    )
    org_id = list(d_token["organizations"].keys())[0]
    if role:
        role_norm = role.replace("_", "-").lower()
        token_roles_norm = [
            r.lower() for r in d_token["organizations"][org_id]["roles"]
        ]
        print("checking for role: ", role_norm)
        print("in token roles: ", token_roles_norm)
        assert role_norm in token_roles_norm
    for aud in include_audiences:
        if prepend_org_id_to_audiences:
            aud = org_id + aud
        print("checking for audience:", aud)
        print("in token audiences: ", d_token["aud"])
        assert aud in d_token["aud"]
    for aud in exclude_audiences:
        if prepend_org_id_to_audiences:
            aud = org_id + aud
        assert aud not in d_token["aud"]


def check_admin_portal_client_token():
    admin_user_name = login_manager.settings.super_admin_name
    admin_user_password = login_manager.settings.super_admin_password

    _, d_token = login_manager.get_access_flow_tokens(
        client_id="portal_client",
        user_name=admin_user_name,
        password=admin_user_password,
    )
    assert "auth_service_client" in d_token["aud"]
    assert "marketplace_service_client" in d_token["aud"]
    assert "event_service_client" in d_token["aud"]
    assert "admin" in d_token["resource_access"]["auth_service_client"]["roles"]
    assert "moderator" in d_token["resource_access"]["auth_service_client"]["roles"]
