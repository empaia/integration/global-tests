from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_service_audiences_for_role_with_client,
)
from tests.utils.settings import PlatformSettings

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


# #############
# ### AS    ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_global_roles_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role="service-app-reader",
        client=platform_settings.as_client_id,
        secret=platform_settings.as_client_secret,
        include_audiences=[],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### AS    ###
# ### Row 9 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_global_roles_2():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role="service-app-config-reader",
        client=platform_settings.as_client_id,
        secret=platform_settings.as_client_secret,
        include_audiences=[],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### AS    ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_global_audiences_include_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.as_client_id,
        secret=platform_settings.as_client_secret,
        include_audiences=["marketplace_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# ##############
# ### AS     ###
# ### Row 17 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_platform_audiences_include_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.as_client_id,
        secret=platform_settings.as_client_secret,
        include_audiences=[".mds"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=True,
    )


# #############
# ### AS    ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_platform_audiences_exclude_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.as_client_id,
        secret=platform_settings.as_client_secret,
        include_audiences=[],
        exclude_audiences=[".us", ".idms"],
        prepend_org_id_to_audiences=True,
    )
