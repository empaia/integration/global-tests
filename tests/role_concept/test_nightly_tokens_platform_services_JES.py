from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_service_audiences_for_role_with_client,
)
from tests.utils.settings import PlatformSettings

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


# #############
# ### JES   ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_jes_roles_1():
    _orga_out_file = DATA_OUT_PATH.joinpath(
        "empaia_compute_organisation_EMPAIA Compute.json"
    )
    env_file = DATA_OUT_PATH.joinpath("empaia_compute_organisation_EMPAIA Compute_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role="service-app-reader",
        client=platform_settings.jes_client_id,
        secret=platform_settings.jes_client_secret,
        include_audiences=[],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### JES   ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_jes_roles_2():
    _orga_out_file = DATA_OUT_PATH.joinpath(
        "empaia_compute_organisation_EMPAIA Compute.json"
    )
    env_file = DATA_OUT_PATH.joinpath("empaia_compute_organisation_EMPAIA Compute_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role="service-compute-provider",
        client=platform_settings.jes_client_id,
        secret=platform_settings.jes_client_secret,
        include_audiences=[],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### JES   ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_wbs_global_audiences_include_1():
    _orga_out_file = DATA_OUT_PATH.joinpath(
        "empaia_compute_organisation_EMPAIA Compute.json"
    )
    env_file = DATA_OUT_PATH.joinpath("empaia_compute_organisation_EMPAIA Compute_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.jes_client_id,
        secret=platform_settings.jes_client_secret,
        include_audiences=["marketplace_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )
