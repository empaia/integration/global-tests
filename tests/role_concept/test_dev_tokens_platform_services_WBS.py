from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_service_audiences_for_role_with_client,
)
from tests.utils import utils_resources
from tests.utils.settings import PlatformSettings

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


# #############
# ### WBS   ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_global_roles_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role="service-app-reader",
        client=platform_settings.wbs_client_id,
        secret=platform_settings.wbs_client_secret,
        include_audiences=[],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### WBS   ###
# ### Row 8 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_global_audiences_include_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.wbs_client_id,
        secret=platform_settings.wbs_client_secret,
        include_audiences=["marketplace_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# ##################
# ### WBS        ###
# ### Row 16, 17 ###
# ##################
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_platform_audiences_include_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.wbs_client_id,
        secret=platform_settings.wbs_client_secret,
        include_audiences=[".mds", ".idms"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=True,
    )


# ##############
# ### WBS    ###
# ### Row 19 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_platform_audiences_include_2():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    orga_out_file_compute = DATA_OUT_PATH.joinpath(
        "dummy_organisations_compute_provider_1.json"
    )
    orga_out_data_compute = utils_resources.get_json_data(orga_out_file_compute)
    compute_orga_id = orga_out_data_compute["organization"]["organization_id"]
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.wbs_client_id,
        secret=platform_settings.wbs_client_secret,
        include_audiences=[f"{compute_orga_id}.cpjes"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# #############
# ### WBS   ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] == "/global_services",
    reason="Can only check dev tokens if in dev deployment",
)
def test_check_wbs_platform_audiences_exclude_1():
    _orga_out_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1.json")
    env_file = DATA_OUT_PATH.joinpath("dummy_organisations_hospital_ref_1_.env")
    platform_settings = PlatformSettings(_env_file=env_file)
    check_service_audiences_for_role_with_client(
        env_file=env_file,
        role=None,
        client=platform_settings.wbs_client_id,
        secret=platform_settings.wbs_client_secret,
        include_audiences=[],
        exclude_audiences=[".us"],
        prepend_org_id_to_audiences=True,
    )
