from pathlib import Path

import pytest

from tests.tests.role_concept.token_tests import (
    check_audiences_for_role_with_client,
)

CWD = Path.cwd()
DATA_OUT_PATH = CWD.joinpath("data_generator_output")


# #############
# ### Row 2 ###
# #############
@pytest.mark.skipif(
    str(CWD)[-4:] != "/nightly",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_clearance_maintainer_portal_client_token_include_1():
    orga_out_file = DATA_OUT_PATH.joinpath(
        "product_provider_organizations_non_prod_Visiopharm.json"
    )
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="CLEARANCE_MAINTAINER",
        client="portal_client",
        include_audiences=["auth_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# ##############
# ### Row 14 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_clearance_maintainer_portal_client_token_include_2():
    orga_out_file = DATA_OUT_PATH.joinpath(
        "product_provider_organizations_non_prod_Visiopharm.json"
    )
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="CLEARANCE_MAINTAINER",
        client="portal_client",
        include_audiences=["event_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )


# ##############
# ### Row 24 ###
# ##############
@pytest.mark.skipif(
    str(CWD)[-8:] != "/nightly" and str(CWD)[-3:] != "/ci",
    reason="Can only check nightly tokens if in nightly deployment",
)
def test_check_clearance_maintainer_portal_client_token_include_3():
    orga_out_file = DATA_OUT_PATH.joinpath(
        "product_provider_organizations_non_prod_Visiopharm.json"
    )
    check_audiences_for_role_with_client(
        orga_out_file=orga_out_file,
        role="CLEARANCE_MAINTAINER",
        client="portal_client",
        include_audiences=["marketplace_service_client"],
        exclude_audiences=[],
        prepend_org_id_to_audiences=False,
    )
