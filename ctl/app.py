import glob
import json
import logging
import os.path
import pprint
import sys

from ..data_generator_utils.datagen_wipe import wipe_auth_service_data
from ..utils.singletons import login_manager

logger = logging.getLogger("testsctl")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def drop_all_data():
    logger.info("Dropping all data...")
    wipe_auth_service_data()
    logger.info("Dropped all data.")


def get_client_token_data(client_id: str, client_secret: str):
    _, decoded_token = login_manager.get_client_credentials_flow_tokens(
        client_id, client_secret
    )
    pprint.pprint(decoded_token)


def get_user_token_data(email_address: str, password: str, client_id: str):
    _, decoded_token = login_manager.get_user_tokens(email_address, password, client_id)
    pprint.pprint(decoded_token)


def generate_production_client_settings():
    production_resource_directory = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "..", "resources", "production"
    )
    assert os.path.exists(production_resource_directory)
    for path in glob.glob(f"{production_resource_directory}/*_clients.json"):
        basename = os.path.basename(path)
        hostname = basename.split("_clients")[0]
        web_origin = f"https://{hostname}.empaia.org"
        mdc_uri = f"{web_origin}/data"
        wbc_uri = f"{web_origin}/wbc"
        wbc2_uri = f"{web_origin}/wbc2"
        with open(path, encoding="utf8") as f:
            data = json.load(f)
        for organization_name in data:
            client_data = data[organization_name]
            client_settings = client_data["clients"]
            if isinstance(
                client_settings, list
            ):  # deprecated client list without settings:
                client_settings = {client_name: {} for client_name in client_settings}
            if "wbc" in client_settings:
                client_settings["wbc"] = {
                    "redirect_uris": [wbc_uri, wbc2_uri],
                    "web_origins": [web_origin],
                }
            if "dmc" in client_settings:
                client_settings["dmc"] = {
                    "redirect_uris": [mdc_uri],
                    "web_origins": [web_origin],
                }
            client_data["clients"] = client_settings
            print(f"Updated client settings for {hostname}: {client_settings}")
        with open(path, "w", encoding="utf8") as f:
            _dump_client_data(data, f)


def _dump_client_data(data, fp):
    content = "\n".join(_client_data_to_lines(data))
    # verify that we generated correct json syntax:
    json.loads(content)
    fp.write(content)


def _client_data_to_lines(data):
    indent = 4
    lines = ["{"]
    for organization_name in data:
        lines += _organization_data_to_lines(
            organization_name, data[organization_name], 1, indent
        )
    lines += ["}"]
    return lines


def _organization_data_to_lines(organization_name, organization_data, depth, indent):
    lines = [f"{' '*indent*depth}\"{organization_name}\": {{"]
    expected_keys = (
        "clients",
        "internal_mappings",
        "internal_user_mappings",
        "external_mappings",
    )
    keys = set(organization_data.keys())
    # assert that we handle all keys:
    assert len(keys.difference(expected_keys)) == 0
    is_first_key = True
    for key in expected_keys:
        if key in keys:
            if is_first_key:
                is_first_key = False
            else:
                lines[-1] += ","
            if key == "clients":
                lines += _client_settings_data_to_lines(
                    organization_data[key], depth + 1, indent
                )
            else:
                lines += _mapping_data_to_lines(
                    key, organization_data[key], depth + 1, indent
                )

    lines += [f"{' '*indent*depth}}}"]
    return lines


def _client_settings_data_to_lines(client_settings_data, depth, indent):
    assert isinstance(client_settings_data, dict)
    lines = [f"{' '*indent*depth}\"clients\": {{"]
    is_first_client = True
    client_names = sorted(list(client_settings_data.keys()))
    for client_name in client_names:
        if is_first_client:
            is_first_client = False
        else:
            lines[-1] += ","
        lines += [f"{' '*indent*(depth+1)}\"{client_name}\": {{"]
        client_settings = client_settings_data[client_name]
        if len(client_settings) == 0:
            lines[-1] += "}"
        else:
            for name in ("redirect_uris", "web_origins"):
                uri_list = '", "'.join(client_settings[name])
                lines += [f"{' '*indent*(depth+2)}\"{name}\": [\"{uri_list}\"]"]
                if name != "web_origins":
                    lines[-1] += ","
            lines += [f"{' '*indent*(depth+1)}}}"]
    lines += [f"{' '*indent*depth}}}"]
    return lines


def _mapping_data_to_lines(mapping_name, mapping_data, depth, indent):
    assert isinstance(mapping_data, list)
    lines = [f"{' '*indent*depth}\"{mapping_name}\": ["]
    is_first_item = True
    for item in mapping_data:
        if is_first_item:
            is_first_item = False
        else:
            lines[-1] += ","
        expected_keys = ("from", "to", "to_orga")
        keys = set(item.keys())
        assert len(keys.difference(expected_keys)) == 0
        item_string = f"{' '*indent*(depth+1)}{{\"from\": \"{item['from']}\", "
        if "to_orga" in item:
            item_string += f"\"to_orga\": \"{item['to_orga']}\", "
        item_string += f"\"to\": \"{item['to']}\"}}"
        lines += [item_string]
    lines += [f"{' '*indent*depth}]"]
    return lines
