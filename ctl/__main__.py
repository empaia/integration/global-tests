import sys

from ..data_generator_utils import wait_for_auth_service_to_be_available
from . import app

if __name__ == "__main__":
    if "drop_all_data" in sys.argv:
        wait_for_auth_service_to_be_available()
        app.drop_all_data()
    elif "get_client_token_data" in sys.argv:
        app.get_client_token_data(client_id=sys.argv[2], client_secret=sys.argv[3])
    elif "get_user_token_data" in sys.argv:
        app.get_user_token_data(
            email_address=sys.argv[2], password=sys.argv[3], client_id=sys.argv[4]
        )
    elif "generate_production_client_settings" in sys.argv:
        wait_for_auth_service_to_be_available()
        app.generate_production_client_settings()
    else:
        raise AssertionError(f"Unknown command: {sys.argv[1:]}")
