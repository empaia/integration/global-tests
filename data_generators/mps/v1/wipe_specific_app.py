import argparse

from tests.utils import singletons
from tests.utils.mps import utils_mps


def wipe_specific_app(mps_headers, p_app_id):
    if not singletons.general_settings.allow_data_wipe:
        error = (
            "Tried to execute data wipe of all MPS apps "
            "but ENV variable [PYTEST_ALLOW_DATA_WIPE] is set to "
            f"{singletons.general_settings.allow_data_wipe}. "
            "This might indicate that the performed action might not be intended "
            "in the current deployment?"
        )
        raise ValueError(error)

    r = utils_mps.generic_delete(
        f"/v1/admin/portal-apps/{p_app_id}", headers=mps_headers
    )
    assert r.status_code == 200
    assert bool(r.content) is True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("portal_app_id", help="ID of the portal app to delete")
    args = parser.parse_args()

    portal_app_id = args.portal_app_id

    headers = singletons.login_manager.super_admin()

    wipe_specific_app(headers, portal_app_id)
