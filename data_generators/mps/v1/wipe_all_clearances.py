import argparse

from tests.utils import singletons
from tests.utils.mps import utils_mps


def wipe_all_clearances(admin_header):
    if not singletons.general_settings.allow_data_wipe:
        error = (
            "Tried to execute data wipe of all MPS apps "
            "but ENV variable [PYTEST_ALLOW_DATA_WIPE] is set to "
            f"{singletons.general_settings.allow_data_wipe}. "
            "This might indicate that the performed action might not be intended "
            "in the current deployment?"
        )
        raise ValueError(error)

    _ = utils_mps.generic_put(
        "/v1/admin/truncate-db-clearances", headers=admin_header, raise_status=True
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    headers = singletons.login_manager.super_admin()

    wipe_all_clearances(headers)
