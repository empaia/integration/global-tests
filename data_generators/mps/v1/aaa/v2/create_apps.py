from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)
from tests.utils.mps.v1 import create_apps


def get_organization_id_by_organization_name(organization_name: str):
    organization = PublicController().get_organization_by_name(organization_name)
    return organization.organization_id


if __name__ == "__main__":
    create_apps.main(get_organization_id_by_organization_name)
