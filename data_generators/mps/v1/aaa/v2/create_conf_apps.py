from tests.data_generators.mps.v1.aaa.v2.create_apps import (
    get_organization_id_by_organization_name,
)
from tests.utils.mps.v1 import create_conf_apps

if __name__ == "__main__":
    create_conf_apps.main(get_organization_id_by_organization_name)
