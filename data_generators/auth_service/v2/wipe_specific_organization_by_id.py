import argparse

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)


def wipe_specific_orga_and_members(organization_id: str):
    AdminController().delete_organization_and_members_only_in_given_organization(
        organization_id
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("organization_id", default=None, help="organization_id")
    args = parser.parse_args()

    wipe_specific_orga_and_members(args.organization_id)
