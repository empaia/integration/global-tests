import argparse

from tests.data_generator_utils import (
    datagen_configure_deployments,
    wait_for_auth_service_to_be_available,
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "deployment_configuration_file",
        help="Name of deployment configuration file in the resources directory.",
    )
    args = parser.parse_args()

    wait_for_auth_service_to_be_available()

    client_group_data_by_client_name_by_organization_name = (
        datagen_configure_deployments.configure_deployments_from_resource_file(
            args.deployment_configuration_file
        )
    )
