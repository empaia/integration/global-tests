import argparse
import json
import os

from tests.data_generator_utils import (
    datagen_organization,
    datagen_user,
    wait_for_auth_service_to_be_available,
)
from tests.utils import utils_resources
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationUserRole,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--recreate-user",
        action="store_true",
        help="Optionally recreate existing user",
    )
    parser.add_argument(
        "--organization-name",
        default=None,
        help="Optionally add user to organization with this name",
    )
    parser.add_argument(
        "--organization-user-role",
        default=None,
        action="append",
        help="Organization user roles to set. Possible values are: "
        + f"{', '.join([r.value.lower() for r in OrganizationUserRole])}",
    )
    parser.add_argument("user_file", default=None, help="Path to user json file.")
    args = parser.parse_args()

    wait_for_auth_service_to_be_available()

    recreate_user = args.recreate_user

    if args.organization_name:
        # validates, that only 1 orga with given name exists
        organization = PublicController().get_organization_by_name(
            args.organization_name
        )

        user, post_user_entity = datagen_user.create_user_from_resource_file(
            args.user_file, recreate_user=recreate_user
        )

        if args.organization_user_role:
            organization_user_roles = [
                OrganizationUserRole(r.upper()) for r in args.organization_user_role
            ]
        else:
            # default if no param given
            organization_user_roles = [
                OrganizationUserRole.DATA_MANAGER,
                OrganizationUserRole.PATHOLOGIST,
            ]
        datagen_organization.add_user_to_organization(
            user, post_user_entity, args.organization_name, organization_user_roles
        )

    else:
        user, post_user_entity = datagen_user.create_user_from_resource_file(
            args.user_file, recreate_user=recreate_user
        )

    out_data = {
        "user": json.loads(user.model_dump_json()),
        "password": post_user_entity.password,
    }
    output_file_name = os.path.basename(args.user_file)
    output_file_name = f"{os.path.splitext(output_file_name)[0]}_{user.username}.json"
    utils_resources.write_data_generator_output_to_json_file(output_file_name, out_data)
