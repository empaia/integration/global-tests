import argparse

from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.public_controller import (
    PublicController,
)


def wipe_specific_orga_and_members(organization_name: str):
    AdminController().delete_organization_and_members_only_in_given_organization(
        PublicController().get_organization_by_name(organization_name).organization_id
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("organization_name", default=None, help="organization_name")
    args = parser.parse_args()

    wipe_specific_orga_and_members(args.organization_name)
