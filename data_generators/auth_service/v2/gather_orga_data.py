import glob
from pathlib import Path
from typing import Dict, List

from tests.data_generator_utils import (
    wait_for_auth_service_to_be_available,
)
from tests.utils import singletons, utils_resources
from tests.utils.auth_service import utils_auth_service
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    ClientCredentials,
    OrganizationDeploymentCredentials,
)


def gather_orgas_data(orgas_output_dir: str):
    orgas_data = []
    for f_name in glob.glob(f"{orgas_output_dir}/*.json"):
        if singletons.general_settings.verbose_requests:
            print(f_name)

        orga_out_data = utils_resources.get_json_data(f_name)

        if "is_orga_out_file" in orga_out_data:
            orga = orga_out_data["organization"]
            orga_id = orga["organization_id"]

            orga_tokens_long_ = {"client_tokens": {}, "user_tokens": {}}
            orga_tokens_short_ = {"client_tokens": {}, "user_tokens": {}}
            orga_envs_ = []

            organization_deployment_credentials = (
                AdminController().get_client_credentials(organization_id=orga_id)
            )

            for client_name in vars(organization_deployment_credentials):
                client_credentials = getattr(
                    organization_deployment_credentials, client_name
                )
                if client_credentials is None:
                    continue

                is_public_client = client_name in [
                    "workbench_client",
                    "data_management_client",
                ]
                if is_public_client:
                    gather_data_from_public_client_token(
                        client_credentials,
                        orga_out_data,
                        orga_tokens_short_,
                        orga_tokens_long_,
                        orga_envs_,
                    )
                else:
                    client_credentials = retireve_credentials_for_client(
                        organization_deployment_credentials,
                        client_credentials.client_id,
                    )
                    gather_data_from_confidential_client_token(
                        client_credentials,
                        client_credentials.secret,
                        orga_tokens_short_,
                        orga_tokens_long_,
                        orga_envs_,
                    )

            gather_orga_envs_from_organization_users(orga_out_data, orga_envs_, orga_id)

            orgas_data.append(
                [
                    Path(f_name),
                    orga_tokens_long_,
                    orga_tokens_short_,
                    orga_envs_,
                ]
            )

    return orgas_data


def retireve_credentials_for_client(
    organization_deployment_credentials: OrganizationDeploymentCredentials,
    client_id: str,
) -> ClientCredentials:
    client_short_name = client_id.split(".")[-1]
    if client_short_name == "wbs":
        return organization_deployment_credentials.workbench_service
    if client_short_name == "mds":
        return organization_deployment_credentials.medical_data_service
    if client_short_name == "as":
        return organization_deployment_credentials.app_service
    if client_short_name == "jes":
        return organization_deployment_credentials.job_execution_service
    if client_short_name == "idms":
        return organization_deployment_credentials.id_mapper_service
    if client_short_name == "us":
        return organization_deployment_credentials.upload_service
    if client_short_name == "cpjes":
        return (
            organization_deployment_credentials.compute_provider_job_execution_service
        )
    return None


def gather_data_from_public_client_token(
    client_credentials: ClientCredentials,
    orga_out_data: Dict,
    orga_tokens_short_: Dict,
    orga_tokens_long_: Dict,
    orga_envs_: List,
):
    client_short_name = client_credentials.client_id.split(".")[-1]
    for client_type, client_type_info in utils_auth_service.CLIENTS.items():
        if client_type == client_short_name:
            for prefix in client_type_info["out_env_prefix"]:
                orga_envs_.append(f"{prefix}_CLIENT_ID={client_credentials.client_id}")

    for user in orga_out_data["organization_users"]:
        user_name = user["user"]["username"]
        user_password = user["password"]
        if user_name not in orga_tokens_long_["user_tokens"]:
            orga_tokens_long_["user_tokens"][user_name] = {}
        if user_name not in orga_tokens_short_["user_tokens"]:
            orga_tokens_short_["user_tokens"][user_name] = {}
        try:
            _header = singletons.login_manager.user(
                user_name, user_password, client_credentials.client_id
            )
            _token, decoded_token = singletons.login_manager.get_access_flow_tokens(
                client_credentials.client_id, user_name, user_password
            )
            orga_tokens_short_["user_tokens"][user_name][
                client_credentials.client_id
            ] = decoded_token["aud"]
            orga_tokens_long_["user_tokens"][user_name][
                client_credentials.client_id
            ] = decoded_token
        except AssertionError as ex:
            orga_tokens_long_["user_tokens"][user_name][
                client_credentials.client_id
            ] = f"Error in gather script, eventually due to invalid redirect_uri:   {ex}"
            orga_tokens_short_["user_tokens"][user_name][
                client_credentials.client_id
            ] = f"Error in gather script, eventually due to invalid redirect_uri    {ex}"


def gather_data_from_confidential_client_token(
    client_credentials: ClientCredentials,
    client_secret: str,
    orga_tokens_short_: Dict,
    orga_tokens_long_: Dict,
    orga_envs_: List,
):
    _token, decoded_token = singletons.login_manager.get_client_credentials_flow_tokens(
        client_credentials.client_id, client_secret
    )
    client_short_name = client_credentials.client_id.split(".")[-1]
    # client_sub = decoded_token["sub"]

    orga_tokens_long_["client_tokens"][client_credentials.client_id] = decoded_token
    orga_tokens_short_["client_tokens"][client_credentials.client_id] = decoded_token[
        "aud"
    ]

    for client_type, client_type_info in utils_auth_service.CLIENTS.items():
        if client_type == client_short_name:
            for prefix in client_type_info["out_env_prefix"]:
                orga_envs_.append(f"{prefix}_CLIENT_ID={client_credentials.client_id}")
                orga_envs_.append(f"{prefix}_CLIENT_SECRET={client_secret}")
                # orga_envs_.append(f"{prefix}_CLIENT_SUB={client_sub}")


def gather_orga_envs_from_organization_users(
    orga_out_data: Dict, orga_envs_: List, orga_id: str
):
    for user in orga_out_data["organization_users"]:
        user_name = user["user"]["username"]
        user_sub = user["user"]["user_id"]
        user_password = user["password"]
        user_roles = user["organization_user_roles"]
        for key in utils_auth_service.USERS.keys():
            if key in user_roles:
                prefix = utils_auth_service.USERS[key]
                orga_envs_.append(f"{prefix}_USER_NAME={user_name}")
                orga_envs_.append(f"{prefix}_USER_PASSWORD={user_password}")
                orga_envs_.append(f"{prefix}_USER_SUB={user_sub}")
                if key == "DATA_MANAGER":
                    orga_envs_.append(f"DMC_STATIC_USER_ID={user_sub}")
                if key == "PATHOLOGIST":
                    orga_envs_.append(f"WBC_WBS_USER_ID={user_sub}")

    orga_envs_.append(f"LOGIN_WBS_ORGANIZATION_ID={orga_id}")
    orga_envs_.append(f"ORGANIZATION_ID={orga_id}")
    orga_envs_.append(f"LOGIN_IDP_URL={singletons.login_manager.settings.idp_url}")


if __name__ == "__main__":
    orga_out_folder = utils_resources.get_data_generator_output_directory()
    gathered_orga_data = gather_orgas_data(orga_out_folder)

    wait_for_auth_service_to_be_available()

    for data in gathered_orga_data:
        input_file = data[0]
        orga_tokens_long = data[1]
        orga_tokens_short = data[2]
        orga_envs = data[3]

        file_name = f"{input_file.stem}_tokens_long.json"
        utils_resources.write_data_generator_output_to_json_file(
            file_name, orga_tokens_long
        )
        file_name = f"{input_file.stem}_tokens_short.json"
        utils_resources.write_data_generator_output_to_json_file(
            file_name, orga_tokens_short
        )
        file_name = f"{input_file.stem}_.env"
        file_name = utils_resources.resolve_data_generator_output_file_name(file_name)
        with open(file_name, "w", encoding="utf8") as f:
            f.write("\n".join(orga_envs))
