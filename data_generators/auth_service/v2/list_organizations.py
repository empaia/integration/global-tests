import argparse
import json

from tests.utils.auth_service.api_controllers.v2.moderator_controller import (
    ModeratorController,
)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--brief",
        action="store_true",
        help="List organizations briefly, omitting details,",
    )
    args = parser.parse_args()

    moderator_controller = ModeratorController()
    organizations_entity = moderator_controller.get_organizations()
    print(f"Found {organizations_entity.total_organizations_count} organizations:")
    for organization in organizations_entity.organizations:
        if args.brief:
            print(f"{organization.organization_id}: {organization.organization_name}")
        else:
            organization_profile = moderator_controller.get_organization(
                organization.organization_id
            )
            print(
                json.dumps(
                    json.loads(organization_profile.model_dump_json()),
                    indent=4,
                    sort_keys=True,
                )
            )


if __name__ == "__main__":
    main()
