from tests.data_generator_utils import (
    wait_for_auth_service_to_be_available,
)
from tests.utils import tools, utils_resources
from tests.utils.auth_service.api_controllers.v2.admin_controller import (
    AdminController,
)
from tests.utils.auth_service.api_controllers.v2.entity_models import (
    OrganizationAccountState,
)

if __name__ == "__main__":
    wait_for_auth_service_to_be_available()

    organization_data_list = []
    controller = AdminController()
    fetched_orgas = controller.get_organizations(
        page=0,
        page_size=9999,
        account_states=[OrganizationAccountState.ACTIVE],
        return_json=True,
    )

    for o in fetched_orgas["organizations"]:
        organization_data_list.append(
            {
                "organization": o,
                "organization_users": [],
                "is_orga_out_file": True,
            }
        )
    for organization_data in organization_data_list:
        organization_name = tools.normalize_name(
            organization_data["organization"]["organization_name"]
        )
        output_file_name = f"_recreated_orga_file__{organization_name}.json"

        utils_resources.write_data_generator_output_to_json_file(
            output_file_name,
            organization_data,
        )
