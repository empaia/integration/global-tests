from os import getcwd
from pathlib import Path
from typing import Callable

from tests.data_generators.auth_service.v2.create_organizations import (
    datagen_organization,
)
from tests.utils import utils_resources


def main(create_organizations_fn: Callable):
    orgas_and_users = []

    vendor_files_dir = (
        Path(__file__).parents[4].joinpath("confidential_apps", "vendors")
    )

    for vendor_dir in vendor_files_dir.glob("*"):
        vendor_dir = vendor_files_dir.joinpath(vendor_dir)
        if vendor_dir.is_dir() and "__pycache__" not in str(vendor_dir):
            data_f_name = vendor_dir.joinpath("data.json")
            try:
                orga_ = create_organizations_fn(data_f_name)
                orgas_and_users.append(orga_[0])
            except Exception as ex:
                if "Organization already exists" not in ex.__str__():
                    raise ex

    for orga in orgas_and_users:
        # save results
        organization_name = orga["organization"].get(
            "organization_name"
        )  # Auth Service V2 API Data Model
        if organization_name is None:
            organization_name = orga["organization"][
                "name"
            ]  # Auth Service V1 API Data Model
        out_file = Path(getcwd()).joinpath(
            "data_generator_output", f"conf_app_vendor_{organization_name}.json"
        )
        utils_resources.write_to_json_file(out_file, orga)


if __name__ == "__main__":
    main(datagen_organization.create_organizations_and_users_from_resource_file)
