from tests.data_generator_utils import (
    wait_for_auth_service_to_be_available,
)
from tests.data_generator_utils.datagen_wipe import (
    wipe_auth_service_data,
)

if __name__ == "__main__":
    wait_for_auth_service_to_be_available()
    wipe_auth_service_data()
