import argparse
import os

from tests.data_generator_utils import (
    datagen_organization,
    wait_for_auth_service_to_be_available,
)
from tests.utils import utils_resources

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "organizations_file_name",
        help="Name of organizations json file in the resources directory.",
    )
    args = parser.parse_args()

    wait_for_auth_service_to_be_available()

    organization_data_list = (
        datagen_organization.create_organizations_and_users_from_resource_file(
            args.organizations_file_name
        )
    )
    output_file_name_prefix = os.path.basename(args.organizations_file_name)
    output_file_name_prefix = os.path.splitext(output_file_name_prefix)[0]
    for organization_data in organization_data_list:
        utils_resources.write_data_generator_output_to_json_file(
            f"{output_file_name_prefix}_{organization_data['organization']['organization_name']}.json",
            organization_data,
        )
