import argparse
import logging
import sys
import time
from pathlib import Path

from keycloak import KeycloakConnectionError

from tests.keycloak_initialize.empaia_keycloak_admin import (
    EmpaiaKeycloakAdmin,
)
from tests.keycloak_initialize.empaia_realm_creator import (
    EMPAIA_REALM_NAME,
    EMPAIARealmCreator,
)
from tests.keycloak_initialize.global_clients_manager import (
    GlobalClientsManager,
)
from tests.keycloak_initialize.global_clients_migrator import (
    GlobalClientsMigrator,
)
from tests.keycloak_initialize.organization_client_scope_creator import (
    OrganizationClientScopeCreator,
)
from tests.keycloak_initialize.settings import InitializeSettings


class AuthServiceInitializer:
    def __init__(self, settings: InitializeSettings):
        self._settings = settings
        self._logger = logging.getLogger("AuthServiceInitializer")
        self._logger.setLevel(logging.DEBUG)
        self._keycloak_admin = self._create_keycloak_admin_interface()
        self._empaia_realm_creator = EMPAIARealmCreator(
            self._keycloak_admin, self._settings, self._logger
        )
        self._global_clients_manager = GlobalClientsManager(
            self._settings.clients, self._keycloak_admin
        )
        self._organization_client_scope_creator = OrganizationClientScopeCreator(
            self._keycloak_admin
        )

    def get_keycloak_admin_interface(self) -> EmpaiaKeycloakAdmin:
        return self._keycloak_admin

    def recreate_keycloak_admin_interface(self) -> EmpaiaKeycloakAdmin:
        self._keycloak_admin = self._create_keycloak_admin_interface()
        return self._keycloak_admin

    def _create_keycloak_admin_interface(self) -> EmpaiaKeycloakAdmin:
        self._logger.info(
            "Connecting to 'master' realm of keycloak instance at '%s'",
            self._settings.keycloak_frontend_url,
        )
        return self.createEmpaiaKeycloakAdmin()

    def switch_realm(self, realm_name: str):
        self._keycloak_admin.connection.realm_name = realm_name

    def createEmpaiaKeycloakAdmin(self) -> EmpaiaKeycloakAdmin:
        urlib3_logger = logging.getLogger("urllib3")
        urlib3_logger_level = urlib3_logger.level
        urlib3_logger.setLevel(logging.ERROR)
        start = time.time()
        maximum_seconds_to_wait = 120
        while True:
            try:
                admin_interface = EmpaiaKeycloakAdmin(
                    server_url=self._settings.keycloak_frontend_url,
                    username=self._settings.keycloak_user,
                    password=self._settings.keycloak_password,
                    realm_name="master",
                    verify=True,
                )
            except KeycloakConnectionError as e:
                elapsed = time.time() - start
                if elapsed > maximum_seconds_to_wait:
                    urlib3_logger.setLevel(urlib3_logger_level)
                    raise e
                else:
                    time.sleep(1)
            else:
                break
        urlib3_logger.setLevel(urlib3_logger_level)
        return admin_interface

    def initialize(self, recreate_empaia_realm: bool):
        realm_was_created = self._empaia_realm_creator.ensure_empaia_realm_exists(
            recreate_empaia_realm
        )
        if not realm_was_created:
            self._logger.info(
                "Exiting, because global auth-service entities in keycloak were already initialized."
            )
            return
        self.switch_realm(EMPAIA_REALM_NAME)

        organization_client_scope_id = (
            self._organization_client_scope_creator.ensure_organization_client_scope_exists()
        )
        self._global_clients_manager.create_clients(
            self._settings.auth_service_client_id,
            self._settings.portal_client_id,
            organization_client_scope_id,
        )

        admin_user_id = self.ensure_admin_user_exists()
        self.assign_admin_user_roles(
            admin_user_id, self._global_clients_manager.id_of_auth_service_client
        )

        self._empaia_realm_creator.set_password_policy()

        self._logger.info(
            "NOTE: You may want to change the admin and client secrets in keycloak for production deployments to "
            "ensure that they are really a secret."
        )

    def ensure_admin_user_exists(self):
        admin_user_id = self._keycloak_admin.get_user_id(self._settings.admin_name)
        if admin_user_id is None:
            assert len(self._settings.admin_password) > 0
            admin_user_data = {
                "username": self._settings.admin_name,
                "emailVerified": True,
                "enabled": True,
                "firstName": EMPAIA_REALM_NAME,
                "lastName": "admin",
                "credentials": [
                    {"value": self._settings.admin_password, "type": "password"}
                ],
            }
            if self._settings.admin_email_address:
                admin_user_data["email"] = self._settings.admin_email_address
            admin_user_id = self._keycloak_admin.create_user(admin_user_data)
        return admin_user_id

    def assign_admin_user_roles(
        self, admin_user_id: str, id_of_auth_service_client: str
    ):
        admin_user_client_role_names = (
            "admin",
            "moderator",
            "super_admin",
        )
        admin_user_client_roles = [
            client_role
            for client_role in self._keycloak_admin.get_client_roles(
                id_of_auth_service_client
            )
            if client_role["name"] in admin_user_client_role_names
        ]
        self._keycloak_admin.assign_client_role(
            admin_user_id, id_of_auth_service_client, admin_user_client_roles
        )

    def update_clients(self):
        self.switch_realm(EMPAIA_REALM_NAME)
        organization_client_scope_id = (
            self._organization_client_scope_creator.ensure_organization_client_scope_exists()
        )
        self._global_clients_manager.update_clients(organization_client_scope_id)
        self._logger.info("All global clients have been updated.")

    def update_auth_service_global_roles(self):
        self.switch_realm(EMPAIA_REALM_NAME)
        auth_service_client_id = self._settings.auth_service_client_id
        organization_client_scope_id = (
            self._organization_client_scope_creator.ensure_organization_client_scope_exists()
        )

        updated_client_role_names = (
            self._global_clients_manager.update_auth_service_client_roles(
                auth_service_client_id, organization_client_scope_id
            )
        )
        self._logger.info(
            f"Auth service client roles have been added: {updated_client_role_names}"
        )

    def migrate_global_clients(
        self,
        dummy_organization_name: str,
        clients_file: Path,
        deployment_directory: Path,
    ):
        self.switch_realm(EMPAIA_REALM_NAME)
        global_clients_migrator = GlobalClientsMigrator(self._keycloak_admin)
        global_clients_migrator.migrate(
            dummy_organization_name, clients_file, deployment_directory
        )


def main():
    deployment_directory = Path.cwd()
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--force-empaia-realm-and-all-data-deletion", action="store_true"
    )
    parser.add_argument("--update-global-clients", action="store_true")
    parser.add_argument("--update-global-roles", action="store_true")
    parser.add_argument("--migrate-global-clients", action="store_true")
    parser.add_argument("--dummy-organization-name", action="store")
    parser.add_argument("--clients-file", action="store")
    args = parser.parse_args(sys.argv[1:])
    initializer = AuthServiceInitializer(InitializeSettings())
    if args.update_global_clients:
        initializer.update_clients()
    if args.update_global_roles:
        initializer.update_auth_service_global_roles()
    elif args.migrate_global_clients:
        dummy_organization_name = args.dummy_organization_name
        assert dummy_organization_name is not None
        assert len(dummy_organization_name) > 0
        clients_file = args.clients_file
        assert clients_file is not None
        assert len(clients_file) > 0
        initializer.migrate_global_clients(
            dummy_organization_name, Path(clients_file), deployment_directory
        )
    else:
        initializer.initialize(args.force_empaia_realm_and_all_data_deletion)


if __name__ == "__main__":
    main()
