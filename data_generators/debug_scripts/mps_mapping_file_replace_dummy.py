import argparse
import uuid

from tests.utils import utils_resources


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("app_mapping_file", help="Path to app mapping file")
    args = parser.parse_args()

    apps_file_data = utils_resources.get_json_data(args.app_mapping_file)

    for key in apps_file_data.keys():
        if apps_file_data[key]["app_id"] == "DUMMY":
            apps_file_data[key].update({"app_id": str(uuid.uuid4())})

    new_filename = args.app_mapping_file.replace(".json", "_NEW.json")
    utils_resources.write_to_json_file(new_filename, apps_file_data)


if __name__ == "__main__":
    main()
