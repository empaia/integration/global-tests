import argparse
from pprint import pprint

from tests.utils import singletons
from tests.utils.mps import utils_mps


def print_mps_v1_customer_app(customer_app_id, mps_headers):
    add_headers = {"organization-id": "dummy"}
    r = utils_mps.generic_get(
        f"/v1/customer/apps/{customer_app_id}",
        headers=mps_headers,
        add_headers=add_headers,
        raise_status=False,
    )
    app = r.json()

    pprint("MPS v1 customer app:")
    pprint(app)

    r = utils_mps.generic_get(
        f"/v1/customer/apps/{customer_app_id}/app-view",
        headers=mps_headers,
        add_headers=add_headers,
        raise_status=False,
    )
    app_view = r.json()

    pprint("MPS v1 customer app view:")
    pprint(app_view)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("client_id", help="Client ID with access to MPS")
    parser.add_argument("client_secret", help="Client secret")
    parser.add_argument("app_id", help="Customer app ID")
    args = parser.parse_args()

    client_id = args.client_id
    client_secret = args.client_secret
    app_id = args.app_id

    token, decoded_token = singletons.login_manager.get_client_credentials_flow_tokens(
        client_id, client_secret
    )
    headers = {"user-id": decoded_token["sub"], "Authorization": f"Bearer {token}"}

    print_mps_v1_customer_app(app_id, headers)
