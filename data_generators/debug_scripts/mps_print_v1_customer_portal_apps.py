import argparse
from pprint import pprint

from tests.utils import singletons
from tests.utils.mps import utils_mps


def print_mps_v1_customer_portal_apps(organization_id: str, mps_headers):
    add_headers = {"organization-id": organization_id}
    r = utils_mps.generic_put(
        "/v1/customer/portal-apps/query",
        json={},
        headers=mps_headers,
        add_headers=add_headers,
        raise_status=False,
    )
    apps = r.json()

    pprint("MPS v1 customer portal apps:")
    pprint(apps)

    app_ids = [app["id"] for app in apps["items"]]
    pprint(f"App count: {apps['item_count']}")
    pprint(f"APP IDs: {app_ids}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("client_id", help="Client ID with access to MPS")
    parser.add_argument("client_secret", help="Client secret")
    parser.add_argument(
        "organization_id", help="Organization ID of requesting organization"
    )
    args = parser.parse_args()

    client_id = args.client_id
    client_secret = args.client_secret
    org_id = args.organization_id

    token, decoded_token = singletons.login_manager.get_client_credentials_flow_tokens(
        client_id, client_secret
    )
    headers = {"user-id": decoded_token["sub"], "Authorization": f"Bearer {token}"}

    print_mps_v1_customer_portal_apps(org_id, headers)
