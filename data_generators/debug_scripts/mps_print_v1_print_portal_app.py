import argparse
from pprint import pprint

from tests.utils import singletons
from tests.utils.mps import utils_mps

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("client_id", help="Client ID with access to MPS")
    parser.add_argument("client_secret", help="Client secret")
    parser.add_argument("portal_app_id", help="Client secret")
    args = parser.parse_args()

    client_id = args.client_id
    client_secret = args.client_secret

    token, d_token = singletons.login_manager.get_client_credentials_flow_tokens(
        client_id, client_secret
    )
    headers = {"Authorization": f"Bearer {token}", "user-id": d_token["sub"]}
    r = utils_mps.generic_get(
        f"/v1/admin/portal-apps/{args.portal_app_id}", headers=headers
    )
    papp = r.json()
    pprint(papp)
